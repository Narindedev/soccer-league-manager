package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.PlayerApi;
import com.apnitor.football.league.api.RefereeApi;
import com.apnitor.football.league.api.request.GetMatchEventsReq;
import com.apnitor.football.league.api.request.GetRefereeScheduleReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateMatchEventRes;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class RefereeRepository {

    private RefereeApi playerApi;

    public RefereeRepository(Application application) {
        playerApi = ApiService.getRefereeApi(application);
    }

    public Single<BaseRes<ArrayList<GetRefereeRes>>> getAllReferee() {
        return playerApi.getAllReferee()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getRefereeSchedule(GetRefereeScheduleReq getTeamDetailReq) {
        return playerApi.getRefereeSchedule(getTeamDetailReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateMatchEventRes>> updateMatchEvents(UpdateMatchEventReq matchEventReq) {
        return playerApi.updateMatchEvents(matchEventReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetMatchEventRes>>> getMatchEvents(GetMatchEventsReq matchEventReq) {
        return playerApi.getMatchEvents(matchEventReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getFollowingSchedules() {
        return playerApi.getFollowingSchedules()
                .subscribeOn(Schedulers.io());
    }
}
