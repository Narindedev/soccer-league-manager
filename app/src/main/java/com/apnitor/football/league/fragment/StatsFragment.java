package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.LeagueStatsListAdapter;
import com.apnitor.football.league.adapter.PointsTableListAdapter;
import com.apnitor.football.league.api.response.GetLeagueRes;

import java.util.Observable;
import java.util.Observer;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class StatsFragment extends BaseFragment implements Observer {
    View view;
    static GetLeagueRes getLeagueRes;
    public static StatsFragment newInstance(GetLeagueRes leagueRes) {
        getLeagueRes=new GetLeagueRes();
        getLeagueRes=leagueRes;
        StatsFragment fragment = new StatsFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_stats, container, false);

        RecyclerView v = view.findViewById(R.id.rvStats);
        v.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        v.setAdapter(new LeagueStatsListAdapter(getContext(), null));

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void update(Observable o, Object arg) {
        getLeagueRes=((LeagueProfileActivity)getActivity()).mGetLeagueRes;

    }
}
