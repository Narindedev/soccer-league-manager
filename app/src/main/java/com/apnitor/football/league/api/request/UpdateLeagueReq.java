package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateLeagueReq implements Serializable {

    @Expose
    @SerializedName("leagueId")
    private String leagueId;

    @Expose
    @SerializedName("name")
    private String leagueTitle;

    @Expose
    @SerializedName("foundationDate")
    private String foundationDate;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("currentChampions")
    private String currentChampions;

    @Expose
    @SerializedName("mostChampions")
    private String mostChampions;

    @Expose
    @SerializedName("contactInfo")
    private String contactInfo;

    @Expose
    @SerializedName("websiteUrl")
    private String website;

    @Expose
    @SerializedName("facebookUrl")
    private String facebookLink;

    @Expose
    @SerializedName("twitterUrl")
    private String twitterLink;

    @Expose
    @SerializedName("instagramUrl")
    private String instaLink;

    @Expose
    @SerializedName("owner")
    private String owner;


    @Expose
    @SerializedName("managerName")
    private String managerName;
    @Expose
    @SerializedName("managerEmail")
    private String managerEmail;

    @Expose
    @SerializedName("managerPhone")
    private String managerPhone;

    @Expose
    @SerializedName("socialMedia")
    public SocialMedia socialMedia;


    public UpdateLeagueReq(){

    }

    public UpdateLeagueReq(String leagueId, String leagueTitle, String foundationDate, String country, String currentChampions,
                           String mostChampions, String contactInfo, String description, String name, String email, String phone, SocialMedia socialMedia,
                           String owner) {
        this.leagueId = leagueId;
        this.leagueTitle = leagueTitle;
        this.foundationDate = foundationDate;
        this.country = country;
        this.currentChampions = currentChampions;
        this.mostChampions = mostChampions;
        this.contactInfo = contactInfo;
        this.website = website;
        this.facebookLink = facebookLink;
        this.twitterLink = twitterLink;
        this.owner = owner;
        this.description=description;
        this.managerName=name;
        this.managerEmail=email;
        this.managerPhone=phone;
        this.socialMedia=socialMedia;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrentChampions() {
        return currentChampions;
    }

    public void setCurrentChampions(String currentChampions) {
        this.currentChampions = currentChampions;
    }

    public String getMostChampions() {
        return mostChampions;
    }

    public void setMostChampions(String mostChampions) {
        this.mostChampions = mostChampions;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getInstaLink() {
        return instaLink;
    }

    public void setInstaLink(String instaLink) {
        this.instaLink = instaLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }
}