package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.ViewPlayerAdapter;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ViewPlayerActivity extends BaseActivity implements ListItemClickCallback {

    private PlayerViewModel playerViewModel;
    private ViewPlayerActivity playerListActivity = this;
    private ArrayList<PlayerRes> mPlayerRes;
    final String MATCH_DETAIL = "match_detail";
    GetTeamScheduleRes mGetMatchDetail;
    androidx.appcompat.widget.Toolbar mToolbar;
    RecyclerView mRvTeams;
    ArrayList<Boolean> mTeam1SelectedPlayers = new ArrayList<>();
    Button mBtnSave;
    String mTeamSelected = "1";
    ViewPlayerAdapter mViewPlayerAdapter;
    ArrayList<GetTeamPlayersRes> mTeamsList = new ArrayList<>();
    String LOG_TAG = "ViewPlayerActivity";
    String mTeamId = "";
    AddScheduleViewModel addScheduleViewModel;
    String TEAM_TYPE = "";

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_player);
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mTeamId = intent.getStringExtra("TEAM_ID");
                TEAM_TYPE = intent.getStringExtra("TEAM_TYPE");
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        //
        setUpLayout();
        mPlayerRes = new ArrayList<>();
        playerViewModel = getViewModel(PlayerViewModel.class);
//        followingViewModel = getViewModel(FollowingViewModel.class);
        setupToolbar(mToolbar, mGetMatchDetail.getTeam1Name());
        setupToolbar(mToolbar, "View Players");
        setupRecyclerView();
        observeApiResponse();
    }

    private void setUpLayout() {
        mToolbar = findViewById(R.id.toolbar);
        mRvTeams = findViewById(R.id.rvAllTeams);
        mBtnSave = findViewById(R.id.saveBtn);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlayersList();
            }
        });
        mViewPlayerAdapter = new ViewPlayerAdapter(playerListActivity, mTeamsList, this, mTeam1SelectedPlayers, mTeamSelected);
    }

    private void observeApiResponse() {
        playerViewModel.getTeamPlayersLiveData().observe(this,
                teams -> {
                    mTeamsList.clear();
                    for (int i = 0; i < teams.size(); i++) {
                        mTeam1SelectedPlayers.add(false);
                    }
                    mTeamsList.addAll(teams);
                    mRvTeams.setAdapter(
                            new ViewPlayerAdapter(playerListActivity, teams, this, mTeam1SelectedPlayers, mTeamSelected)
                    );
                });
        playerViewModel.getTeamPlayers(new GetTeamPlayersReq(mTeamId));
    }

    private void setupRecyclerView() {
        mRvTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
        mRvTeams.setAdapter(mViewPlayerAdapter);
    }

    public void onBackClick(View view) {
        onBackPressed();
        //  finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mPlayerRes.size() > 0) {
            Intent mIntent = new Intent();
            mIntent.putParcelableArrayListExtra("playrList", mPlayerRes);
            setResult(101, mIntent);
        }
    }

    @Override
    public void onListItemClick(Object object) {
        PlayerRes playerRes = (PlayerRes) object;
    }


    void setPlayersList() {
        if (mTeamsList.size() <= 0)
            return;
        String team1Formation = "4-4-2";
        // Team 1 Players
        ArrayList<TeamPlaying11> team1 = new ArrayList<>();
        for (int i = 0; i < mTeam1SelectedPlayers.size(); i++) {
            if (mTeam1SelectedPlayers.get(i).equals(true)) {
                team1.add(new TeamPlaying11(mTeamsList.get(i).getPlayerId(), "10", "accept", "1", "1", true));
            } else {
                team1.add(new TeamPlaying11(mTeamsList.get(i).getPlayerId(), "10", "accept", "1", "1", false));
            }
        }


        AddPlayerToTeamReq addPlayerToTeamReq = new AddPlayerToTeamReq();
        addPlayerToTeamReq.setLeagueId(mGetMatchDetail.getLeagueId());
        addPlayerToTeamReq.setDivisionId(mGetMatchDetail.getDivisionId());
        addPlayerToTeamReq.setSeasonId(mGetMatchDetail.getSeasonId());
        addPlayerToTeamReq.setScheduleId(mGetMatchDetail.getScheduleId());
        if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
            addPlayerToTeamReq.setTeam1Formation(team1Formation);
            addPlayerToTeamReq.setTeam1Playing11(team1);
        } else {
            addPlayerToTeamReq.setTeam2Formation(team1Formation);
            addPlayerToTeamReq.setTeam2Playing11(team1);
        }

        addScheduleViewModel.updateScheduleTeam(addPlayerToTeamReq);
        addScheduleViewModel.updateScheduleLiveData().observe(this, addScheduleRes -> {
            finish();
        });
    }
}
