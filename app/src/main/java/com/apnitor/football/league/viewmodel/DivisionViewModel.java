package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.DeleteDivisionReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.UpdateDivisionReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.UpdateDivisionRes;
import com.apnitor.football.league.repository.DivisionRepository;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class DivisionViewModel extends BaseViewModel {

    private DivisionRepository divisionRepository;
    private MutableLiveData<UpdateDivisionRes> updateDivisionResLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetDivisionRes>> divisonsListResLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> deleteDivisionResLiveData = new MutableLiveData<>();

    public DivisionViewModel(@NonNull Application application) {
        super(application);
        divisionRepository = new DivisionRepository(application);
    }


    public void createUpdateDivision(UpdateDivisionReq updateDivisionReq) {
        consumeApi(
                divisionRepository.createUpdateDivision(updateDivisionReq),
                data -> updateDivisionResLiveData.setValue(data)
        );
    }


    public void getDivisions(GetLeagueDivisionsReq getLeagueDivisionsReq) {
        consumeApi(
                divisionRepository.getDivisions(getLeagueDivisionsReq),
                data -> divisonsListResLiveData.setValue(data)
        );
    }

    public void deleteDivision(DeleteDivisionReq deleteDivisionReq) {
        consumeApi(
                divisionRepository.deleteDivision(deleteDivisionReq),
                data -> deleteDivisionResLiveData.setValue(data)
        );
    }

    public LiveData<UpdateDivisionRes> getUpdateDivisionResLiveData() {
        return updateDivisionResLiveData;
    }

    public LiveData<List<GetDivisionRes>> getDivisonsListResLiveData() {
        return divisonsListResLiveData;
    }

    public LiveData<Object> getDeleteDivisionResLiveData() {
        return deleteDivisionResLiveData;
    }
}

