package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SeasonListAdapter extends RecyclerView.Adapter<SeasonListAdapter.ViewHolder> {

    private Context context;
    private List<GetSeasonRes> leagueList;
    ListItemClickCallback listItemClickCallback;

    public SeasonListAdapter(Context context, List<GetSeasonRes> leagueList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.leagueList = leagueList;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_league_item, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetSeasonRes model = leagueList.get(position);
            holder.txtLeagueName.setText(model.getSeasonYear());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        if (leagueList != null)
            return leagueList.size();
        else
            return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txtLeagueName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            txtLeagueName = (TextView) itemView.findViewById(R.id.tvLeagueName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(leagueList.get(getLayoutPosition()));
                }
            });
        }
    }
}
