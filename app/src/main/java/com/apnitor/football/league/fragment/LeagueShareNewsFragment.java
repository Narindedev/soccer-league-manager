package com.apnitor.football.league.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;

import com.apnitor.football.league.activity.socailmedia.SocialVIewPagerAdapter;
import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.FragmentShareNewsBinding;
import com.apnitor.football.league.util.CustomViewPager;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class LeagueShareNewsFragment extends Fragment {
    static GetLeagueRes mGetLeagueRes;
    CustomViewPager viewPager;
    SocialVIewPagerAdapter adapter;
    SocialMedia socialMedia;
    FragmentShareNewsBinding binding;
    public static LeagueShareNewsFragment newInstance(int i, String s,GetLeagueRes mLeagueRes) {
        LeagueShareNewsFragment fragment = new LeagueShareNewsFragment();
        mGetLeagueRes=mLeagueRes;
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding=FragmentShareNewsBinding.inflate(inflater,container,false);

        viewPager = binding.mediaViewPager;
        //socialMedia=mGetLeagueRes.socialMedia;
      /*  adapter = new SocialVIewPagerAdapter(getChildFragmentManager(), socialMedia);
        viewPager.setAdapter(adapter);*/

        //viewPager.setAdapter(new LeaguePagerAdapater(getChildFragmentManager(), mGetLeagueRes));
        //
        binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.colorAccent));
        binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
        binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));

        //
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setPage(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

     //   viewPager.setCurrentItem(0);


       // onClickListener();

        return binding.getRoot();
    }


    private void onClickListener(){
        binding.ivfacebookTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });

        binding.ivTwitterTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });

        binding.ivInstagramTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2);
            }
        });

    }

    @SuppressLint("RestrictedApi")
    void setPage(int position) {

        switch (position) {
            case 0:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));
                break;
            case 1:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));
                break;
            case 2:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                break;
         default:
             break;
        }
    }

}
