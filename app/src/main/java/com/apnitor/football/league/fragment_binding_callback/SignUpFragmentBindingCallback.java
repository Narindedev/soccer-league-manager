package com.apnitor.football.league.fragment_binding_callback;

public interface SignUpFragmentBindingCallback {

    void onSignUp();

    void onProfilePhotoClick();

    void onBack();
    void onVisiblePassword();
}
