package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.PlayerApi;
import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetPlayerDetailReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class PlayerRepository {

    private PlayerApi playerApi;

    public PlayerRepository(Application application) {
        playerApi = ApiService.getPlayerApi(application);
    }

    public Single<BaseRes<ArrayList<PlayerRes>>> getAllPlayers() {
        return playerApi.getAllPlayers()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<PlayerRes>>> getAllPlayers(GetAllTeamReq getAllTeamReq) {
        return playerApi.getAllPlayers(getAllTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamPlayersRes>>> getTeamPlayers(GetTeamPlayersReq getTeamPlayersReq) {
        return playerApi.getTeamPlayers(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<PlayerRes>> getInvitePlayer(GetTeamPlayersReq getTeamPlayersReq) {
        return playerApi.invitePlayer(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<PlayerRes>> getJoinTeam(GetTeamPlayersReq getTeamPlayersReq) {
        return playerApi.joinTeam(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<PlayerRes>> getRejectTeamInvite(GetTeamPlayersReq getTeamPlayersReq) {
        return playerApi.rejectTeamInvite(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<PlayerProfileRes>> getPlayerDetail(GetPlayerDetailReq getPlayerDetailReq) {
        return playerApi.getPlayerDetail(getPlayerDetailReq)
                .subscribeOn(Schedulers.io());
    }
}
