package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ShowTeamLeaguesAdapter extends RecyclerView.Adapter<ShowTeamLeaguesAdapter.ViewHolder> {

    ListItemClickCallback listItemClickCallback;
    private Context context;
    private List<GetLeagueRes> leagueList;
    private int pos = -1;


    public ShowTeamLeaguesAdapter(Context context, List<GetLeagueRes> leagueList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.leagueList = leagueList;

        this.listItemClickCallback = listItemClickCallback;

    }

    @Override
    public ShowTeamLeaguesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_show_team_leagues_adapter, parent, false);
        return new ShowTeamLeaguesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowTeamLeaguesAdapter.ViewHolder holder, int position) {
        try {
            GetLeagueRes model = leagueList.get(position);
            holder.tvName.setText(model.getLeagueTitle());
            if (model.getImageUrl() != null)
                Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_league)).into(holder.ivLeagueImg);
            else {
                holder.ivLeagueImg.setImageResource(R.drawable.ic_league);
            }

            //
            if (pos == position) {
                holder.bottomBorder.setVisibility(View.VISIBLE);
            } else {
                holder.bottomBorder.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return leagueList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivLeagueImg;
        TextView tvName;
        LinearLayout linearLayout;
View bottomBorder;
        ViewHolder(View itemView) {
            super(itemView);
            bottomBorder= (View) itemView.findViewById(R.id.viewBottomLine);
            tvName = (TextView) itemView.findViewById(R.id.tvLeagueName);
            ivLeagueImg = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.llRow);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        pos = getAdapterPosition();


                    listItemClickCallback.onListItemClick(leagueList.get(getAdapterPosition()));
                    notifyDataSetChanged();
                    // mCb.performClick();
                }
            });
        }
    }
}