package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.PointsTableFragment;
import com.apnitor.football.league.fragment.SeasonsResultFragment;
import com.apnitor.football.league.fragment.StatsFragment;
import com.apnitor.football.league.fragment.TeamInfoFullFragment;
import com.apnitor.football.league.fragment.TeamInfoNewsFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TeamInfoPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 2;
    private GetTeamRes getTeamRes;
  /*  public TeamInfoPagerAdapter(FragmentManager fm,GetTeamRes getTeamRes) {
        super(fm);
        this.getTeamRes = getTeamRes;
    }*/

    public TeamInfoPagerAdapter(FragmentManager childFragmentManager) {
        super(childFragmentManager);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return TeamInfoNewsFragment.newInstance(0,"");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return TeamInfoNewsFragment.newInstance(0,"");
           /* case 2: // Fragment # 1 - This will show SecondFragment
                return PointsTableFragment.newInstance(0, "Page # 1");
            case 3: // Fragment # 1 - This will show SecondFragment
                return StatsFragment.newInstance(0, "Page # 1");
*/
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
