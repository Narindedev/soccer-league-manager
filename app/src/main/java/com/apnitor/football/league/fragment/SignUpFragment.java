package com.apnitor.football.league.fragment;


import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LoginActivity;
import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.databinding.FragmentSignUpBinding;
import com.apnitor.football.league.fragment_binding_callback.SignUpFragmentBindingCallback;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.SignUpViewModel;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class SignUpFragment extends BaseFragment implements SignUpFragmentBindingCallback {

    private FragmentSignUpBinding binding;
    private SignUpViewModel signUpViewModel;
    private LoginActivity loginActivity;
    private String email = "", otp = "";
    private String phone = "";
    private boolean isPasswordVisible = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginActivity = (LoginActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        signUpViewModel = getViewModel(SignUpViewModel.class);
        signUpViewModel.getSignUpResLiveData().observe(
                this,
                signUpRes -> {
                    getPrefHelper().setUserID(signUpRes.getUserId());
                    Toast.makeText(loginActivity, "Sign up successfully.", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new VerifyUserFragment();
                    Bundle mBundle = new Bundle();
                    mBundle.putString("email", email);
                    mBundle.putString("phone", phone);
                    startNewFragment(fragment, mBundle);

                   /* loginActivity.login(new LogInReq(
                            UIUtility.getEditTextValue(binding.usernameEt),
                            UIUtility.getEditTextValue(binding.passwordEt)
                    ));*/
                }
        );
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSignUpBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        passwordValidations();
        return binding.getRoot();
    }

    @Override
    public void onSignUp() {
        email = "";
        phone = "";

        String firstName = UIUtility.getEditTextValue(binding.firstNameEt);
        String lastName = UIUtility.getEditTextValue(binding.lastNameEt);
        String strEmlPhn = UIUtility.getEditTextValue(binding.emailEt);
        String password = UIUtility.getEditTextValue(binding.passwordEt);

        if (strEmlPhn.contains("@")) {
            email = strEmlPhn;
        } else {
            phone = strEmlPhn;
        }
         if (firstName.isEmpty()) {
            showErrorOnEditText(binding.firstNameEt, "Please enter first name.");
            return;
        } else if (lastName.isEmpty()) {
            showErrorOnEditText(binding.lastNameEt, "Please enter last name.");
            return;
        } else if (!checkUser(binding.emailEt, strEmlPhn, email, phone)) {
        } else if (password.isEmpty()) {
            showErrorOnEditText(binding.passwordEt, "Please enter password.");
            return;
        } else

        {
            try {
                Log.e("password", "" + UIUtility.generateStorngPasswordHash(password));
                Log.e("password22", "" + UIUtility.hashString(password));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            }
        }
        signUpViewModel.signUp("", email, password, firstName, lastName, phone);
//        Toast.makeText(loginActivity, "API CALL", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProfilePhotoClick() {
        showToast("Functionality is not implemented yet!");
    }

    @Override
    public void onBack() {
        navController.navigateUp();
    }

    @Override
    public void onVisiblePassword() {
        if (isPasswordVisible) {
            isPasswordVisible = false;
            binding.ivPasswordVisibility.setImageResource(R.drawable.ic_visibility_off);
            binding.passwordEt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            binding.passwordEt.setSelection(binding.passwordEt.getText().length());
        } else {
            isPasswordVisible = true;
            binding.ivPasswordVisibility.setImageResource(R.drawable.ic_visibility);
            binding.passwordEt.setInputType(InputType.TYPE_CLASS_TEXT);
            binding.passwordEt.setSelection(binding.passwordEt.getText().length());
        }
    }

    private void startNewFragment(Fragment fragment, Bundle mBundle) {
        fragment.setArguments(mBundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.my_nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }

    private void passwordValidations() {
        binding.passwordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.cardPasswordValid.setVisibility(View.VISIBLE);
                } else {
                    binding.cardPasswordValid.setVisibility(View.GONE);
                }

            }
        });


        binding.passwordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkCharacters(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkCharacters(String s) {
        if (UIUtility.checkSpcChar(s)) {
            binding.tvPasswordSpcChr.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordSpcChr.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }

        if (UIUtility.checkSmall(s)) {
            binding.tvPasswordSmall.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordSmall.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }

        if (UIUtility.checkCaps(s)) {
            binding.tvPasswordCapital.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordCapital.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }
        if (s.length() < 8) {
            binding.tvPasswordLength.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        } else {
            binding.tvPasswordLength.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        }


    }

}
