package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.UpdateDivisionReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.UpdateDivisionRes;
import com.apnitor.football.league.databinding.ActivityCreateDivisionBinding;
import com.apnitor.football.league.fragment.DivisionListFragment;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.DivisionViewModel;

import static com.apnitor.football.league.fragment.LeagueSchedulesFragment.mGetLeagueRes;

public class CreateDivisionActivity extends BaseActivity {

    public static final String DIVISION_DETAIL = "division_detail";
    private ActivityCreateDivisionBinding binding;
    private DivisionViewModel divisionViewModel;
    private boolean isCreating = true;
    private String leagueId;
    private UpdateDivisionReq updateDivisionReq;
    String selectedDivisionToUpdate;
    EditText mEtName, mEtDesc;
    GetDivisionRes getDivisionRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_create_division);
        divisionViewModel = getViewModel(DivisionViewModel.class);

        mEtName = (EditText) findViewById(R.id.et_div_name);
        mEtDesc = (EditText) findViewById(R.id.et_desc);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {

            leagueId = intent.getExtras().getString("leagueId");

            if (intent.getExtras() != null && intent.getExtras().getSerializable(DIVISION_DETAIL) != null) {
                getDivisionRes = (GetDivisionRes) intent
                        .getExtras()
                        .getSerializable(DIVISION_DETAIL);
                selectedDivisionToUpdate="true";
                updateDivisionReq = new UpdateDivisionReq(getDivisionRes.getDivisionId(), leagueId, getDivisionRes.getDivisionName(), null, getDivisionRes.getDescription(),getDivisionRes.entryFee,getDivisionRes.insuranceFee,getDivisionRes.fieldRentalfee,getDivisionRes.winningPrize);
                binding.setDivisionData(this.getDivisionRes);
                isCreating = false;
               // binding.deleteBtn.setVisibility(View.VISIBLE);
            } else
                binding.deleteBtn.setVisibility(View.GONE);
        }
        if (getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
            binding.saveBtn.setVisibility(View.VISIBLE);
        }else{
            binding.saveBtn.setVisibility(View.GONE);
        }

        setupToolbar(binding.toolbar, "Add Division");
        observeApiResponse();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
    }

    private void observeApiResponse() {
        divisionViewModel.getUpdateDivisionResLiveData().observe(this,
                res -> {
//                    divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
                    LeagueProfileActivity.isSeasonDivisionUpdated=true;

                   // UpdateDivisionRes divisionRes = res;
                    String divisionId = res.getDivisionId();
                    updateDivisionReq.setDivisionId(divisionId);

                    /**
                     * Return Created Division to League Profile Activity
                     */

                    GetDivisionRes divisionRes = new GetDivisionRes(divisionId, updateDivisionReq.getDivisionName());
                    divisionRes.setDescription(updateDivisionReq.getDescription());
                    divisionRes.setEntryFee(updateDivisionReq.getEntryFee());
                    divisionRes.setFieldRentalfee(updateDivisionReq.getFieldRentalfee());
                    divisionRes.setInsuranceFee(updateDivisionReq.getInsuranceFee());
                    divisionRes.setWinningPrize(updateDivisionReq.winningPrize);
                    /**
                     * TODO
                     *
                     * Update rest of the fields here
                     */


                    Intent divisionIntent = new Intent();
                    divisionIntent.putExtra("Division",divisionRes);
                    setResult(ResultCodes.DIVISION_RESULT_CODE,divisionIntent);

                    finish();
                });


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onSaveClick(View view) {
        String rentalFee=UIUtility.getEditTextValue(binding.rentalFeeEt);
        String insuranceFee=UIUtility.getEditTextValue(binding.insuranceFeeEt);
        String entryFee=UIUtility.getEditTextValue(binding.entryFeeEt);
        String prizeFee=UIUtility.getEditTextValue(binding.winningPrizeEt);
        String divisionName = UIUtility.getEditTextValue(mEtName);
        String divisionDesc = UIUtility.getEditTextValue(mEtDesc);
        if (divisionName.equals(""))
            Toast.makeText(this, "'Division name' should not be empty.", Toast.LENGTH_LONG).show();
        else {
            if (selectedDivisionToUpdate != null)
                updateDivisionReq = new UpdateDivisionReq(getDivisionRes.getDivisionId(), leagueId, divisionName, selectedDivisionToUpdate, divisionDesc,entryFee,insuranceFee,entryFee,prizeFee);
            else
                updateDivisionReq = new UpdateDivisionReq(null, leagueId, divisionName, null, divisionDesc,entryFee,insuranceFee,rentalFee,prizeFee);
            divisionViewModel.createUpdateDivision(updateDivisionReq);
        }
    }

    public void onDeleteClick(View view) {
//        seasonViewModel.deleteSeason(new DeleteSeasonReq(updateSeasonReq.getLeagueId()));
//        seasonViewModel.getUpdateSeasonResLiveData().observe(this, res -> {
//            finishStartActivity(HomeActivity.class);
//        });
        Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }

}
