package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.SeasonResultsListAdapter;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetEventRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.LeagueScheduleResultRes;
import com.apnitor.football.league.api.response.ScheduleResultRes;
import com.apnitor.football.league.api.response.SelectedSeasonDivisionRes;
import com.apnitor.football.league.util.EndlessRecyclerViewScrollListener;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class SeasonsResultFragment extends BaseFragment implements Observer {


    private static GetLeagueRes mGetLeagueRes;
    String LOG_TAG = "SeasonsResultFragment";
    View view;
    private boolean isMoreAvailable;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private TextView mTvSeason;
    private TextView mTvDivision;
    private TextView mTvWeek;
    private SeasonViewModel seasonViewModel;
    private DivisionViewModel divisionViewModel;
    private LeagueViewModel leagueViewModel;
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private List<ScheduleResultRes> leagueScheduleList = new ArrayList<>();
    private List<String> mSeasonNameList = new ArrayList<>();
    private List<String> mDivisionNameList = new ArrayList<>();//
    private List<String> mWeekNameList = new ArrayList<>();
    private SeasonResultsListAdapter mSeasonResultsListAdapter;
    private String mSeasonId, mDivisionId, seasonStatus;
    private RecyclerView mRvMatches;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    private List<GetDivisionRes> mDivisionsList = new ArrayList<>();

    public static SeasonsResultFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes = new GetLeagueRes();
        mGetLeagueRes = getLeagueRes;
        SeasonsResultFragment fragment = new SeasonsResultFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeekNameList.add("Week 1");
        mWeekNameList.add("Week 2");
        mWeekNameList.add("Week 3");
        mWeekNameList.add("Week 4");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_season_results, container, false);
        //
        seasonViewModel = getViewModel(SeasonViewModel.class);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        //


        //
        mTvDivision = view.findViewById(R.id.tvSelectDivisionBtn);
        mTvSeason = view.findViewById(R.id.tvSelectSeasonBtn);
        mTvWeek = view.findViewById(R.id.tvSelectWeekBtn);


        //
        mTvSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSeasonDialog();
            }
        });
        //
        mTvWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeekDialog();
            }
        });
        //
        mTvDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDivisionDialog();
            }
        });
        //
        mRvMatches = view.findViewById(R.id.rvResults);
        mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRvMatches.setLayoutManager(mLayoutManager);
        mRvMatches.setNestedScrollingEnabled(false);

        /*Observe Data from get league detail*/
        observeLeagueData(mGetLeagueRes);
        //observeApiResponse();


//        mRvMatches.setHasFixedSize(false);
//            mSeasonResultsListAdapter = new SeasonResultsListAdapter(getContext(), null, leagueScheduleList);
//            mRvMatches.setAdapter(mSeasonResultsListAdapter);


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showSeasonDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Season");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < mSeasonNameList.size(); i++) {
            arrayAdapter.add(mSeasonNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mSeasonId = seasonsList.get(which).getSeasonId();
                mTvSeason.setText(strName);

                List<SelectedSeasonDivisionRes> seasonDivisionRes=((LeagueProfileActivity)getActivity()).selectedSeasonDivision(mDivisionId,mSeasonId,which);
                if(seasonDivisionRes!=null && seasonDivisionRes.size()>0){
                    mTvDivision.setText(seasonDivisionRes.get(0).getDivisionName());
                    mDivisionId=seasonDivisionRes.get(0).getDivisionId();
                }
                if(mSeasonId!=null && mDivisionId!=null){
                    leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                }
            }
        });
        builderSingle.show();
    }

    private void showDivisionDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Division");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        for (int i = 0; i < mDivisionNameList.size(); i++) {
            arrayAdapter.add(mDivisionNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvDivision.setText(strName);
                mDivisionId = mDivisionsList.get(which).getDivisionId();

                List<GetSeasonRes> seasonResList=((LeagueProfileActivity)getActivity()).getDivisionSeason(mDivisionId);
                if(seasonResList!=null && seasonResList.size()>0){
                    mSeasonId = seasonResList.get(0).getSeasonId();
                    mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                }
                if(mSeasonId!=null && mDivisionId!=null){
                    leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                }
            }
        });
        builderSingle.show();
    }


    private void showWeekDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Week");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        for (int i = 0; i < mWeekNameList.size(); i++) {
            arrayAdapter.add(mWeekNameList.get(i));
        }

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvWeek.setText(strName);
                mSeasonId = seasonsList.get(which).toString();
            }
        });
        builderSingle.show();
    }

    private void observeLeagueData(GetLeagueRes leagueRes) {
        /*Show  Season Data*/
        try {
            seasonsList = leagueRes.seasonList;
            Log.d(LOG_TAG, "Seasons are " + seasonsList.toString());
            mSeasonNameList.clear();
            for (int i = 0; i < leagueRes.seasonList.size(); i++) {
                mSeasonNameList.add(leagueRes.seasonList.get(i).getSeasonYear());
            }

            mDivisionsList = leagueRes.divisionList;
            Log.e(LOG_TAG, "Seasons are " + mDivisionNameList.toString());
            mDivisionNameList.clear();
            for (int i = 0; i < leagueRes.divisionList.size(); i++) {
                mDivisionNameList.add(leagueRes.divisionList.get(i).getDivisionName());
            }

            List<SelectedSeasonDivisionRes> seasonDivisionResList=((LeagueProfileActivity)getActivity()).getActiveSeasonDivision();
            if(seasonDivisionResList!=null && seasonDivisionResList.size()>0){
                SelectedSeasonDivisionRes res=seasonDivisionResList.get(0);
                mTvSeason.setText(res.getSesonName());
                seasonStatus = res.getSeasonStatus();
                mSeasonId =res.getSesonId();
                mTvDivision.setText(res.getDivisionName());
                mDivisionId=res.getDivisionId();
            }else{
                mTvSeason.setVisibility(View.GONE);
                mTvDivision.setVisibility(View.GONE);
                if (mDivisionNameList.size() > 0) {
                    mTvDivision.setVisibility(View.VISIBLE);
                    mTvDivision.setText(mDivisionNameList.get(0));
                    mDivisionId = mDivisionsList.get(0).getDivisionId();

                } else {
                    mTvDivision.setVisibility(View.GONE);
                }
                if(mDivisionId!=null) {
                    List<GetSeasonRes> seasonResList = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
                    if (seasonResList!=null && seasonResList.size() > 0) {
                        mTvSeason.setVisibility(View.VISIBLE);
                        mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                        seasonStatus = seasonResList.get(0).getSeasonStatus();
                        mSeasonId = seasonResList.get(0).getSeasonId();
                        //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                    } else {
                        mTvSeason.setVisibility(View.GONE);
                    }
                }/*else{
                    mTvSeason.setVisibility(View.GONE);
                    mTvDivision.setVisibility(View.GONE);
                }*/
                }



            //
           /* if (mSeasonNameList.size() > 0) {
                mTvSeason.setVisibility(View.VISIBLE);
                mTvSeason.setText(mSeasonNameList.get(0));
                seasonStatus = seasonsList.get(0).getSeasonStatus();
                mTvWeek.setText(mWeekNameList.get(0));
                mSeasonId = seasonsList.get(0).getSeasonId();
                //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
            } else {
                mTvSeason.setVisibility(View.GONE);
            }*/


            /*Show  Division Data*/

            //
           /* if (mDivisionNameList.size() > 0) {
                mTvDivision.setVisibility(View.VISIBLE);
                mTvDivision.setText(mDivisionNameList.get(0));
                mDivisionId = mDivisionsList.get(0).getDivisionId();

            } else {
                mTvDivision.setVisibility(View.GONE);
            }*/
            //
            if (mSeasonId != null && mDivisionId != null) {
                leagueScheduleList.clear();
                leagueScheduleList.addAll(getSchedulesResult(mDivisionId,mSeasonId));
                if (leagueScheduleList != null && leagueScheduleList.size() > 0) {
                    leagueScheduleList = getTeamsScore(leagueRes.resultList);
                    mSeasonResultsListAdapter = new SeasonResultsListAdapter(getContext(), null, leagueScheduleList, mGetLeagueRes);
                    mRvMatches.setAdapter(mSeasonResultsListAdapter);
                } else {
                    if (seasonStatus.equals("Not Started")) {
                        if (LeagueProfileActivity.isPastResultLoadFIrtsTime) {
                            LeagueProfileActivity.isPastResultLoadFIrtsTime=false;
                            leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                        }
                    }
                }
            }


            leagueViewModel.getLeaguScheduleResultLiveData().observe(this, addScheduleLeagueReq -> {
                isMoreAvailable = addScheduleLeagueReq.isMoreAvailable();
                //  leagueScheduleList.addAll(addScheduleLeagueReq.getScheduleList());
                leagueScheduleList.clear();
                leagueScheduleList.addAll(getTeamsScore(addScheduleLeagueReq.getScheduleList()));
//            mSeasonResultsListAdapter.notifyDataSetChanged();
                mSeasonResultsListAdapter = new SeasonResultsListAdapter(getContext(), null, leagueScheduleList, mGetLeagueRes);
                mRvMatches.setAdapter(mSeasonResultsListAdapter);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setResultAdapter(GetLeagueRes leagueRes) {
        if (mSeasonId != null && mDivisionId != null) {
            leagueScheduleList.clear();
            leagueScheduleList.addAll(leagueRes.resultList);
            if (leagueScheduleList != null && leagueScheduleList.size() > 0) {


                //
                leagueScheduleList = getTeamsScore(leagueRes.resultList);
                mSeasonResultsListAdapter = new SeasonResultsListAdapter(getContext(), null, leagueScheduleList, mGetLeagueRes);
                mRvMatches.setAdapter(mSeasonResultsListAdapter);
            } else {

                if (seasonStatus.equals("Not Staarted")) {
                    leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));

                }
            }
        }

    }

    private List<ScheduleResultRes> getTeamsScore(List<ScheduleResultRes> leagueScheduleList) {
        if (leagueScheduleList.size() > 0) {
            for (int i = 0; i < leagueScheduleList.size(); i++) {
                int team1Score = 0;
                int team2Score = 0;
                List<GetEventRes> mEventRes = leagueScheduleList.get(i).eventList;
                if (mEventRes != null && mEventRes.size() > 0) {

                    for (int m = 0; m < mEventRes.size(); m++) {
                        GetEventRes eventRes = mEventRes.get(m);
                        if (eventRes.getEventType().equals("Goal")) {
                            if (eventRes.getScoredByTeam().equals(leagueScheduleList.get(i).getTeam1Id())) {
                                team1Score = team1Score + 1;
                            } else {
                                team2Score = team2Score + 1;
                            }
                        }

                    }
                    leagueScheduleList.get(i).setTeam1Score(team1Score);
                    leagueScheduleList.get(i).setTeam2Score(team2Score);
                } else {
                    leagueScheduleList.get(i).setTeam1Score(0);
                    leagueScheduleList.get(i).setTeam2Score(0);
                }
            }
        }
        return leagueScheduleList;
    }

    private void observeApiResponse() {
        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    try {
                        seasonsList = seasons;
                        Log.d(LOG_TAG, "Seasons are " + seasonsList.toString());
                        mSeasonNameList.clear();
                        for (int i = 0; i < seasons.size(); i++) {
                            mSeasonNameList.add(seasons.get(i).getSeasonYear());
                        }
                        //
                        if (mSeasonNameList.size() > 0) {
                            mTvSeason.setText(mSeasonNameList.get(0));
                            mTvWeek.setText(mWeekNameList.get(0));
                            mSeasonId = seasonsList.get(0).getSeasonId();
                            divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
        //
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    mDivisionsList = divisions;
                    Log.d(LOG_TAG, "Seasons are " + mDivisionNameList.toString());
                    mDivisionNameList.clear();
                    for (int i = 0; i < divisions.size(); i++) {
                        mDivisionNameList.add(divisions.get(i).getDivisionName());
                    }
                    //
                    mTvDivision.setText(mDivisionNameList.get(0));
                    mDivisionId = mDivisionsList.get(0).getDivisionId();
                    //
                    if (mSeasonId != null && mDivisionId != null)
                        leagueViewModel.getLeagueSchedule(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));

                });
        //
        leagueViewModel.getLeaguScheduleResultLiveData().observe(this, addScheduleLeagueReq -> {
            leagueScheduleList = addScheduleLeagueReq.getScheduleList();
//            mSeasonResultsListAdapter.notifyDataSetChanged();
            mSeasonResultsListAdapter = new SeasonResultsListAdapter(getContext(), null, leagueScheduleList, mGetLeagueRes);
            mRvMatches.setAdapter(mSeasonResultsListAdapter);
        });
    }


    @Override
    public void update(Observable o, Object arg) {
        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;
        //observeLeagueData(mGetLeagueRes);
    }


    private void onLoadMore() {

        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {


            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                String date = leagueScheduleList.get(leagueScheduleList.size() - 1).getDate();
                Log.e("TAG", "pastResult" + totalItemsCount);
                if (isMoreAvailable) {
                    leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId, date));
                }
            }
        };
        mRvMatches.addOnScrollListener(scrollListener);

     /*   mRvMatches.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                Log.e("TAG",visibleItemCount+"pastResult"+pastVisiblesItems+","+totalItemCount);
                if (!isMoreAvailable) {
                    if ((visibleItemCount + pastVisiblesItems) >= 20) {
                        //Action here
                        String date = leagueScheduleList.get(leagueScheduleList.size() - 1).getDate();
                        Log.e("TAG","pastResult"+date);
                        leagueViewModel.getLeagueResults(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId, date));

                    }
                }
            }
        });*/
    }

    public List<ScheduleResultRes> getSchedulesResult(String divisionId, String seasonId){

        List<ScheduleResultRes> scheduleList =  mGetLeagueRes.resultList;

        List<ScheduleResultRes> selectedList = new ArrayList<ScheduleResultRes>();

        if(scheduleList!=null && scheduleList.size()>0){
            for(ScheduleResultRes leagueSchedule:scheduleList){
                String scheduleDivisionId=leagueSchedule.getDivisionId().trim();
                String scheduleSeasonId=leagueSchedule.getSeasonId().trim();
                if(scheduleDivisionId.equals(divisionId) && scheduleSeasonId.equals(seasonId)){
                    selectedList.add(leagueSchedule);
                }
            }
        }
        return selectedList;
    }
}
