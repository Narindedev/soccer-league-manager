package com.apnitor.football.league.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.LeaguePagerAdapater;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.FragmentProfileLeagueBinding;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;

import java.util.Observable;
import java.util.Observer;

import androidx.viewpager.widget.ViewPager;

public class ProfileLeagueFragment extends BaseFragment implements ProfileSeasonFragmentBindingCallback, Observer {

    static GetLeagueRes mGetLeagueRes;
    CustomViewPager viewPager;
    LeaguePagerAdapater adapter;
    private FragmentProfileLeagueBinding binding;
    private LeagueProfileActivity leagueProfileActivity;

    public static ProfileLeagueFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes = new GetLeagueRes();
        mGetLeagueRes = getLeagueRes;
        ProfileLeagueFragment fragment = new ProfileLeagueFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.leagueProfileActivity = (LeagueProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProfileLeagueBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        viewPager = binding.seasonPageViewPager;
        adapter = new LeaguePagerAdapater(getChildFragmentManager(), mGetLeagueRes);
        viewPager.setAdapter(adapter);

        //viewPager.setAdapter(new LeaguePagerAdapater(getChildFragmentManager(), mGetLeagueRes));
        //
        binding.ivResultsTab.setImageResource(R.drawable.ic_infoclr);
        binding.ivPointTableTab.setImageResource(R.drawable.ic_division);
        binding.ivSeasTab.setImageResource(R.drawable.ic_seasion);
        binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
        //
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setPage(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(0);
        return binding.getRoot();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showResults() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void showSchedules() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showPointsTable() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void showStats() {
        viewPager.setCurrentItem(3);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null) {
            viewPager.setCurrentItem(0);
            setPage(viewPager.getCurrentItem());
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Observable observable = o;
        Object object = arg;
        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;
        adapter.updateFragments();
        /**
         * Update data
         */
    }

    @SuppressLint("RestrictedApi")
    void setPage(int position) {
        LeagueProfileActivity.fabSchedule.setVisibility(View.GONE);
        switch (position) {
            case 0:
                LeagueProfileActivity.FAB_TYPE = "";
                //
                LeagueProfileActivity.fabDivision.setVisibility(View.GONE);
                LeagueProfileActivity.fabAddSeason.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_infoclr);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_division);
                binding.ivSeasTab.setImageResource(R.drawable.ic_seasion);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
                break;
            case 1:
                LeagueProfileActivity.FAB_TYPE = "Division";
                //
                if(getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
                    LeagueProfileActivity.fabDivision.setVisibility(View.VISIBLE);
                }
                LeagueProfileActivity.fabAddSeason.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_info);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_divisionclr);
                binding.ivSeasTab.setImageResource(R.drawable.ic_seasion);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
                break;
            case 2:
                LeagueProfileActivity.FAB_TYPE = "Season";
                //
                LeagueProfileActivity.fabDivision.setVisibility(View.GONE);
                if(getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())){
                LeagueProfileActivity.fabAddSeason.setVisibility(View.VISIBLE);
                }
                binding.ivResultsTab.setImageResource(R.drawable.ic_info);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_division);
                binding.ivSeasTab.setImageResource(R.drawable.ic_seasionclr);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
                break;
            case 3:
                LeagueProfileActivity.FAB_TYPE = "";
                //
                LeagueProfileActivity.fabDivision.setVisibility(View.GONE);
                LeagueProfileActivity.fabAddSeason.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_info);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_division);
                binding.ivSeasTab.setImageResource(R.drawable.ic_seasion);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_mediaclr);
                break;
        }
    }
}
