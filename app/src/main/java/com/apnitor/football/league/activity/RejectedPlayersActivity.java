package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.RejectedPlayerAdapter;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RejectedPlayersActivity extends BaseActivity implements ListItemClickCallback {

    private PlayerViewModel playerViewModel;
    private RejectedPlayersActivity playerListActivity = this;
    private ArrayList<PlayerRes> mPlayerRes;
    final String MATCH_DETAIL = "match_detail";
    GetTeamScheduleRes mGetMatchDetail;
    androidx.appcompat.widget.Toolbar mToolbar;
    RecyclerView mRvTeams;
    ArrayList<Boolean> mTeam1SelectedPlayers = new ArrayList<>();
    Button mBtnSave;
    String mTeamSelected = "1";
    RejectedPlayerAdapter mViewPlayerAdapter;
    ArrayList<GetTeamPlayersRes> mTeamsList = new ArrayList<>();
    String LOG_TAG = "RejectedPlayersActivity";
    String mTeamId = "";
    AddScheduleViewModel addScheduleViewModel;
    String TEAM_TYPE = "";

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_player);
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mTeamId = intent.getStringExtra("TEAM_ID");
                TEAM_TYPE = intent.getStringExtra("TEAM_TYPE");
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        //
        setUpLayout();
        mPlayerRes = new ArrayList<>();
        playerViewModel = getViewModel(PlayerViewModel.class);
        setupToolbar(mToolbar, "Rejected Players");
        setupRecyclerView();
        observeApiResponse();
    }

    private void setUpLayout() {
        mTeamsList.clear();
        mToolbar = findViewById(R.id.toolbar);
        mRvTeams = findViewById(R.id.rvAllTeams);
        mBtnSave = findViewById(R.id.saveBtn);
        filterRejectedPlayers();
    }

    private void observeApiResponse() {
        playerViewModel.getTeamPlayersLiveData().observe(this,
                teams -> {
                    mTeamsList.clear();
                    for (int i = 0; i < teams.size(); i++) {
                        mTeam1SelectedPlayers.add(false);
                    }
                    mTeamsList.addAll(teams);
                    mRvTeams.setAdapter(
                            new RejectedPlayerAdapter(playerListActivity, teams, this, mTeam1SelectedPlayers, mTeamSelected)
                    );
                });
    }

    private void setupRecyclerView() {
        mRvTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
        mRvTeams.setAdapter(mViewPlayerAdapter);
    }

    public void onBackClick(View view) {
        onBackPressed();
        //  finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in, R.anim.anim_right_out);
    }

    @Override
    public void onListItemClick(Object object) {
        PlayerRes playerRes = (PlayerRes) object;
    }

    void filterRejectedPlayers() {
        mTeamsList.clear();
        //
        if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
            for (int i = 0; i < mGetMatchDetail.getTeam1Playing11().size(); i++) {
                if (mGetMatchDetail.getTeam1Playing11().get(i).getStatus().equalsIgnoreCase("reject")) {
                    GetTeamPlayersRes getTeamPlayersRes = new GetTeamPlayersRes();
                    getTeamPlayersRes.setProfilePhotoUrl(mGetMatchDetail.getTeam1Playing11().get(i).getImageUrl());
                    getTeamPlayersRes.setFirstName(mGetMatchDetail.getTeam1Playing11().get(i).getFirstName());
                    getTeamPlayersRes.setLastName(mGetMatchDetail.getTeam1Playing11().get(i).getLastName());
                    mTeamsList.add(getTeamPlayersRes);
                }
            }
        } else {
            for (int i = 0; i < mGetMatchDetail.getTeam2Playing11().size(); i++) {
                if (mGetMatchDetail.getTeam2Playing11().get(i).getStatus().equalsIgnoreCase("reject")) {
                    GetTeamPlayersRes getTeamPlayersRes = new GetTeamPlayersRes();
                    getTeamPlayersRes.setProfilePhotoUrl(mGetMatchDetail.getTeam2Playing11().get(i).getImageUrl());
                    getTeamPlayersRes.setFirstName(mGetMatchDetail.getTeam2Playing11().get(i).getFirstName());
                    getTeamPlayersRes.setLastName(mGetMatchDetail.getTeam2Playing11().get(i).getLastName());
                    mTeamsList.add(getTeamPlayersRes);
                }
            }
        }
        //
        mViewPlayerAdapter = new RejectedPlayerAdapter(playerListActivity, mTeamsList, this, mTeam1SelectedPlayers, mTeamSelected);
        setupRecyclerView();
    }
}
