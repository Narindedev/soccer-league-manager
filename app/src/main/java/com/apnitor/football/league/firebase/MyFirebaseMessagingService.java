package com.apnitor.football.league.firebase;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.NotificationActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.util.RequestCodes;
import com.apnitor.football.league.util.ResultCodes;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

import androidx.core.app.NotificationCompat;


/**
 * Created by Acer on 10/15/2017 , 10:13 PM.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String message;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getMessageId());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            sendNotification(remoteMessage);
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

    }
    // [END on_new_token]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");

    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        Map<String, String> messageBody = remoteMessage.getData();
        String type = messageBody.get("type");
        String showMessage = messageBody.get("body");
        String message = messageBody.get("message");
        try {
            JSONObject mObject = new JSONObject(message);
            Intent intent = null;
            NotificationResponse response = new NotificationResponse();


            if (type.equals("managerInvite")) {
                intent = new Intent(this, NotificationActivity.class);
                /* {type=rejectLeagueInvite, message={"teamName":"Barcelona","teamId":"5c7d286b344446001cbbefbb","imageUrl":""}}*/
                response.leagueName = mObject.optString("leagueName");
                response.leagueId = mObject.optString("leagueId");
                response.teamName = "";
                response.teamId = "";
                response.teamImageUrl = "";
                response.type = type;

                showMessage="You're added as a League Manager in league '"+response.leagueName+"'.";
                //message = response.teamName + " refused to join your league.";

                response.message = showMessage;
            }

            if (type.equals("inviteTeam")) {
                intent = new Intent(this, NotificationActivity.class);
                response.leagueName = mObject.optString("leagueName");
                response.seasonId = mObject.optString("seasonId");
                response.leagueId = mObject.optString("leagueId");
                response.seasonName = mObject.optString("seasonName");
                response.divisionName = mObject.optString("divisionName");
                response.divisionId = mObject.optString("divisionId");
                response.teamId = mObject.optString("teamId");
                response.teamName = mObject.optString("teamName");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.type = type;
                // message="Invitation to join "+ response.leagueName+" league.";

                response.message = showMessage;
            }
            if (type.equals("joinLeague")) {
                intent = new Intent(this, NotificationActivity.class);
                /* {type=joinLeague, message={"teamName":"Barcelona","teamId":"5c7d286b344446001cbbefbb","imageUrl":""}}*/
                response.leagueName = mObject.optString("leagueName");
                response.leagueId = mObject.optString("leagueId");
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.type = type;
                //message = response.teamName + " joined your league '" + response.leagueName + "'.";

                response.message = showMessage;
            }
            if (type.equals("rejectLeagueInvite")) {
                intent = new Intent(this, NotificationActivity.class);
                /* {type=rejectLeagueInvite, message={"teamName":"Barcelona","teamId":"5c7d286b344446001cbbefbb","imageUrl":""}}*/
                response.leagueName = mObject.optString("leagueName");
                response.leagueId = mObject.optString("leagueId");
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.type = type;
                //message = response.teamName + " refused to join your league.";

                response.message = showMessage;
            }

            if (type.equals("acceptLeagueInvite")) {
                intent = new Intent(this, LeagueProfileActivity.class);
                /* {type=rejectLeagueInvite, message={"teamName":"Barcelona","teamId":"5c7d286b344446001cbbefbb","imageUrl":""}}*/
                response.leagueName = mObject.optString("leagueName");
                response.leagueId = mObject.optString("leagueId");
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.type = type;
                //message = response.teamName + " refused to join your league.";


                response.message = showMessage;
                //Intent intent1 = new Intent(RequestCodes.LEAGUE_PROFILE_TEAM_BROADCAST);
                intent.putExtra("league_id", response.leagueId);
                intent.putExtra("league_name",response.leagueName);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Intent intent1 = new Intent();
                /*Set the SAME ACTION WHCIH you will set in Broadcast reciever in your activity or in manifest*/
                intent1.setAction(RequestCodes.ADD_TEAM_TO_LEAGUE);
                intent1.putExtra("data",response);
                intent1.putExtra("type",type);
                sendBroadcast(intent1);
            }

            if (type.equals("invitePlayer")) {
                intent = new Intent(this, NotificationActivity.class);
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.firstName = mObject.optString("playerFirstName");
                response.lastName = mObject.optString("playerLastName");
                response.playerImageUrl = mObject.optString("playerImageUrl");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.playerId = mObject.optString("playerId");
                response.type = type;

                //  message = "Invitation to join " + response.teamName + " team.";
                response.message = showMessage;

            }
            if (type.equals("joinTeam")) {
                intent = new Intent(this, NotificationActivity.class);
                /* {type=joinTeam, message={"firstName":"Graak","lastName":"Graak","imageUrl":"https:\/\/graph.facebook.com\/2015978405136508\/picture?type=square","playerId":"5c7d21f7344446001cbbefb6"}}*/
                response.firstName = mObject.optString("playerFirstName");
                response.lastName = mObject.optString("playerLastName");
                response.playerId = mObject.optString("playerId");
                response.teamName = mObject.optString("teamName");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.teamId = mObject.optString("teamId");
                response.playerImageUrl = mObject.optString("playerImageUrl");
                response.type = type;
                //  message = response.firstName + " " + response.lastName + " joined your team.";

                response.message = showMessage;
            }
            if (type.equals("rejectTeamInvite")) {
                intent = new Intent(this, NotificationActivity.class);
                /*  {type=rejectTeamInvite, message={"firstName":"Graak","lastName":"Graak","imageUrl":"https:\/\/graph.facebook.com\/2015978405136508\/picture?type=square","playerId":"5c7d21f7344446001cbbefb6"}}*/
                response.firstName = mObject.optString("playerFirstName");
                response.lastName = mObject.optString("playerLastName");
                response.playerId = mObject.optString("playerId");
                response.playerImageUrl = mObject.optString("playerImageUrl");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.type = type;
                // message = response.firstName + " " + response.lastName + " refused to join your team.";

                response.message = showMessage;
            }
            if (type.equals("acceptTeamInvite")) {
                intent = new Intent(this, TeamProfileActivity.class);
                /*  {type=rejectTeamInvite, message={"firstName":"Graak","lastName":"Graak","imageUrl":"https:\/\/graph.facebook.com\/2015978405136508\/picture?type=square","playerId":"5c7d21f7344446001cbbefb6"}}*/
                response.firstName = mObject.optString("playerFirstName");
                response.lastName = mObject.optString("playerLastName");
                response.playerId = mObject.optString("playerId");
                response.playerImageUrl = mObject.optString("playerImageUrl");
                response.teamImageUrl = mObject.optString("teamImageUrl");
                response.teamName = mObject.optString("teamName");
                response.teamId = mObject.optString("teamId");
                response.type = type;
                // message = response.firstName + " " + response.lastName + " refused to join your team.";

                response.message = showMessage;
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("team_id", response.teamId);
                intent.putExtra("team_name",response.teamName);
                //Intent intent2 = new Intent(RequestCodes.LEAGUE_PROFILE_TEAM_BROADCAST);
                Intent intent2 = new Intent();
                intent2.setAction(RequestCodes.ADD_PLAYER_TO_TEAM);
                intent2.putExtra("data",response);
                intent2.putExtra("type",type);
                sendBroadcast(intent2);
            }

            if (intent == null) {
                intent = new Intent(this, NotificationActivity.class);
            }

            // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("data", response);
            intent.putExtra("type", type);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, ResultCodes.ADD_TEAM_RESULT_CODE /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);


            String channelId = getString(R.string.default_channel_id);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.drawable.ic_logo_small)
                            .setColor(getResources().getColor(R.color.app_background))
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(showMessage)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(showMessage))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Log.e(TAG, "Playersss: " + showMessage);
            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_HIGH);
                channel.enableLights(true);
                channel.enableVibration(true);

                channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Message data payload: {body=You have received Invitation to join the league , type=managerInvite, title=Invitation to Join the League, message={"owner":"5ca10fb8a9538f001c6b7e1b","leagueName":"league154","leagueId":"5cac73e46acc5f001cea8d9c"}}
E/MyFirebaseMsgService: Playersss: You have received Invitation to join the league */
}