package com.apnitor.football.league.api.request;

/**
 * Created by deepakkanyan on 06/04/19 at 10:28 PM.
 */

public class AddLeagueManagerReq {
    private String name;
    private String leagueId;
    private String managerId;

    public AddLeagueManagerReq(String name){
        this.name=name;
    }
    public AddLeagueManagerReq(String leagueId,String managerId){
        this.leagueId=leagueId;
        this.managerId=managerId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
