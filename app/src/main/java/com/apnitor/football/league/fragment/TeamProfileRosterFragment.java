package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.AddLeagueRosterAdapter;
import com.apnitor.football.league.adapter.ShowTeamLeaguesAdapter;
import com.apnitor.football.league.adapter.TeamPlayersListAdapter;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.FragmentTeamProfileRosterBinding;
import com.apnitor.football.league.databinding.FragmentTeamRosterBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.CustomViewPager;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class TeamProfileRosterFragment extends BaseFragment implements Observer {

    public static ArrayList<GetTeamPlayersRes> teamListRoster = new ArrayList<>();
    static GetTeamRes mGetTeamRes;
    static GetTeamDetailRes getTeamDetailRes;
    CustomViewPager viewPager;
    ArrayList<Boolean> mAddRosterBoolean = new ArrayList<>();
    private FragmentTeamProfileRosterBinding binding;
    private TeamProfileActivity teamProfileActivity;
    private String selectedTeamLeagueId;
    private PlayerViewModel playerViewModel;
    private View mView;
    private List<GetLeagueRes> leagueList;

    public static TeamProfileRosterFragment newInstance(GetTeamDetailRes getLeagueRes) {
        getTeamDetailRes = getLeagueRes;
        mGetTeamRes = getLeagueRes.getTeamModel();
        TeamProfileRosterFragment fragment = new TeamProfileRosterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.teamProfileActivity = (TeamProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        binding = FragmentTeamProfileRosterBinding.inflate(inflater, container, false);
        setupRecyclerView();
//        binding.setLeagueInfo(null);
        playerViewModel = getViewModel(PlayerViewModel.class);
        playerViewModel.getTeamPlayersLiveData().observe(this,
                teams -> {
                    teamListRoster.clear();
                    teamListRoster.addAll(teams);
                    if (teamListRoster != null && teamListRoster.size() > 0) {
                        binding.tvTeamPlayers.setVisibility(View.VISIBLE);
                        //       sortLisAplhabatically(teamListRoster, binding.rvTeamPlayers);
                    } else {
                        binding.tvTeamPlayers.setVisibility(View.GONE);
                    }

                });
        loadTeamPlayers(mGetTeamRes);

        //setRetainInstance(true);
        return binding.getRoot();
    }




    private void setupRecyclerView() {
        binding.rvTeamPlayers.setLayoutManager(
                new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false)
        );
        binding.rvInvitedPlayers.setLayoutManager(
                new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false)
        );
    }

    private void loadTeamPlayers(GetTeamRes getTeamRes) {
        List<PlayerRes> acceptedList = getTeamRes.getAcceptedPlayerList();
        List<PlayerRes> invitedList = getTeamRes.getInvitedPlayerList();
        if (acceptedList != null && !acceptedList.isEmpty() || invitedList != null && !invitedList.isEmpty()) {
            binding.llEmptyPlayers.setVisibility(View.GONE);
            if (acceptedList != null && !acceptedList.isEmpty()) {

                binding.tvTeamPlayers.setVisibility(View.VISIBLE);
                sortLisAplhabatically(acceptedList, binding.rvTeamPlayers);
            } else {

                binding.tvTeamPlayers.setVisibility(View.GONE);
            }


            if (invitedList != null && !invitedList.isEmpty()) {
                binding.tvInvitedPlayers.setVisibility(View.VISIBLE);
                sortLisAplhabatically(invitedList, binding.rvInvitedPlayers);
            } else {
                binding.tvInvitedPlayers.setVisibility(View.GONE);
            }
        } else {
            binding.llEmptyPlayers.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        // playerViewModel.getTeamPlayers(new GetTeamPlayersReq(mGetTeamRes.getTeamId()));
//        playerViewModel.getTeamPlayers(new GetTeamPlayersReq("5c5c77d18efce4001cfb766d"));
    }

    void sortLisAplhabatically(List<PlayerRes> teamList, RecyclerView recyclerView) {
        if (teamList.size() > 0) {
            Collections.sort(teamList, new Comparator<PlayerRes>() {
                @Override
                public int compare(final PlayerRes object1, final PlayerRes object2) {
                    return (object1.getFirstName() + object1.getLastName()).compareTo(object2.getFirstName() + object2.getLastName());
                }
            });
        }

        recyclerView.setAdapter(
                new TeamPlayersListAdapter(getActivity(), teamList, "ROSTER")
        );
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetTeamRes = TeamProfileActivity.getTeamDetailRes.getTeamModel();
        loadTeamPlayers(mGetTeamRes);
    }


}
