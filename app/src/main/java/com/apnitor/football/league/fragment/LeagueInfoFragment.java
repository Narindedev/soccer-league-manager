package com.apnitor.football.league.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.adapter.SeasonPagePagerAdapater;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.FragmentLeagueInfoBinding;
import com.apnitor.football.league.databinding.FragmentProfileSeasonsBinding;
import com.apnitor.football.league.fragment.BaseFragment;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;
import com.apnitor.football.league.viewmodel.DivisionViewModel;

import java.util.Observable;
import java.util.Observer;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

public class LeagueInfoFragment extends BaseFragment implements Observer {

    private static GetLeagueRes mGetLeagueRes;
    private FragmentLeagueInfoBinding binding;
    private LeagueProfileActivity leagueProfileActivity;
    CustomViewPager viewPager;
    private DivisionViewModel divisionViewModel;
    private final static String LEAGUE_INFO = "league_info";

    public static LeagueInfoFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes=getLeagueRes;
        LeagueInfoFragment fragment = new LeagueInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(LEAGUE_INFO, getLeagueRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.leagueProfileActivity = (LeagueProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentLeagueInfoBinding.inflate(inflater, container, false);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        // TODO: get from arguments and set, remove null
        if (getArguments() != null && getArguments().getSerializable(LEAGUE_INFO) != null) {
            mGetLeagueRes=(GetLeagueRes) getArguments().getSerializable(LEAGUE_INFO);
            Log.e("mGetLeagueRes",""+mGetLeagueRes.getLeagueTitle());
            binding.setLeagueInfo(mGetLeagueRes);
        }
        socialClickListener();
        binding.rvLeagueInfo.setLayoutManager(new LinearLayoutManager(getActivity()));
        observeApiResponse();
        //binding.rvLeagueInfo.setAdapter();
        return binding.getRoot();
    }

    private void socialClickListener() {
        binding.ivFbLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mGetLeagueRes.socialMedia.facebookLink;
                if (url!=null && !url.isEmpty()){
                    openSocial(url);
                }else{
                    showToast("No link found");
                }
            }
        });
        binding.ivTwitterLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mGetLeagueRes.socialMedia.twitterLink;
                if (url!=null && !url.isEmpty()){
                    openSocial(url);
                }else{
                    showToast("No link found");
                }
            }
        });

        binding.ivInstaLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mGetLeagueRes.socialMedia.instaLink;
                if (url!=null && !url.isEmpty()){
                    openSocial(url);
                }else{
                    showToast("No link found");
                }
            }
        });
    }
    private void openSocial(String url){
        if (!url.toLowerCase().contains("http://")){
            if(!url.toLowerCase().contains("www.")){
                url="www."+url;
            }
                url="http://"+url;
        }

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(url));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void observeApiResponse() {
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    binding.rvLeagueInfo.setAdapter(
                            new DivisionsListAdapter(getActivity(), divisions, null)
                    );
                });

        divisionViewModel.getUpdateDivisionResLiveData().observe(this,
                res -> {
                    divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                });
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetLeagueRes=((LeagueProfileActivity)getActivity()).mGetLeagueRes;
        binding.setLeagueInfo(mGetLeagueRes);
    }
}
