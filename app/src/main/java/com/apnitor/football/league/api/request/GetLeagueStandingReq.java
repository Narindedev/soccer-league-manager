package com.apnitor.football.league.api.request;

public class GetLeagueStandingReq {
    private String leagueId;
    private String divisionId;
    private String seasonId;

  public GetLeagueStandingReq(String leagueId,String divisionId,String seasonId){
      this.divisionId=divisionId;
      this.leagueId=leagueId;
      this.seasonId=seasonId;
  }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }
}
