package com.apnitor.football.league.api.response;

import java.io.Serializable;

public class GetEventRes implements Serializable {
private String createdAt;
    private String updatedAt;
    private String eventTime;
    private String eventType;
    private String _id;
    private String scoredByTeam;
    private String concededByTeam;
    private String goalScoredBy;
    private String goalAssistedBy;

    private String givenTo;
    private String teamId;
    private String playerIn;
    private String playerOut;


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getScoredByTeam() {
        return scoredByTeam;
    }

    public void setScoredByTeam(String scoredByTeam) {
        this.scoredByTeam = scoredByTeam;
    }

    public String getConcededByTeam() {
        return concededByTeam;
    }

    public void setConcededByTeam(String concededByTeam) {
        this.concededByTeam = concededByTeam;
    }

    public String getGoalScoredBy() {
        return goalScoredBy;
    }

    public void setGoalScoredBy(String goalScoredBy) {
        this.goalScoredBy = goalScoredBy;
    }

    public String getGoalAssistedBy() {
        return goalAssistedBy;
    }

    public void setGoalAssistedBy(String goalAssistedBy) {
        this.goalAssistedBy = goalAssistedBy;
    }

    public String getGivenTo() {
        return givenTo;
    }

    public void setGivenTo(String givenTo) {
        this.givenTo = givenTo;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getPlayerIn() {
        return playerIn;
    }

    public void setPlayerIn(String playerIn) {
        this.playerIn = playerIn;
    }

    public String getPlayerOut() {
        return playerOut;
    }

    public void setPlayerOut(String playerOut) {
        this.playerOut = playerOut;
    }
}
