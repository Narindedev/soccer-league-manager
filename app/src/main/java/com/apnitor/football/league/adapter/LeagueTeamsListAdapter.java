package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueTeamsListAdapter extends RecyclerView.Adapter<LeagueTeamsListAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
//    private LanguageInterface languageInterface;


    public LeagueTeamsListAdapter(Context context, List<GetTeamRes> teamList, int orientation, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_team_item, parent, false);
        if (orientation == LinearLayoutManager.VERTICAL)
            view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);

        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamRes model = teamList.get(position);

            holder.tvTeamName.setText(model.getTeamName());
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                }
            });
        }
    }
}
