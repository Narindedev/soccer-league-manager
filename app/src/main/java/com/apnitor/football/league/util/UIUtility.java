package com.apnitor.football.league.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class UIUtility {

    public static String getEditTextValue(EditText editText) {
        if (editText != null && editText.getText() != null) {
            return editText.getText().toString().trim();
        }
        return "";
    }

    public static boolean isNotValidEmail(CharSequence target) {
        return TextUtils.isEmpty(target) || !Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isNotValidPhone(CharSequence target) {
        return TextUtils.isEmpty(target) || !Patterns.PHONE.matcher(target).matches() || target.length() > 15 || target.length() < 10;
    }


    public static boolean isValidPassword(String s) {
        String strPattern = "((?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,25})";
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                strPattern);

        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
    }

    public static boolean checkSpcChar(String str) {
        Pattern regex = Pattern.compile("[$&+,:;=\\\\?@#|/'<>.^*()%!-]");

        if (regex.matcher(str).find()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkCaps(String str) {
        Pattern pattern = Pattern.compile("(?=.*[A-Z])");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkSmall(String str) {
        Pattern pattern = Pattern.compile("(?=.*[a-z])");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }


    public static String generateStorngPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 1000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return iterations + ":" + toHex(salt) + ":" + toHex(hash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    public static String hashString(String message) {
        String bytte = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA512");
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));

            bytte = convertByteArrayToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {

        }
        return bytte;
    }

    private static String convertByteArrayToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
                    .substring(1));
        }
        return stringBuffer.toString();
    }

    public static String getAssetJsonData(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }

        Log.e("data", json);
        return json;
    }

    public static String getFormattedDate(String date) {
        if (date == null) {
            return "";
        }
        //
        String sMyDate = date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.ENGLISH);
            Date myDate = sdf.parse(date);
            sdf.applyPattern("EEE, d MMM yyyy");
            sMyDate = sdf.format(myDate);
//            System.out.println("PREV DATE IS " + date);
//            System.out.println("FINAL DATE IS " + sMyDate);
        } catch (Exception e) {
            e.printStackTrace();
            sMyDate = date;
        }
        return sMyDate;
    }


    public static String getDateFormat(String date){
        String newDateStr="";
        if (date == null) {
            return "";
        }
        //
        String sMyDate = date;
        try {
            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
            Date dateObj = curFormater.parse(date);

             newDateStr = curFormater.format(dateObj);
        } catch (Exception e) {
            e.printStackTrace();
            sMyDate = date;
        }
        return newDateStr;
    }

    public static boolean checkCurrentDate(String strDate1) {
        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = df.format(c);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(strDate1);
            Date date2 = sdf.parse(currentDate);

            if (date1.compareTo(date2) == 0 || date1.compareTo(date2) < 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
