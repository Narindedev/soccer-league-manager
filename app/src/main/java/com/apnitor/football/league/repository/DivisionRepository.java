package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.DivisionApi;
import com.apnitor.football.league.api.request.DeleteDivisionReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.UpdateDivisionReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.UpdateDivisionRes;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class DivisionRepository {

    private DivisionApi divisionApi;

    public DivisionRepository(Application application) {
        divisionApi = ApiService.getDivisionApi(application);
    }

    public Single<BaseRes<List<GetDivisionRes>>> getDivisions(GetLeagueDivisionsReq getLeagueDivisionsReq) {
        return divisionApi.getDivision(getLeagueDivisionsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateDivisionRes>> createUpdateDivision(UpdateDivisionReq updateSeasonReq) {
        return divisionApi.createUpdateDivision(updateSeasonReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<Object>> deleteDivision(DeleteDivisionReq deleteDivisionReq) {
        return divisionApi.deleteDivision(deleteDivisionReq)
                .subscribeOn(Schedulers.io());
    }
}
