package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.fragment.DivisionListFragment;
import com.apnitor.football.league.fragment.LeagueInfoFragment;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.LeagueShareNewsFragment;
import com.apnitor.football.league.fragment.PointsTableFragment;
import com.apnitor.football.league.fragment.SeasonListFragment;
import com.apnitor.football.league.fragment.SeasonsResultFragment;
import com.apnitor.football.league.fragment.StatsFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class LeaguePagerAdapater extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    GetLeagueRes getLeagueRes;
    private Observable mObservers = new FragmentObserver();


    public LeaguePagerAdapater(FragmentManager fm) {
        super(fm);
    }


    public LeaguePagerAdapater(FragmentManager fm, GetLeagueRes getLeagueRes) {
        super(fm);
        this.getLeagueRes = getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                LeagueInfoFragment leagueInfoFragment=LeagueInfoFragment.newInstance(getLeagueRes);
                if (leagueInfoFragment instanceof Observer)
                    mObservers.addObserver((Observer)leagueInfoFragment);


                return leagueInfoFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title


                DivisionListFragment divisionListFragment = DivisionListFragment.newInstance(0, "Page # 1", getLeagueRes);

                if(divisionListFragment instanceof Observer)
                    mObservers.addObserver((Observer) divisionListFragment);

                return divisionListFragment;


               // return DivisionListFragment.newInstance(0, "Page # 1", getLeagueRes);



            case 2: // Fragment # 1 - This will show SecondFragment

                SeasonListFragment seasonListFragment = SeasonListFragment.newInstance(0, "Page # 1",getLeagueRes);

                if(seasonListFragment instanceof Observer)
                    mObservers.addObserver((Observer) seasonListFragment);


                return seasonListFragment;


           /* case 3: // Fragment # 1 - This will show SecondFragment
                LeagueShareNewsFragment shareNewsFragment = LeagueShareNewsFragment.newInstance(0, "Page # 1",getLeagueRes);

                if(shareNewsFragment instanceof Observer)
                    mObservers.addObserver((Observer) shareNewsFragment);
                return shareNewsFragment;
*/

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void updateFragments(){
        mObservers.notifyObservers();
    }
}
