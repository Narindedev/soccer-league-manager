package com.apnitor.football.league.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;

import com.apnitor.football.league.util.CustomViewPager;


public class TeamInfoFullFragment extends Fragment {
   private CustomViewPager customViewPager;
    private final static String TEAM_INFO = "team_info";

    public static TeamInfoFragment newInstance(int i,String getTeamRes) {
        TeamInfoFragment fragment = new TeamInfoFragment();
        /*Bundle args = new Bundle();
        args.putSerializable(TEAM_INFO, getTeamRes);
        fragment.setArguments(args);*/
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_team_info_full, container, false);
//        customViewPager=(CustomViewPager)view.findViewById(R.id.teamInfoPageViewPager);
        if (getArguments() != null && getArguments().getSerializable(TEAM_INFO) != null) {
            //binding.setTeamInfo((GetTeamRes) getArguments().getSerializable(TEAM_INFO));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
