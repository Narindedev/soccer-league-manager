package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueShareNewsAdapter extends RecyclerView.Adapter<LeagueShareNewsAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
//    private LanguageInterface languageInterface;


    public LeagueShareNewsAdapter(Context context, ArrayList<String> teamList) {
        this.context = context;
        this.teamList = teamList;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_league_share_news, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
//        viewHolder.dayScheduleRv.setRecycledViewPool(viewPool);
        return viewHolder;
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

//            holder.dayScheduleRv.setHasFixedSize(true);
//            holder.dayScheduleRv.setNestedScrollingEnabled(false);
//
//            LeagueDayScheduleListAdapter adapter = new LeagueDayScheduleListAdapter(context, null);
//            holder.dayScheduleRv.setAdapter(adapter);
            //            GetTeamRes model = teamList.get(position);

//            holder.tvTeamName2.setText(model.getTeamName());
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return 0;
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;
        private RecyclerView dayScheduleRv;

        ViewHolder(View itemView) {
            super(itemView);
//            tvTeamName2 = (TextView) itemView.findViewById(R.id.tvScheduleDayDate);
//            dayScheduleRv = itemView.findViewById(R.id.rvDaySchedules);
//            dayScheduleRv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        }
    }
}
