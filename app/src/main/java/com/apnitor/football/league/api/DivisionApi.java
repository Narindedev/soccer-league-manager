package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteDivisionReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.UpdateDivisionReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.UpdateDivisionRes;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface DivisionApi {

    @POST("updateLeagueDivision")
    Single<BaseRes<UpdateDivisionRes>> createUpdateDivision(@Body UpdateDivisionReq updateSeasonReq);

    @POST("deleteDivision")
    Single<BaseRes<Object>> deleteDivision(@Body DeleteDivisionReq deleteDivisionReq);

    @POST("getLeagueDivisions")
    Single<BaseRes<List<GetDivisionRes>>> getDivision(@Body GetLeagueDivisionsReq getLeagueDivisionsReq);
}
