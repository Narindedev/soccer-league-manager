package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetPlayerDetailReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PlayerApi {

    @GET("getAllPlayers")
    Single<BaseRes<ArrayList<PlayerRes>>> getAllPlayers();

    @POST("getAllPlayers")
    Single<BaseRes<ArrayList<PlayerRes>>> getAllPlayers(@Body GetAllTeamReq getAllTeamReq);

    @POST("getTeamPlayers")
    Single<BaseRes<ArrayList<GetTeamPlayersRes>>> getTeamPlayers(@Body GetTeamPlayersReq getTeamPlayersReq);


    @POST("invitePlayer")
    Single<BaseRes<PlayerRes>> invitePlayer(@Body GetTeamPlayersReq getTeamPlayersReq);

    @POST("joinTeam")
    Single<BaseRes<PlayerRes>> joinTeam(@Body GetTeamPlayersReq getTeamPlayersReq);

    @POST("rejectTeamInvite")
    Single<BaseRes<PlayerRes>> rejectTeamInvite(@Body GetTeamPlayersReq getTeamPlayersReq);

    @POST("getPlayer")
    Single<BaseRes<PlayerProfileRes>> getPlayerDetail(@Body GetPlayerDetailReq getPlayerDetailReq);



}
