package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.TeamInfoPagerAdapater;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.FragmentTeamInfoBinding;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;

import java.util.Observable;
import java.util.Observer;

import androidx.viewpager.widget.ViewPager;

public class TeamInfoFragment extends BaseFragment implements ProfileSeasonFragmentBindingCallback, Observer {

    static GetTeamRes mGetTeamRes;
    CustomViewPager viewPager;
    TeamInfoPagerAdapater infoPagerAdapater;
    private FragmentTeamInfoBinding binding;
    private TeamProfileActivity leagueProfileActivity;

    public static TeamInfoFragment newInstance(GetTeamRes getLeagueRes) {
        mGetTeamRes = getLeagueRes;
        TeamInfoFragment fragment = new TeamInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.leagueProfileActivity = (TeamProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentTeamInfoBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        //
        viewPager = binding.seasonPageViewPager;
        infoPagerAdapater = new TeamInfoPagerAdapater(getChildFragmentManager(), mGetTeamRes);
        viewPager.setAdapter(infoPagerAdapater);
        //
        binding.ivResultsTab.setImageResource(R.drawable.ic_infoclr);
        binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
        viewPager.beginFakeDrag();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_infoclr);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_media);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                       /* binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                    case 1:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_info);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_mediaclr);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                    /*    binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return binding.getRoot();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showResults() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void showSchedules() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showPointsTable() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void showStats() {
        viewPager.setCurrentItem(3);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetTeamRes = ((TeamProfileActivity) getActivity()).getTeamRes;
        infoPagerAdapater.notifyDataSetChanged();
        infoPagerAdapater.updateFragments();
    }
}
