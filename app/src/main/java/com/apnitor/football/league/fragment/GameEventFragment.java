package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.referee.MatchDetailActivity;
import com.apnitor.football.league.adapter.GameEventAdapter;
import com.apnitor.football.league.api.request.GetMatchEventsReq;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GameEventFragment extends BaseFragment {

    RecyclerView mRvNinty;
    RecyclerView mRvFortyFive;
    TextView mTvTeam1, mTvTeam2, tvMatchNotStarted;
    static GetTeamScheduleRes mGetMatchDetail;
    RefereeViewModel refereeViewModel;
    ArrayList<GetMatchEventRes> mUpdateMatchEventReq = new ArrayList<>();
    String LOG_TAG = "GameEventFragment";
    boolean isApiCall = true;

    public static GameEventFragment newInstance(int i, String s, GetTeamScheduleRes matchDetail) {
        mGetMatchDetail = matchDetail;
        GameEventFragment fragment = new GameEventFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_event, container, false);
        setUpLayout(view);
        return view;
    }

    private void setUpLayout(View view) {

        refereeViewModel = getViewModel(RefereeViewModel.class);
        mTvTeam1 = view.findViewById(R.id.tv_team_1);
        mTvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvTeam1.setTextColor(getActivity().getResources().getColor(R.color.dark_black));
                mTvTeam2.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round);
            }
        });

        //
        mTvTeam2 = view.findViewById(R.id.tv_team_2);

        mTvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvTeam1.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                mTvTeam2.setTextColor(getActivity().getResources().getColor(R.color.dark_black));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
            }
        });
        mTvTeam1.performClick();
        //
        mRvNinty = view.findViewById(R.id.rv_ninty);
        mRvNinty.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvNinty.setNestedScrollingEnabled(false);
        //
        mRvFortyFive = view.findViewById(R.id.rv_fortyfive);
        mRvFortyFive.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvFortyFive.setNestedScrollingEnabled(false);
        //
        mRvNinty.setAdapter(new GameEventAdapter(getContext(), mUpdateMatchEventReq, mGetMatchDetail));
        mRvFortyFive.setAdapter(new GameEventAdapter(getContext(), mUpdateMatchEventReq, mGetMatchDetail));
        //
        if (mGetMatchDetail.getRefereeId() != null && !mGetMatchDetail.getRefereeId().equalsIgnoreCase(PreferenceHandler.readString(getActivity(), PreferenceHandler.PREF_USER_ID, ""))) {
            view.findViewById(R.id.tv_add_game_event).setVisibility(View.GONE);
        }
        if (!checkCurrentDate(mGetMatchDetail.getDate())) {
            view.findViewById(R.id.tv_add_game_event).setVisibility(View.GONE);
            isApiCall = false;
        }
        view.findViewById(R.id.tv_add_game_event).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MatchDetailActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("match_detail", mGetMatchDetail);
                b.putBoolean("isMatchStarted", mUpdateMatchEventReq.size() > 0 ? true : false);
                intent.putExtras(b);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        tvMatchNotStarted = view.findViewById(R.id.tvMatchNotStarted);
    }

    private void getMatchDetailsApi() {
        refereeViewModel.getMatchEventsLiveData().observe(this,
                addScheduleRes -> {
                    try {
                        mUpdateMatchEventReq.clear();
                        mUpdateMatchEventReq.addAll(addScheduleRes);
                        if (mUpdateMatchEventReq.size() < 1) {
                            PreferenceHandler.writeString(getActivity(), PreferenceHandler.PREF_KEY_MATCH_STATUS, "MATCH NOT STARTED");
                            tvMatchNotStarted.setVisibility(View.VISIBLE);
                        } else {
                            tvMatchNotStarted.setVisibility(View.GONE);
                            sortMatchEvents(addScheduleRes);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        );
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    refereeViewModel.getMatchEvents(new GetMatchEventsReq(mGetMatchDetail.getLeagueId(), mGetMatchDetail.getScheduleId()));
                    getMatchDetailsApi();
                }
            }, 500);
        }
    }


    void sortMatchEvents(ArrayList<GetMatchEventRes> mUpdateMatchEventReq) {
        int size = mUpdateMatchEventReq.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                mUpdateMatchEventReq.get(i).eventTime = setMatchEventTime(mUpdateMatchEventReq.get(i));
            }
            //
            Collections.sort(mUpdateMatchEventReq, new Comparator<GetMatchEventRes>() {
                @Override
                public int compare(final GetMatchEventRes object1, final GetMatchEventRes object2) {
//                    Log.d(LOG_TAG, " Comparing 1st " + object1.eventTime + " with " + object2.eventTime);
                    return object1.eventTime.compareTo(object2.eventTime);
                }
            });
        }
        //
        Collections.reverse(mUpdateMatchEventReq);
        //
        mRvNinty.setAdapter(new GameEventAdapter(getContext(), mUpdateMatchEventReq, mGetMatchDetail));
    }

    String setMatchEventTime(GetMatchEventRes mUpdateMatchEventReq) {
        String time = mUpdateMatchEventReq.eventTime;
        String[] timeArray = time.split(Pattern.quote(":"));
        if (timeArray.length == 3) {
            int minutes = 60 * Integer.parseInt(timeArray[0]) + Integer.parseInt(timeArray[1]);
            time = minutes + "";
        } else {
            int minutes = Integer.parseInt(timeArray[0]);
            time = minutes + "";
        }
        return time;
    }

    public static boolean checkCurrentDate(String strDate1) {
        try {
            Date c = Calendar.getInstance().getTime();
            System.out.println("Current time => " + c);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String currentDate = df.format(c);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(strDate1);
            Date date2 = sdf.parse(currentDate);
            if (date1.compareTo(date2) == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
}
