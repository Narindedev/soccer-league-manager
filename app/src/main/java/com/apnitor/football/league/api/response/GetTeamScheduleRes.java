package com.apnitor.football.league.api.response;

import com.apnitor.football.league.api.request.TeamPlaying11;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetTeamScheduleRes implements Serializable {

    @Expose
    @SerializedName("leagueId")
    private String leagueId;

    @Expose
    @SerializedName("leagueName")
    private String leagueName;

    @Expose
    @SerializedName("scheduleId")
    private String scheduleId;

    @Expose
    @SerializedName("seasonId")
    private String seasonId;

    @Expose
    @SerializedName("divisionId")
    private String divisionId;

    @Expose
    @SerializedName("date")
    private String date;

    @Expose
    @SerializedName("team1Id")
    private String team1Id;

    @Expose
    @SerializedName("team2Id")
    private String team2Id;

    @Expose
    @SerializedName("team1Name")
    private String team1Name;

    @Expose
    @SerializedName("team2Name")
    private String team2Name;

    @Expose
    @SerializedName("location")
    private String location;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("zipcode")
    private String zipcode;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("team1Playing11")
    private ArrayList<TeamPlaying11> team1Playing11;

    @Expose
    @SerializedName("team2Playing11")
    private ArrayList<TeamPlaying11> team2Playing11;

    @Expose
    @SerializedName("refereeId")
    private String refereeId;


    @Expose
    @SerializedName("team1Formation")
    private String team1Formation;

    @Expose
    @SerializedName("team2Formation")
    private String team2Formation;


    @Expose
    @SerializedName("team1Score")
    private Integer team1Score;

    @Expose
    @SerializedName("team2Score")
    private Integer team2Score;

    @Expose
    @SerializedName("eventList")
    public List<GetEventRes> eventList;

    private TeamLeagueRes team1Detail;

    private TeamLeagueRes team2Detail;

    public GetTeamScheduleRes(String leagueId, String leagueName, String scheduleId, String seasonId, String divisionId, String date, String team1Id, String team2Id, String team1Name, String team2Name, String location, String state, String city, String zipcode, String description) {
        this.leagueId = leagueId;
        this.leagueName = leagueName;
        this.scheduleId = scheduleId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
        this.date = date;
        this.team1Id = team1Id;
        this.team2Id = team2Id;
        this.team1Name = team1Name;
        this.team2Name = team2Name;
        this.location = location;
        this.state = state;
        this.city = city;
        this.zipcode = zipcode;
        this.description = description;
    }

    public GetTeamScheduleRes() {

    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(String team1Id) {
        this.team1Id = team1Id;
    }

    public String getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(String team2Id) {
        this.team2Id = team2Id;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public ArrayList<TeamPlaying11> getTeam1Playing11() {
        return team1Playing11;
    }

    public void setTeam1Playing11(ArrayList<TeamPlaying11> team1Playing11) {
        this.team1Playing11 = team1Playing11;
    }

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }


    public ArrayList<TeamPlaying11> getTeam2Playing11() {
        return team2Playing11;
    }

    public void setTeam2Playing11(ArrayList<TeamPlaying11> team2Playing11) {
        this.team2Playing11 = team2Playing11;
    }

    public Integer getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(Integer team1Score) {
        this.team1Score = team1Score;
    }

    public Integer getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(Integer team2Score) {
        this.team2Score = team2Score;
    }

    public String getTeam1Formation() {
        return team1Formation;
    }

    public void setTeam1Formation(String team1Formation) {
        this.team1Formation = team1Formation;
    }

    public String getTeam2Formation() {
        return team2Formation;
    }

    public void setTeam2Formation(String team2Formation) {
        this.team2Formation = team2Formation;
    }

    public TeamLeagueRes getTeam1Detail() {
        return team1Detail;
    }

    public void setTeam1Detail(TeamLeagueRes team1Detail) {
        this.team1Detail = team1Detail;
    }

    public TeamLeagueRes getTeam2Detail() {
        return team2Detail;
    }

    public void setTeam2Detail(TeamLeagueRes team2Detail) {
        this.team2Detail = team2Detail;
    }

    @Override
    public String toString() {
        return "GetTeamScheduleRes{" +
                "leagueId='" + leagueId + '\'' +
                ", leagueName='" + leagueName + '\'' +
                ", scheduleId='" + scheduleId + '\'' +
                ", seasonId='" + seasonId + '\'' +
                ", divisionId='" + divisionId + '\'' +
                ", date='" + date + '\'' +
                ", team1Id='" + team1Id + '\'' +
                ", team2Id='" + team2Id + '\'' +
                ", team1Name='" + team1Name + '\'' +
                ", team2Name='" + team2Name + '\'' +
                ", location='" + location + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", description='" + description + '\'' +
                ", team1Playing11=" + team1Playing11 +
                ", team2Playing11=" + team2Playing11 +
                ", refereeId='" + refereeId + '\'' +
                ", team1Formation='" + team1Formation + '\'' +
                ", team2Formation='" + team2Formation + '\'' +
                ", team1Score=" + team1Score +
                ", team2Score=" + team2Score +
                ", eventList=" + eventList +
                '}';
    }
}
