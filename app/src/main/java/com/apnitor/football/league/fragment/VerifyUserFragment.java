package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LoginActivity;
import com.apnitor.football.league.databinding.FragmentVerifyUserBinding;
import com.apnitor.football.league.fragment_binding_callback.VerifyUserFragmentBindingCallback;
import com.apnitor.football.league.viewmodel.ForgotPasswordViewModel;
import com.poovam.pinedittextfield.PinField;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class VerifyUserFragment extends BaseFragment implements VerifyUserFragmentBindingCallback {

    private FragmentVerifyUserBinding binding;
    private LoginActivity loginActivity;
    private ForgotPasswordViewModel forgotPasswordViewModel;
    private String enteredOTP = "", email = "", phone = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginActivity = (LoginActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email = getArguments().getString("email");
        phone = getArguments().getString("phone");

        forgotPasswordViewModel = getViewModel(ForgotPasswordViewModel.class);
        forgotPasswordViewModel.getVerifyUserResLiveData().observe(
                this,
                signUpRes -> {

                    Toast.makeText(loginActivity, "Your account is verified successfully.", Toast.LENGTH_SHORT).show();

                    Fragment fragment = new LoginFragment();
                    Bundle mBundle = new Bundle();
                    startNewFragment(fragment, mBundle);
                    navController.navigateUp();
                }
        );

     /*   forgotPasswordViewModel.getForgotPasswordLiveData().observe(
                this,
                signUpRes ->{
                    Toast.makeText(loginActivity, "OTP resent to "+email+phone, Toast.LENGTH_SHORT).show();
                });*/
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentVerifyUserBinding.inflate(inflater, container, false);
        binding.setCallback(this);

        if (!email.isEmpty()) {
            binding.tvVerifyTitle.setText(R.string.email_verify);
            binding.tvMessage.setText(R.string.email_message);
            binding.imgLogo.setImageResource(R.drawable.ic_email);
        } else {
            binding.imgLogo.setImageResource(R.drawable.ic_phone);
            binding.tvVerifyTitle.setText(R.string.phone_verify);
            binding.tvMessage.setText(R.string.phone_message);
        }

        binding.lineField.setOnTextCompleteListener(new PinField.OnTextCompleteListener() {
            @Override
            public boolean onTextComplete(@NotNull String enteredText) {
                enteredOTP = enteredText;
                if (enteredOTP.isEmpty() || enteredOTP.length() != 4) {
                    showErrorOnEditText(binding.lineField, "Please enter valid OTP");
                } else {
                    hitService();
                }
                return false; // Return true to keep the keyboard open else return false to close the keyboard
            }
        });
        return binding.getRoot();
    }

    private void hitService() {
        if (phone.isEmpty()) {
            forgotPasswordViewModel.verifyUserOnEmail(getPrefHelper().getUserId(), enteredOTP);
        } else {
            forgotPasswordViewModel.verifyUserOnPhone(getPrefHelper().getUserId(), enteredOTP);
        }

    }

    @Override
    public void onDoneClick() {


        if (enteredOTP.isEmpty() || enteredOTP.length() != 4)
            showErrorOnEditText(binding.lineField, "Please enter valid OTP.");
        else {
            hitService();
        }
    }

    @Override
    public void onResendCode() {
        showToast("This Functionality is not implemented yet.");
        //  forgotPasswordViewModel.forgotPassword(email,phone);
    }

    private void startNewFragment(Fragment fragment, Bundle mBundle) {
        fragment.setArguments(mBundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.my_nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }
}
