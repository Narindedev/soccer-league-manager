package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.NotificationAdapter;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.InviteTeamReq;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.databinding.ActivityNotificationBinding;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class NotificationActivity extends BaseActivity implements ListItemMultipleCallback {

    private ActivityNotificationBinding binding;
    private ArrayList<NotificationResponse> list;
    private TeamViewModel teamViewModel;
    private PlayerViewModel playerViewModel;
    private NotificationResponse mResponse;
    private LeagueViewModel leagueViewModel;
    private FollowingViewModel followingViewModel;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_notification);
        setupToolbar(binding.toolbar, "Notifications");
        teamViewModel = getViewModel(TeamViewModel.class);
        followingViewModel = getViewModel(FollowingViewModel.class);
        playerViewModel = getViewModel(PlayerViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        Intent mIntent = getIntent();
        list = new ArrayList<>();
        if (mIntent != null) {
            NotificationResponse response = (NotificationResponse) mIntent.getSerializableExtra("data");
            if (response != null) {
                if (!list.isEmpty()) {
                    list.clear();
                }
                list.add(response);
                NotificationAdapter mAdapter = new NotificationAdapter(this, list, this);
                binding.rvNotifications.setLayoutManager(new LinearLayoutManager(this));
                binding.rvNotifications.setAdapter(mAdapter);


            } else {
                followingViewModel.getAllNotificationsResLiveData().observe(this, responses -> {
                    if (!list.isEmpty()) {
                        list.clear();
                    }
                    list = responses.getNotifications();
                    NotificationAdapter mAdapter = new NotificationAdapter(this, list, this);
                    binding.rvNotifications.setLayoutManager(new LinearLayoutManager(this));
                    binding.rvNotifications.setAdapter(mAdapter);
                });
                followingViewModel.getAllNotifications();
            }

        }

        teamViewModel.getUpdateTeamPlayersResLiveData().observe(this, res -> {
            showToast("Player Added Successfully.");
            finish();
        });

        playerViewModel.getPlayersLiveData().observe(this,res->{
            showToast("Team Request Rejected.");
            finish();
        });


        teamViewModel.getTeamsLiveData().observe(this,res->{
            showToast("League Request Rejected.");
            finish();
        });




        leagueViewModel.getUpdateLeagueTeamResLiveData().observe(this, res -> {
            showToast("Team Added Successfully.");

            GetTeamRes invitedTeam = new GetTeamRes();
            invitedTeam.setTeamId(mResponse.teamId);
            invitedTeam.setTeamName(mResponse.teamName);
            invitedTeam.setImageUrl(mResponse.teamImageUrl);
            LeagueTeam addLeagueTeam = new LeagueTeam();
            ArrayList<GetTeamRes> addedTeams = new ArrayList<GetTeamRes>();
            addedTeams.add(invitedTeam);
            addLeagueTeam.setTeamIds(addedTeams);
            addLeagueTeam.setSeasonId(mResponse.seasonId);
            addLeagueTeam.setDivisionId(mResponse.divisionId);
            Intent intent = new Intent();
            intent.putExtra("AddedLeagueTeam", addLeagueTeam);
            setResult(ResultCodes.ADD_TEAM_RESULT_CODE, intent);

            finish();
        });
    }

    @Override
    public void onListItemClick(Object object) {
        NotificationResponse mResponse = (NotificationResponse) object;
        String type = mResponse.type;
        if (type.equals("inviteTeam")) {
            teamViewModel.rejectLeagueInvitation(new InviteTeamReq(mResponse.leagueId, mResponse.seasonId, mResponse.divisionId, mResponse.teamId, mResponse.notificationId));
        }
        if (mResponse.type.equals("invitePlayer")) {
            GetTeamPlayersReq getTeamPlayersReq = new GetTeamPlayersReq(mResponse.teamId, mResponse.playerId);
            getTeamPlayersReq.setNotificationId(mResponse.notificationId);
            playerViewModel.getRejectTeamInvite(getTeamPlayersReq);
        }
    }

    @Override
    public void onLeagueListItemClick(Object object) {
        try {


        mResponse = (NotificationResponse) object;
        String type = mResponse.type;
        if (type.equals("inviteTeam")) {
            List<String> strings = new ArrayList<>();
            strings.add(mResponse.teamId);
            UpdateLeagueTeamReq updateLeagueTeamReq = new UpdateLeagueTeamReq(mResponse.leagueId, mResponse.seasonId, mResponse.divisionId, strings);
            updateLeagueTeamReq.setNotificationId(mResponse.notificationId);
            leagueViewModel.updateLeagueTeams(updateLeagueTeamReq);
        }
        if (mResponse.type.equals("invitePlayer")) {
            List<String> strings = new ArrayList<>();
            strings.add(mResponse.playerId);
            UpdateTeamPlayersReq updateTeamPlayersReq = new UpdateTeamPlayersReq(mResponse.teamId, strings);
            updateTeamPlayersReq.setNotificationId(mResponse.notificationId);
            teamViewModel.updateTeamPlayers(updateTeamPlayersReq);
        }
        }catch (Exception e){
            e.printStackTrace();
        }
        //anil 5c7e6897761753001c24cb5b
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
