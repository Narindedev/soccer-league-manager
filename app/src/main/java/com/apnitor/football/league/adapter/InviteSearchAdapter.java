package com.apnitor.football.league.adapter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class InviteSearchAdapter extends RecyclerView.Adapter<InviteSearchAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamSearchRes> teamList;
    private ListItemClickCallback listItemClickCallback;

    public InviteSearchAdapter(Context context, ArrayList<GetTeamSearchRes> teamList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public InviteSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_invite_search_adapter, parent, false);

        return new InviteSearchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InviteSearchAdapter.ViewHolder holder, int position) {
        try {
            GetTeamSearchRes model = teamList.get(position);
            holder.tvInvite.setText("Invite Team");
            if (model.getState()!=null && !model.getState().isEmpty()) {
                holder.tvState.setText("State : " + model.getState());
            } else {
                holder.tvState.setVisibility(View.GONE);
            }
            if (model.getCity() != null && !model.getCity().isEmpty()) {
                holder.tvCity.setText("City : " + model.getCity());
            } else {
                holder.tvCity.setVisibility(View.GONE);
            }
            holder.tvTeamName.setText(model.getTeamName());
            // holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark));

            Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                    .centerCrop()).into(holder.img);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName, tvInvite, tvState, tvCity;
        RelativeLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            tvInvite = (TextView) itemView.findViewById(R.id.tvTeamInvite);
            linearLayout = itemView.findViewById(R.id.llRow);
            tvState = (TextView) itemView.findViewById(R.id.tvTeamState);
            tvCity = (TextView) itemView.findViewById(R.id.tvTeamCity);
            tvInvite.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    GetTeamSearchRes model = teamList.get(getAdapterPosition());
                    listItemClickCallback.onListItemClick(model);

                }
            });
        }
    }
}