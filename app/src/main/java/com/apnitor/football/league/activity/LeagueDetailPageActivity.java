package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueTeamsListAdapter;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivityLeagueDetailPageBinding;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class LeagueDetailPageActivity extends BaseActivity {

    private static final String EXTRA_LEAGUE_DETAIL = "league_detail";
    private ActivityLeagueDetailPageBinding binding;
    private GetLeagueRes leagueDetail;

    TeamViewModel teamViewModel;

    LeagueDetailPageActivity leagueDetailPageActivity = this;

    List<GetTeamRes> teamList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = bindContentView(R.layout.activity_league_detail_page);
        teamViewModel = getViewModel(TeamViewModel.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getSerializable(EXTRA_LEAGUE_DETAIL) != null) {

            leagueDetail = (GetLeagueRes) extras.getSerializable(EXTRA_LEAGUE_DETAIL);
            String leagueTitle = leagueDetail.getLeagueTitle();
            binding.leagueTitle.setText(leagueTitle == null ? "" : leagueTitle);
            String startDate = leagueDetail.getFoundationDate();
            String endDate = leagueDetail.getFoundationDate();
            if (startDate == null) startDate = "";
            if (endDate == null) endDate = "";
            binding.startDateEndDate.setText(startDate + " to " + endDate);
            binding.rvLeagueTeams.setLayoutManager(new LinearLayoutManager(leagueDetailPageActivity, RecyclerView.VERTICAL, false));
            teamViewModel.getLeagueTeamsLiveData().observe(this,
                    teams -> {
                        teamList = teams.getTeamIds();
                        binding.rvLeagueTeams.setAdapter(
                                new LeagueTeamsListAdapter(leagueDetailPageActivity, teamList, RecyclerView.VERTICAL, null)
                        );
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        teamViewModel.getLeagueTeams(new GetLeagueTeamsReq(leagueDetail.getLeagueId()));
    }

    public void onPhotoClick(View view) {
        showToast("Functionality is not implemented yet.");
    }

    public void onAddTeamClick(View view) {
        Intent i = new Intent(leagueDetailPageActivity, SelectLeagueTeamsActivity.class);
        Bundle b = new Bundle();
        b.putString("leagueId", leagueDetail.getLeagueId());
        b.putSerializable("alreadyAddedTeams", (Serializable) teamList);
        i.putExtras(b);
        startActivity(i);
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void leagueEdit(View view) {
        Intent intent = new Intent(leagueDetailPageActivity, CreateLeagueActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("league_data", leagueDetail);
        intent.putExtras(b);
        startActivity(intent);
    }
}
