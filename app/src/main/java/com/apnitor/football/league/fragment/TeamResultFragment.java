package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.SeasonResultsListAdapter;
import com.apnitor.football.league.adapter.TeamResultListAdapter;
import com.apnitor.football.league.adapter.TeamSchedulesListAdapter;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetEventRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamResultRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.ScheduleResultRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamResultFragment extends BaseFragment implements ListItemClickCallback {

    //TextView mTvSeason;
    // TextView tvSelectLeagueBtn;
    private TeamViewModel teamViewModel;
    static GetTeamDetailRes mGetTeamRes;
    List<GetTeamScheduleRes> mGetTeamScheduleRes = new ArrayList<>();
    TeamResultListAdapter mTeamResultListAdapter;


    public static TeamResultFragment newInstance(int i, String s, GetTeamDetailRes getTeamRes) {
        mGetTeamRes = getTeamRes;
        TeamResultFragment fragment = new TeamResultFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_team_results, container, false);
        teamViewModel = getViewModel(TeamViewModel.class);
        RecyclerView v = view.findViewById(R.id.rvResults);
        v.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mTeamResultListAdapter = new TeamResultListAdapter(getContext(), mGetTeamScheduleRes, this, mGetTeamRes.getTeamModel().getTeamId());
        v.setAdapter(mTeamResultListAdapter);
        observeApiResponse();
        setRetainInstance(true);
        return view;
    }

    private void observeApiResponse() {
        teamViewModel.getGetTeamResultLiveData().observe(this, getTeamScheduleRes -> {
            TeamProfileActivity.isResultFistTime = false;
            if (getTeamScheduleRes != null) {
                List<GetTeamScheduleRes> resList = getTeamScheduleRes.getPast20schedules();
                if(resList!=null && resList.size()>0) {
                    filterTeamScheduleData(resList);
                }
            }
                }
        );
        if (mGetTeamRes != null)
            if(TeamProfileActivity.isResultFistTime) {
                teamViewModel.getTeamResult(new GetTeamDetailReq(mGetTeamRes.getTeamModel().getTeamId()));
            }
    }

    @Override
    public void onResume() {
        super.onResume();

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onListItemClick(Object object) {
    }

    void filterTeamScheduleData(List<GetTeamScheduleRes> getTeamScheduleRes) {
        mGetTeamScheduleRes.clear();
        int size = getTeamScheduleRes.size();
        for (int i = 0; i < size; i++) {
            if (mGetTeamRes.getTeamModel().getTeamId().equalsIgnoreCase(getTeamScheduleRes.get(i).getTeam1Id())||mGetTeamRes.getTeamModel().getTeamId().equalsIgnoreCase(getTeamScheduleRes.get(i).getTeam2Id())) {
                mGetTeamScheduleRes.add(getTeamScheduleRes.get(i));
            }
        }
        //
        List<GetTeamScheduleRes> getTeamScheduleRes1=mGetTeamScheduleRes;
        mGetTeamScheduleRes=getTeamsScore(getTeamScheduleRes1);

        mTeamResultListAdapter.notifyDataSetChanged();
    }
    private List<GetTeamScheduleRes> getTeamsScore(List<GetTeamScheduleRes> leagueScheduleList){
        if(leagueScheduleList.size()>0) {
            for (int i=0;i<leagueScheduleList.size();i++) {
                int team1Score=0;
                int team2Score=0;
                List<GetEventRes> mEventRes = leagueScheduleList.get(i).eventList;
                if (mEventRes!=null && mEventRes.size()>0){

                    for(int m=0;m<mEventRes.size();m++) {
                        GetEventRes eventRes = mEventRes.get(m);
                        if(eventRes.getEventType().equals("Goal")){
                            if(eventRes.getScoredByTeam().equals(leagueScheduleList.get(i).getTeam1Id())){
                                team1Score=team1Score+1;
                            }else{
                                team2Score=team2Score+1;
                            }
                        }

                    }
                    leagueScheduleList.get(i).setTeam1Score(team1Score);
                    leagueScheduleList.get(i).setTeam2Score(team2Score);
                }else{
                    leagueScheduleList.get(i).setTeam1Score(0);
                    leagueScheduleList.get(i).setTeam2Score(0);
                }
            }
        }
        return leagueScheduleList;
    }

}


