package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateMatchEventRes {
    @Expose
    @SerializedName("matchEventId")
    public String matchEventId;
}
