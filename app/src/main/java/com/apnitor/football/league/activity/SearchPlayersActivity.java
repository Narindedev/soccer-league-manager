package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.InviteSearchPlayerAdapter;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivitySearchPlayerBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import java.io.Serializable;

import androidx.recyclerview.widget.LinearLayoutManager;

public class SearchPlayersActivity extends BaseActivity implements ListItemClickCallback {
    private final String TEAM_ID = "teamId";
    String mPlayerId;
    PlayerRes mPlayerRes;
    private ActivitySearchPlayerBinding binding;
    private FollowingViewModel followingViewModel;
    private PlayerViewModel playerViewModel;
    private String teamId, teamName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_player);
        binding = bindContentView(R.layout.activity_search_player);
        setupToolbar(binding.toolbar, "Search Player");
        followingViewModel = getViewModel(FollowingViewModel.class);
        playerViewModel = getViewModel(PlayerViewModel.class);
        setupToolbar(binding.toolbar, "Invite Players");

        Intent i = getIntent();
        if (i.getExtras() != null) {
            teamId = i.getExtras().getString(TEAM_ID);
            teamName = i.getExtras().getString("teamName");
        }
        playerViewModel.getPlayersLiveData().observe(this, res -> {
            Intent mIntent = new Intent();
            mIntent.putExtra("inviteTeamPlayer", (Serializable) mPlayerRes);
            setResult(ResultCodes.ADD_Player_RESULT_CODE, mIntent);
            finish();
        });
        playerViewModel.getAllPlayersLiveData().observe(this, res -> {
            if (res.size() == 0) {
                binding.llSendInvitation.setVisibility(View.VISIBLE);
                binding.rvSearchList.setVisibility(View.GONE);
            } else {
                binding.rvSearchList.setVisibility(View.VISIBLE);
                binding.llSendInvitation.setVisibility(View.GONE);
            }
            binding.rvSearchList.setLayoutManager(new LinearLayoutManager(this));
            binding.rvSearchList.setAdapter(new InviteSearchPlayerAdapter(this, res, this));
        });

        binding.tvInvitePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });
        binding.tvSendInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });


    }


    private void sendInvitation() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Football League");
            String shareMessage = "\nI am recommend you to download this app and join team '" + teamName + "'.\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.apnitor.football.league\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public void onSearchClick(View view) {
        String teamName = binding.playerNameEt.getText().toString();
        String teamCity = binding.cityEt.getText().toString();
        String teamZipCode = binding.zipCodeEt.getText().toString();
        if (teamName.isEmpty()) {
            showToast("Please enter player name.");
        } else {
            hideKeyboard(this);
            playerViewModel.getAllPlayers(new GetAllTeamReq(teamName, teamCity, teamZipCode));
        }

    }

    @Override
    public void onListItemClick(Object object) {
        mPlayerRes = (PlayerRes) object;
        playerViewModel.getInviteTeam(new GetTeamPlayersReq(teamId, mPlayerRes.getId()));

    }
}
