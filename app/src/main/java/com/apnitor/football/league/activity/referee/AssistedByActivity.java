package com.apnitor.football.league.activity.referee;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivityAssistedByBinding;
import com.apnitor.football.league.databinding.ActivityGoalDetailBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class AssistedByActivity extends BaseActivity implements ListItemClickCallback {
    private ActivityAssistedByBinding binding;
    private List<TeamPlaying11> mTeamPlayersRes;
    private PlayerViewModel playerViewModel;
    private LeagueViewModel leagueViewModel;
    private RefereeViewModel refereeViewModel;
    private String playerId = "";
    private GoalDetailAdapter mTeam1Adapter;
    private GoalDetailAdapter mTeam2Adapter;
    private List<TeamPlaying11> team1List, team2List;
    //    private List<AddScheduleLeagueReq> leagueScheduleList;
    private String mSeasonId, mDivisionId, mLeagueId, scheduleId;
    private GetTeamScheduleRes teamScheduleRes;
    private String team1Id = "", team2Id = "", mConcededById = "", mScoredByTeamId = "";
    private String titleType, eventTime, eventId;
    UpdateMatchEventReq mUpdateMatchEventReq;
    String mTeamId, matchId;
    String mGoalScoredPlayerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playerViewModel = getViewModel(PlayerViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        refereeViewModel = getViewModel(RefereeViewModel.class);
        binding = bindContentView(R.layout.activity_assisted_by);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            titleType = b.getString("title");
            eventTime = b.getString("eventTime");
            matchId = b.getString("matchId");
            eventId = b.getString("matchEventId");
            mTeamId = b.getString("team_1_id");
            mGoalScoredPlayerId = b.getString("playerId");
            teamScheduleRes = (GetTeamScheduleRes) b.getSerializable("match_detail");
            mUpdateMatchEventReq = (UpdateMatchEventReq) b.getSerializable("event_detail");

            if (mTeamId.equalsIgnoreCase(mUpdateMatchEventReq.scoredByTeam)) {
                binding.tvTeam2.setEnabled(false);
            } else {
                binding.tvTeam1.setEnabled(false);
            }
            if (teamScheduleRes != null) {
                mSeasonId = teamScheduleRes.getSeasonId();
                mDivisionId = teamScheduleRes.getDivisionId();
                mLeagueId = teamScheduleRes.getLeagueId();
                scheduleId = teamScheduleRes.getScheduleId();
                binding.tvTeam1.setText(teamScheduleRes.getTeam1Name());
                binding.tvTeam2.setText(teamScheduleRes.getTeam2Name());
            }
        }

        setupToolbar(binding.toolbar, "Assisted By");

        mTeamPlayersRes = new ArrayList<>();
        team1List = new ArrayList<>();
        team2List = new ArrayList<>();
        observeApiResponse();
    }

    private void observeApiResponse() {

        team1List.clear();
        team2List.clear();
        //
        int team1size = teamScheduleRes.getTeam1Playing11().size();
        for (int i = 0; i < team1size; i++) {
            if (teamScheduleRes.getTeam1Playing11().get(i).getPlaying())
                team1List.add(teamScheduleRes.getTeam1Playing11().get(i));
        }
        //
        int team2size = teamScheduleRes.getTeam2Playing11().size();
        for (int i = 0; i < team2size; i++) {
            if (teamScheduleRes.getTeam2Playing11().get(i).getPlaying())
                team2List.add(teamScheduleRes.getTeam2Playing11().get(i));
        }
        //
        if (mTeamId.equalsIgnoreCase(mUpdateMatchEventReq.scoredByTeam)) {
            // TEAM 1

            for (int i = 0; i < team1List.size(); i++) {
                if (team1List.get(i).getPlayerId().equalsIgnoreCase(mGoalScoredPlayerId)) {
                    team1List.remove(i);
                    break;
                }
            }
            mTeamPlayersRes.clear();
            mTeamPlayersRes.addAll(team1List);
            mTeam1Adapter = new GoalDetailAdapter(this, mTeamPlayersRes, this);
            binding.rvSelectPlayer.setLayoutManager(new LinearLayoutManager(this));
            binding.rvSelectPlayer.setAdapter(mTeam1Adapter);
            mTeam1Adapter.notifyDataSetChanged();
        } else {
            // TEAM 2
            mTeamPlayersRes.clear();
            mTeamPlayersRes.addAll(team2List);
            for (int i = 0; i < mTeamPlayersRes.size(); i++) {
                if (mTeamPlayersRes.get(i).getPlayerId().equalsIgnoreCase(mGoalScoredPlayerId)) {
                    mTeamPlayersRes.remove(i);
                    break;
                }
            }
            mTeam2Adapter = new GoalDetailAdapter(this, mTeamPlayersRes, this);
            binding.rvSelectPlayer.setLayoutManager(new LinearLayoutManager(this));
            binding.rvSelectPlayer.setAdapter(mTeam2Adapter);
        }
        //

        refereeViewModel.updateMatchEventsLiveData().observe(this, res -> {
//            Intent mIntent = new Intent();
//            eventId = res.matchEventId;
//            mIntent.putExtra("eventId", eventId);
//            setResult(101, mIntent);
            onBackPressed();
        });

        onClick();
    }

    private void onClick() {
        binding.tvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.tvTeam1.setTextColor(getResources().getColor(R.color.dark_black));
                    binding.tvTeam2.setTextColor(getResources().getColor(R.color.colorAccent));
                    binding.tvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                    binding.tvTeam2.setBackgroundResource(R.drawable.shape_right_round);
                    mTeamPlayersRes.clear();
                    mTeamPlayersRes.addAll(team1List);
                    team1Id = teamScheduleRes.getTeam1Id();
                    //
                    mConcededById = team2Id;
                    mScoredByTeamId = team1Id;
                    //
                    mTeam1Adapter.notifyDataSetChanged();
                    binding.rvSelectPlayer.setAdapter(mTeam1Adapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //
        binding.tvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.tvTeam1.setTextColor(getResources().getColor(R.color.colorAccent));
                    binding.tvTeam2.setTextColor(getResources().getColor(R.color.dark_black));
                    //
                    binding.tvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                    binding.tvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
                    //
                    mTeamPlayersRes.clear();
                    mTeamPlayersRes.addAll(team2List);
                    mTeam2Adapter.notifyDataSetChanged();
                    binding.rvSelectPlayer.setAdapter(mTeam2Adapter);
                    //
                    team2Id = teamScheduleRes.getTeam2Id();
                    //
                    mConcededById = team1Id;
                    mScoredByTeamId = team2Id;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        binding.tvTeam1.performClick();
    }

    @Override
    public void onListItemClick(Object object) {
        TeamPlaying11 playingList = (TeamPlaying11) object;
        playerId = playingList.getPlayerId();
    }

    public void onDoneClick(View view) {
        if (playerId.isEmpty()) {
            Toast.makeText(this, "Please select a player.", Toast.LENGTH_SHORT).show();
            return;
        }
        mUpdateMatchEventReq.matchEventId = matchId;
        mUpdateMatchEventReq.goalAssistedBy = playerId;
        refereeViewModel.updateMatchEvents(mUpdateMatchEventReq);
    }

    public void onCancelClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
