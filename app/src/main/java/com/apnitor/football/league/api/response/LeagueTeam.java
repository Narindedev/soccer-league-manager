package com.apnitor.football.league.api.response;

import java.io.Serializable;
import java.util.List;

import androidx.annotation.Nullable;

public class LeagueTeam implements Serializable {

    public LeagueTeam() {
    }

   private String _id;
   private String seasonId;
   private String divisionId;
   private List<GetTeamRes> invitedTeams;
  // private  List<GetTeamRes> teamRequests;
  private  List<GetTeamRes> teamIds;
  // private List<GetTeamRes> rejectedInvitedTeams;
  // private List<GetTeamRes> rejectedTeamRequests;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public List<GetTeamRes> getInvitedTeams() {
        return invitedTeams;
    }

    public void setInvitedTeams(List<GetTeamRes> invitedTeams) {
        this.invitedTeams = invitedTeams;
    }
/*
    public List<GetTeamRes> getTeamRequests() {
        return teamRequests;
    }

    public void setTeamRequests(List<GetTeamRes> teamRequests) {
        this.teamRequests = teamRequests;
    }*/

    public List<GetTeamRes> getTeamIds() {
        return teamIds;
    }

    public void setTeamIds(List<GetTeamRes> teamIds) {
        this.teamIds = teamIds;
    }
  /*  public List<GetTeamRes> getRejectedInvitedTeams() {
        return rejectedInvitedTeams;
    }

    public void setRejectedInvitedTeams(List<GetTeamRes> rejectedInvitedTeams) {
        this.rejectedInvitedTeams = rejectedInvitedTeams;
    }

    public List<GetTeamRes> getRejectedTeamRequests() {
        return rejectedTeamRequests;
    }

    public void setRejectedTeamRequests(List<GetTeamRes> rejectedTeamRequests) {
        this.rejectedTeamRequests = rejectedTeamRequests;
    }*/


    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final LeagueTeam other = (LeagueTeam) obj;
        if (seasonId == null) {
            if (other.seasonId != null)
                return false;
        } else if (!seasonId.equals(other.seasonId)){
            return false;
        }

        if(divisionId==null){
            if(other.divisionId!=null)
                return false;
        }else if(!divisionId.equals(other.divisionId)){
            return false;
        }

        if(seasonId.equals(other.seasonId) && divisionId.equals(other.divisionId))
            return true;

        return false;
    }

    @Override
    public int hashCode() {

        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((seasonId == null) ? 0 : seasonId.hashCode()) + ((divisionId == null) ? 0 : divisionId.hashCode());
        return result;

    }



}
