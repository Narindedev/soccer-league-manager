package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.fragment.LineUpFragment;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.LoginViewModel;
import com.bumptech.glide.Glide;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.Toolbar;

public class SwipePlayerActivity extends BaseActivity {

    private ActivityTeamListBinding binding;
    private SwipeFlingAdapterView flingContainer;
    public static ViewHolder viewHolder;
    Toolbar mToolbar;
    public static MyAppAdapter myAppAdapter;
    ArrayList<TeamPlaying11> mPlaying11List = new ArrayList<>();
    ArrayList<TeamPlaying11> mSwipeableList = new ArrayList<>();
    GetTeamScheduleRes mGetMatchDetail;
    String MATCH_DETAIL = "match_detail";
    String selectedTeam;
    TextView mTvNoMore;
    AddScheduleViewModel addScheduleViewModel;
    int swipeCount = -1;
    private LoginViewModel loginViewModel;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_swipe_player);
        //
        loginViewModel = getViewModel(LoginViewModel.class);
        loginViewModel.getLogInResLiveData();
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
                selectedTeam = intent.getStringExtra("TEAM");
            }
        }
        mPlaying11List.clear();
        if (selectedTeam.equalsIgnoreCase("TEAM1"))
            mPlaying11List.addAll(mGetMatchDetail.getTeam1Playing11());
        if (selectedTeam.equalsIgnoreCase("TEAM2"))
            mPlaying11List.addAll(mGetMatchDetail.getTeam2Playing11());

        mSwipeableList.clear();
        mSwipeableList.addAll(mPlaying11List);
        //
        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTvNoMore = (TextView) findViewById(R.id.tv_no_more);
        setupToolbar(mToolbar, "Select Players");
        //
        myAppAdapter = new MyAppAdapter(mSwipeableList, SwipePlayerActivity.this);
        flingContainer.setAdapter(myAppAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                swipeCount++;
                mSwipeableList.remove(0);
                mPlaying11List.get(swipeCount).setStatus("Reject");
                myAppAdapter.notifyDataSetChanged();
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                if (mSwipeableList.size() < 1) {
                    mTvNoMore.setVisibility(View.VISIBLE);
                    setPlayersList();
                }
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                swipeCount++;
                mPlaying11List.get(swipeCount).setStatus("Accept");
                mSwipeableList.remove(0);
                myAppAdapter.notifyDataSetChanged();
                if (mSwipeableList.size() < 1) {
                    mTvNoMore.setVisibility(View.VISIBLE);
                    setPlayersList();
                }
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    public class MyAppAdapter extends BaseAdapter {


        public List<TeamPlaying11> playing11List;
        public Context context;

        private MyAppAdapter(List<TeamPlaying11> apps, Context context) {
            this.playing11List = apps;
            this.context = context;
        }

        @Override
        public int getCount() {
            return playing11List.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.list_item_swipe_player, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.mTvJerseyNo = (TextView) rowView.findViewById(R.id.playerbio);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.DataText.setText(playing11List.get(position).getFirstName() + " " + playing11List.get(position).getLastName());
            if (playing11List.get(position).getJerseyNumber() != null)
                viewHolder.mTvJerseyNo.setText("Jersey No. "+playing11List.get(position).getJerseyNumber()+"");
            if (playing11List.get(position).getImageUrl() != null)
                Glide.with(SwipePlayerActivity.this).load(playing11List.get(position).getImageUrl()).into(viewHolder.cardImage);

            return rowView;
        }
    }

    public static class ViewHolder {
        public static FrameLayout background;
        public TextView DataText;
        public TextView mTvJerseyNo;
        public ImageView cardImage;
    }

    void setPlayersList() {
        if (mPlaying11List.size() <= 0)
            return;
        String teamFormation = "4-4-2";
        AddPlayerToTeamReq addPlayerToTeamReq = new AddPlayerToTeamReq();
        addPlayerToTeamReq.setLeagueId(mGetMatchDetail.getLeagueId());
        addPlayerToTeamReq.setDivisionId(mGetMatchDetail.getDivisionId());
        addPlayerToTeamReq.setSeasonId(mGetMatchDetail.getSeasonId());
        addPlayerToTeamReq.setScheduleId(mGetMatchDetail.getScheduleId());
        if (selectedTeam.equalsIgnoreCase("TEAM1")) {
            addPlayerToTeamReq.setTeam1Formation(teamFormation);
            addPlayerToTeamReq.setTeam1Playing11(mPlaying11List);
        } else {
            addPlayerToTeamReq.setTeam2Formation(teamFormation);
            addPlayerToTeamReq.setTeam2Playing11(mPlaying11List);
        }
        addScheduleViewModel.updateScheduleTeam(addPlayerToTeamReq);

        addScheduleViewModel.updateScheduleLiveData().observe(this, addScheduleRes -> {
            if (selectedTeam.equalsIgnoreCase("TEAM1")) {
                LineUpFragment.mGetMatchDetail.setTeam1Playing11(mPlaying11List);
                RefereeScheduleActivity.updateObjectTeam1(mPlaying11List);
            } else {
                LineUpFragment.mGetMatchDetail.setTeam2Playing11(mPlaying11List);
                RefereeScheduleActivity.updateObjectTeam2(mPlaying11List);
            }
            finish();
        });
    }


}
