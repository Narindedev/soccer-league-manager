package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.socailmedia.SocialMediaActivity;
import com.apnitor.football.league.adapter.TeamProfileViewPagerAdapter;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivityTeamProfileBinding;
import com.apnitor.football.league.fragment.TeamProfileRosterFragment;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.util.RequestCodes;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.viewmodel.TeamViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class TeamProfileActivity extends BaseActivity {

    public static GetTeamRes getTeamRes;
    public static FloatingActionButton fab1;
    public static GetTeamDetailRes getTeamDetailRes;
    public static Boolean isResultFistTime;
    private static String checkPlayerIsContainned = "";
    private static TeamProfileViewPagerAdapter mAdapter;
    private final String TEAM_ID = "team_id";
    private final String TEAM_NAME = "team_name";
    private final String TEAM_Detail = "team_detail";
public static Boolean isTeamOwner;
    private String invitationType;

    LinearLayout llAddPlayers;
    View fabBGLayout;
    ArrayList<GetTeamPlayersRes> teamList = new ArrayList<>();
    private TeamViewModel teamViewModel;
    private String teamId;
    private ActivityTeamProfileBinding binding;
    private boolean isFABOpen;
    private String imageUrl;
    private CustomPlayerBroadcastReceiver customPlayerBroadcastReceiver = new CustomPlayerBroadcastReceiver();

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = bindContentView(R.layout.activity_team_profile);

        teamViewModel = getViewModel(TeamViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            //getTeamRes=(GetTeamRes)i.getExtras().getSerializable(TEAM_Detail);
            teamId = i.getExtras().getString(TEAM_ID);
            String teamName = i.getExtras().getString(TEAM_NAME);
            invitationType = i.getExtras().getString("type");
            binding.tvName.setText(teamName);
        }
        if (teamId != null) {
            setLeagueDetails();
        }
        // TODO: Remove this if api success built
        // TODO: Remove this if api success built
        // TODO: Remove this if api success built
        // TODO: Remove this if api success built
        setUpViewPager();

        binding.ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        llAddPlayers = binding.llAddPlayers;
        //

        //
        fab1 = binding.fabAddPlayers;

        fabBGLayout = binding.fabBGLayout;


        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Bundle bundle = new Bundle();
//                bundle.putString("teamId", teamId);
//                startActivity(PlayerListActivity.class, bundle);
                //
                Intent i = new Intent(TeamProfileActivity.this, SearchPlayersActivity.class);
                Bundle b = new Bundle();
                b.putString("teamId", teamId);
                b.putString("teamName", getTeamRes.getTeamName());
                teamList.clear();
                teamList.addAll(TeamProfileRosterFragment.teamListRoster);
                b.putSerializable("alreadyAddedPlayers", teamList);
                i.putExtras(b);
                startActivityForResult(i, RequestCodes.ADD_Player_REQUEST_CODE);
                // closeFABMenu();

            }
        });

        binding.ivEditImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(TeamProfileActivity.this, CreateTeamActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("screenType", "edit");
                bundle.putSerializable("team_data", getTeamRes);
                mIntent.putExtras(bundle);
                startActivityForResult(mIntent, RequestCodes.EDIT_TEAM_CODE);
                //
                overridePendingTransition(R.anim.anim_left_in,R.anim.anim_left_out);
            }
        });

        binding.ivSocialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(TeamProfileActivity.this, SocialMediaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("socialMedia", getTeamRes.socialMedia);
                mIntent.putExtras(bundle);
                startActivity(mIntent);
                //
                overridePendingTransition(R.anim.anim_left_in,R.anim.anim_left_out);
            }
        });
    }

    private void setLeagueDetails() {
        teamViewModel.getTeamsDetailLiveData().observe(this,
                league -> {
                    getTeamDetailRes = league;
                    getTeamRes = league.getTeamModel();
                    //
                    if(getTeamRes.getOwner().equalsIgnoreCase(PreferenceHandler.readString(this,PreferenceHandler.PREF_USER_ID,""))){
                        isTeamOwner=true;
                    }else{
                        isTeamOwner=false;
                    }
                    //

                    isResultFistTime = true;
                    if (imageUrl == null) {
                        Glide.with(this).load(getTeamRes.getImageUrl()).apply(new RequestOptions()
                                .centerCrop().placeholder(R.drawable.ic_team)).into(binding.ivProfileImage);
                    }
                    binding.setLeague(getTeamRes);
                    ViewPager viewPager = binding.profileViewPager;
                    binding.tabProfile.setupWithViewPager(viewPager);
                    mAdapter = new TeamProfileViewPagerAdapter(getSupportFragmentManager(), getTeamDetailRes);
                    viewPager.setAdapter(mAdapter);
                    viewPager.setOffscreenPageLimit(3);
                    if (invitationType != null && invitationType.equals("acceptTeamInvite")) {
                        viewPager.setCurrentItem(2);
                    }

                    if (getPrefHelper().getUserId().equals(getTeamRes.getOwner())) {
                        binding.ivEditImg.setVisibility(View.VISIBLE);
                    } else {
                        binding.ivEditImg.setVisibility(View.GONE);
                    }
                    // viewPager.beginFakeDrag();
                }
        );
        teamViewModel.getTeamDetail(new GetTeamDetailReq(teamId));
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ResultCodes.ADD_Player_RESULT_CODE) {
            /**
             * 1. Search object in mGetLeagueRes.leagueTeamList on the basis of seasonId and divisionId
             * 2. Add Invitation team object
             */


            PlayerRes invitedTeamPlayer = (PlayerRes) data.getSerializableExtra("inviteTeamPlayer");


            /**
             * TODO
             *
             * TEST IT
             */
            //  getTeamRes = getTeamDetailRes.getTeamModel();
            if (getTeamRes.getInvitedPlayerList() == null) {
                List<PlayerRes> invitedPlayerList = new ArrayList<PlayerRes>();
                invitedPlayerList.add(invitedTeamPlayer);
                getTeamRes.setInvitedPlayerList(new ArrayList<PlayerRes>());
                getTeamRes.getInvitedPlayerList().add(invitedTeamPlayer);
            } else {
                getTeamRes.getInvitedPlayerList().add(invitedTeamPlayer);
            }
            getTeamDetailRes.setTeamModel(getTeamRes);
            /**
             * Update Adapter with this new value
             */
            mAdapter.updateFragments();

        }else if (resultCode == ResultCodes.EDIT_TEAM_RESULT_CODE) {
            GetTeamRes mTeamRes=(GetTeamRes)data.getSerializableExtra("team_detail");
            binding.tvName.setText(mTeamRes.getTeamName());
            getTeamRes.setTeamName(mTeamRes.getTeamName());
            getTeamRes.setDescription(mTeamRes.getDescription());
            getTeamRes.setFoundationDate(mTeamRes.getFoundationDate());
            getTeamRes.setManagerName(mTeamRes.getManagerName());
            getTeamRes.setManagerEmail(mTeamRes.getManagerEmail());
            getTeamRes.setManagerPhone(mTeamRes.getManagerPhone());
            getTeamRes.setCoachName(mTeamRes.getCoachName());
            getTeamRes.setHomeUniform(mTeamRes.getHomeUniform());
            getTeamRes.setAwayUniform(mTeamRes.getAwayUniform());
            getTeamRes.setState(mTeamRes.getState());
            getTeamRes.setCity(mTeamRes.getCity());
            getTeamRes.setZipcode(mTeamRes.getZipcode());
            getTeamRes.setStadium(mTeamRes.getStadium());
            getTeamRes.socialMedia=mTeamRes.socialMedia;
            getTeamRes.setImageUrl(mTeamRes.getImageUrl());
            getTeamDetailRes.setTeamModel(getTeamRes);
            imageUrl=getTeamRes.getImageUrl();
            if(imageUrl!=null){
                Glide.with(this).load(imageUrl).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_team)).into(binding.ivProfileImage);
            }
            mAdapter.updateFragments();

        }
    }

    private void setUpViewPager() {
        binding.profileViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onPageSelected(int position) {
                //closeFABMenu();
                switch (position) {
                    case 0:
                        fab1.setVisibility(View.GONE);
                        break;
                    case 1:
                        fab1.setVisibility(View.GONE);
                        break;
                    case 2:
                        if(getPrefHelper().getUserId().equals(getTeamRes.getOwner())) {
                            fab1.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();

        // Add network connectivity change action.
        intentFilter.addAction(RequestCodes.ADD_PLAYER_TO_TEAM);

        // Set broadcast receiver priority.
        intentFilter.setPriority(8);
        registerReceiver(customPlayerBroadcastReceiver, intentFilter);
    }



    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(customPlayerBroadcastReceiver);
    }


    public static class CustomPlayerBroadcastReceiver extends BroadcastReceiver {
        public CustomPlayerBroadcastReceiver(){

        }
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (RequestCodes.ADD_PLAYER_TO_TEAM.equals(action)) {
                try {

                    if (intent != null) {
                        NotificationResponse response = (NotificationResponse) intent.getSerializableExtra("data");
                        if (response != null) {
                            PlayerRes mPlayerRes = new PlayerRes();
                            mPlayerRes.setId(response.playerId);
                            mPlayerRes.setFirstName(response.firstName);
                            mPlayerRes.setLastName(response.lastName);
                            mPlayerRes.setProfilePhotoUrl(response.playerImageUrl);

                            List<PlayerRes> mPlayerResList = getTeamRes.getAcceptedPlayerList();
                            if (mPlayerResList != null) {
                                if (!checkPlayerIsContainned.equals(mPlayerRes.getId())) {
                                    // mTeamResList.add(mTeamRes);
                                    Log.e("mTeamResList Size", "" + mPlayerResList);
                                    getTeamRes.getAcceptedPlayerList().add(mPlayerRes);
                                    List<PlayerRes> mInviitedPlayerResList = getTeamRes.getInvitedPlayerList();
                                    if (mInviitedPlayerResList.size() > 0) {
                                        for (int i = 0; i < mInviitedPlayerResList.size(); i++) {
                                            if (mPlayerRes.getId().equals(mInviitedPlayerResList.get(i).getId()))
                                                getTeamRes.getInvitedPlayerList().remove(i);
                                        }
                                    }
                                }
                            } else {
                                getTeamRes.setAcceptedPlayerList(new ArrayList<PlayerRes>());


                                Log.e("mTeamResList Size", "" + mPlayerResList);
                                getTeamRes.getAcceptedPlayerList().add(mPlayerRes);
                                List<PlayerRes> mInviitedList = getTeamRes.getInvitedPlayerList();
                                if (mInviitedList.size() > 0) {
                                    for (int i = 0; i < mInviitedList.size(); i++) {
                                        if (mPlayerRes.getId().equals(mInviitedList.get(i).getId()))
                                            getTeamRes.getInvitedPlayerList().remove(i);
                                    }
                                }
                            }
                            getTeamDetailRes.setTeamModel(getTeamRes);
                            mAdapter.updateFragments();
                            checkPlayerIsContainned = mPlayerRes.getId();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
