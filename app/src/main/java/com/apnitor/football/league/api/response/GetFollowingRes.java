package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetFollowingRes {


    @Expose
    @SerializedName("teamList")
    public ArrayList<FollowingList> teamList;

    @Expose
    @SerializedName("playerList")
    public ArrayList<FollowingList> playerList;

    @Expose
    @SerializedName("leagueList")
    public ArrayList<FollowingList> leagueList;

   public class FollowingList{
    @Expose
    @SerializedName("id")
    public String id;

       @Expose
       @SerializedName("_id")
       public String id2;

    @Expose
    @SerializedName("name")
    public String name;

       @Expose
       @SerializedName("firstName")
       public String firstName;


       @Expose
       @SerializedName("lastName")
       public String lastName;


       @Expose
    @SerializedName("imageUrl")
    public String imageUrl;
    }
}
