package com.apnitor.football.league.api.request;

public class GetLeagueTeamsReq {


    private String leagueId;
    String seasonId;
    String divisionId;

    public GetLeagueTeamsReq(String leagueId) {
        this.leagueId = leagueId;
    }

    public GetLeagueTeamsReq(String leagueId, String seasonId, String divisionId) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
