package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.fragment.TeamInfoDescFragment;
import com.apnitor.football.league.fragment.TeamShareNewsFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TeamInfoPagerAdapater extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 2;
    GetTeamRes getLeagueRes;
    private Observable mObservers = new FragmentObserver();

    public TeamInfoPagerAdapater(FragmentManager fm) {
        super(fm);
    }


    public TeamInfoPagerAdapater(FragmentManager fm, GetTeamRes getLeagueRes) {
        super(fm);
        this.getLeagueRes = getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment

                TeamInfoDescFragment teamInfoDescFragment = TeamInfoDescFragment.newInstance(getLeagueRes);

                if (teamInfoDescFragment instanceof Observer) {
                    mObservers.addObserver((Observer) teamInfoDescFragment);
                }

                return teamInfoDescFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title


                return TeamShareNewsFragment.newInstance(0, "Page # 1");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public void updateFragments() {
        mObservers.notifyObservers();
    }
}
