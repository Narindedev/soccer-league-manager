package com.apnitor.football.league.api.response;

import java.util.List;

public class AddLeagueRosterRes {
    private List<PlayerRes> players;

    public List<PlayerRes> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerRes> players) {
        this.players = players;
    }
}
