package com.apnitor.football.league.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.FragmentLeagueInfoBinding;
import com.apnitor.football.league.databinding.FragmentTeamInfoDescBinding;
import com.apnitor.football.league.util.CustomViewPager;
import com.apnitor.football.league.viewmodel.DivisionViewModel;

import java.util.Observable;
import java.util.Observer;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.LinearLayoutManager;

public class TeamInfoDescFragment extends BaseFragment implements Observer {

    private static GetTeamRes mGetTeamRes;
    private FragmentTeamInfoDescBinding binding;
    CustomViewPager viewPager;
    private final static String LEAGUE_INFO = "league_info";

    public static TeamInfoDescFragment newInstance(GetTeamRes getLeagueRes) {
        mGetTeamRes=getLeagueRes;
        TeamInfoDescFragment fragment = new TeamInfoDescFragment();
        Bundle args = new Bundle();
        args.putSerializable(LEAGUE_INFO, getLeagueRes);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTeamInfoDescBinding.inflate(inflater, container, false);
        if (mGetTeamRes != null) {
            setDataInViewObjects();
            binding.setTeamRes(mGetTeamRes);
        }
        observeApiResponse();
        socialClickListener();
        return binding.getRoot();
    }

    private void socialClickListener() {
    binding.ivFbLinkBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url=mGetTeamRes.socialMedia.facebookLink;
            if (url!=null&& !url.isEmpty()){
                openSocial(url);
            }else{
                showToast("No link found");
            }
        }
    });
        binding.ivTwitterLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mGetTeamRes.socialMedia.twitterLink;
                if (url!=null&& !url.isEmpty()){
                    openSocial(url);
                }else{
                    showToast("No link found");
                }
            }
        });

        binding.ivInstaLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url=mGetTeamRes.socialMedia.instaLink;
                if (url!=null&& !url.isEmpty()){
                    openSocial(url);
                }else{
                    showToast("No link found");
                }
            }
        });
    }
    private void openSocial(String url){
        if (!url.toLowerCase().contains("http://")){
            if(!url.toLowerCase().contains("www.")){
                url="www."+url;
            }
            url="http://"+url;
        }
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(url));
    }

    private void setDataInViewObjects() {
        binding.tvFoundationDate.setText(mGetTeamRes.getFoundationDate());
        binding.tvCountry.setText(mGetTeamRes.getCountry());
        binding.tvMostChamps.setText(mGetTeamRes.getOwner());
        binding.scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    }




    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void observeApiResponse() {

    }
    @Override
    public void update(Observable o, Object arg) {
        mGetTeamRes=((TeamProfileActivity)getActivity()).getTeamRes;
        if (mGetTeamRes != null) {
            setDataInViewObjects();
            binding.setTeamRes(mGetTeamRes);
        }
    }
}
