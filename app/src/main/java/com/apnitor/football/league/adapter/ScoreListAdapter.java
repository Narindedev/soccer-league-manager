package com.apnitor.football.league.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.MatchDetailsActivity;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.util.UIUtility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;
import java.util.regex.Pattern;

public class ScoreListAdapter extends RecyclerView.Adapter<ScoreListAdapter.ViewHolder> {

    private Context context;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    List<GetTeamScheduleRes> mGetTeamScheduleRes;
    String LOG_TAG = "ScheduleAdapter";

    public ScoreListAdapter() {

    }

    public ScoreListAdapter(Context context, List<GetTeamScheduleRes> mGetTeamScheduleRes, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.mGetTeamScheduleRes = mGetTeamScheduleRes;
        viewPool = new RecyclerView.RecycledViewPool();
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ScoreListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_score_list_adapter, parent, false);
        ScoreListAdapter.ViewHolder viewHolder = new ScoreListAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ScoreListAdapter.ViewHolder holder, int position) {
        try {
            GetTeamScheduleRes model = mGetTeamScheduleRes.get(position);
            holder.tvTeamName1.setText(model.getTeam1Name());
            holder.tvTeamName2.setText(model.getTeam2Name());
            holder.tvDate.setText(UIUtility.getFormattedDate(model.getDate()));
            holder.tvVenue.setText(model.getLocation());
            // check for referee's match
//            Log.d(LOG_TAG, " Login res " + PreferenceHandler.readString(context, PreferenceHandler.PREF_USER_ID, ""));
//            Log.d(LOG_TAG, " Login api " + model.getRefereeId());
            if (model.getTeam1Score() == -1 && model.getTeam2Score() == -1) {
                holder.tvTeam1Score.setText("");
                holder.tvTeam2Score.setText("");
            } else {
                holder.tvTeam1Score.setText(model.getTeam1Score()+"");
                holder.tvTeam2Score.setText(model.getTeam2Score()+"");
            }

            Glide.with(context).load("").apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeamImage1);
            Glide.with(context).load("").apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeamImage2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mGetTeamScheduleRes.size();
//        return teamList.size();getMatchDetails
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName2;
        TextView tvTeamName1;
        TextView tvTeam1Score;
        TextView tvTeam2Score;
        TextView tvDate;
        // TextView tvTime;
        TextView tvVenue;
        ImageView ivTeamImage1, ivTeamImage2;

        ViewHolder(View itemView) {
            super(itemView);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            //   tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_teamname1);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_teamname2);
            tvTeam1Score = (TextView) itemView.findViewById(R.id.tv_team1Score);
            tvTeam2Score = (TextView) itemView.findViewById(R.id.tv_team2Score);
            tvVenue = (TextView) itemView.findViewById(R.id.tv_venue);
            ivTeamImage1 = (ImageView) itemView.findViewById(R.id.iv_teamimage1);
            ivTeamImage2 = (ImageView) itemView.findViewById(R.id.iv_teamimage2);
            //
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(mGetTeamScheduleRes.get(getLayoutPosition()));
                    //
                    GetTeamScheduleRes model = mGetTeamScheduleRes.get(getLayoutPosition());
                    Intent intent = new Intent(context, MatchDetailsActivity.class);
                    Bundle b = new Bundle();
                    b.putSerializable("match_detail", model);
                    intent.putExtras(b);
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(R.anim.anim_left_in,
                            R.anim.anim_left_out);
                }
            });
        }
    }


}
