package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateSeasonRes implements Serializable {

    @Expose
    @SerializedName("seasonId")
    private String seasonId;

    @Expose
    @SerializedName("seasonYear")
    private String seasonYear;

    @Expose
    @SerializedName("entryFee")
    private String entryFee;

    @Expose
    @SerializedName("winningPrize")
    private String winningPrize;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("zipCode")
    private String zipCode;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("owner")
    private String owner;

    public UpdateSeasonRes(String seasonId, String seasonYear, String entryFee, String winningPrize, String state, String city, String zipCode, String description, String owner) {
        this.seasonId = seasonId;
        this.seasonYear = seasonYear;
        this.entryFee = entryFee;
        this.winningPrize = winningPrize;
        this.state = state;
        this.city = city;
        this.zipCode = zipCode;
        this.description = description;
        this.owner = owner;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonYear() {
        return seasonYear;
    }

    public void setSeasonYear(String seasonYear) {
        this.seasonYear = seasonYear;
    }

    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getWinningPrize() {
        return winningPrize;
    }

    public void setWinningPrize(String winningPrize) {
        this.winningPrize = winningPrize;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}