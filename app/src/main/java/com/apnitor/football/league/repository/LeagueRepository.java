package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.LeagueApi;
import com.apnitor.football.league.api.request.AddLeagueManagerReq;
import com.apnitor.football.league.api.request.AddLeagueRosterReq;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.DeleteLeagueReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.request.GetLeagueStandingReq;
import com.apnitor.football.league.api.request.UpdateLeagueReq;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.AddLeagueManagerRes;
import com.apnitor.football.league.api.response.AddLeagueRosterRes;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueStandingRes;
import com.apnitor.football.league.api.response.LeagueScheduleResultRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateLeagueRes;
import com.apnitor.football.league.api.response.UpdateLeagueTeamRes;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class LeagueRepository {

    private LeagueApi leagueApi;

    public LeagueRepository(Application application) {
        leagueApi = ApiService.getLeagueApi(application);
    }

    public Single<BaseRes<List<GetLeagueRes>>> getMyLeagues() {
        return leagueApi.getMyLeagues()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<List<GetLeagueRes>>> getMyAllLeagues() {
        return leagueApi.getAllLeagues()
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<List<GetLeagueRes>>> getMyAllLeagues(GetAllTeamReq getAllTeamReq) {
        return leagueApi.getAllLeagues(getAllTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateLeagueRes>> createUpdateLeague(UpdateLeagueReq updateLeagueReq) {
        return leagueApi.updateLeague(updateLeagueReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateLeagueTeamRes>> updateLeagueTeams(UpdateLeagueTeamReq updateLeagueTeamReq) {
        return leagueApi.updateLeagueTeams(updateLeagueTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<Object>> deleteLeague(DeleteLeagueReq deleteLeagueReq) {
        return leagueApi.deleteLeague(deleteLeagueReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetLeagueRes>> getLeagueDetails(GetLeagueDetailsReq getLeagueDetailsReq) {
        return leagueApi.getLeagueDetails(getLeagueDetailsReq)
                .subscribeOn(Schedulers.io());
    }


    public Single<BaseRes<List<AddScheduleLeagueReq>>> getLeagueSchedule(GetLeagueScheduleReq getLeagueDetailsReq) {
        return leagueApi.getLeagueSchedules(getLeagueDetailsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<LeagueScheduleResultRes>> getLeagueResults(GetLeagueScheduleReq getLeagueDetailsReq) {
        return leagueApi.getLeagueResults(getLeagueDetailsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<List<GetLeagueStandingRes>>> getLeagueStanding(GetLeagueStandingReq getLeagueStandingReq) {
        return leagueApi.getLeagueStanding(getLeagueStandingReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<AddLeagueRosterRes>> addLeagueRoaster(AddLeagueRosterReq addLeagueRosterReq) {
        return leagueApi.addLeagueRoaster(addLeagueRosterReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<AddLeagueRosterRes>> getLeagueRoaster(AddLeagueRosterReq addLeagueRosterReq) {
        return leagueApi.getLeagueRoaster(addLeagueRosterReq)
                .subscribeOn(Schedulers.io());
    }


    public Single<BaseRes<AddLeagueManagerRes>> addLeagueManager(AddLeagueManagerReq addLeagueManagerReq) {
        return leagueApi.addLeagueManager(addLeagueManagerReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<AddLeagueManagerRes>> searchLeagueManager(AddLeagueManagerReq addLeagueManagerReq) {
        return leagueApi.searchLeagueManager(addLeagueManagerReq)
                .subscribeOn(Schedulers.io());
    }
}
