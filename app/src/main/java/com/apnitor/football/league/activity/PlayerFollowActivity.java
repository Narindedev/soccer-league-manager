package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.PlayerFollowAdapter;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerFollowActivity extends BaseActivity implements ListItemClickCallback {

    private ActivityTeamListBinding binding;
    private PlayerViewModel playerViewModel;
    private FollowingViewModel followingViewModel;
    private PlayerFollowActivity playerListActivity = this;
    private String followMessage;
    private ArrayList<PlayerRes> mPlayerRes;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_list);
        mPlayerRes = new ArrayList<>();
        playerViewModel = getViewModel(PlayerViewModel.class);
        followingViewModel = getViewModel(FollowingViewModel.class);
        setupToolbar(binding.toolbar, "Follow Players");
        setupRecyclerView();
        observeApiResponse();
        binding.tvTeamInfo.setText("Search Player");
        binding.teamNameEt.setHint("Player Name");
        binding.cityEt.setHint("Player City");
        binding.zipCodeEt.setHint("Zip Code");
        binding.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teamName = binding.teamNameEt.getText().toString();
                String teamCity = binding.cityEt.getText().toString();
                String teamZipCode = binding.zipCodeEt.getText().toString();

                if (teamName.isEmpty()) {
                    showToast("Please enter player name.");
                } else {
                    hideKeyboard(PlayerFollowActivity.this);
                    playerViewModel.getAllPlayers(new GetAllTeamReq(teamName, teamCity, teamZipCode));
                }

            }
        });
    }

    private void observeApiResponse() {
        followingViewModel.getfollowPlayerRes().observe(this, follow -> {
            FollowingFragment.isFollowing = true;
            showToast(followMessage);
            finish();
        });
        playerViewModel.getAllPlayersLiveData().observe(this,
                teams -> {
                    binding.rvAllTeams.setAdapter(
                            new PlayerFollowAdapter(playerListActivity, teams, RecyclerView.VERTICAL, this)
                    );
                });
        // playerViewModel.getAllPlayers();
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {

        onBackPressed();
        //  finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //  finishStartActivity(HomeActivity.class);
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


    @Override
    public void onListItemClick(Object object) {
        PlayerRes playerRes = (PlayerRes) object;
        if (playerRes.isFollowing) {
            mPlayerRes.add(playerRes);
            followMessage = "Player Followed";
            followingViewModel.followTeam(new GetTeamPlayersReq("", playerRes.getId()));
        } else {
            if (mPlayerRes.size() > 0) {
                mPlayerRes.remove(playerRes);
            }
            followMessage = "Player Unfollowed";
            followingViewModel.unfollowTeam(new GetTeamPlayersReq("", playerRes.getId()));
        }
//        Intent intent = new Intent(playerListActivity, TeamDetailPageActivity.class);
//        Bundle b = new Bundle();
//        b.putSerializable("team_detail", getTeamRes);
//        intent.putExtras(b);
//        startActivity(intent);
    }
}
