package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.PointsTableFragment;
import com.apnitor.football.league.fragment.SeasonsResultFragment;
import com.apnitor.football.league.fragment.StatsFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class SeasonPagePagerAdapater extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;
    GetLeagueRes mGetLeagueRes;
    private Observable mObservers = new FragmentObserver();

    public SeasonPagePagerAdapater(FragmentManager fm, GetLeagueRes getLeagueRes) {
        super(fm);
        mGetLeagueRes = getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                /**
                 *   ProfileSeasonsFragment profileSeasonsFragment = ProfileSeasonsFragment.newInstance(getLeagueRes);
                 *
                 *                 mObservers.deleteObservers(); // Clear existing observers.
                 *
                 *                 if(profileSeasonsFragment instanceof Observer)
                 *                     mObservers.addObserver((Observer) profileSeasonsFragment);
                 */
                SeasonsResultFragment seasonsResultFragment = SeasonsResultFragment.newInstance(mGetLeagueRes);
                if (seasonsResultFragment instanceof Observer) {
                    mObservers.addObserver((Observer) seasonsResultFragment);
                }
                return seasonsResultFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title
      //          mObservers.deleteObservers();
                LeagueSchedulesFragment leagueSchedulesFragment = LeagueSchedulesFragment.newInstance(mGetLeagueRes);
                if (leagueSchedulesFragment instanceof Observer) {
                    mObservers.deleteObservers();
                    mObservers.addObserver((Observer) leagueSchedulesFragment);
                }
                return leagueSchedulesFragment;
            case 2: // Fragment # 1 - This will show SecondFragment
                PointsTableFragment pointsTableFragment = PointsTableFragment.newInstance(mGetLeagueRes);
                if (pointsTableFragment instanceof Observer) {
                    mObservers.addObserver((Observer) pointsTableFragment);
                }
                return pointsTableFragment;
            case 3: // Fragment # 1 - This will show SecondFragment
                StatsFragment statsFragment = StatsFragment.newInstance(mGetLeagueRes);
                if (statsFragment instanceof Observer) {
                    mObservers.addObserver((Observer) statsFragment);
                }
                return statsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }



    public void updateFragments() {
        mObservers.notifyObservers();
    }
}
