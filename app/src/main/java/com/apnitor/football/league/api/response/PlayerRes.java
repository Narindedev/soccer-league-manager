package com.apnitor.football.league.api.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.apnitor.football.league.api.request.SocialMedia;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PlayerRes implements Serializable,Parcelable{
    public PlayerRes(){

    }
    @Expose
    @SerializedName("_id")
    private String id;

    @Expose
    @SerializedName("playerId")
    private String leaguePlayerId;

    @Expose
    @SerializedName("imageUrl")
    private String profilePhotoUrl;

    @Expose
    @SerializedName("firstName")
    private String firstName;

    @Expose
    @SerializedName("lastName")
    private String lastName;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("phone")
    private String phone;


    @Expose
    @SerializedName("isFollowing")
    public Boolean isFollowing;

    @Expose
    @SerializedName("countryName")
    private String countryName;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("zipCode")
    private String zipCode;

    @Expose
    @SerializedName("city")
    private String city;

    private String playingPosition;
    private String dateBirth;
    private String otherPositions;
    private String currentTeam;
    private String previousTeam;
    private String[] leaguePlayed;
    private String[] followingTeamList;
    private String[] followingPlayerList;
    private String[] followingLeagueList;
    private boolean isSelected;
    private boolean emailVerified;
    private boolean phoneVerified;
    private Integer matchesPlayed;
    private Integer goalScored;
    private Integer goalAssisted;
    private Integer redCardCount;
    private Integer yellowCardCount;
    private Integer age;
    private Double height;
    private Double weight;
    private Integer shirtNumber;
    private Integer minutesPerGoal;
    private Integer followers;
    private Integer lineUp;
    private Integer substitutions;
    private Integer timePlayed;
    private SocialMedia socialMedia;
public Boolean isLeaguePlayer=false;

    protected PlayerRes(Parcel in) {
        id = in.readString();
        profilePhotoUrl = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        byte tmpIsFollowing = in.readByte();
        isFollowing = tmpIsFollowing == 0 ? null : tmpIsFollowing == 1;
        countryName = in.readString();
        state = in.readString();
        zipCode = in.readString();
        city = in.readString();
        playingPosition = in.readString();
        dateBirth = in.readString();
        otherPositions = in.readString();
        currentTeam = in.readString();
        previousTeam = in.readString();
        leaguePlayed = in.createStringArray();
        followingTeamList = in.createStringArray();
        followingPlayerList = in.createStringArray();
        followingLeagueList = in.createStringArray();
        isSelected = in.readByte() != 0;
        emailVerified = in.readByte() != 0;
        phoneVerified = in.readByte() != 0;
        if (in.readByte() == 0) {
            matchesPlayed = null;
        } else {
            matchesPlayed = in.readInt();
        }
        if (in.readByte() == 0) {
            goalScored = null;
        } else {
            goalScored = in.readInt();
        }
        if (in.readByte() == 0) {
            goalAssisted = null;
        } else {
            goalAssisted = in.readInt();
        }
        if (in.readByte() == 0) {
            redCardCount = null;
        } else {
            redCardCount = in.readInt();
        }
        if (in.readByte() == 0) {
            yellowCardCount = null;
        } else {
            yellowCardCount = in.readInt();
        }
        if (in.readByte() == 0) {
            age = null;
        } else {
            age = in.readInt();
        }
        if (in.readByte() == 0) {
            height = null;
        } else {
            height = in.readDouble();
        }
        if (in.readByte() == 0) {
            weight = null;
        } else {
            weight = in.readDouble();
        }
        if (in.readByte() == 0) {
            shirtNumber = null;
        } else {
            shirtNumber = in.readInt();
        }
        if (in.readByte() == 0) {
            minutesPerGoal = null;
        } else {
            minutesPerGoal = in.readInt();
        }
        if (in.readByte() == 0) {
            followers = null;
        } else {
            followers = in.readInt();
        }
        if (in.readByte() == 0) {
            lineUp = null;
        } else {
            lineUp = in.readInt();
        }
        if (in.readByte() == 0) {
            substitutions = null;
        } else {
            substitutions = in.readInt();
        }
        if (in.readByte() == 0) {
            timePlayed = null;
        } else {
            timePlayed = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(profilePhotoUrl);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeByte((byte) (isFollowing == null ? 0 : isFollowing ? 1 : 2));
        dest.writeString(countryName);
        dest.writeString(state);
        dest.writeString(zipCode);
        dest.writeString(city);
        dest.writeString(playingPosition);
        dest.writeString(dateBirth);
        dest.writeString(otherPositions);
        dest.writeString(currentTeam);
        dest.writeString(previousTeam);
        dest.writeStringArray(leaguePlayed);
        dest.writeStringArray(followingTeamList);
        dest.writeStringArray(followingPlayerList);
        dest.writeStringArray(followingLeagueList);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (emailVerified ? 1 : 0));
        dest.writeByte((byte) (phoneVerified ? 1 : 0));
        if (matchesPlayed == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(matchesPlayed);
        }
        if (goalScored == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(goalScored);
        }
        if (goalAssisted == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(goalAssisted);
        }
        if (redCardCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(redCardCount);
        }
        if (yellowCardCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(yellowCardCount);
        }
        if (age == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(age);
        }
        if (height == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(height);
        }
        if (weight == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(weight);
        }
        if (shirtNumber == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(shirtNumber);
        }
        if (minutesPerGoal == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(minutesPerGoal);
        }
        if (followers == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(followers);
        }
        if (lineUp == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lineUp);
        }
        if (substitutions == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(substitutions);
        }
        if (timePlayed == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(timePlayed);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlayerRes> CREATOR = new Creator<PlayerRes>() {
        @Override
        public PlayerRes createFromParcel(Parcel in) {
            return new PlayerRes(in);
        }

        @Override
        public PlayerRes[] newArray(int size) {
            return new PlayerRes[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getFollowing() {
        return isFollowing;
    }

    public void setFollowing(Boolean following) {
        isFollowing = following;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPlayingPosition() {
        return playingPosition;
    }

    public void setPlayingPosition(String playingPosition) {
        this.playingPosition = playingPosition;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public String getOtherPositions() {
        return otherPositions;
    }

    public void setOtherPositions(String otherPositions) {
        this.otherPositions = otherPositions;
    }

    public String getCurrentTeam() {
        return currentTeam;
    }

    public void setCurrentTeam(String currentTeam) {
        this.currentTeam = currentTeam;
    }

    public String getPreviousTeam() {
        return previousTeam;
    }

    public void setPreviousTeam(String previousTeam) {
        this.previousTeam = previousTeam;
    }

    public String[] getLeaguePlayed() {
        return leaguePlayed;
    }

    public void setLeaguePlayed(String[] leaguePlayed) {
        this.leaguePlayed = leaguePlayed;
    }

    public String[] getFollowingTeamList() {
        return followingTeamList;
    }

    public void setFollowingTeamList(String[] followingTeamList) {
        this.followingTeamList = followingTeamList;
    }

    public String[] getFollowingPlayerList() {
        return followingPlayerList;
    }

    public void setFollowingPlayerList(String[] followingPlayerList) {
        this.followingPlayerList = followingPlayerList;
    }

    public String[] getFollowingLeagueList() {
        return followingLeagueList;
    }

    public void setFollowingLeagueList(String[] followingLeagueList) {
        this.followingLeagueList = followingLeagueList;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public Integer getMatchesPlayed() {
        return matchesPlayed;
    }

    public void setMatchesPlayed(Integer matchesPlayed) {
        this.matchesPlayed = matchesPlayed;
    }

    public Integer getGoalScored() {
        return goalScored;
    }

    public void setGoalScored(Integer goalScored) {
        this.goalScored = goalScored;
    }

    public Integer getGoalAssisted() {
        return goalAssisted;
    }

    public void setGoalAssisted(Integer goalAssisted) {
        this.goalAssisted = goalAssisted;
    }

    public Integer getRedCardCount() {
        return redCardCount;
    }

    public void setRedCardCount(Integer redCardCount) {
        this.redCardCount = redCardCount;
    }

    public Integer getYellowCardCount() {
        return yellowCardCount;
    }

    public void setYellowCardCount(Integer yellowCardCount) {
        this.yellowCardCount = yellowCardCount;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(Integer shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public Integer getMinutesPerGoal() {
        return minutesPerGoal;
    }

    public void setMinutesPerGoal(Integer minutesPerGoal) {
        this.minutesPerGoal = minutesPerGoal;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public Integer getLineUp() {
        return lineUp;
    }

    public void setLineUp(Integer lineUp) {
        this.lineUp = lineUp;
    }

    public Integer getSubstitutions() {
        return substitutions;
    }

    public void setSubstitutions(Integer substitutions) {
        this.substitutions = substitutions;
    }

    public Integer getTimePlayed() {
        return timePlayed;
    }

    public void setTimePlayed(Integer timePlayed) {
        this.timePlayed = timePlayed;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getLeaguePlayerId() {
        return leaguePlayerId;
    }

    public void setLeaguePlayerId(String leaguePlayerId) {
        this.leaguePlayerId = leaguePlayerId;
    }
}
