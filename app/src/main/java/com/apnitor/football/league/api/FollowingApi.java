package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetFollowingReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateImageReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetFollowingRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetNotificationRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.api.response.PlayerRes;

import java.util.ArrayList;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface FollowingApi {
    @POST("followLeague")
    Single<BaseRes<GetLeagueRes>> followLeague(@Body GetLeagueDetailsReq getLeagueDetailsReq);

    @POST("unfollowLeague")
    Single<BaseRes<GetLeagueRes>> unfollowLeague(@Body GetLeagueDetailsReq getLeagueDetailsReq);

    @POST("followTeam")
    Single<BaseRes<GetTeamRes>> followTeam(@Body DeleteTeamReq deleteTeamReq);

    @POST("unfollowTeam")
    Single<BaseRes<GetTeamRes>> unFollowTeam(@Body DeleteTeamReq deleteTeamReq);

    @POST("followPlayer")
    Single<BaseRes<PlayerRes>> followPlayers(@Body GetTeamPlayersReq getTeamPlayersReq);

    @POST("unfollowPlayer")
    Single<BaseRes<PlayerRes>> unfollowPlayers(@Body GetTeamPlayersReq getTeamPlayersReq);

    @GET("getAllFollowings")
    Single<BaseRes<GetFollowingRes>> getFollowing();


    @POST("updateImageUrl")
    Single<BaseRes<GetFollowingRes>> updateImageUrl(@Body UpdateImageReq updateImageReq);


    @GET("getAllFollowingScores")
    Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getFollowingScores();

    @GET("getAllNotifications")
    Single<BaseRes<GetNotificationRes>> getAllNotifications();
}
