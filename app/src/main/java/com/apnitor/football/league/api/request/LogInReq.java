package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogInReq {

    @Expose
    @SerializedName("username")
    private String userName;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("deviceId")
    private String deviceId;

    @Expose
    @SerializedName("uniqueId")
    private String uniqueId;

    @Expose
    @SerializedName("deviceType")
    private String deviceType;

    // For Facebook login:
    @Expose
    @SerializedName("fbUserId")
    private String fbUserId;

    @Expose
    @SerializedName("firstName")
    private String fbFirstName;

    @Expose
    @SerializedName("lastName")
    private String fbLastName;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("imageUrl")
    private String imageUrl;

    public LogInReq(String userName, String password,String phone,String email,String uniqueId,String deviceType,String deviceId) {
        this(userName, password, "", "", "", email,"",phone,deviceId,deviceType,uniqueId);
    }

    public LogInReq(String fbUserId, String fbFirstName, String fbLastName, String fbEmail,String imgUrl, String uniqueId,String deviceType,String deviceId) {
        this("", "", fbUserId, fbFirstName, fbLastName, fbEmail,imgUrl,"",deviceId,deviceType,uniqueId);
    }

    public LogInReq(String userName, String password, String fbUserId, String fbFirstName, String fbLastName, String fbEmail,String imageUrl,String phone,String deviceId,String deviceType,String uniqueId) {

        this.userName = userName;
        this.password = password;
        this.fbUserId = fbUserId;
        this.fbFirstName = fbFirstName;
        this.fbLastName = fbLastName;
        this.email = fbEmail;
        this.phone=phone;
        this.imageUrl=imageUrl;
        this.deviceId=deviceId;
        this.deviceType=deviceType;
        this.uniqueId=uniqueId;

    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFbUserId() {
        return fbUserId;
    }

    public void setFbUserId(String fbUserId) {
        this.fbUserId = fbUserId;
    }

    public String getFbFirstName() {
        return fbFirstName;
    }

    public void setFbFirstName(String fbFirstName) {
        this.fbFirstName = fbFirstName;
    }

    public String getFbLastName() {
        return fbLastName;
    }

    public void setFbLastName(String fbLastName) {
        this.fbLastName = fbLastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
