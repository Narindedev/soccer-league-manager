package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerFollowAdapter extends RecyclerView.Adapter<PlayerFollowAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
//    private LanguageInterface languageInterface;


    public PlayerFollowAdapter(Context context, ArrayList<PlayerRes> teamList, int orientation, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_follow_team, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PlayerRes model = teamList.get(position);
         //   model.isFollowing=false;
            holder.tvTeamName.setText(model.getFirstName() + " "+model.getLastName());
            if(model.isFollowing){
                holder.ivFollowImage.setImageResource(R.drawable.ic_favorite_selected);
            }else{
                holder.ivFollowImage.setImageResource(R.drawable.ic_favorite_border);
            }
            if (model.getProfilePhotoUrl() != null) {
                Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_player)
                        .centerCrop()).into(holder.img);
            }  else{
                holder.img.setImageResource(R.drawable.ic_player);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img,ivFollowImage;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            ivFollowImage= (ImageView) itemView.findViewById(R.id.ivFollowImage);

            ivFollowImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlayerRes model=teamList.get(getAdapterPosition());
                    model.isFollowing=!model.isFollowing;
                    ((ImageView) v).setImageResource(model.isFollowing ?R.drawable.ic_favorite_selected : R.drawable.ic_favorite_border);
                   /* if(!model.isFollowing){
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_selected);
                    }else{
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_border);
                    }*/

                       /* if (((ImageView) v).getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_favorite_selected).getConstantState()) {
                            ((ImageView) v).setImageResource(R.drawable.ic_favorite_border);
                        }else
                            ((ImageView) v).setImageResource(R.drawable.ic_favorite_selected);*/

                    listItemClickCallback.onListItemClick(model);
                }
            });

           /* itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));

                }
            });*/
        }
    }
}
