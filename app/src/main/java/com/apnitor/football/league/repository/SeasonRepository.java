package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.SeasonApi;
import com.apnitor.football.league.api.request.DeleteSeasonReq;
import com.apnitor.football.league.api.request.GetLeagueSeasonsReq;
import com.apnitor.football.league.api.request.UpdateSeasonReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.UpdateSeasonRes;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class SeasonRepository {

    private SeasonApi seasonApi;

    public SeasonRepository(Application application) {
        seasonApi = ApiService.getSeasonApi(application);
    }

    public Single<BaseRes<List<GetSeasonRes>>> getSeasons(GetLeagueSeasonsReq getLeagueSeasonsReq) {
//        return Single.create((SingleOnSubscribe<BaseRes<LeagueListRes>>) emitter -> {

//            ArrayList temp = new ArrayList<CreateLeagueRes>();
//            temp.add(new CreateLeagueRes("11", "My League", "5-12-2018", "15-12-2018", 2, 500,
//                    "India", "Punjab", "Mohali", "9876543210", "First leagues ever happening"));
//            temp.add(new CreateLeagueRes("22", "My League Two", "25-12-2018", "30-12-2018", 2, 500,
//                    "India", "Punjab", "Mohali", "9876543210", "Second league ever happening"));
//            LeagueListRes res = new LeagueListRes(temp);
//            emitter.onSuccess(new BaseRes<>(true, res, 0, "", ""));
//        })
//                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io());
        return seasonApi.getLeagueSeasons(getLeagueSeasonsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateSeasonRes>> createUpdateSeason(UpdateSeasonReq updateSeasonReq) {
//        return Single.create((SingleOnSubscribe<BaseRes<UpdateProfileRes>>) emitter ->
//                emitter.onSuccess(new BaseRes<>(true, new UpdateProfileRes(), 0, "", "")))
//                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io());
        return seasonApi.updateSeason(updateSeasonReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<Object>> deleteSeason(DeleteSeasonReq deleteSeasonReq) {
        return seasonApi.deleteSeason(deleteSeasonReq)
                .subscribeOn(Schedulers.io());
    }
}
