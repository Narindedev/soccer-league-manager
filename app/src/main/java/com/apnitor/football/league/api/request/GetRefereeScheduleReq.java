package com.apnitor.football.league.api.request;

public class GetRefereeScheduleReq {


    private String refereeId;

    public GetRefereeScheduleReq(String leagueId) {
        this.refereeId = leagueId;
    }

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }


}
