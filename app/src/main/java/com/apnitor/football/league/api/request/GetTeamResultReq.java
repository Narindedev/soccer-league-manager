package com.apnitor.football.league.api.request;

public class GetTeamResultReq {
    private String teamId;
    private String leagueId;
    private String seasonId;
    private String date;

    public GetTeamResultReq(String teamId,String leagueId,String seasonId,String date){
        this.teamId=teamId;
        this.leagueId=leagueId;
        this.seasonId=seasonId;
        this.date=date;
    }
}
