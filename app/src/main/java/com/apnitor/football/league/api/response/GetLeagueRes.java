package com.apnitor.football.league.api.response;

import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.SocialMedia;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetLeagueRes implements Serializable {

    @Expose
    @SerializedName("_id")
    private String leagueId;

    @Expose
    @SerializedName("leagueId")
    private String teamLeagueId;

    @Expose
    @SerializedName("name")
    private String leagueTitle;

    @Expose
    @SerializedName("foundationDate")
    private String foundationDate;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("currentChampions")
    private String currentChampions;

    @Expose
    @SerializedName("mostChampions")
    private String mostChampions;

    @Expose
    @SerializedName("contactInfo")
    private String contactInfo;

    @Expose
    @SerializedName("websiteUrl")
    private String website;

    @Expose
    @SerializedName("facebookUrl")
    private String facebookLink;

    @Expose
    @SerializedName("twitterUrl")
    private String twitterLink;

    @Expose
    @SerializedName("imageUrl")
    private String imageUrl;

    @Expose
    @SerializedName("owner")
    private String owner;

    @Expose
    @SerializedName("isFollowing")
    public Boolean isFollowing;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("managerName")
    private String managerName;
    @Expose
    @SerializedName("managerEmail")
    private String managerEmail;

    @Expose
    @SerializedName("managerPhone")
    private String managerPhone;

    @Expose
    @SerializedName("socialMedia")
    public SocialMedia socialMedia;


    @Expose
    @SerializedName("divisionList")
    public List<GetDivisionRes> divisionList;


    @Expose
    @SerializedName("seasonList")
    public List<GetSeasonRes> seasonList;

    @Expose
    @SerializedName("scheduleList")
    private List<AddScheduleLeagueReq> scheduleList;

    @Expose
    @SerializedName("Past20schedules")
    public List<ScheduleResultRes> resultList;
    @Expose
    @SerializedName("leagueTeamList")
    private List<LeagueTeam> leagueTeamList;

    @Expose
    @SerializedName("getLeagueStandings")
    private List<GetLeagueStandingRes> getLeagueStandings;

    public List<LeagueTeam> getLeagueTeamList() {
        return leagueTeamList;
    }

    public void setLeagueTeamList(List<LeagueTeam> leagueTeamList) {
        this.leagueTeamList = leagueTeamList;
    }


    public GetLeagueRes(){

    }

    public GetLeagueRes(String leagueId, String leagueTitle, String foundationDate, String country,
                        String currentChampions, String mostChampions, String contactInfo, String website, String facebookLink,
                        String twitterLink, String imageUrl, String owner, Boolean isFollowing, String description, String managerName,
                        String managerEmail, String managerPhone, SocialMedia socialMedia, List<GetDivisionRes> divisionList,
                        List<GetSeasonRes> seasonList, List<AddScheduleLeagueReq> scheduleList, List<LeagueTeam> leagueTeamList) {
        this.leagueId = leagueId;
        this.leagueTitle = leagueTitle;
        this.foundationDate = foundationDate;
        this.country = country;
        this.currentChampions = currentChampions;
        this.mostChampions = mostChampions;
        this.contactInfo = contactInfo;
        this.website = website;
        this.facebookLink = facebookLink;
        this.twitterLink = twitterLink;
        this.imageUrl = imageUrl;
        this.owner = owner;
        this.isFollowing = isFollowing;
        this.description = description;
        this.managerName = managerName;
        this.managerEmail = managerEmail;
        this.managerPhone = managerPhone;
        this.socialMedia = socialMedia;
        this.divisionList = divisionList;
        this.seasonList = seasonList;
        this.scheduleList = scheduleList;
        this.leagueTeamList = leagueTeamList;
    }

    public GetLeagueRes(String leagueId, String leagueTitle, String foundationDate, String country, String currentChampions,
                        String mostChampions, String contactInfo, String website, String facebookLink, String twitterLink,
                        String imageUrl, String owner) {
        this.leagueId = leagueId;
        this.leagueTitle = leagueTitle;
        this.foundationDate = foundationDate;
        this.country = country;
        this.currentChampions = currentChampions;
        this.mostChampions = mostChampions;
        this.contactInfo = contactInfo;
        this.website = website;
        this.facebookLink = facebookLink;
        this.twitterLink = twitterLink;
        this.imageUrl = imageUrl;
        this.owner = owner;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueTitle() {
        return leagueTitle;
    }

    public void setLeagueTitle(String leagueTitle) {
        this.leagueTitle = leagueTitle;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrentChampions() {
        return currentChampions;
    }

    public void setCurrentChampions(String currentChampions) {
        this.currentChampions = currentChampions;
    }

    public String getMostChampions() {
        return mostChampions;
    }

    public void setMostChampions(String mostChampions) {
        this.mostChampions = mostChampions;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }


    public String getTeamLeagueId() {
        return teamLeagueId;
    }

    public void setTeamLeagueId(String teamLeagueId) {
        this.teamLeagueId = teamLeagueId;
    }

    public List<AddScheduleLeagueReq> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<AddScheduleLeagueReq> scheduleList) {
        this.scheduleList = scheduleList;
    }

    public List<GetLeagueStandingRes> getGetLeagueStandings() {
        return getLeagueStandings;
    }

    public void setGetLeagueStandings(List<GetLeagueStandingRes> getLeagueStandings) {
        this.getLeagueStandings = getLeagueStandings;
    }
}
