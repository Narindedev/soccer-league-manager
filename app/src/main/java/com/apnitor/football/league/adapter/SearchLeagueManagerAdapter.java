package com.apnitor.football.league.adapter;

import android.content.Context;
import android.os.Build;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.AddLeagueManagerListRes;
import com.apnitor.football.league.api.response.AddLeagueManagerRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

public class SearchLeagueManagerAdapter extends RecyclerView.Adapter<SearchLeagueManagerAdapter.ViewHolder> {

    private Context context;
    private List<AddLeagueManagerListRes> managerList;
    private ListItemClickCallback listItemClickCallback;
    public SearchLeagueManagerAdapter(Context context, List<AddLeagueManagerListRes> managerList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.managerList = managerList;
        this.listItemClickCallback=listItemClickCallback;
    }
    public  SearchLeagueManagerAdapter(){

    }

    @Override
    public SearchLeagueManagerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_search_league_manager, parent, false);

        return new SearchLeagueManagerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchLeagueManagerAdapter.ViewHolder holder, int position) {
        try {
            AddLeagueManagerListRes model = managerList.get(position);
            holder.tvAddManager.setText("Add Manager");
            if (model.getEmail()!=null && !model.getEmail().isEmpty()) {
                holder.tvEmail.setText(model.getEmail());
            }else{
                holder.tvEmail.setVisibility(View.GONE);
            }

            holder.tvManagerName.setText(model.getFirstName()+" "+model.getLastName());
            // holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark));
            Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_manager)
                    .centerCrop()).into(holder.img);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return managerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvManagerName,tvAddManager,tvEmail;
        RelativeLayout linearLayout;
        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivManagerImage);
            tvManagerName = (TextView) itemView.findViewById(R.id.tvManagerName);
            tvEmail=(TextView) itemView.findViewById(R.id.tvEmail);

            tvAddManager= (TextView) itemView.findViewById(R.id.tvAddManager);
            linearLayout = itemView.findViewById(R.id.llRow);

            tvAddManager.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    AddLeagueManagerListRes model = managerList.get(getAdapterPosition());
                    listItemClickCallback.onListItemClick(model);

                }
            });
        }
    }
}
