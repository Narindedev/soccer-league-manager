package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteDivisionReq {

    @Expose
    @SerializedName("devisionId")
    private String devisionId;

    public DeleteDivisionReq(String devisionId) {
        this.devisionId = devisionId;
    }

    public String getDevisionId() {
        return devisionId;
    }

    public void setDevisionId(String devisionId) {
        this.devisionId = devisionId;
    }
}
