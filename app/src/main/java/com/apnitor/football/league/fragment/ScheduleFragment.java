package com.apnitor.football.league.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.HomeActivity;
import com.apnitor.football.league.activity.RefereeScheduleActivity;
import com.apnitor.football.league.activity.referee.StartMatchActivity;
import com.apnitor.football.league.adapter.GameEventAdapter;
import com.apnitor.football.league.adapter.RefereeScheduleAdapter;
import com.apnitor.football.league.adapter.ScheduleAdapter;
import com.apnitor.football.league.api.request.GetRefereeScheduleReq;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.databinding.FragmentScheduleBinding;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ScheduleFragment extends BaseFragment {

    private RefereeViewModel refereeViewModel;
    private FragmentScheduleBinding binding;
    LogInRes logInRes;
    List<GetTeamScheduleRes> mGetTeamScheduleRes = new ArrayList<>();
    ScheduleAdapter mTeamSchedulesListAdapter;
    String LOG_TAG="ScheduleFragment";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentScheduleBinding.inflate(inflater, container, false);
        binding.saveBtn.setVisibility(View.GONE);
        binding.cardEdit.setVisibility(View.GONE);


        refereeViewModel = getViewModel(RefereeViewModel.class);

        setupRecyclerView();
        observeApiResponse();


        binding.ivReferee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RefereeScheduleActivity.class));
            }
        });
        return binding.getRoot();
    }


    private void observeApiResponse() {
        refereeViewModel.getFollowingSchedulesLiveData().observe(this,
                teams -> {
                    mGetTeamScheduleRes.clear();
                    mGetTeamScheduleRes.addAll(teams);
                    sortScheduleByDate(mGetTeamScheduleRes);
                    setupRecyclerView();
                });
        refereeViewModel.getFollowingSchedules();
    }


    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false)
        );
        mTeamSchedulesListAdapter = new ScheduleAdapter(getActivity(), mGetTeamScheduleRes, null);
        binding.rvAllTeams.setAdapter(mTeamSchedulesListAdapter);
    }

    void filterScheduleResponse(List<GetTeamScheduleRes> teams) {
        mGetTeamScheduleRes.clear();
        int size = teams.size();
        for (int i = 0; i < size; i++) {
            if (teams.get(i).getRefereeId() != null && teams.get(i).getRefereeId().equalsIgnoreCase(logInRes.getUserId())) {
                mGetTeamScheduleRes.add(teams.get(i));
            }
        }
        mTeamSchedulesListAdapter.notifyDataSetChanged();
    }

    void sortScheduleByDate(List<GetTeamScheduleRes> mGetTeamScheduleRes) {
        int size = mGetTeamScheduleRes.size();
        if (size > 0) {
            //
            Collections.sort(mGetTeamScheduleRes, new Comparator<GetTeamScheduleRes>() {
                @Override
                public int compare(final GetTeamScheduleRes object1, final GetTeamScheduleRes object2) {
//                    Log.d(LOG_TAG, " Comparing 1st " + object1.getDate() + " with " + object2.getDate());
                    return object1.getDate().compareTo(object2.getDate());
                }
            });
        }
        //
        Collections.reverse(mGetTeamScheduleRes);
        //
        mTeamSchedulesListAdapter = new ScheduleAdapter(getActivity(), mGetTeamScheduleRes, null);
        binding.rvAllTeams.setAdapter(mTeamSchedulesListAdapter);
    }

}
