package com.apnitor.football.league.activity.referee;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class GoalDetailAdapter extends RecyclerView.Adapter<GoalDetailAdapter.ViewHolder> {
    private int pos = -1;
    private Context context;
    private List<TeamPlaying11> mTeamPlayersRes;
    ListItemClickCallback listItemClickCallback;
    private Boolean isSelected = false;
    LinearLayout mOldLinearLayout;

    public GoalDetailAdapter(Context context, List<TeamPlaying11> mTeamPlayersRes, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.mTeamPlayersRes = mTeamPlayersRes;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public GoalDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_goal_detail_adapter, parent, false);
        return new GoalDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GoalDetailAdapter.ViewHolder holder, int position) {
        try {
            TeamPlaying11 model = mTeamPlayersRes.get(position);
            //
            holder.tvPlayerName.setText(model.getFirstName() + " " + model.getLastName());
            //
            if (pos == position) {
                holder.followImg.setImageResource(R.drawable.ic_check);
            } else {
                holder.followImg.setImageResource(R.drawable.ic_uncheck);
            }
            //
//            holder.followImg.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (position == pos) {
//                        holder.followImg.setImageResource(R.drawable.ic_uncheck);
//                    } else {
//                        holder.followImg.setImageResource(R.drawable.ic_check);
//                    }
//                }
//            });
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if (mOldLinearLayout != null) {
                        mOldLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.button_text_dark));
                    }
                    //
                    holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
                    mOldLinearLayout = (LinearLayout) v;
                    //
                    TeamPlaying11 model = mTeamPlayersRes.get(position);
                    listItemClickCallback.onListItemClick(model);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mTeamPlayersRes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPlayer, followImg;
        TextView tvPlayerName;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            imgPlayer = (ImageView) itemView.findViewById(R.id.ivPlayerImg);
            tvPlayerName = (TextView) itemView.findViewById(R.id.tvPlayerName);
            followImg = (ImageView) itemView.findViewById(R.id.ivSelectImg);
            linearLayout = itemView.findViewById(R.id.ll_league_manager);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = getAdapterPosition();
                    //
                    TeamPlaying11 model = mTeamPlayersRes.get(getAdapterPosition());
                    listItemClickCallback.onListItemClick(model);
                    //
                    linearLayout.performClick();
                }
            });
        }
    }
}
