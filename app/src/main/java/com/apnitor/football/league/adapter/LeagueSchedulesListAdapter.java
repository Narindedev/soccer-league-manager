package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.UIUtility;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueSchedulesListAdapter extends RecyclerView.Adapter<LeagueSchedulesListAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    //    private LanguageInterface languageInterface;
    private List<AddScheduleLeagueReq> leagueScheduleList;
//    private LanguageInterface languageInterface;


    public LeagueSchedulesListAdapter(Context context,List<AddScheduleLeagueReq> leagueScheduleList) {
        this.context = context;
        this.leagueScheduleList = leagueScheduleList;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_league_schedule, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.dayScheduleRv.setRecycledViewPool(viewPool);
        return viewHolder;
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            holder.dayScheduleRv.setHasFixedSize(true);
            holder.dayScheduleRv.setNestedScrollingEnabled(false);

            if (leagueScheduleList.size() > 0) {
                AddScheduleLeagueReq model = leagueScheduleList.get(position);
                holder.tvTeamName.setText(UIUtility.getFormattedDate(model.getDate()));
            }

            LeagueDayScheduleListAdapter adapter = new LeagueDayScheduleListAdapter(context, null, leagueScheduleList.get(position), position);
            holder.dayScheduleRv.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueScheduleList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;
        private RecyclerView dayScheduleRv;

        ViewHolder(View itemView) {
            super(itemView);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvScheduleDayDate);
            dayScheduleRv = itemView.findViewById(R.id.rvDaySchedules);
            dayScheduleRv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        }
    }
}
