package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.navigation.KeepStateNavigator;
import com.apnitor.football.league.viewmodel.LoginViewModel;
import com.facebook.login.LoginManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

public class HomeActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private NavHostFragment navHostFragment;
    private BottomNavigationView bottomNavigationView;
    private NavigationView navigationView;
    private NavController navController;
    private LoginViewModel loginViewModel;
    private ImageView mNotifications, mPlayerProfile;
    private LogInRes logInRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findViews();
        setupToolbar();
        navigationView.setNavigationItemSelectedListener(this::onNavigationItemSelected);
        setupCustomNavigator();
        setupNavigation();
        onClick();
        loginViewModel = getViewModel(LoginViewModel.class);
    }

    private void onClick() {
        mNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(NotificationActivity.class);
                overridePendingTransition(R.anim.anim_left_in,
                        R.anim.anim_left_out);
            }
        });
        mPlayerProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(HomeActivity.this, PlayerProfileActivity.class);
                Bundle b = new Bundle();
                //b.putSerializable("team_detail", getTeamRes);
                b.putString("player_id", logInRes.getUserId());
                b.putString("player_name", logInRes.getFirstName() + " " + logInRes.getLastName());
                intent.putExtras(b);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_left_in,
                        R.anim.anim_left_out);
            }
        });
    }
   /* private void getKeyhash() {
        try {
            // jitender Ubjkfrfl2y64l4mdaUrZbKc1cGU=
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.apnitor.football.league",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }*/

    private void findViews() {
        logInRes = getPrefHelper().getLogInRes();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);

        // get navigation host fragment and nav controller
        navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        assert navHostFragment != null;
        navController = navHostFragment.getNavController();
        mNotifications = (ImageView) findViewById(R.id.ivNotifications);
        mPlayerProfile = (ImageView) findViewById(R.id.ivPlayerProfile);
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        navigationView = findViewById(R.id.nav_view);

        if(logInRes.getUserTypes().contains("Player")){
            mPlayerProfile.setVisibility(View.VISIBLE);
        }else{
            mPlayerProfile.setVisibility(View.GONE);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.vector_menu);
    }

    private boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(true);
        // close drawer when item is tapped
        mDrawerLayout.closeDrawers();

        // Add code here to update the UI based on the item selected
        // For example, swap UI fragments here
        switch (menuItem.getItemId()) {

            case R.id.profile:
                startActivity(ProfileActivity.class);
                break;
            case R.id.notification:
                startActivity(NotificationSettings.class);
                break;

            case R.id.share_app:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Football League");
                    String shareMessage = "\nI am recommend you to download this app.\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.apnitor.football.league\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
                break;

            case R.id.logout:
                logout();
                break;
        }

        return true;
    }

    private void setupCustomNavigator() {
        // setup custom navigator
        KeepStateNavigator navigator = new KeepStateNavigator(this, navHostFragment.getChildFragmentManager(), R.id.nav_host_fragment);
        navController.getNavigatorProvider().addNavigator(navigator);
        navController.setGraph(R.navigation.main_nav_graph);
    }

    private void setupNavigation() {
        // setup bottom navigation
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        loginViewModel.logout();
        startActivity(new Intent(this, LoginActivity.class));
        getPrefHelper().logOut();
        LoginManager.getInstance().logOut();
        finish();
    }
}
