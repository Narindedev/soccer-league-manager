package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.InviteSearchAdapter;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.InviteTeamReq;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.databinding.ActivitySearchTeamBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;

public class SearchTeamActivity extends BaseActivity implements ListItemClickCallback {
    private ActivitySearchTeamBinding binding;
    private FollowingViewModel followingViewModel;
    private TeamViewModel teamViewModel;
    private String leagueId;
    String divisionName, seasonName, mSeasonId, mDivisionId,mDivisionName,leagueName;
    GetTeamSearchRes mSearchedInvitedTeam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_team);
        binding = bindContentView(R.layout.activity_search_team);
        followingViewModel = getViewModel(FollowingViewModel.class);
        teamViewModel = getViewModel(TeamViewModel.class);
        setupToolbar(binding.toolbar, "Search Team");
        Intent i = getIntent();
        if (i.getExtras() != null) {
            mSeasonId = i.getExtras().getString("seasonId");
            mDivisionId = i.getExtras().getString("divisionId");
            mDivisionName = i.getExtras().getString("divisionName");
            leagueId = i.getExtras().getString("leagueId");
            leagueName=i.getExtras().getString("leagueName");

        }
        teamViewModel.getTeamsLiveData().observe(this,res->{
            LeagueProfileActivity.isSeasonDivisionUpdated=true;

            /**
             * Update League object in League Profile Activity
             *
             */
            GetTeamRes invitedTeam = new GetTeamRes();
            invitedTeam.setTeamId(mSearchedInvitedTeam.getTeamId());
            invitedTeam.setTeamName(mSearchedInvitedTeam.getTeamName());
            invitedTeam.setImageUrl(mSearchedInvitedTeam.getImageUrl());


            LeagueTeam invitedLeagueTeam = new LeagueTeam();
            ArrayList<GetTeamRes> invitedTeams = new ArrayList<GetTeamRes>();
            ArrayList<GetTeamRes> teamIds = new ArrayList<GetTeamRes>();
            invitedTeams.add(invitedTeam);

            invitedLeagueTeam.setInvitedTeams(invitedTeams);
            invitedLeagueTeam.setTeamIds(teamIds);
            invitedLeagueTeam.setSeasonId(mSeasonId);
            invitedLeagueTeam.setDivisionId(mDivisionId);


            Intent inviteTeamIntent = new Intent();
            inviteTeamIntent.putExtra("selectedDivisionName",mDivisionName);
            inviteTeamIntent.putExtra("selectedDivisionId",mDivisionId);
            inviteTeamIntent.putExtra("InvitedLeagueTeam",invitedLeagueTeam);

            setResult(ResultCodes.TEAM_RESULT_CODE,inviteTeamIntent);

            finish();
        });
        teamViewModel.getGetAllTeamsSearchLiveData().observe(this, res -> {
            if (res.size()==0){
                binding.llSendInvitation.setVisibility(View.VISIBLE);
            }else{
                binding.llSendInvitation.setVisibility(View.GONE);

                 }
            binding.rvSearchList.setLayoutManager(new LinearLayoutManager(this));
            binding.rvSearchList.setAdapter(new InviteSearchAdapter(this, res, this));

        });

        binding.tvSendInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });
        binding.tvInviteTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });
    }


    private void sendInvitation(){
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Football League");
            String shareMessage = "\nI am recommend you to download this app and join league '"+leagueName+"'.\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.apnitor.football.league\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }
    public void onSearchClick(View view) {
        String teamName = binding.teamNameEt.getText().toString();
        String teamCity = binding.cityEt.getText().toString();
        String teamZipCode = binding.zipCodeEt.getText().toString();

        if (teamName.isEmpty()) {
            showToast("Please enter team name.");
        } else {
            hideKeyboard(this);
            teamViewModel.getAllTeamsSearch(new GetAllTeamReq(teamName, teamCity, teamZipCode));
        }

    }

    @Override
    public void onListItemClick(Object object) {
        mSearchedInvitedTeam = (GetTeamSearchRes) object;
        teamViewModel.getInviteTeam(new InviteTeamReq(leagueId, mSeasonId, mDivisionId, mSearchedInvitedTeam.getTeamId(),""));

    }
}
