package com.apnitor.football.league.fragment_binding_callback;

public interface FollowingFragmentBindingCallback {

    void onBack();

    void addLeague();

    void editLeague();

    void addTeam();

    void editTeam();

}
