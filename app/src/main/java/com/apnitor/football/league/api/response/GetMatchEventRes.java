package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetMatchEventRes implements Serializable {
    @Expose
    @SerializedName("matchEventId")
    public String matchEventId;

    @Expose
    @SerializedName("scheduleId")
    public String scheduleId;

    @Expose
    @SerializedName("leagueId")
    public String leagueId;

    @Expose
    @SerializedName("eventType")
    public String eventType;

    @Expose
    @SerializedName("eventTime")
    public String eventTime;

    @Expose
    @SerializedName("goalScoredBy")
    public GoalScoredBy goalScoredBy;

    @Expose
    @SerializedName("goalAssistedBy")
    public GoalScoredBy goalAssistedBy;

    @Expose
    @SerializedName("givenTo")
    public GoalScoredBy givenTo;

    @Expose
    @SerializedName("playerIn")
    public GoalScoredBy playerIn;

    @Expose
    @SerializedName("playerOut")
    public GoalScoredBy playerOut;

    @Expose
    @SerializedName("startDateTime")
    public String startDateTime;

    @Expose
    @SerializedName("teamId")
    public Team teamId;

    @Expose
    @SerializedName("scoredByTeam")
    public Team scoredByTeam;

    @Expose
    @SerializedName("concededByTeam")
    public Team concededByTeam;

    @Expose
    @SerializedName("endDateTime")
    public String endDateTime;

    @Expose
    @SerializedName("duration")
    public String duration;


    public class GoalScoredBy implements Serializable {

        @Expose
        @SerializedName("firstName")
        public String firstName;

        @Expose
        @SerializedName("lastName")
        public String lastName;

        @Expose
        @SerializedName("_id")
        public String id;

    }

    public class Team implements Serializable {

        @Expose
        @SerializedName("name")
        public String name;

        @Expose
        @SerializedName("imageUrl")
        public String imageUrl;

        @Expose
        @SerializedName("_id")
        public String id;

    }

}
