package com.apnitor.football.league.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.TeamLeagueRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class LeagueTeamListAdapter extends RecyclerView.Adapter<LeagueTeamListAdapter.ViewHolder> {

    private Context context;
    //    private List<String> teamList;
    List<GetTeamRes> teamList;

    public LeagueTeamListAdapter(Context context, List<GetTeamRes> teamList) {
        this.context = context;
        this.teamList = teamList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_league_team, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamRes model = teamList.get(position);

            if (model.getImageUrl() != null)
                Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_team)).into(holder.img);
            holder.tvName.setText(model.getTeamName());
            /*holder.ivFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((ImageView) v).getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_favorite_selected).getConstantState()) {
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_border);
                    } else
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_selected);
                }
            });*/

//            holder.tvTeamName2.setText(model.getTeamName());
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSerialNumber;
        TextView tvName;
        ImageView ivFavorite, img;

        ViewHolder(View itemView) {
            super(itemView);
            tvSerialNumber = (TextView) itemView.findViewById(R.id.tvSerialNumber);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            ivFavorite = (ImageView) itemView.findViewById(R.id.iv_favorite);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
        }
    }
}
