package com.apnitor.football.league.fragment_binding_callback;

public interface VerifyUserFragmentBindingCallback {


    void onDoneClick();

    void onResendCode();
}
