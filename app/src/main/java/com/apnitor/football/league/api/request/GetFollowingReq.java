package com.apnitor.football.league.api.request;

import com.google.gson.annotations.SerializedName;

public class GetFollowingReq {
    @SerializedName("type")
    private String followingType;

    public GetFollowingReq(String type) {
        this.followingType = type;
    }

    public String getTeamId() {
        return followingType;
    }

    public void setTeamId(String followingType) {
        this.followingType = followingType;
    }
}
