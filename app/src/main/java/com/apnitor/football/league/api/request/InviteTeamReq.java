package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InviteTeamReq {
    @Expose
    @SerializedName("leagueId")
    public String leagueId;

    @Expose
    @SerializedName("seasonId")
    public String seasonId;

    @Expose
    @SerializedName("divisionId")
    private String divisionId;

    @Expose
    @SerializedName("teamId")
    private String teamId;

    @Expose
    @SerializedName("notificationId")
    private String notificationId;

    public InviteTeamReq(String leagueId, String seasonId, String divisionId, String teamId,String notificationId) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
        this.teamId = teamId;
        this.notificationId = notificationId;
    }
}
