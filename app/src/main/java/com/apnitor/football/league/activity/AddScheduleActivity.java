package com.apnitor.football.league.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.adapter.SeasonListAdapter;
import com.apnitor.football.league.api.GetAddress;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.TeamLeagueRes;
import com.apnitor.football.league.databinding.ActivityAddScheduleBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AddScheduleActivity extends BaseActivity {

    AlertDialog mSeasonAlert;
    AlertDialog mDivisionAlert;
    String mSeasonId = "", mSeasonName = "", mDivisionId = "", mDivisionName = "", mTeamId1 = "", mTeamId2 = "", mLocation = "", mState, mCity, mZipCode, mDescription, mTeam1Name, mTeam2Name, mRefereeID = "";
    ArrayList<GetAddress> mGetAddress = new ArrayList<>();
    String mDate = "", mFinalDate = "", mTime = "";
    AddScheduleLeagueReq mAddScheduleLeagueReq;
    AddScheduleViewModel addScheduleViewModel;
    private ActivityAddScheduleBinding binding;
    private TeamViewModel teamViewModel;
    private SeasonViewModel seasonViewModel;
    private DivisionViewModel divisionViewModel;
    private AddScheduleActivity activity = this;
    private LeagueViewModel leagueViewModel;
    private String leagueId;
    private ArrayList<GetLeagueTeamsRes> teamList = new ArrayList<>();
    private List<GetSeasonRes> mSeasonsList = new ArrayList<>();
    private List<GetDivisionRes> mDivisionsList = new ArrayList<>();
    private SeasonListAdapter seasonListAdapter;
    private DivisionsListAdapter divisionsListAdapter;
    private Dialog seasonsDialog;
    private RecyclerView lvSeasonPopUpSelect;
    private AlertDialog.Builder seasonAlertDialog;
    private AlertDialog.Builder divisionsDialog;
    ListItemClickCallback divisionListItemClickCallback = new ListItemClickCallback() {
        @Override
        public void onListItemClick(Object object) {
            GetDivisionRes getSeasonRes = (GetDivisionRes) object;
            binding.tvSelectDivision.setText(getSeasonRes.getDivisionName());
            mDivisionId = getSeasonRes.getDivisionId();
            if (mDivisionAlert != null && mDivisionAlert.isShowing()) {
                divisionsDialog = null;
                mDivisionAlert.dismiss();
            }
        }
    };
    private RecyclerView lvDivisionPopUpSelect;
    private boolean isSeasonSelected;
    ListItemClickCallback seasonListItemClickCallback = new ListItemClickCallback() {
        @Override
        public void onListItemClick(Object object) {
            GetSeasonRes getSeasonRes = (GetSeasonRes) object;
            binding.tvSelectSeasonBtn.setText(getSeasonRes.getSeasonYear());
            mSeasonId = getSeasonRes.getSeasonId();
            isSeasonSelected = true;
            if (mSeasonAlert != null && mSeasonAlert.isShowing()) {
                seasonsDialog = null;
                mSeasonAlert.dismiss();
            }
        }
    };
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_add_schedule);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        seasonViewModel = getViewModel(SeasonViewModel.class);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        //
        Intent i = getIntent();
        if (i.getExtras() != null) {
            leagueId = i.getExtras().getString("leagueId");
            mSeasonId = i.getExtras().getString("seasonId");
            mSeasonName = i.getExtras().getString("seasonName");
            mDivisionId = i.getExtras().getString("divisionId");
            mDivisionName = i.getExtras().getString("divisionName");
            binding.tvSelectSeasonBtn.setText(mSeasonName);
            binding.tvSelectDivision.setText(mDivisionName);
        }

        seasonListAdapter = new SeasonListAdapter(activity, mSeasonsList, new ListItemClickCallback() {
            @Override
            public void onListItemClick(Object object) {
                GetSeasonRes getSeasonRes = (GetSeasonRes) object;
                binding.tvSelectSeasonBtn.setText(getSeasonRes.getSeasonYear());
                isSeasonSelected = true;
                mSeasonId = getSeasonRes.getSeasonId();
            }
        });

        divisionsListAdapter = new DivisionsListAdapter(activity, mDivisionsList, new ListItemClickCallback() {
            @Override
            public void onListItemClick(Object object) {
                GetDivisionRes division = (GetDivisionRes) object;
                binding.tvSelectSeasonBtn.setText(division.getDivisionName());
                teamViewModel.getLeagueTeams(new GetLeagueTeamsReq(leagueId, binding.tvSelectSeasonBtn.getText().toString(),
                        division.getDivisionName()));
                mDivisionId = division.getDivisionId();
            }
        });

        binding.selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate();
            }
        });

        binding.selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDate.isEmpty()) {
                    selectTime();
                } else {
                    showToast("Please select date first.");
                }
            }
        });

        seasonAlertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View content = inflater.inflate(R.layout.custom_dialog, null);
        seasonAlertDialog.setTitle("Select Season");

        binding.tvSelectSeasonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDivisionId.isEmpty()) {
                    seasonAlertDialog = new AlertDialog.Builder(AddScheduleActivity.this);
                    LayoutInflater inflater = LayoutInflater.from(AddScheduleActivity.this);
                    View content = inflater.inflate(R.layout.custom_dialog, null);
                    seasonAlertDialog.setView(content);
                    seasonAlertDialog.setTitle("Select Season");
                    lvSeasonPopUpSelect = (RecyclerView) content.findViewById(R.id.List);
                    lvSeasonPopUpSelect.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
//                lvSeasonPopUpSelect.setAdapter(seasonListAdapter);
                    lvSeasonPopUpSelect.setAdapter(new SeasonListAdapter(activity, mSeasonsList, seasonListItemClickCallback));
                    seasonAlertDialog.setCancelable(true);
//                seasonAlertDialog.show();
                    mSeasonAlert = seasonAlertDialog.show();
                } else {
                    showToast("Please select division first.");
                }
            }
        });

        binding.tvSelectDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                divisionsDialog = new AlertDialog.Builder(AddScheduleActivity.this);
                LayoutInflater inflater = LayoutInflater.from(AddScheduleActivity.this);
                View content = inflater.inflate(R.layout.custom_dialog, null);
                divisionsDialog.setView(content);
                divisionsDialog.setTitle("Select Division");
                lvDivisionPopUpSelect = (RecyclerView) content.findViewById(R.id.List);
                lvDivisionPopUpSelect.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
                lvDivisionPopUpSelect.setAdapter(new DivisionsListAdapter(activity, mDivisionsList, divisionListItemClickCallback));
                divisionsDialog.setCancelable(true);
                mDivisionAlert = divisionsDialog.show();

            }
        });

        binding.tvSelectTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDivisionId.isEmpty()) {
                    onSelectTeam(1);
                } else {
                    showToast("Please select season first.");
                }
            }
        });

        binding.tvSelectTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDivisionId.isEmpty()) {
                    onSelectTeam(2);
                } else {
                    showToast("Please select season first.");
                }
            }
        });

        setupToolbar(binding.toolbar, "Add Schedule");

        setupRecyclerView();

        observeApiResponse();
    }

    private void setupRecyclerView() {

    }

    private void observeApiResponse() {
        //  seasonViewModel.getSeasons(new GetLeagueSeasonsReq(leagueId));
        //   divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
        //
        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    mSeasonsList.clear();
                    mSeasonsList.addAll(seasons);
                    seasonListAdapter.notifyDataSetChanged();
                    if (mSeasonsList.size() == 1) {
                        autoSelectFirstSeason();
                    }
                });
        //
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    mDivisionsList.clear();
                    mDivisionsList.addAll(divisions);
                    divisionsListAdapter.notifyDataSetChanged();
                    if (mDivisionsList.size() == 1) {
                        autoSelectFirstDivision();
                    }
                });

        addScheduleViewModel.getUpdateDivisionResLiveData().observe(this, res -> {
            try {
                showToast("Match scheduled successfully");
                mAddScheduleLeagueReq.set_id(res.getScheduleId());
                TeamLeagueRes team1LeagueRes = new TeamLeagueRes();
                team1LeagueRes.teamId = mTeamId1;
                team1LeagueRes.teamName = mTeam1Name;
                mAddScheduleLeagueReq.setTeam1Detail(team1LeagueRes);
                TeamLeagueRes team2LeagueRes = new TeamLeagueRes();
                team2LeagueRes.teamId = mTeamId2;
                team2LeagueRes.teamName = mTeam2Name;
                mAddScheduleLeagueReq.setTeam2Detail(team2LeagueRes);
                mAddScheduleLeagueReq.setDate(mFinalDate);
                mAddScheduleLeagueReq.setDivisionId(mDivisionId);
                mAddScheduleLeagueReq.setSeasonId(mSeasonId);
                mAddScheduleLeagueReq.setLeagueId(leagueId);
                Intent mIntent = new Intent();
                mIntent.putExtra("AddLeagueSchedule", mAddScheduleLeagueReq);
                mIntent.putExtra("selectedSeasonId", mSeasonId);
                mIntent.putExtra("selectedDivsionId", mDivisionId);
                mIntent.putExtra("selectedDivsionName", mDivisionName);

                setResult(ResultCodes.ADD_Schedule_RESULT_CODE, mIntent);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //LeagueProfileActivity.isSeasonDivisionUpdated=true;
        });

        //
//        teamViewModel.getLeagueTeams().observe(this,
//                teams -> {
//                    teamList = teams;
//
//                });
        //


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onAddScheduleClick(View view) {
        if (mDivisionId.isEmpty()) {
            showToast("Please select division.");
        } else if (mSeasonId.isEmpty()) {
            showToast("Please select season.");
        } else if (mTeamId1.isEmpty()) {
            showToast("Please select team 1.");
        } else if (mTeamId2.isEmpty()) {
            showToast("Please select team 2.");
        } else if (mDate.isEmpty()) {
            showToast("Please select date.");
        } else if (mTime.isEmpty()) {
            showToast("Please select time.");
        } else if (mLocation.isEmpty()) {
            showToast("Please select location.");
        } else if (mRefereeID.isEmpty()) {
            showToast("Please select referee.");
        } else {
            mAddScheduleLeagueReq = new AddScheduleLeagueReq(leagueId, null, null, mSeasonId, mDivisionId,
                    mDate, mTime, mTeamId1, mTeamId2, mLocation, mState, mCity, mZipCode, mDescription, mTeam1Name, mTeam2Name, mRefereeID);
            addScheduleViewModel.addUpdateSchedule(mAddScheduleLeagueReq);
        }
    }

    void onSelectTeam(int team) {
        Intent i = new Intent(this, PickSeasonTeamActivity.class);
        i.putExtra("leagueId", leagueId);
        i.putExtra("seasonId", mSeasonId);
        i.putExtra("divisionId", mDivisionId);
        if (team == 1) {
            i.putExtra("teamId", mTeamId2);
            i.putExtra("TEAM", 1);
            startActivityForResult(i, 111);
        } else if (team == 2) {
            i.putExtra("teamId", mTeamId1);
            i.putExtra("TEAM", 2);
            startActivityForResult(i, 112);
        }
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }

    public void onLocationClick(View view) {
        String latitude = "";
        String longitude = "";
        Bundle mBundle = new Bundle();
        if (mGetAddress != null && mGetAddress.size() > 0) {
            if (mGetAddress.get(0).latitude != null) {
                latitude = mGetAddress.get(0).latitude.toString();
            }
            if (mGetAddress.get(0).longitude != null) {
                longitude = mGetAddress.get(0).longitude.toString();
            }
            mBundle.putString("prevActivity", "CreateJob");
            mBundle.putString("latitude", latitude);
            mBundle.putString("longitude", longitude);
        }
        startActivityForResultWithExtra(MapsActivity.class, 101, mBundle);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }

    public void onRefereeClick(View view) {
        Bundle mBundle = new Bundle();
        startActivityForResultWithExtra(RefereeListActivity.class, 201, mBundle);
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == 101) {
            if (data != null) {
                try {
                    Bundle bundle = data.getExtras();
                    mGetAddress.clear();
                    if (bundle != null) {
                        GetAddress getAddress = (GetAddress) bundle.getParcelable("Address");
                        mGetAddress.add(getAddress);
                        //
                        binding.selectLocation.setText(getAddress.street);
                        //
                        mLocation = getAddress.street;
                        mState = getAddress.state;
                        mCity = getAddress.city;
                        mZipCode = getAddress.zipCode + "";
                        mDescription = " This is sample match description";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        //
        if (requestCode == 111) {
            if (data != null) {
                try {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        GetTeamRes getAddress = (GetTeamRes) bundle.get("name");
                        binding.tvSelectTeam1.setText(getAddress.getTeamName());
                        mTeamId1 = getAddress.getTeamId();
                        mTeam1Name = getAddress.getTeamName();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //
        if (requestCode == 112) {
            if (data != null) {
                try {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        GetTeamRes getAddress = (GetTeamRes) bundle.get("name");
                        binding.tvSelectTeam2.setText(getAddress.getTeamName());
                        mTeamId2 = getAddress.getTeamId();
                        mTeam2Name = getAddress.getTeamName();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        //
        if (requestCode == 201) {
            if (data != null) {
                try {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        GetRefereeRes referee = (GetRefereeRes) bundle.get("referee_detail");
                        binding.selectReferee.setText(referee.getFirstName() + " " + referee.getLastName());
                        mRefereeID = referee.getId();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }


    private void selectDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int yearMonth = monthOfYear + 1;
                        String month = "";
                        String day = "";
                        if (yearMonth < 10) {
                            month = "0" + yearMonth;
                        } else {
                            month = "" + yearMonth;
                        }

                        if (dayOfMonth < 10) {
                            day = "0" + dayOfMonth;
                        } else {
                            day = "" + dayOfMonth;
                        }

                        mDate = day + "-" + month + "-" + year;
                        mFinalDate = year + "-" + month + "-" + day;
                        binding.selectDate.setText(mDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void selectTime() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        String date = mDay + "-" + (mMonth + 1) + "-" + mYear;
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        if (date.equals(mDate)) {
                            Calendar datetime = Calendar.getInstance();
                            Calendar c = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);
                            if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                                //it's after current
                                mTime = getTime(hourOfDay, minute);
                                binding.selectTime.setText(mTime);
                            } else {
                                //it's before current'
                                showToast("Invalid Time");

                                //  Toast.makeText(getApplicationContext(), "Invalid Time", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            mTime = getTime(hourOfDay, minute);
                            binding.selectTime.setText(mTime);
                        }

                    }
                }, mHour, mMinute, true);
        timePickerDialog.show();
    }

    private String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }


    void autoSelectFirstSeason() {
        try {
            GetSeasonRes getSeasonRes = mSeasonsList.get(0);
            binding.tvSelectSeasonBtn.setText(getSeasonRes.getSeasonYear());
            isSeasonSelected = true;
            mSeasonId = getSeasonRes.getSeasonId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void autoSelectFirstDivision() {
        try {
            GetDivisionRes getSeasonRes = mDivisionsList.get(0);
            binding.tvSelectDivision.setText(getSeasonRes.getDivisionName());
            mDivisionId = getSeasonRes.getDivisionId();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
