package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.LeagueSchedulesListAdapter;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.SelectedSeasonDivisionRes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueSchedulesFragment extends BaseFragment implements Observer {
    public static GetLeagueRes mGetLeagueRes;
    public static String mSeasonId, mDivisionId, mSeasonName, mDivisionName;
    View view;
    TextView mTvSeason;
    TextView mTvDivision;
    String mSeasonStatus = "";
    String LOG_TAG = "SeasonsResultFragment";
    LeagueSchedulesListAdapter mLeagueSchedulesListAdapter;
    RecyclerView mRvResults;
    private boolean loading, isMoreAvailable;
    private LinearLayoutManager mLayoutManager;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private SeasonViewModel seasonViewModel;
    private DivisionViewModel divisionViewModel;
    private LeagueViewModel leagueViewModel;
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private List<AddScheduleLeagueReq> leagueScheduleList = new ArrayList<>();
    private List<AddScheduleLeagueReq> filterList = new ArrayList<>();
    private LinearLayout mEmptySchedules;
    private List<String> mSeasonNameList = new ArrayList<>();
    private List<String> mDivisionNameList = new ArrayList<>();//
    private List<GetDivisionRes> mDivisions = new ArrayList<>();
    private List<String> mWeekNameList = new ArrayList<>();
    private List<GetDivisionRes> mDivisionsList = new ArrayList<>();

    public static LeagueSchedulesFragment newInstance(GetLeagueRes leagueRes) {
        mGetLeagueRes = leagueRes;
        LeagueSchedulesFragment fragment = new LeagueSchedulesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_league_schedules, container, false);


        //viewmodels
        seasonViewModel = getViewModel(SeasonViewModel.class);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);


        //
        //
        mTvDivision = view.findViewById(R.id.tvSelectDivisionBtn);
        mTvSeason = view.findViewById(R.id.tvSelectSeasonBtn);


        //
        mEmptySchedules = view.findViewById(R.id.llEmptySchedule);
        mRvResults = view.findViewById(R.id.rvLeagueSchedules);
        mLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mRvResults.setLayoutManager(mLayoutManager);
        mRvResults.setNestedScrollingEnabled(false);
        //
        mTvSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSeasonDialog();
            }
        });

        //
        mTvDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDivisionDialog();
            }
        });
        //

        /*Observe Data from get league detail*/
        //observeLeagueData();
        observeNewData();


        leagueViewModel.getLeaguScheduleResLiveData().observe(this, addScheduleLeagueReq -> {
            if (((LeagueProfileActivity) getActivity()).scheduleSeasonId.isEmpty()) {
                leagueScheduleList.clear();
                leagueScheduleList.addAll(addScheduleLeagueReq);
                mLeagueSchedulesListAdapter = new LeagueSchedulesListAdapter(getContext(), leagueScheduleList);
                mRvResults.setAdapter(mLeagueSchedulesListAdapter);
                sortList();
                if (leagueScheduleList != null && leagueScheduleList.size() > 0) {
                    mEmptySchedules.setVisibility(View.GONE);
                    // sortList();
                } else {
                    mEmptySchedules.setVisibility(View.VISIBLE);
                }

            }
            ((LeagueProfileActivity) getActivity()).scheduleSeasonId = "";
        });
        return view;
    }


    void showSeasonDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Season");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < mSeasonNameList.size(); i++) {
            arrayAdapter.add(mSeasonNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mSeasonId = seasonsList.get(which).getSeasonId();
                mSeasonName = seasonsList.get(which).getSeasonYear();
                mTvSeason.setText(strName);

                List<SelectedSeasonDivisionRes> seasonDivisionRes = ((LeagueProfileActivity) getActivity()).selectedSeasonDivision(mDivisionId, mSeasonId, which);
                if (seasonDivisionRes != null && seasonDivisionRes.size() > 0) {
                    mTvDivision.setText(seasonDivisionRes.get(0).getDivisionName());
                    mDivisionName = seasonDivisionRes.get(0).getDivisionName();
                    mDivisionId = seasonDivisionRes.get(0).getDivisionId();
                }

                if (mSeasonId != null && mDivisionId != null) {
                    ((LeagueProfileActivity) getActivity()).scheduleSeasonId = "";
                    ((LeagueProfileActivity) getActivity()).scheduleDivisionId = "";
                    ((LeagueProfileActivity) getActivity()).scheduleDivisionName = "";
                    leagueViewModel.getLeagueSchedule(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                }
            }
        });
        builderSingle.show();
    }

    void showDivisionDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Division");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        for (int i = 0; i < mDivisionNameList.size(); i++) {
            arrayAdapter.add(mDivisionNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvDivision.setText(strName);
                mDivisionId = mDivisionsList.get(which).getDivisionId();
                mDivisionName = mDivisionsList.get(which).getDivisionName();

                List<GetSeasonRes> seasonResList = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
                if (seasonResList != null && seasonResList.size() > 0) {
                    mSeasonId = seasonResList.get(0).getSeasonId();
                    mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                    mSeasonName = seasonResList.get(0).getSeasonYear();
                }
                if (mSeasonId != null && mDivisionId != null) {
                    ((LeagueProfileActivity) getActivity()).scheduleSeasonId = "";
                    ((LeagueProfileActivity) getActivity()).scheduleDivisionId = "";
                    ((LeagueProfileActivity) getActivity()).scheduleDivisionName = "";
                    leagueViewModel.getLeagueSchedule(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                }
            }
        });
        builderSingle.show();
    }

    private void observeLeagueData() {

        /*Show  Season Data*/
        try {
            mDivisionId = null;
            mSeasonId = null;
            seasonsList = mGetLeagueRes.seasonList;
            Log.d(LOG_TAG, "Seasons are " + seasonsList.toString());
            mSeasonNameList.clear();
            for (int i = 0; i < mGetLeagueRes.seasonList.size(); i++) {
                mSeasonNameList.add(mGetLeagueRes.seasonList.get(i).getSeasonYear());
            }
            //
            if (mSeasonNameList.size() > 0) {
                mTvSeason.setVisibility(View.VISIBLE);
                mTvSeason.setText(mSeasonNameList.get(0));
                mSeasonId = seasonsList.get(0).getSeasonId();
                mSeasonName = seasonsList.get(0).getSeasonYear();
                //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
            } else {
                mTvSeason.setVisibility(View.GONE);
            }


            /*Show  Division Data*/
            mDivisionsList = mGetLeagueRes.divisionList;
            Log.e(LOG_TAG, "Seasons are " + mDivisionNameList.toString());
            mDivisionNameList.clear();
            for (int i = 0; i < mGetLeagueRes.divisionList.size(); i++) {
                mDivisionNameList.add(mGetLeagueRes.divisionList.get(i).getDivisionName());
            }
            //
            if (mDivisionNameList.size() > 0) {
                mTvDivision.setVisibility(View.VISIBLE);
                mTvDivision.setText(mDivisionNameList.get(0));
                mDivisionId = mDivisionsList.get(0).getDivisionId();
                mDivisionName = mDivisionsList.get(0).getDivisionName();
            } else {
                mTvDivision.setVisibility(View.GONE);
            }
            //
            if (mSeasonId != null && mDivisionId != null) {


                leagueScheduleList = mGetLeagueRes.getScheduleList();
                if (leagueScheduleList != null && leagueScheduleList.size() > 0) {
                    mEmptySchedules.setVisibility(View.GONE);
                    mLeagueSchedulesListAdapter = new LeagueSchedulesListAdapter(getContext(), leagueScheduleList);
                    mRvResults.setAdapter(mLeagueSchedulesListAdapter);

                    sortList();
                } else {
                    mEmptySchedules.setVisibility(View.VISIBLE);
                }
            } else {
                mEmptySchedules.setVisibility(View.VISIBLE);
            }


            leagueViewModel.getLeaguScheduleResLiveData().observe(this, addScheduleLeagueReq -> {

                leagueScheduleList = addScheduleLeagueReq;
                mLeagueSchedulesListAdapter = new LeagueSchedulesListAdapter(getContext(), leagueScheduleList);
                mRvResults.setAdapter(mLeagueSchedulesListAdapter);
                sortList();
                if (leagueScheduleList != null && leagueScheduleList.size() > 0) {
                    mEmptySchedules.setVisibility(View.GONE);
                    // sortList();
                } else {
                    mEmptySchedules.setVisibility(View.VISIBLE);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void observeNewData() {
        try {
            mDivisionId = null;
            mSeasonId = null;
            seasonsList = mGetLeagueRes.seasonList;
            Log.d(LOG_TAG, "Seasons are " + seasonsList.toString());
            mSeasonNameList.clear();
            for (int i = 0; i < mGetLeagueRes.seasonList.size(); i++) {
                mSeasonNameList.add(mGetLeagueRes.seasonList.get(i).getSeasonYear());
            }
            //



            /*Show  Division Data*/
            mDivisionsList = mGetLeagueRes.divisionList;
            Log.e(LOG_TAG, "Seasons are " + mDivisionNameList.toString());
            mDivisionNameList.clear();
            for (int i = 0; i < mGetLeagueRes.divisionList.size(); i++) {
                mDivisionNameList.add(mGetLeagueRes.divisionList.get(i).getDivisionName());
            }



        if (mSeasonNameList.size() > 0) {
            mTvSeason.setVisibility(View.VISIBLE);
            mTvSeason.setText(mSeasonNameList.get(0));
            mSeasonId = seasonsList.get(0).getSeasonId();
            mSeasonName = seasonsList.get(0).getSeasonYear();
            //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
        } else {
            mTvSeason.setVisibility(View.GONE);
        }
            //
        if (mDivisionNameList.size() > 0) {
            mTvDivision.setVisibility(View.VISIBLE);
            mTvDivision.setText(mDivisionNameList.get(0));
            mDivisionId = mDivisionsList.get(0).getDivisionId();
            mDivisionName = mDivisionsList.get(0).getDivisionName();

        } else {
            mTvDivision.setVisibility(View.GONE);
        }


           /* List<SelectedSeasonDivisionRes> seasonDivisionResList = ((LeagueProfileActivity) getActivity()).getActiveSeasonDivision();
            if (seasonDivisionResList != null && seasonDivisionResList.size() > 0) {
                mTvDivision.setVisibility(View.VISIBLE);
                mTvSeason.setVisibility(View.VISIBLE);
                SelectedSeasonDivisionRes res = seasonDivisionResList.get(0);
                mTvSeason.setText(res.getSesonName());
                mSeasonName = res.getSesonName();
                mSeasonStatus = res.getSeasonStatus();

                mSeasonId = res.getSesonId();
                mDivisionName = res.getDivisionName();
                mTvDivision.setText(res.getDivisionName());
                mDivisionId = res.getDivisionId();
            } else {
                if (mDivisionNameList.size() > 0) {
                    mTvDivision.setVisibility(View.VISIBLE);
                    mTvDivision.setText(mDivisionNameList.get(0));
                    mDivisionId = mDivisionsList.get(0).getDivisionId();
                    mDivisionName = mDivisionsList.get(0).getDivisionName();
                } else {
                    mTvDivision.setVisibility(View.GONE);
                }
                if (mDivisionId != null) {
                    List<GetSeasonRes> seasonResList = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
                    if (seasonResList != null && seasonResList.size() > 0) {
                        mTvSeason.setVisibility(View.VISIBLE);
                        mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                        mSeasonStatus = seasonResList.get(0).getSeasonStatus();
                        mSeasonId = seasonResList.get(0).getSeasonId();
                        mSeasonName = seasonResList.get(0).getSeasonYear();
                        //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                    } else {
                        mTvSeason.setVisibility(View.GONE);
                    }
                }
            }*/
if((!((LeagueProfileActivity) getActivity()).scheduleDivisionId.isEmpty())){
    mDivisionId = ((LeagueProfileActivity) getActivity()).scheduleDivisionId;
    mDivisionName=((LeagueProfileActivity) getActivity()).scheduleDivisionName;
    mTvDivision.setText(mDivisionName);
}

            List<GetSeasonRes> selectedivisonSeason = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
            if (selectedivisonSeason != null) {
                String newSeasonId = ((LeagueProfileActivity) getActivity()).scheduleSeasonId;
                for (GetSeasonRes res : seasonsList) {
                    if (res.getSeasonId().equals(newSeasonId)) {
                        mSeasonId = newSeasonId;
                        mTvSeason.setVisibility(View.VISIBLE);
                        mTvSeason.setText(res.getSeasonYear());
                        mSeasonStatus = res.getSeasonStatus();
                        mSeasonName = res.getSeasonYear();
                    }
                }

            }

            if (mSeasonId != null && mDivisionId != null) {
                leagueScheduleList.clear();

                leagueScheduleList.addAll(getSchedules(mDivisionId, mSeasonId));
                if (leagueScheduleList != null && leagueScheduleList.size() > 0) {
                    mEmptySchedules.setVisibility(View.GONE);
                    mLeagueSchedulesListAdapter = new LeagueSchedulesListAdapter(getContext(), leagueScheduleList);
                    mRvResults.setAdapter(mLeagueSchedulesListAdapter);
                    sortList();
                } else {
                    mEmptySchedules.setVisibility(View.VISIBLE);
                    if (mSeasonStatus.equals("Not Started")) {
                        if (LeagueProfileActivity.isLeagueScheduleLoadFIrtsTime) {
                            LeagueProfileActivity.isLeagueScheduleLoadFIrtsTime = false;
                            leagueViewModel.getLeagueSchedule(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId));
                        }
                    }
                }
            } else {
                mEmptySchedules.setVisibility(View.VISIBLE);
            }


        } catch (Exception e) {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void update(Observable observable, Object data) {

        Observable o = observable;
        Object obj = data;
        leagueScheduleList = ((LeagueProfileActivity) getActivity()).mGetLeagueRes.getScheduleList();


        // observeLeagueData();
        observeNewData();
    }


    public List<AddScheduleLeagueReq> getSchedules(String divisionId, String seasonId) {

        List<AddScheduleLeagueReq> scheduleList = mGetLeagueRes.getScheduleList();

        List<AddScheduleLeagueReq> selectedList = new ArrayList<AddScheduleLeagueReq>();

        if (scheduleList != null && scheduleList.size() > 0) {
            for (AddScheduleLeagueReq leagueSchedule : scheduleList) {
                String scheduleDivisionId = leagueSchedule.getDivisionId().trim();
                String scheduleSeasonId = leagueSchedule.getSeasonId().trim();
                if (scheduleDivisionId.equals(divisionId) && scheduleSeasonId.equals(seasonId)) {
                    selectedList.add(leagueSchedule);
                }
            }
        }
        return selectedList;
    }

    private void sortList() {
        for (int i = 0; i < leagueScheduleList.size(); i++) {
            leagueScheduleList.get(i).setDate(UIUtility.getDateFormat(leagueScheduleList.get(i).getDate()));
        }
        Collections.sort(leagueScheduleList, new Comparator<AddScheduleLeagueReq>() {
            public int compare(AddScheduleLeagueReq obj1, AddScheduleLeagueReq obj2) {
                // ## Ascending order
                return obj1.getDate().compareToIgnoreCase(obj2.getDate()); // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values

                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            }
        });
        mLeagueSchedulesListAdapter.notifyDataSetChanged();

    }

    void filterSchedulebyDate(List<AddScheduleLeagueReq> teams) {
        filterList.clear();

        int size = teams.size();
        for (int i = 0; i < size; i++) {
            teams.get(i).setDate(UIUtility.getFormattedDate(teams.get(i).getDate()));
            if (UIUtility.checkCurrentDate(teams.get(i).getDate()))
                filterList.add(teams.get(i));
        }
        leagueScheduleList = filterList;
        mLeagueSchedulesListAdapter = new LeagueSchedulesListAdapter(getContext(), leagueScheduleList);
        mRvResults.setAdapter(mLeagueSchedulesListAdapter);
    }


    private void onLoadMore() {
        mRvResults.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                if (isMoreAvailable) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = false;
                        //Action here
                        String date = leagueScheduleList.get(leagueScheduleList.size() - 1).getDate();
                        leagueViewModel.getLeagueSchedule(new GetLeagueScheduleReq(mGetLeagueRes.getLeagueId(), mSeasonId, mDivisionId, date));

                    }
                }
            }
        });
    }
}
