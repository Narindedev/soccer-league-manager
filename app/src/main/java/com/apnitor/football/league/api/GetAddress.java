package com.apnitor.football.league.api;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAddress implements Parcelable {
    public GetAddress() {

    }

    @Expose
    @SerializedName("houseNo")
    public String houseNo;

    @Expose
    @SerializedName("street")
    public String street;

    @Expose
    @SerializedName("city")
    public String city;

    @Expose
    @SerializedName("state")
    public String state;

    @Expose
    @SerializedName("zipCode")
    public Integer zipCode;

    @Expose
    @SerializedName("country")
    public String country;

    @Expose
    @SerializedName("latitude")
    public Double latitude;

    @Expose
    @SerializedName("longitude")
    public Double longitude;


    protected GetAddress(Parcel in) {
        houseNo = in.readString();
        street = in.readString();
        city = in.readString();
        state = in.readString();
        if (in.readByte() == 0) {
            zipCode = null;
        } else {
            zipCode = in.readInt();
        }
        country = in.readString();
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
    }

    public static final Creator<GetAddress> CREATOR = new Creator<GetAddress>() {
        @Override
        public GetAddress createFromParcel(Parcel in) {
            return new GetAddress(in);
        }

        @Override
        public GetAddress[] newArray(int size) {
            return new GetAddress[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(houseNo);
        dest.writeString(street);
        dest.writeString(city);
        dest.writeString(state);
        if (zipCode == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(zipCode);
        }
        dest.writeString(country);
        if (latitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latitude);
        }
        if (longitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(longitude);
        }
    }
}
