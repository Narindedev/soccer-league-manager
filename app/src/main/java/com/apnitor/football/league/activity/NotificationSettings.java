package com.apnitor.football.league.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;

import com.apnitor.football.league.R;

public class NotificationSettings extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_settings);
        Toolbar mToolbar=(Toolbar)findViewById(R.id.toolbar);
        setupToolbar(mToolbar,"Notification Setting");
    }
}
