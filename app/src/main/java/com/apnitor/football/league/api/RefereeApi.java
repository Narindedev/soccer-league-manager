package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetMatchEventsReq;
import com.apnitor.football.league.api.request.GetRefereeScheduleReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateMatchEventRes;
import com.apnitor.football.league.api.response.UpdateTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RefereeApi {

    @GET("getAllReferees")
    Single<BaseRes<ArrayList<GetRefereeRes>>> getAllReferee();

    @POST("getRefereeSchedule")
    Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getRefereeSchedule(@Body GetRefereeScheduleReq getLeagueTeamsReq);

    @POST("updateMatchEvents")
    Single<BaseRes<UpdateMatchEventRes>> updateMatchEvents(@Body UpdateMatchEventReq matchEventReq);

    @POST("getMatchEvents")
    Single<BaseRes<ArrayList<GetMatchEventRes>>> getMatchEvents(@Body GetMatchEventsReq matchEventReq);

    /*Get All Schedule*/
    @GET("getFollowingSchedules")
    Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getFollowingSchedules();

}
