package com.apnitor.football.league.activity.referee;

import androidx.annotation.Nullable;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.AddNewPlayerActivity;
import com.apnitor.football.league.activity.BaseActivity;
import com.apnitor.football.league.activity.SelectLineUpActivity;
import com.apnitor.football.league.activity.ViewPlayerActivity;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivityMatchDetailBinding;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.Calendar;

public class MatchDetailActivity extends BaseActivity {
    private ActivityMatchDetailBinding binding;
    private int timeInMilis = 0;
    public Calendar calendar;
    private RefereeViewModel refereeViewModel;
    private String startTime;
    private String endTime;
    private String eventType = "", savedEventType = "";
    private GetTeamScheduleRes teamScheduleRes;
    public boolean isStarted = false;
    private String mSeasonId, mDivisionId, mLeagueId, scheduleId, eventId = "";
    boolean isMatchStarted;
    long mStartTime, resumeTime = 0;
    String LOG_TAG = "MatchDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_match_detail);
        setupToolbar(binding.toolbar, "Match");
        //
        disableAllButtons();
        //
        Typeface font = Typeface.createFromAsset(getAssets(), "DS_DIGI.TTF");
        binding.tvTime1Countdown.setTypeface(font, Typeface.NORMAL);
        binding.tvTime1Countdown.setTextSize(25);
        //
        refereeViewModel = getViewModel(RefereeViewModel.class);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            teamScheduleRes = (GetTeamScheduleRes) b.getSerializable("match_detail");
            if (teamScheduleRes != null) {
                mSeasonId = teamScheduleRes.getSeasonId();
                mDivisionId = teamScheduleRes.getDivisionId();
                mLeagueId = teamScheduleRes.getLeagueId();
                scheduleId = teamScheduleRes.getScheduleId();
            }
        }

        observeApiResponse();
    }

    private void observeApiResponse() {
        refereeViewModel.updateMatchEventsLiveData().observe(this, res -> {
            eventId = res.matchEventId;
            PreferenceHandler.writeString(this, PreferenceHandler.PREF_KEY_MATCH_STATUS, eventType);
            updateUIEvent();
        });
    }

    public void onGoalClick(View view) {
        checkIfMatchStartedorRunning();
        //
        String currentTime = binding.tvTime1Countdown.getText().toString();
        GetTeamScheduleRes model = teamScheduleRes;
        Intent intent = new Intent(this, GoalDetailActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", model);
        b.putString("matchEventId", eventId);
        b.putString("title", "Goal");
        b.putString("eventTime", currentTime);
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in,
                R.anim.anim_left_out);
    }

    public void onRedCardClick(View view) {
        checkIfMatchStartedorRunning();
        //
        String currentTime = binding.tvTime1Countdown.getText().toString();
        GetTeamScheduleRes model = teamScheduleRes;
        Intent intent = new Intent(this, GoalDetailActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", model);
        b.putString("matchEventId", eventId);
        b.putString("title", "RedCard");
        b.putString("eventTime", currentTime);
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in,
                R.anim.anim_left_out);
    }

    public void onYellowCardClick(View view) {
        checkIfMatchStartedorRunning();
        //
        String currentTime = binding.tvTime1Countdown.getText().toString();
        GetTeamScheduleRes model = teamScheduleRes;
        Intent intent = new Intent(this, GoalDetailActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", model);
        b.putString("matchEventId", eventId);
        b.putString("title", "YellowCard");
        b.putString("eventTime", currentTime);
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in,
                R.anim.anim_left_out);
    }


    public void onSubstitutionClick(View view) {
        checkIfMatchStartedorRunning();
        //
        String currentTime = binding.tvTime1Countdown.getText().toString();
        GetTeamScheduleRes model = teamScheduleRes;
        Intent intent = new Intent(this, SubstitutionActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", model);
        b.putString("matchEventId", eventId);
        b.putString("title", "Substitution");
        b.putString("eventTime", currentTime);
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in,
                R.anim.anim_left_out);
    }

    public void onAddPlayerClick(View view) {
        Intent intent = new Intent(this, AddNewPlayerActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", teamScheduleRes);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void onStartMatch(View view) {
        if (binding.startText.getText().toString().equals("End First Half")) {
            binding.startImage.setImageResource(R.drawable.ic_start_match);
            binding.startText.setText("Start Second Half");
            eventType = "EndFirstHalf";
            disableAllButtons();
        } else if (binding.startText.getText().toString().equals("End Match")) {
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("Match Ended");
            eventType = "EndMatch";
            stopCountdownTimer();
            disableAllButtons();
        } else if (binding.startText.getText().toString().equals("Start Match")) {
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("End First Half");
            eventType = "StartMatch";
            endTime = "";
            saveCurrentTime();
            enableAllButtons();
        } else if (binding.startText.getText().toString().equals("Start Second Half")) {
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("End Match");
            eventType = "StartSecondHalf";
            enableAllButtons();
        } else if (binding.startText.getText().toString().equals("Match Ended")) {
            Toast.makeText(this, "Match has ended", Toast.LENGTH_SHORT).show();
        }
        startTime = binding.tvTime1Countdown.getText().toString();
        endTime = binding.tvTime1Countdown.getText().toString();
        Log.e("eventtype", "" + eventType);
        //
        callApi();
    }

    private void callApi() {
        refereeViewModel.updateMatchEvents(new UpdateMatchEventReq(scheduleId, mLeagueId, eventType, binding.tvTime1Countdown.getText().toString()));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 101) {
            if (data != null) {
                eventId = data.getStringExtra("eventId");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    void checkIfMatchStartedorRunning() {
        if (!isMatchStarted) {
            Toast.makeText(this, "Please start match first.", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    void startCountdownTimer() {
        mStartTime = SystemClock.elapsedRealtime() - resumeTime;
        binding.tvTime1Countdown.setBase(mStartTime);
        binding.tvTime1Countdown.start();
    }

    void stopCountdownTimer() {
        mStartTime = SystemClock.elapsedRealtime() - resumeTime;
        binding.tvTime1Countdown.setBase(mStartTime);
        binding.tvTime1Countdown.stop();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    void saveCurrentTime() {
        PreferenceHandler.writeLong(this, PreferenceHandler.PREF_KEY_TIME, System.currentTimeMillis());
    }

    void clearSavedTime() {
        PreferenceHandler.writeLong(this, PreferenceHandler.PREF_KEY_TIME, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceHandler.readLong(this, PreferenceHandler.PREF_KEY_TIME, 0) == 0) {
            resumeTime = 0;
            saveCurrentTime();
        } else
            resumeTime = System.currentTimeMillis() - PreferenceHandler.readLong(this, PreferenceHandler.PREF_KEY_TIME, 0);
        //
        updateUIEvent();
    }


    void updateUIEvent() {
        savedEventType = PreferenceHandler.readString(this, PreferenceHandler.PREF_KEY_MATCH_STATUS, "");
        Log.d(LOG_TAG, " Saved Event Type is " + savedEventType);
        if (savedEventType.equalsIgnoreCase("EndFirstHalf")) {
            isMatchStarted = false;
            //
            binding.startImage.setImageResource(R.drawable.ic_start_match);
            binding.startText.setText("Start Second Half");
        } else if (savedEventType.equalsIgnoreCase("EndMatch")) {
            isMatchStarted = false;
            //
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("Match Ended");
        } else if (savedEventType.equalsIgnoreCase("StartMatch")) {
            isMatchStarted = true;
            //
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("End First Half");
            //
            isMatchStarted = true;
            //
            startCountdownTimer();
        } else if (savedEventType.equalsIgnoreCase("StartSecondHalf")) {
            isMatchStarted = true;
            //
            binding.startImage.setImageResource(R.drawable.ic_end_match);
            binding.startText.setText("End Match");
            //
            startCountdownTimer();
        } else if (savedEventType.equalsIgnoreCase("")) {
            binding.tvTime1Countdown.stop();
            binding.tvTime1Countdown.setText("00:00");
            //08041122556/08040973122
        }
    }

    void disableAllButtons(){
        binding.cardGoal.setEnabled(false);
        binding.cardYellowCard.setEnabled(false);
        binding.cardRedCard.setEnabled(false);
        binding.cardAddPlayer.setEnabled(false);
        binding.cardTeal.setEnabled(false);
        //
        binding.cardGoal.setCardBackgroundColor(getResources().getColor(R.color.result_score_bg));
        binding.cardYellowCard.setCardBackgroundColor(getResources().getColor(R.color.result_score_bg));
        binding.cardRedCard.setCardBackgroundColor(getResources().getColor(R.color.result_score_bg));
        binding.cardAddPlayer.setCardBackgroundColor(getResources().getColor(R.color.result_score_bg));
        binding.cardTeal.setCardBackgroundColor(getResources().getColor(R.color.result_score_bg));
    }

    void enableAllButtons(){
        binding.cardGoal.setEnabled(true);
        binding.cardYellowCard.setEnabled(true);
        binding.cardRedCard.setEnabled(true);
        binding.cardAddPlayer.setEnabled(true);
        binding.cardTeal.setEnabled(true);
        //
        binding.cardGoal.setCardBackgroundColor(getResources().getColor(R.color.goalColor));
        binding.cardYellowCard.setCardBackgroundColor(getResources().getColor(R.color.yellowColor));
        binding.cardRedCard.setCardBackgroundColor(getResources().getColor(R.color.redCardColor));
        binding.cardAddPlayer.setCardBackgroundColor(getResources().getColor(R.color.facebook));
        binding.cardTeal.setCardBackgroundColor(getResources().getColor(R.color.login_button_border_1));
    }
}
