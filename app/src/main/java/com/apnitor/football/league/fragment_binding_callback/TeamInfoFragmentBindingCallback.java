package com.apnitor.football.league.fragment_binding_callback;

public interface TeamInfoFragmentBindingCallback {
    void showInfo();

    void showNews();
}
