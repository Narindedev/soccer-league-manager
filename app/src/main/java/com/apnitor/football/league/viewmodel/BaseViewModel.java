package com.apnitor.football.league.viewmodel;

import android.app.Application;
import android.widget.Toast;

import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.pojo.ApiError;
import com.apnitor.football.league.pojo.ApiProgress;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class BaseViewModel extends AndroidViewModel {

    private MutableLiveData<ApiProgress> apiProgressLiveData = new MediatorLiveData<>();
    private MutableLiveData<ApiError> apiErrorLiveData = new MediatorLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Application application;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public LiveData<ApiProgress> getApiProgressLiveData() {
        return apiProgressLiveData;
    }

    public LiveData<ApiError> getApiErrorLiveData() {
        return apiErrorLiveData;
    }

    <T> void consumeApi(Single<BaseRes<T>> apiCall, Consumer<T> consumer) {

        compositeDisposable.add(apiCall
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> apiProgressLiveData.setValue(ApiProgress.start()))
                .doFinally(() -> apiProgressLiveData.setValue(ApiProgress.stop()))
                .subscribe(data -> {
                    if (data.isSuccess())
                        consumer.accept(data.getData());
                    else
                        apiErrorLiveData.setValue(ApiError.create(data.getErrorMessage(), data.getErrorCode()));

                }, err -> {
                    Toast.makeText(application, err.getMessage(), Toast.LENGTH_SHORT).show();
                    apiErrorLiveData.setValue(ApiError.create(err, err.getMessage(), -2));
                })
        );
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        // Dispose subscriptions..
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.clear();
        }
    }
}
