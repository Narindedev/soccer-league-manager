package com.apnitor.football.league.api.request;

import java.util.ArrayList;
import java.util.List;

public class AddPlayerToTeamReq {

    private String scheduleId;
    private String leagueId;
    private String seasonId;
    private String divisionId;
    private String team1Formation;
    private String team2Formation;
    private ArrayList<TeamPlaying11> team1Playing11;
    private ArrayList<TeamPlaying11> team2Playing11;

    public AddPlayerToTeamReq(String leagueId, String divisionId,String seasonId, String scheduleId, String team1Formation, ArrayList<TeamPlaying11> team1Playing11) {
        this.leagueId = leagueId;
        this.divisionId=divisionId;
        this.seasonId=seasonId;
        this.scheduleId = scheduleId;
        this.team1Formation = team1Formation;
        this.team1Playing11 = team1Playing11;
    }

    public  AddPlayerToTeamReq(){

    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getTeam1Formation() {
        return team1Formation;
    }

    public void setTeam1Formation(String team1Formation) {
        this.team1Formation = team1Formation;
    }

    public String getTeam2Formation() {
        return team2Formation;
    }

    public void setTeam2Formation(String team2Formation) {
        this.team2Formation = team2Formation;
    }

    public ArrayList<TeamPlaying11> getTeam1Playing11() {
        return team1Playing11;
    }

    public void setTeam1Playing11(ArrayList<TeamPlaying11> team1Playing11) {
        this.team1Playing11 = team1Playing11;
    }

    public ArrayList<TeamPlaying11> getTeam2Playing11() {
        return team2Playing11;
    }

    public void setTeam2Playing11(ArrayList<TeamPlaying11> team2Playing11) {
        this.team2Playing11 = team2Playing11;
    }


    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }
    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }


    @Override
    public String toString() {
        return "AddPlayerToTeamReq{" +
                "scheduleId='" + scheduleId + '\'' +
                ", leagueId='" + leagueId + '\'' +
                ", seasonId='" + seasonId + '\'' +
                ", divisionId='" + divisionId + '\'' +
                ", team1Formation='" + team1Formation + '\'' +
                ", team2Formation='" + team2Formation + '\'' +
                ", team1Playing11=" + team1Playing11 +
                ", team2Playing11=" + team2Playing11 +
                '}';
    }
}
