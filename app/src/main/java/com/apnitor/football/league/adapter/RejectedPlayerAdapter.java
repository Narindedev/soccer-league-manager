package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class RejectedPlayerAdapter extends RecyclerView.Adapter<RejectedPlayerAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamPlayersRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    ArrayList<Boolean> mTeam1SelectedPlayers;
    CheckBox mOldCb;


    public RejectedPlayerAdapter(Context context, ArrayList<GetTeamPlayersRes> teamList, ListItemClickCallback listItemClickCallback, ArrayList<Boolean> mTeam1SelectedPlayers, String mTeamSelected) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
        this.mTeam1SelectedPlayers = mTeam1SelectedPlayers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_select_team, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamPlayersRes model = teamList.get(position);
            holder.tvTeamName.setText(model.getFirstName() + " " + model.getLastName());
            if (model.getProfilePhotoUrl() != null)
                Glide.with(context).load(model.getProfilePhotoUrl()).into(holder.mIv);
            //
            holder.mCb.setVisibility(View.INVISIBLE);
            //
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox mCb;
        TextView tvTeamName;
        ImageView mIv;

        ViewHolder(View itemView) {
            super(itemView);
            mCb = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            mIv = (ImageView) itemView.findViewById(R.id.ivTeamImage);

        }
    }
}
