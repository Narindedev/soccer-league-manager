package com.apnitor.football.league.api.request;

public class GetLeagueDivisionsReq {


    private String leagueId;

    public GetLeagueDivisionsReq(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
