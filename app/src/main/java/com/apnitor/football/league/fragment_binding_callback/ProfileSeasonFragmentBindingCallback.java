package com.apnitor.football.league.fragment_binding_callback;

public interface ProfileSeasonFragmentBindingCallback {

    void showResults();
    void showSchedules();
    void showPointsTable();
    void showStats();
}
