package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.SignUpReq;
import com.apnitor.football.league.api.response.SignUpRes;
import com.apnitor.football.league.repository.ProfileRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SignUpViewModel extends BaseViewModel {

    private ProfileRepository profileRepository;
    private MutableLiveData<SignUpRes> signUpResLiveData = new MutableLiveData<>();

    public SignUpViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository(application);
    }

    public LiveData<SignUpRes> getSignUpResLiveData() {
        return signUpResLiveData;
    }

    public void signUp(String username, String email, String password,String firstName,String lastName,String phone) {
        SignUpReq signUpReq = new SignUpReq(username, email, password,firstName,lastName,phone);
        consumeApi(profileRepository.signUp(signUpReq), data -> signUpResLiveData.setValue(data));
    }
}
