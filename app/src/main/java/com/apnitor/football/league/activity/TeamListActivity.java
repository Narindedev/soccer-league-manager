package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.TeamListAdapter;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamListActivity extends BaseActivity implements ListItemMultipleCallback {

    private ActivityTeamListBinding binding;
    private TeamViewModel teamViewModel;
    private TeamListActivity teamListActivity = this;
private ArrayList<PlayerRes> mPlayerRes;
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_list);

        teamViewModel = getViewModel(TeamViewModel.class);

        setupToolbar(binding.toolbar, "My Teams");
        setupRecyclerView();
        observeApiResponse();

    }

    private void observeApiResponse() {
        teamViewModel.getMyTeamListLiveData().observe(this,
                teams -> {
                    binding.rvAllTeams.setAdapter(
                            new TeamListAdapter(teamListActivity, teams, RecyclerView.VERTICAL, this)
                    );
                });
        teamViewModel.getMyTeams();
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
        GetTeamRes getTeamRes = (GetTeamRes) object;
        Intent intent = new Intent(teamListActivity, TeamProfileActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("team_detail", getTeamRes);
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    public void onLeagueListItemClick(Object object) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


}
