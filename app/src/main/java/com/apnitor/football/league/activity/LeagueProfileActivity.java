package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.socailmedia.SocialMediaActivity;
import com.apnitor.football.league.adapter.ProfileViewPagerAdapater;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.api.response.SelectedSeasonDivisionRes;
import com.apnitor.football.league.databinding.ActivityLeagueProfileBinding;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.LeagueTeamsFragment;
import com.apnitor.football.league.util.RequestCodes;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;

public class LeagueProfileActivity extends BaseActivity {

    public static FloatingActionButton fabSchedule, fabDivision, fabAddSeason, fabAddTeams;
    public static String FAB_TYPE = "";
    public static boolean isLeagueTeamLoadFIrtsTime = false;
    public static boolean isPastResultLoadFIrtsTime = false;
    public static boolean isLeagueScheduleLoadFIrtsTime = false;
    public static boolean isLeagueStandingLoadFIrtsTime = false;
    static boolean isSeasonDivisionUpdated = false;
    private final String LEAGUE_ID = "league_id";
    private final String LEAGUE_NAME = "league_name";
    public String scheduleSeasonId = "";
    public String scheduleDivisionId = "";
    public String scheduleDivisionName = "";
    public String teamDivisionId = "";
    public String teamDivisionName = "";

    public static String checkTeamIsContainned = "";
    LinearLayout llAddDivisionBtn, llAddSeasonBtn, llAddTeams, llAddSchedule;
    View fabBGLayout;
    int mViewPagerPosition = 0;
    public static GetLeagueRes mGetLeagueRes;
    private static ProfileViewPagerAdapater mAdapater;
    private String invitationType;
    private CustomBroadcastReceiver broadcastReceiver = new CustomBroadcastReceiver();
    private NavController navController;
    private NavHostFragment navHostFragment;
    private LeagueViewModel leagueViewModel;
    private String leagueId;
    private ActivityLeagueProfileBinding binding;
    private boolean isFABOpen;
    private String imageUrl;

    public String getLeagueId() {
        return leagueId;
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_league_profile);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            leagueId = i.getExtras().getString(LEAGUE_ID);
            binding.tvName.setText(i.getExtras().getString(LEAGUE_NAME));
            invitationType = i.getExtras().getString("type");
            imageUrl = i.getExtras().getString("imageUrl");
            if (imageUrl != null) {
                Glide.with(this).load(imageUrl).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_league)).into(binding.ivProfileImage);
            }

        }
        if (leagueId != null) {
            // setLeagueDetails();
        }
        // TODO: Remove this if api success built
        setUpViewPager();

        llAddDivisionBtn = binding.llAddDivisionBtn;
        llAddSeasonBtn = binding.llAddSeasonBtn;
        llAddTeams = binding.llAddTeams;
        llAddSchedule = binding.llAddSchedule;
        //
        //fab = binding.floatingActionButton;
        fabSchedule = binding.fabAddSchedule;
        fabDivision = binding.fabAddDivision;
        fabAddSeason = binding.fabAddSeason;
        fabAddTeams = binding.fabAddTeams;

        //  fabSchedule.setVisibility(View.VISIBLE);
        //
        fabBGLayout = binding.fabBGLayout;

       /* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    switch (mViewPagerPosition) {
                        case 0:
                            showScheduleFABMenu();
                            break;
                        case 1:
                            showOnlySeasonDivFabMenu();
                            break;
                        case 2:
                            showOnlyAddTeamFABMenu();
                            break;
                        default:
                            showFABMenu();
                            break;
                    }

                } else {
                    closeFABMenu();
                }
            }
        });*/


        fabSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LeagueSchedulesFragment.mSeasonId != null && !LeagueSchedulesFragment.mSeasonId.isEmpty()|| LeagueSchedulesFragment.mDivisionId != null && !LeagueSchedulesFragment.mDivisionId.isEmpty()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("leagueId", leagueId);
                    bundle.putString("seasonId", LeagueSchedulesFragment.mSeasonId);
                    bundle.putString("seasonName", LeagueSchedulesFragment.mSeasonName);
                    bundle.putString("divisionId", LeagueSchedulesFragment.mDivisionId);
                    bundle.putString("divisionName", LeagueSchedulesFragment.mDivisionName);
                    startActivityForResultWithExtra(AddScheduleActivity.class, RequestCodes.SCHEDULE_REQUEST_CODE, bundle);
                    overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                } else {
                    showToast("Please Create Division and Season First. To create Division and Season please go to Info tab.");
                }
            }
        });

        fabDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("leagueId", leagueId);
                //startActivity(CreateDivisionActivity.class, b);
                startActivityForResultWithExtra(CreateDivisionActivity.class, RequestCodes.DIVISION_REQUEST_CODE, b);
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                // closeFABMenu();
            }
        });

        fabAddSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGetLeagueRes.divisionList != null && !mGetLeagueRes.divisionList.isEmpty()) {
                    Bundle b = new Bundle();
                    b.putString("leagueId", leagueId);
                    b.putSerializable("divisions", (Serializable) mGetLeagueRes.divisionList);
                    b.putSerializable("seasons", (Serializable) mGetLeagueRes.seasonList);
                    // startActivity(CreateSeasonActivity.class, b);
                    startActivityForResultWithExtra(CreateSeasonActivity.class, RequestCodes.SEASON_REQUEST_CODE, b);
                } else {
                    showToast("Please Create Division First.");
                }
                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                // closeFABMenu();
            }
        });

        fabAddTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Bundle bundle = new Bundle();
                bundle.putString("leagueId", leagueId);
                startActivity(AddLeagueTeamsActivity.class, bundle);*/

                if (LeagueTeamsFragment.mSeasonId.isEmpty() || LeagueTeamsFragment.mDivisionId.isEmpty()) {
                    showToast("Please Create Division and Season First. To create Division and Season please go to Info tab.");
                } else {
                    Bundle b = new Bundle();
                    b.putString("leagueId", mGetLeagueRes.getLeagueId());
                    b.putSerializable("leagueName", mGetLeagueRes.getLeagueTitle());
                    b.putString("seasonId", LeagueTeamsFragment.mSeasonId);
                    b.putString("divisionId", LeagueTeamsFragment.mDivisionId);
                    b.putString("divisionName", LeagueTeamsFragment.mDivisionName);
                    //startActivity(SearchTeamActivity.class, b);

                    startActivityForResultWithExtra(SearchTeamActivity.class, RequestCodes.TEAM_REQUEST_CODE, b);
                    overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                }

                //closeFABMenu();
            }
        });

        binding.ivEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(LeagueProfileActivity.this, CreateLeagueActivity.class);
                mIntent.putExtra("screenType", "edit");
                Bundle b = new Bundle();
                b.putSerializable("league_detail", mGetLeagueRes);
                mIntent.putExtras(b);
                startActivityForResult(mIntent, RequestCodes.EDIT_LEAGUE_CODE);
            }
        });
        binding.ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        binding.ivAddManagerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(LeagueProfileActivity.this, SearchLeagueManager.class);
                Bundle b = new Bundle();
                b.putSerializable("leagueId", mGetLeagueRes.getLeagueId());
                b.putSerializable("leagueName", mGetLeagueRes.getLeagueTitle());
                mIntent.putExtras(b);
                startActivityForResult(mIntent, RequestCodes.EDIT_LEAGUE_CODE);
            }
        });

        binding.ivSocialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(LeagueProfileActivity.this, SocialMediaActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("socialMedia", mGetLeagueRes.socialMedia);
                mIntent.putExtras(bundle);
                startActivity(mIntent);
            }
        });

        setLeagueDetails();
    }

    @SuppressLint("RestrictedApi")
    private void showOnlySeasonDivFabMenu() {
        if (FAB_TYPE.equalsIgnoreCase("division")) {
            // showOnlyDivisionFABMenu();
            fabDivision.setVisibility(View.VISIBLE);
            fabAddSeason.setVisibility(View.GONE);
        } else {
            fabDivision.setVisibility(View.GONE);
            fabAddSeason.setVisibility(View.VISIBLE);
            //showSeasonFABMenu();
        }
    }


    private void setLeagueDetails() {
        FAB_TYPE = "";
        ViewPager viewPager = binding.profileViewPager;
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onPageSelected(int position) {
                //   closeFABMenu();
                switch (position) {
                    case 0:
                        FAB_TYPE = "Schedule";
                        fabSchedule.setVisibility(View.GONE);
                        fabDivision.setVisibility(View.GONE);
                        fabAddSeason.setVisibility(View.GONE);
                        fabAddTeams.setVisibility(View.GONE);
                        mViewPagerPosition = 0;
                        break;
                    case 1:
                        FAB_TYPE = "";
                        fabSchedule.setVisibility(View.GONE);
                        fabDivision.setVisibility(View.GONE);
                        fabAddSeason.setVisibility(View.GONE);
                        fabAddTeams.setVisibility(View.GONE);
                        mViewPagerPosition = 1;
                        break;
                    case 2:
                        if (getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
                            FAB_TYPE = "";
                            fabSchedule.setVisibility(View.GONE);
                            fabDivision.setVisibility(View.GONE);
                            fabAddSeason.setVisibility(View.GONE);
                            fabAddTeams.setVisibility(View.VISIBLE);
                        }
                        mViewPagerPosition = 2;
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.tabProfile.setupWithViewPager(viewPager);

        leagueViewModel.getLeagueDetailsResLiveData().observe(this,
                league -> {

                    // getLeagueRes = league;
                    //  binding.setLeague(getLeagueRes);
                    // setUpViewPager();

                  /*  getLeagueRes=new GetLeagueRes(league.getLeagueId(),
                            league.getLeagueTitle(),league.getFoundationDate(),
                            league.getCountry(),league.getCurrentChampions(),league.getMostChampions(),
                            league.getContactInfo(),league.getWebsite(),league.getFacebookLink(),
                            league.getTwitterLink(),league.getImageUrl(),league.getOwner());*/
                    // getLeagueRes = league;
                    /*
                    DOn't delete this will use to update data via Observer
                    * */
                    scheduleSeasonId = "";
                    scheduleDivisionId="";
                    scheduleDivisionName="";
                    teamDivisionId="";
                    teamDivisionName="";

                    /*
                     * TOdo
                     * Load first time web service if season is not started
                     *
                     * */
                    isLeagueTeamLoadFIrtsTime = true;
                    isPastResultLoadFIrtsTime = true;
                    isLeagueStandingLoadFIrtsTime = true;
                    isLeagueScheduleLoadFIrtsTime = true;

                    mGetLeagueRes = new GetLeagueRes();
                    mGetLeagueRes = league;
                    binding.setLeague(mGetLeagueRes);
                    binding.tvName.setText(mGetLeagueRes.getLeagueTitle());
                    if (imageUrl == null)
                        Glide.with(this).load(mGetLeagueRes.getImageUrl()).apply(new RequestOptions()
                                .centerCrop().placeholder(R.drawable.ic_league)).into(binding.ivProfileImage);
                    mAdapater = new ProfileViewPagerAdapater(getSupportFragmentManager(), mGetLeagueRes);
                    viewPager.setAdapter(mAdapater);
                    viewPager.setOffscreenPageLimit(3);
                    if (invitationType != null && invitationType.equals("acceptLeagueInvite")) {
                        viewPager.setCurrentItem(2);
                    }

                    if (getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
                        binding.ivEditBtn.setVisibility(View.VISIBLE);
                        binding.ivAddManagerBtn.setVisibility(View.VISIBLE);
                    } else {
                        binding.ivAddManagerBtn.setVisibility(View.GONE);
                        binding.ivEditBtn.setVisibility(View.GONE);
                    }
                }
        );
        leagueViewModel.getLeagueDetails(new GetLeagueDetailsReq(leagueId));
    }

    private void setUpViewPager() {
        //ViewPager viewPager = binding.profileViewPager;
        // binding.tabProfile.setupWithViewPager(viewPager);
        //viewPager.setAdapter(new ProfileViewPagerAdapater(getSupportFragmentManager(), getLeagueRes));
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Create an IntentFilter instance.


        // Add network connectivity change action.
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(RequestCodes.ADD_TEAM_TO_LEAGUE);
        registerReceiver(broadcastReceiver, intentFilter);

        //intentFilter.addAction(RequestCodes.LEAGUE_PROFILE_TEAM_BROADCAST1);


        // Set broadcast receiver priority.
        //  intentFilter.setPriority(9);


        /*if(isSeasonDivisionUpdated) {
            isSeasonDivisionUpdated=false;
            binding.profileViewPager.setCurrentItem(0);
            //onRestart();
            leagueViewModel.getLeagueDetails(new GetLeagueDetailsReq(leagueId));
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ResultCodes.EDIT_LEAGUE_RESULT_CODE) {
            GetLeagueRes leagueRes = (GetLeagueRes) data.getSerializableExtra("league_detail");
            binding.tvName.setText(leagueRes.getLeagueTitle());
            mGetLeagueRes.setLeagueTitle(leagueRes.getLeagueTitle());
            mGetLeagueRes.setDescription(leagueRes.getDescription());
            mGetLeagueRes.setFoundationDate(leagueRes.getFoundationDate());
            mGetLeagueRes.setManagerName(leagueRes.getManagerName());
            mGetLeagueRes.setManagerEmail(leagueRes.getManagerEmail());
            mGetLeagueRes.setManagerPhone(leagueRes.getManagerPhone());
            mGetLeagueRes.setImageUrl(leagueRes.getImageUrl());
            imageUrl = leagueRes.getImageUrl();
            mGetLeagueRes.socialMedia = leagueRes.socialMedia;
            if (imageUrl != null) {
                Glide.with(this).load(mGetLeagueRes.getImageUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_league)).into(binding.ivProfileImage);
            }
            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();
        } else if (resultCode == ResultCodes.ADD_Schedule_RESULT_CODE) {
            scheduleSeasonId = data.getStringExtra("selectedSeasonId");
            scheduleDivisionId = data.getStringExtra("selectedDivsionId");
            scheduleDivisionName= data.getStringExtra("selectedDivsionName");
            AddScheduleLeagueReq addLeagueSchedule = (AddScheduleLeagueReq) data.getSerializableExtra("AddLeagueSchedule");
            if (mGetLeagueRes.getScheduleList() == null) {
                List<AddScheduleLeagueReq> reqList = new ArrayList<>();
                mGetLeagueRes.setScheduleList(reqList);
                mGetLeagueRes.getScheduleList().add(addLeagueSchedule);
            } else if (mGetLeagueRes.getScheduleList().size() == 0) {
                mGetLeagueRes.getScheduleList().add(addLeagueSchedule);
            } else {
                mGetLeagueRes.getScheduleList().add(addLeagueSchedule);
            }

            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();
        } else if (resultCode == ResultCodes.DIVISION_RESULT_CODE) {
            GetDivisionRes newlyAddedDivision = (GetDivisionRes) data.getSerializableExtra("Division");
            if(mGetLeagueRes.divisionList!=null && !mGetLeagueRes.divisionList.isEmpty()) {
                for (int i=0;i<mGetLeagueRes.divisionList.size();i++) {
                    if(mGetLeagueRes.divisionList.get(i).getDivisionId().equals(newlyAddedDivision.getDivisionId())){
                        mGetLeagueRes.divisionList.remove(i);
                    }
                }
            }
            mGetLeagueRes.divisionList.add(newlyAddedDivision);
            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();
        } else if (resultCode == ResultCodes.SEASON_RESULT_CODE) {
            GetSeasonRes seasonRes = (GetSeasonRes) data.getSerializableExtra("Season");

            if(mGetLeagueRes.seasonList!=null && !mGetLeagueRes.seasonList.isEmpty()) {
                for (int i=0;i<mGetLeagueRes.seasonList.size();i++) {
                    if(mGetLeagueRes.seasonList.get(i).getSeasonId().equals(seasonRes.getSeasonId())){
                        mGetLeagueRes.seasonList.remove(i);
                    }
                }
            }


            mGetLeagueRes.seasonList.add(seasonRes);
            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();
        } else if (resultCode == ResultCodes.TEAM_RESULT_CODE) {
            /**
             * 1. Search object in mGetLeagueRes.leagueTeamList on the basis of seasonId and divisionId
             * 2. Add Invitation team object
             */


            LeagueTeam invitedLeagueTeam = (LeagueTeam) data.getSerializableExtra("InvitedLeagueTeam");
            /**
             * TODO
             *
             * TEST IT
             */
            teamDivisionId=data.getStringExtra("selectedDivisionId");;
            teamDivisionName=data.getStringExtra("selectedDivisionName");;
            if (mGetLeagueRes.getLeagueTeamList() == null) {

                List<LeagueTeam> leagueTeamList = new ArrayList<LeagueTeam>();
                leagueTeamList.add(invitedLeagueTeam);
                mGetLeagueRes.setLeagueTeamList(new ArrayList<LeagueTeam>());
                mGetLeagueRes.getLeagueTeamList().add(invitedLeagueTeam);
            } else if (mGetLeagueRes.getLeagueTeamList().size() == 0) {
                mGetLeagueRes.getLeagueTeamList().add(invitedLeagueTeam);
            } else {

              /*  if(mGetLeagueRes.getLeagueTeamList().size()==1){
                    LeagueTeam leagueTeam = mGetLeagueRes.getLeagueTeamList().get(0);
                    if(leagueTeam.getInvitedTeams()==null){
                        mGetLeagueRes.getLeagueTeamList().add(invitedLeagueTeam);
                    }else if(leagueTeam.getInvitedTeams().size()==0){
                        leagueTeam.getInvitedTeams().addAll(invitedLeagueTeam.getInvitedTeams());
                    }else{
                int indexOfInvitedTeam = mGetLeagueRes.getLeagueTeamList().indexOf(invitedLeagueTeam);

                if (indexOfInvitedTeam != -1) {
                    leagueTeam = mGetLeagueRes.getLeagueTeamList().get(indexOfInvitedTeam);
                    leagueTeam.getInvitedTeams().addAll(invitedLeagueTeam.getInvitedTeams());
                }
                    }
                }else{*/
                int indexOfInvitedTeam = mGetLeagueRes.getLeagueTeamList().indexOf(invitedLeagueTeam);

                if (indexOfInvitedTeam != -1) {
                    LeagueTeam leagueTeam = mGetLeagueRes.getLeagueTeamList().get(indexOfInvitedTeam);
                    leagueTeam.getInvitedTeams().addAll(invitedLeagueTeam.getInvitedTeams());
                } else {
                    List<LeagueTeam> leagueTeamList = new ArrayList<LeagueTeam>();
                    leagueTeamList.add(invitedLeagueTeam);
                    mGetLeagueRes.setLeagueTeamList(leagueTeamList);
                }


            }

            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();

        } else if (resultCode == ResultCodes.ADD_TEAM_RESULT_CODE) {
            /**
             * 1. Search object in mGetLeagueRes.leagueTeamList on the basis of seasonId and divisionId
             * 2. Add Invitation team object
             */

            LeagueTeam addedLeagueTeam = (LeagueTeam) data.getSerializableExtra("AddedLeagueTeam");
            /**
             * TODO
             *
             * TEST IT
             */

            if (mGetLeagueRes.getLeagueTeamList() == null) {
                List<LeagueTeam> leagueTeamList = new ArrayList<LeagueTeam>();
                leagueTeamList.add(addedLeagueTeam);
                mGetLeagueRes.setLeagueTeamList(new ArrayList<LeagueTeam>());
                mGetLeagueRes.getLeagueTeamList().add(addedLeagueTeam);
            } else if (mGetLeagueRes.getLeagueTeamList().size() == 0) {
                mGetLeagueRes.getLeagueTeamList().add(addedLeagueTeam);
            } else {
                int indexOfInvitedTeam = mGetLeagueRes.getLeagueTeamList().indexOf(addedLeagueTeam);

                if (indexOfInvitedTeam != -1) {
                    LeagueTeam leagueTeam = mGetLeagueRes.getLeagueTeamList().get(indexOfInvitedTeam);
                    leagueTeam.getTeamIds().addAll(addedLeagueTeam.getTeamIds());
                }
            }

            /**
             * Update Adapter with this new value
             */
            mAdapater.updateFragments();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    /**
     * Get list of schedules on the basis of division id and season id
     *
     * @param
     * @param
     * @return
     */


    public List<SelectedSeasonDivisionRes> getActiveSeasonDivision() {
      List<SelectedSeasonDivisionRes> seasonDivisionList=new ArrayList<>();
        List<GetSeasonRes> seasonList = mGetLeagueRes.seasonList;

        List<GetDivisionRes> divisionList = mGetLeagueRes.divisionList;

        /**
         * TODO
         * Check conditions
         */


        if (seasonList != null && seasonList.size() > 0) {
            for (GetSeasonRes seasonRes : seasonList) {
                List<String> divisionIds = seasonRes.getDivisionIds();
                SelectedSeasonDivisionRes seasonDivisionRes1=new SelectedSeasonDivisionRes();
                /**
                 * TODO
                 * Check the conditions what division ids could be
                 */

                if (seasonRes.getSeasonStatus().equals("Active")) {
                    if(seasonRes.getDivisionIds()!=null && !seasonRes.getDivisionIds().isEmpty()) {
                        String divisionId = seasonRes.getDivisionIds().get(0);
                        for (GetDivisionRes divisionRes : divisionList) {
                            if (divisionId.equals(divisionRes.getDivisionId())) {
                                seasonDivisionRes1.setDivisionId(divisionRes.getDivisionId());
                                seasonDivisionRes1.setDivisionName(divisionRes.getDivisionName());
                                seasonDivisionRes1.setSeasonStatus(seasonRes.getSeasonStatus());
                                seasonDivisionRes1.setSesonId(seasonRes.getSeasonId());
                                seasonDivisionRes1.setSesonName(seasonRes.getSeasonYear());
                                seasonDivisionList.add(seasonDivisionRes1);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return seasonDivisionList;
    }

    public List<SelectedSeasonDivisionRes> selectedSeasonDivision(String divisionId, String seasonId,Integer position){
        String newDivisionId="";
        List<SelectedSeasonDivisionRes> seasonDivisionList=new ArrayList<>();
       GetSeasonRes seasonRes = mGetLeagueRes.seasonList.get(position);
        List<GetDivisionRes> divisionList = mGetLeagueRes.divisionList;
        /**
         * TODO
         * Check the conditions what division ids could be
         */
        if (seasonRes.getDivisionIds() != null && seasonRes.getDivisionIds().size() > 0) {
            for (String id : seasonRes.getDivisionIds()) {

                    if (divisionId.equals(id)) {
                        newDivisionId = id;

                        break;
                    } else{
                        newDivisionId=id;
                    }

            }

            for(GetDivisionRes divisionRes :divisionList){
                if(divisionRes.getDivisionId().equals(newDivisionId)){
                    SelectedSeasonDivisionRes divisionRes1=new SelectedSeasonDivisionRes();
                    divisionRes1.setDivisionName(divisionRes.getDivisionName());
                    divisionRes1.setDivisionId(divisionRes.getDivisionId());
                    seasonDivisionList.add(divisionRes1);
                    break;
                }
            }

        }

        return seasonDivisionList;
    }

    public List<GetSeasonRes> getDivisionSeason(String divisionId) {

        List<GetSeasonRes> seasonList = mGetLeagueRes.seasonList;

        List<GetSeasonRes> divisionSeasons = new ArrayList<GetSeasonRes>();

        /**
         * TODO
         * Check conditions
         */


        if (seasonList != null && seasonList.size() > 0) {
            for (GetSeasonRes seasonRes : seasonList) {
                List<String> divisionIds = seasonRes.getDivisionIds();

                /**
                 * TODO
                 * Check the conditions what division ids could be
                 */
                if (divisionIds != null && divisionIds.size() > 0) {
                    for (String id : divisionIds) {
                        if (id.equals(divisionId)) {
                            divisionSeasons.add(seasonRes);
                            break;
                        }
                    }
                }

            }
        }

        return divisionSeasons;
    }


    public static class CustomBroadcastReceiver extends BroadcastReceiver {

        public CustomBroadcastReceiver() {

        }

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (RequestCodes.ADD_TEAM_TO_LEAGUE.equals(action)) {
                try {

                    if (intent != null) {
                        NotificationResponse response = (NotificationResponse) intent.getSerializableExtra("data");
                        if (response != null) {
                            GetTeamRes mTeamRes = new GetTeamRes();
                            mTeamRes.setTeamId(response.teamId);
                            mTeamRes.setTeamName(response.teamName);
                            mTeamRes.setImageUrl(response.teamImageUrl);
                            if (mGetLeagueRes.getLeagueTeamList() != null) {
                                List<GetTeamRes> mTeamResList = mGetLeagueRes.getLeagueTeamList().get(0).getTeamIds();
                                if (mTeamResList != null) {
                                    if (!checkTeamIsContainned.equals(mTeamRes.getTeamId())) {
                                        // mTeamResList.add(mTeamRes);
                                        Log.e("mTeamResList Size", "" + mTeamResList);
                                        mGetLeagueRes.getLeagueTeamList().get(0).getTeamIds().add(mTeamRes);
                                        List<GetTeamRes> mInviitedTeam = mGetLeagueRes.getLeagueTeamList().get(0).getInvitedTeams();
                                        if (mInviitedTeam.size() > 0) {
                                            for (int i = 0; i < mInviitedTeam.size(); i++) {
                                                if (mTeamRes.getTeamId().equals(mInviitedTeam.get(i).getTeamId()))
                                                    mGetLeagueRes.getLeagueTeamList().get(0).getInvitedTeams().remove(i);
                                            }
                                        }

                                    }
                                } else {
                                    mGetLeagueRes.getLeagueTeamList().get(0).setTeamIds(new ArrayList<GetTeamRes>());
                                    Log.e("mTeamResList Size", "" + mTeamResList);
                                    mGetLeagueRes.getLeagueTeamList().get(0).getTeamIds().add(mTeamRes);
                                    List<GetTeamRes> mInviitedTeam = mGetLeagueRes.getLeagueTeamList().get(0).getInvitedTeams();
                                    if (mInviitedTeam.size() > 0) {
                                        for (int i = 0; i < mInviitedTeam.size(); i++) {
                                            if (mTeamRes.getTeamId().equals(mInviitedTeam.get(i).getTeamId()))
                                                mGetLeagueRes.getLeagueTeamList().get(0).getInvitedTeams().remove(i);
                                        }
                                    }
                                }
                                checkTeamIsContainned = mTeamRes.getTeamId();
                                mAdapater.updateFragments();
                            } else {
                                Log.e("mTeamResList 99Size", "");
                            }


                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}

