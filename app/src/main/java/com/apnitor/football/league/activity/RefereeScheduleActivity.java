package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.RefereeScheduleAdapter;
import com.apnitor.football.league.api.request.GetRefereeScheduleReq;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemClickCallbackWithPosition;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RefereeScheduleActivity extends BaseActivity implements ListItemClickCallbackWithPosition {

    private ActivityTeamListBinding binding;
    private RefereeViewModel refereeViewModel;
    private RefereeScheduleActivity teamListActivity = this;
    LogInRes logInRes;
    public static List<GetTeamScheduleRes> mGetTeamScheduleRes = new ArrayList<>();
    RefereeScheduleAdapter mTeamSchedulesListAdapter;
    static int position = -1;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_list);
        binding.saveBtn.setVisibility(View.GONE);
        binding.cardEdit.setVisibility(View.GONE);

        //
        logInRes = getPrefHelper().getLogInRes();
        //
        refereeViewModel = getViewModel(RefereeViewModel.class);
        setupToolbar(binding.toolbar, "My Schedule");
        setupRecyclerView();
        observeApiResponse();
    }

    private void observeApiResponse() {
        refereeViewModel.getRefereeScheduleLiveData().observe(this,
                teams -> {
                    filterScheduleResponse(teams);
                });
        refereeViewModel.getRefereeSchedule(new GetRefereeScheduleReq(logInRes.getUserId()));
    }


    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
        mTeamSchedulesListAdapter = new RefereeScheduleAdapter(this, mGetTeamScheduleRes, this);
        binding.rvAllTeams.setAdapter(mTeamSchedulesListAdapter);
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.anim_right_in,R.anim.anim_right_out);
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    @Override
    public void onListItemClick(Object object, int position) {
        this.position = position;
        GetTeamScheduleRes model = (GetTeamScheduleRes) object;
        Intent intent = new Intent(teamListActivity, MatchDetailsActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("match_detail", model);
        intent.putExtras(b);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_left_in,
                R.anim.anim_left_out);
    }


    void filterScheduleResponse(List<GetTeamScheduleRes> teams) {
        mGetTeamScheduleRes.clear();
        int size = teams.size();
        for (int i = 0; i < size; i++) {
            if (teams.get(i).getRefereeId() != null && teams.get(i).getRefereeId().equalsIgnoreCase(logInRes.getUserId())) {
                mGetTeamScheduleRes.add(teams.get(i));
            }
        }
        mTeamSchedulesListAdapter.notifyDataSetChanged();
    }

    public static void updateObjectTeam1(ArrayList<TeamPlaying11> mPlaying11List) {
        try {
            if (mGetTeamScheduleRes != null && mGetTeamScheduleRes.size() > 0)
                mGetTeamScheduleRes.get(position).setTeam1Playing11(mPlaying11List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateObjectTeam2(ArrayList<TeamPlaying11> mPlaying11List) {
        try {
            if (mGetTeamScheduleRes != null && mGetTeamScheduleRes.size() > 0)
                mGetTeamScheduleRes.get(position).setTeam2Playing11(mPlaying11List);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
