package com.apnitor.football.league.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.apnitor.football.league.application.FootballLeagueApplication;
import com.apnitor.football.league.application.SharedPreferenceHelper;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.BaseViewModel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();
    NavController navController;
    private BaseViewModel baseViewModel;
    private Toast toast;
    private ProgressDialog progressDialog;
    private Context context;
    private FootballLeagueApplication application;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @SuppressLint("ShowToast")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
        application = (FootballLeagueApplication) getActivity().getApplication();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            navController = NavHostFragment.findNavController(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    <T extends BaseViewModel> T getViewModel(Class<T> tClass) {
        T viewModel = ViewModelProviders.of(this).get(tClass);
        this.baseViewModel = viewModel;
        handleProgress();
        handleError();
        return viewModel;
    }

    private void handleError() {

        baseViewModel.getApiErrorLiveData().observe(this, apiError -> {

            showToast(apiError.getErrorMessage());
            Log.d(TAG, apiError.toString());
        });
    }

    private void handleProgress() {

        baseViewModel.getApiProgressLiveData().observe(this, apiProgress -> {

            if (apiProgress.isInProgress()) {
                progressDialog.setMessage(apiProgress.getProgressMessage());
                progressDialog.show();
            } else {
                progressDialog.setMessage("");
                progressDialog.dismiss();
            }
        });
    }

    void showToast(String message) {
        toast.setText(message);
        toast.show();
    }

    void showErrorOnEditText(EditText editText, String error) {
        editText.setError(error);
        editText.requestFocus();
    }

    public boolean checkUser(EditText editText, String username, String email, String phone) {
        Boolean validUserbool = true;
        if (!username.isEmpty()) {
            //showErrorOnEditText(binding.etUsername, "Please enter 'username'.");

            if (isInteger(username)) {
                phone = username;
                if (UIUtility.isNotValidPhone(phone)) {
                    validUserbool = false;
                    showErrorOnEditText(editText, "Please enter valid Phone Number.");
                } else {
                    validUserbool = true;
                }
            }
//            else if (username.contains("@")) {
            else {
                email = username;
                if (UIUtility.isNotValidEmail(email)) {
                    validUserbool = false;
                    showErrorOnEditText(editText, "Please enter valid Email.");
                } else {
                    validUserbool = true;
                }
            }
        } else {
            showErrorOnEditText(editText, "Please enter Email or Phone Number.");
            validUserbool = false;
        }
        return validUserbool;
    }

    SharedPreferenceHelper getPrefHelper() {
        return application.getSharedPreferenceHelper();
    }

    public boolean isInteger(String text) {
        if (text.matches("[0-9]+") && text.length() > 2) {
            return true;
        } else {
            return false;
        }

    }
    <T> void startActivity(Class<T> tClass, Bundle bundle) {
        Intent intent = new Intent(getActivity(), tClass);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
