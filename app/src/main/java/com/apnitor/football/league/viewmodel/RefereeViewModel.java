package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.GetMatchEventsReq;
import com.apnitor.football.league.api.request.GetRefereeScheduleReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateMatchEventRes;
import com.apnitor.football.league.api.response.UpdateTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;
import com.apnitor.football.league.repository.RefereeRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class RefereeViewModel extends BaseViewModel {

    private RefereeRepository refereeRepository;


    private MutableLiveData<ArrayList<GetRefereeRes>> getAllRefereeLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamScheduleRes>> getRefereeScheduleLiveData = new MutableLiveData<>();
    private MutableLiveData<UpdateMatchEventRes> getMatchEventLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetMatchEventRes>> getMatchEventsLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetTeamScheduleRes>> getTeamScheduleLiveData = new MutableLiveData<>();


    public RefereeViewModel(@NonNull Application application) {
        super(application);
        refereeRepository = new RefereeRepository(application);
    }


    public LiveData<ArrayList<GetRefereeRes>> getAllRefereeLiveData() {
        return getAllRefereeLiveData;
    }

    public void getAllReferee() {
        consumeApi(
                refereeRepository.getAllReferee(),
                data -> getAllRefereeLiveData.setValue(data)
        );
    }


    public LiveData<ArrayList<GetTeamScheduleRes>> getRefereeScheduleLiveData() {
        return getRefereeScheduleLiveData;
    }

    public void getRefereeSchedule(GetRefereeScheduleReq teamDetailReq) {
        consumeApi(
                refereeRepository.getRefereeSchedule(teamDetailReq),
                data -> getRefereeScheduleLiveData.setValue(data)
        );
    }


    //Update match event
    public LiveData<UpdateMatchEventRes> updateMatchEventsLiveData() {
        return getMatchEventLiveData;
    }

    public void updateMatchEvents(UpdateMatchEventReq matchEventReq) {
        consumeApi(
                refereeRepository.updateMatchEvents(matchEventReq),
                data -> getMatchEventLiveData.setValue(data)
        );
    }

    //
    public LiveData<ArrayList<GetMatchEventRes>> getMatchEventsLiveData() {
        return getMatchEventsLiveData;
    }

    public void getMatchEvents(GetMatchEventsReq matchEventReq) {
        consumeApi(
                refereeRepository.getMatchEvents(matchEventReq),
                data -> getMatchEventsLiveData.setValue(data)
        );
    }

    public LiveData<ArrayList<GetTeamScheduleRes>> getFollowingSchedulesLiveData() {
        return getRefereeScheduleLiveData;
    }

    public void getFollowingSchedules() {
        consumeApi(
                refereeRepository.getFollowingSchedules(),
                data -> getRefereeScheduleLiveData.setValue(data)
        );
    }

}
