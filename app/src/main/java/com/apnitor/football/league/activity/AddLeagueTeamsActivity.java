package com.apnitor.football.league.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.adapter.LeagueTeamsListAdapter;
import com.apnitor.football.league.adapter.SeasonListAdapter;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.GetLeagueSeasonsReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivityAddLeagueTeamsBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AddLeagueTeamsActivity extends BaseActivity {

    private ActivityAddLeagueTeamsBinding binding;
    private TeamViewModel teamViewModel;
    private SeasonViewModel seasonViewModel;
    private DivisionViewModel divisionViewModel;
    private AddLeagueTeamsActivity activity = this;
    private LeagueViewModel leagueViewModel;
    private String leagueId;
    private List<GetTeamRes> teamList = new ArrayList<>();
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private List<GetDivisionRes> divisionsList = new ArrayList<>();
    private SeasonListAdapter seasonListAdapter;
    private DivisionsListAdapter divisionsListAdapter;
    private Dialog seasonsDialog;
    private RecyclerView lvSeasonPopUpSelect;
    private AlertDialog.Builder seasonAlertDialog;
    private AlertDialog.Builder divisionsDialog;
    private RecyclerView lvDivisionPopUpSelect;
    private boolean isSeasonSelected;
    AlertDialog mSeasonAlert;
    AlertDialog mDivisionAlert;
    String mSeasonId = "", mDivisionId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_add_league_teams);

        teamViewModel = getViewModel(TeamViewModel.class);
        seasonViewModel = getViewModel(SeasonViewModel.class);
        divisionViewModel = getViewModel(DivisionViewModel.class);

        Intent i = getIntent();
        if (i.getExtras() != null) {
            leagueId = i.getExtras().getString("leagueId");
        }

        seasonListAdapter = new SeasonListAdapter(activity, seasonsList, new ListItemClickCallback() {
            @Override
            public void onListItemClick(Object object) {
                GetSeasonRes getSeasonRes = (GetSeasonRes) object;
                binding.selectSeasonEt.setText(getSeasonRes.getSeasonYear());
                isSeasonSelected = true;
//                seasonAlertDialog.dismiss();
                mSeasonId = getSeasonRes.getSeasonId();
            }
        });

        divisionsListAdapter = new DivisionsListAdapter(activity, divisionsList, new ListItemClickCallback() {
            @Override
            public void onListItemClick(Object object) {
                GetDivisionRes division = (GetDivisionRes) object;
                binding.selectDivisionEt.setText(division.getDivisionName());
                teamViewModel.getLeagueTeams(new GetLeagueTeamsReq(leagueId, binding.selectSeasonEt.getText().toString(),
                        division.getDivisionName()));
//                divisionsDialog.dismiss();
                mDivisionId = division.getDivisionId();
            }
        });

        seasonAlertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View content = inflater.inflate(R.layout.custom_dialog, null);
//        seasonAlertDialog.setView(content);
        seasonAlertDialog.setTitle("Select Season");
//        lvSeasonPopUpSelect = (RecyclerView) content.findViewById(R.id.List);
//        lvSeasonPopUpSelect.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
//        lvSeasonPopUpSelect.setAdapter(seasonListAdapter);
//        seasonAlertDialog.setCancelable(true);

//        seasonsDialog = new Dialog(activity);
//        seasonsDialog.setContentView(R.layout.custom_dialog);
//        lvSeasonPopUpSelect = (RecyclerView) seasonsDialog.findViewById(R.id.List);
//        seasonsDialog.setTitle("Select Season");
//        lvSeasonPopUpSelect.setAdapter(seasonListAdapter);


        binding.selectSeasonEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seasonAlertDialog = new AlertDialog.Builder(AddLeagueTeamsActivity.this);
                LayoutInflater inflater = LayoutInflater.from(AddLeagueTeamsActivity.this);
                View content = inflater.inflate(R.layout.custom_dialog, null);
                seasonAlertDialog.setView(content);
                seasonAlertDialog.setTitle("Select Season");
                lvSeasonPopUpSelect = (RecyclerView) content.findViewById(R.id.List);
                lvSeasonPopUpSelect.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
//                lvSeasonPopUpSelect.setAdapter(seasonListAdapter);
                lvSeasonPopUpSelect.setAdapter(new SeasonListAdapter(activity, seasonsList, seasonListItemClickCallback));
                seasonAlertDialog.setCancelable(true);
//                seasonAlertDialog.show();
                mSeasonAlert = seasonAlertDialog.show();
            }
        });

        binding.selectDivisionEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSeasonSelected) {
                    divisionsDialog = new AlertDialog.Builder(AddLeagueTeamsActivity.this);
                    LayoutInflater inflater = LayoutInflater.from(AddLeagueTeamsActivity.this);
                    View content = inflater.inflate(R.layout.custom_dialog, null);
                    divisionsDialog.setView(content);
                    divisionsDialog.setTitle("Select Division");
                    lvDivisionPopUpSelect = (RecyclerView) content.findViewById(R.id.List);
                    lvDivisionPopUpSelect.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
                    lvDivisionPopUpSelect.setAdapter(new DivisionsListAdapter(activity, divisionsList, divisionListItemClickCallback));
                    divisionsDialog.setCancelable(true);
                    mDivisionAlert = divisionsDialog.show();
                } else
                    Toast.makeText(activity, "Select Season first", Toast.LENGTH_LONG).show();
            }
        });

        setupToolbar(binding.toolbar, "Season Teams");
        setupRecyclerView();
        observeApiResponse();
    }

    private void setupRecyclerView() {
        binding.rvLeagueTeams.setLayoutManager(
                new LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        );
    }

    private void observeApiResponse() {
        seasonViewModel.getSeasons(new GetLeagueSeasonsReq(leagueId));
        divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
        //
        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    seasonsList = seasons;
                    if (seasonsList.size() == 1) {
                        mSeasonId = seasonsList.get(0).getSeasonId();
                        binding.selectSeasonEt.setText(seasonsList.get(0).getSeasonYear());
                        isSeasonSelected = true;
                    }
                    seasonListAdapter.notifyDataSetChanged();
                });

        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    divisionsList = divisions;
                    if (divisionsList.size() == 1) {
                        mDivisionId = divisionsList.get(0).getDivisionId();
                        binding.selectDivisionEt.setText(divisionsList.get(0).getDivisionName());
                    }
                    divisionsListAdapter.notifyDataSetChanged();
                });


        teamViewModel.getLeagueTeamsLiveData().observe(this,
                teams -> {
                    teamList = teams.getTeamIds();
                    binding.rvLeagueTeams.setAdapter(
                            new LeagueTeamsListAdapter(activity, teamList, RecyclerView.VERTICAL, null)
                    );
                });


    }


    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void onAddTeamClick(View view) {
        if (mSeasonId.isEmpty() || mDivisionId.isEmpty()) {
            showToast("Please select season and division first");
        } else {
            Bundle b = new Bundle();
            b.putString("leagueId", leagueId);
            b.putString("seasonName", binding.selectSeasonEt.getText().toString());
            b.putString("divisionName", binding.selectDivisionEt.getText().toString());
            b.putString("seasonId", mSeasonId);
            b.putString("divisionId", mDivisionId);
            b.putSerializable("alreadyAddedTeams", (Serializable) teamList);
            startActivity(SearchTeamActivity.class, b);
        }
    }

    ListItemClickCallback seasonListItemClickCallback = new ListItemClickCallback() {
        @Override
        public void onListItemClick(Object object) {
            GetSeasonRes getSeasonRes = (GetSeasonRes) object;
            binding.selectSeasonEt.setText(getSeasonRes.getSeasonYear());
            mSeasonId = getSeasonRes.getSeasonId();
            isSeasonSelected = true;
            if (mSeasonAlert != null && mSeasonAlert.isShowing()) {
                seasonsDialog = null;
                mSeasonAlert.dismiss();
            }
        }
    };

    ListItemClickCallback divisionListItemClickCallback = new ListItemClickCallback() {
        @Override
        public void onListItemClick(Object object) {
            GetDivisionRes getSeasonRes = (GetDivisionRes) object;
            binding.selectDivisionEt.setText(getSeasonRes.getDivisionName());
            mDivisionId = getSeasonRes.getDivisionId();
            if (mDivisionAlert != null && mDivisionAlert.isShowing()) {
                divisionsDialog = null;
                mDivisionAlert.dismiss();
            }
        }
    };

}
