package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.DeleteDivisionReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateDivisionRes;
import com.apnitor.football.league.repository.AddScheduleRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class AddScheduleViewModel extends BaseViewModel {

    private AddScheduleRepository addScheduleRepository;
    private MutableLiveData<AddScheduleRes> updateDivisionResLiveData = new MutableLiveData<>();
    private MutableLiveData<AddScheduleRes> updateScheduleLiveData = new MutableLiveData<>();
    private MutableLiveData<List<AddScheduleRes>> addScheduleListResLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> deleteDivisionResLiveData = new MutableLiveData<>();

    public AddScheduleViewModel(@NonNull Application application) {
        super(application);
        addScheduleRepository = new AddScheduleRepository(application);
    }

    public LiveData<AddScheduleRes> getUpdateDivisionResLiveData() {
        return updateDivisionResLiveData;
    }

    public void addUpdateSchedule(AddScheduleLeagueReq updateDivisionReq) {
        consumeApi(
                addScheduleRepository.getDivisions(updateDivisionReq),
                data -> updateDivisionResLiveData.setValue(data)
        );
    }

    public void updateScheduleTeam(AddPlayerToTeamReq updateDivisionReq) {
        consumeApi(
                addScheduleRepository.updateTeamPlayers(updateDivisionReq),
                data -> updateScheduleLiveData.setValue(data)
        );
    }


    public LiveData<List<AddScheduleRes>> getAddScheduleListResLiveData() {
        return addScheduleListResLiveData;
    }

    public LiveData<Object> getDeleteDivisionResLiveData() {
        return deleteDivisionResLiveData;
    }

    public LiveData<AddScheduleRes> updateScheduleLiveData() {
        return updateScheduleLiveData;
    }
}

