package com.apnitor.football.league.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SeasonDayResultsListAdapter extends RecyclerView.Adapter<SeasonDayResultsListAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;
    List<AddScheduleLeagueReq> leagueScheduleList;
    String LOG_TAG = "SeasonDayResultsListAdapter";
    int headerPosition;

    public SeasonDayResultsListAdapter(Context context, ArrayList<String> teamList, List<AddScheduleLeagueReq> leagueScheduleList, int headerPosition) {
        this.context = context;
        this.teamList = teamList;
        this.leagueScheduleList = leagueScheduleList;
        this.headerPosition = headerPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_season_day_results, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            if (leagueScheduleList.size() > 0) {
                if (position == headerPosition) {
                    AddScheduleLeagueReq model = leagueScheduleList.get(position);
                    holder.tvTeamName1.setText(model.getTeam1Id());
                    holder.tvTeamName2.setText(model.getTeam2Id());
                    holder.tvVenue.setText(model.getCity() + ", " + model.getState());
                }
            }else{

            }
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueScheduleList.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName1;
        TextView tvTeamName2;
        TextView tvVenue;

        ViewHolder(View itemView) {
            super(itemView);
            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_team_1);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_team_2);
            tvVenue = (TextView) itemView.findViewById(R.id.tvVenue);
        }
    }
}
