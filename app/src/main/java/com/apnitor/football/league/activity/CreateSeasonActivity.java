package com.apnitor.football.league.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.UpdateSeasonReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetUsStateRes;
import com.apnitor.football.league.databinding.ActivityCreateSeasonBinding;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.appcompat.app.AlertDialog;

import static com.apnitor.football.league.fragment.LeagueSchedulesFragment.mGetLeagueRes;

public class CreateSeasonActivity extends BaseActivity {

    public static final String SEASON_DETAIL = "season_detail";
    ArrayList<String> mAllDivisions = new ArrayList<>();
    ArrayList<Integer> selectedDivisions = new ArrayList<Integer>();
    ArrayList<String> selectedDivisionsIds = new ArrayList<String>();
    ArrayAdapter<String> arrayAdapter = null;
    ArrayList<String> scopeList, statusList;
    ArrayList<GetUsStateRes.StateData> stateList;
    String[] citiesArray;
    ArrayList<String> cityList;
    ArrayList<Integer> selectedStates;
    String selectedItem;
    private ActivityCreateSeasonBinding binding;
    private SeasonViewModel seasonViewModel;
    private UpdateSeasonReq updateSeasonReq;
    private DivisionViewModel divisionViewModel;
    private boolean isCreating = true;
    private String leagueId;
    private CreateSeasonActivity createSeasonActivity = this;
    private List<GetDivisionRes> mDivisions = new ArrayList<>();
    private List<GetSeasonRes> mSeasonsList = new ArrayList<>();
    private int mYear, mMonth, mDay, mHour, mMinute;
    private boolean isAlreadyActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stateList = new ArrayList<>();
        selectedStates = new ArrayList<>();
        scopeList = new ArrayList<String>();
        scopeList.add("Local");
        scopeList.add("State");
        scopeList.add("Regional");
        scopeList.add("National");

        statusList = new ArrayList<String>();
        statusList.add("Not Started");
        statusList.add("Active");
        statusList.add("Completed");

        binding = bindContentView(R.layout.activity_create_season);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            leagueId = intent.getExtras().getString("leagueId");

            mDivisions = (List<GetDivisionRes>) intent.getSerializableExtra("divisions");
            mSeasonsList = (List<GetSeasonRes>) intent.getSerializableExtra("seasons");

            if (mDivisions.size() == 1) {
                autoSelectFirstDivision();
            }

            if (intent.getExtras() != null && intent.getExtras().getSerializable(SEASON_DETAIL) != null) {
                GetSeasonRes getSeasonRes = (GetSeasonRes) intent
                        .getExtras()
                        .getSerializable(SEASON_DETAIL);
                this.updateSeasonReq = new UpdateSeasonReq(leagueId, getSeasonRes.getSeasonId(), getSeasonRes.getDivisionIds(), getSeasonRes.getSeasonYear(), getSeasonRes.getEntryFee(), getSeasonRes.getWinningPrize()
                        , getSeasonRes.getState(), getSeasonRes.getCity(), getSeasonRes.getZipCode(), getSeasonRes.getDescription(), getSeasonRes.getOwner(), getSeasonRes.getSeasonStatus()
                        , getSeasonRes.scope, getSeasonRes.timelineSendingRoaster, getSeasonRes.timelineModifiedRoster, getSeasonRes.timelinePayFee, getSeasonRes.tentativeStartDate, getSeasonRes.tentativeEndDate);
                binding.setSeasonData(getSeasonRes);
                isCreating = false;
                // binding.deleteBtn.setVisibility(View.VISIBLE);


                /*
                 * show added divisions
                 * */
                selectedDivisions.clear();
                selectedDivisionsIds.clear();
                mAllDivisions.clear();
                for (int i = 0; i < mDivisions.size(); i++) {
                    for (String divId : getSeasonRes.getDivisionIds()) {
                        if (divId.equals(mDivisions.get(i).getDivisionId())) {
                            selectedDivisions.add(i);
                            selectedDivisionsIds.add(mDivisions.get(i).getDivisionId());
                        }
                    }
                    mAllDivisions.add(mDivisions.get(i).getDivisionName());
                }
                showSelectedDivisions();


                /*Check Scope and set value accordingly*/
                if (getSeasonRes.scope != null && !getSeasonRes.scope.isEmpty()) {
                    if (getSeasonRes.scope.equals("State")) {

                        binding.tilState.setVisibility(View.VISIBLE);
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.GONE);
                        binding.tvSelectedState.setText(getSeasonRes.getState());
                    } else if (getSeasonRes.scope.equals("Regional")) {
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.GONE);
                        binding.tvSelectedState.setText(getSeasonRes
                                .getState());
                    } else if (getSeasonRes.scope.equals("Local")) {
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.VISIBLE);
                        binding.tilState.setVisibility(View.VISIBLE);
                        binding.tvSelectedState.setText(getSeasonRes.getState());
                    } else {
                        binding.tilState.setVisibility(View.GONE);
                        binding.tilCity.setVisibility(View.GONE);
                        binding.tvSelectedState.setVisibility(View.GONE);
                    }
                }


            } else {
                binding.deleteBtn.setVisibility(View.GONE);

            }
            if (getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
                binding.saveBtn.setVisibility(View.VISIBLE);
            }else{
                binding.saveBtn.setVisibility(View.GONE);
            }

        }
        //Get Us All state name
        getStatesdata();
        //        
        binding.tvSelectDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDivisionDialog();

            }
        });
        //
        binding.tvSelectScope.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStatusDialog(scopeList, "Select Scope");

            }
        });

        binding.stateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedState = binding.tvSelectScope.getText().toString();
                if (selectedState.equals("State") || selectedState.equals("Local")) {
                    showStatesDialog("State");
                } else {
                    showMultipleStatesDialog();
                }
            }
        });

        binding.cityEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedState = binding.tvSelectedState.getText().toString();
                if (selectedState.isEmpty()) {
                    showToast("Please select state first");
                } else {

                    //showStatesDialog();
                    getCitiesData(selectedState);

                }
            }
        });

        binding.tvTimeLineSendRoaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(binding.tvTimeLineSendRoaster);

            }
        });
        binding.tvTimeLineModifiedRoaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(binding.tvTimeLineModifiedRoaster);

            }
        });
        binding.tvTimeLinePayFee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(binding.tvTimeLinePayFee);

            }
        });

        binding.tentativeStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(binding.tentativeStartDate);

            }
        });

        binding.tentativeEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(binding.tentativeEndDate);

            }
        });

        binding.tvTimeLineStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStatusDialog(statusList, "Select Status");

            }
        });
        //
        seasonViewModel = getViewModel(SeasonViewModel.class);

        seasonViewModel.getUpdateSeasonResLiveData()
                .observe(this, res -> {
                    isCreating = false;
                    LeagueProfileActivity.isSeasonDivisionUpdated = true;

                    /**
                     * Season is created
                     *
                     * Send this response to League Profile
                     */

                    GetSeasonRes seasonRes = new GetSeasonRes(res.getSeasonId(), updateSeasonReq.divisionIds, updateSeasonReq.getSeasonYear(),
                            updateSeasonReq.getState(), updateSeasonReq.getCity(), updateSeasonReq.getZipCode(), updateSeasonReq.getDescription(),
                            updateSeasonReq.getOwner(), updateSeasonReq.getSeasonStatus(), updateSeasonReq.scope, updateSeasonReq.timelineSendingRoaster,
                            updateSeasonReq.timelineModifiedRoster, updateSeasonReq.timelinePayFee, updateSeasonReq.tentativeStartDate, updateSeasonReq.tentativeEndDate);


                    Intent seasonIntent = new Intent();
                    seasonIntent.putExtra("Season", seasonRes);
                    setResult(ResultCodes.SEASON_RESULT_CODE, seasonIntent);
                    onBackPressed();

                });
        setupToolbar(binding.toolbar, "Add Season");
        // observeApiResponse();

        //  getDivisionList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
    }

   /* private void observeApiResponse() {
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    mDivisions = divisions;
                });


    }*/


    /*private void getDivisionList(){
        mDivisions = getActi;
    }*/


    void showDivisionDialog() {
        selectedDivisions.clear();
        selectedDivisionsIds.clear();
        mAllDivisions.clear();
        for (int i = 0; i < mDivisions.size(); i++) {
            mAllDivisions.add(mDivisions.get(i).getDivisionName());
        }
        //
        boolean[] checkedItems = new boolean[mDivisions.size()];
        //
        for (int i = 0; i < mDivisions.size(); i++) {
            if (selectedDivisions.contains(i)) {
                checkedItems[i] = true;
            } else {
                checkedItems[i] = false;
            }
        }
        //
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select Division");
        builderSingle.setCancelable(true);
        final CharSequence[] dialogList = mAllDivisions.toArray(new CharSequence[mAllDivisions.size()]);
        builderSingle.setMultiChoiceItems(dialogList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked) {
                    // if the user checked the item, add it to the selected items
                    selectedDivisions.add(which);
                    selectedDivisionsIds.add(mDivisions.get(which).getDivisionId());
                } else if (selectedDivisions.contains(which)) {
                    // else if the item is already in the mPlaying11List, remove it
                    selectedDivisions.remove(Integer.valueOf(which));
                    selectedDivisionsIds.remove(mDivisions.get(which).getDivisionId());
                }

                // you can also add other codes here,
                // for example a tool tip that gives user an idea of what he is selecting
                // showToast("Just an example description.");
            }
        })
                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // user clicked OK, so save the mSelectedItems results somewhere
                        // here we are trying to retrieve the selected items indices
                        String selectedIndex = "";

                        for (Integer i : selectedDivisions) {
                            selectedIndex += i + ", ";
                        }
                        Log.e("Show selectred division", "" + selectedDivisionsIds.toString());
                        //showToast("Selected index: " + selectedIndex);
                        showSelectedDivisions();
                    }


                })

                .show();

    }

    private void showSelectedDivisions() {
        String selectedDivision = "";
        for (int i = 0; i < selectedDivisions.size(); i++) {
            selectedDivision = selectedDivision + mAllDivisions.get(selectedDivisions.get(i)) + ", ";
            // selectedDivisionsIds.add(mAllDivisions.get(selectedDivisions.get(i)));
        }
        if (!selectedDivision.isEmpty()) {
            selectedDivision = selectedDivision.substring(0, selectedDivision.length() - 2);
            binding.tvSelectedDivision.setText(selectedDivision);
        } else {
            binding.tvSelectedDivision.setText("No division selected");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onSaveClick(View view) {

        String seasonId = isCreating ? null : updateSeasonReq.getSeasonId();
        String seasonYear = UIUtility.getEditTextValue(binding.seasonYearEt);
        String state = UIUtility.getEditTextValue(binding.stateEt);
        String city = UIUtility.getEditTextValue(binding.cityEt);
        String zipCode = UIUtility.getEditTextValue(binding.zipcodeEt);
        String website = UIUtility.getEditTextValue(binding.descriptionEt);
        String seasonStatus = binding.tvTimeLineStatus.getText().toString();
        String scope = binding.tvSelectScope.getText().toString();
        String sendingRoaster = binding.tvTimeLineSendRoaster.getText().toString();
        String modifiedRoster = binding.tvTimeLineModifiedRoaster.getText().toString();
        String payFee = binding.tvTimeLinePayFee.getText().toString();
        String tentativeStartDate = binding.tentativeStartDate.getText().toString();
        String tentativeEndDate = binding.tentativeEndDate.getText().toString();
        if (seasonYear.equals("")) {
            binding.seasonYearEt.setError("'Season name' should not be empty.");
        } else if (selectedDivisions == null || selectedDivisions.size() == 0) {
            showToast("Please add division first");
        }else if (seasonStatus.isEmpty() || seasonStatus.equals("Season Status")) {
            showToast("Please select season status");
        } else if (checkActiveSeason() && seasonStatus.equals("Active")) {
            showToast("More than one season can not be active at same time");
        } else {
            updateSeasonReq = new UpdateSeasonReq(leagueId, seasonId, selectedDivisionsIds, seasonYear, "", "", state, city,
                    zipCode, website, getPrefHelper().getUserId(), seasonStatus, scope, sendingRoaster, modifiedRoster, payFee, tentativeStartDate, tentativeEndDate
            );
            seasonViewModel.createUpdateSeason(updateSeasonReq);
        }
    }

    public void onDeleteClick(View view) {
//        seasonViewModel.deleteSeason(new DeleteSeasonReq(updateSeasonReq.getLeagueId()));
//        seasonViewModel.getUpdateSeasonResLiveData().observe(this, res -> {
//            finishStartActivity(HomeActivity.class);
//        });
        Toast.makeText(this, "Coming Soon", Toast.LENGTH_SHORT).show();
    }


    private void selectDate(TextView textView) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String mDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        textView.setText(mDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void showMoreField(View view) {
        binding.consMoreEditField.setVisibility(View.VISIBLE);
        binding.cardTimeline.setVisibility(View.VISIBLE);
        binding.tvMoreField.setVisibility(View.GONE);
    }


    String showStatusDialog(ArrayList<String> list, String title) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle(title);
        builderSingle.setCancelable(true);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < list.size(); i++) {
            arrayAdapter.add(list.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedItem = arrayAdapter.getItem(which);
                if (title.equals("Select City")) {
                    binding.cityEt.setText(selectedItem);
                } else if (title.equals("Select Status")) {
                    binding.tvTimeLineStatus.setText(selectedItem);

                } else if (title.equals("Select Scope")) {
                    binding.tvSelectScope.setText(selectedItem);

                    if (selectedItem.equals("State")) {
                        binding.tilState.setVisibility(View.VISIBLE);
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.GONE);
                        showStatesDialog("State");

                    } else if (selectedItem.equals("Regional")) {
                        showMultipleStatesDialog();
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.GONE);
                    } else if (selectedItem.equals("Local")) {
                        showStatesDialog("Local");
                        binding.tvSelectedState.setVisibility(View.VISIBLE);
                        binding.tilCity.setVisibility(View.VISIBLE);
                        binding.tilState.setVisibility(View.VISIBLE);
                    } else {
                        binding.tilCity.setVisibility(View.GONE);
                        binding.tilState.setVisibility(View.GONE);
                        binding.tvSelectedState.setVisibility(View.GONE);
                    }
                }

            }
        });
        builderSingle.show();

        return selectedItem;
    }


    void showScopeDialog() {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select Scope");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < scopeList.size(); i++) {
            arrayAdapter.add(scopeList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                binding.tvSelectScope.setText(strName);
                if (strName.equals("State")) {
                    binding.tilState.setVisibility(View.VISIBLE);
                    binding.tvSelectedState.setVisibility(View.VISIBLE);
                    binding.tilCity.setVisibility(View.GONE);
                    showStatesDialog("State");

                } else if (strName.equals("Regional")) {
                    showMultipleStatesDialog();
                    binding.tvSelectedState.setVisibility(View.VISIBLE);
                    binding.tilState.setVisibility(View.VISIBLE);
                    binding.tilCity.setVisibility(View.GONE);
                } else if (strName.equals("Local")) {
                    binding.tvSelectedState.setVisibility(View.VISIBLE);
                    binding.tilCity.setVisibility(View.VISIBLE);
                    binding.tilState.setVisibility(View.VISIBLE);
                } else {
                    binding.tilCity.setVisibility(View.GONE);
                    binding.tilState.setVisibility(View.GONE);
                    binding.tvSelectedState.setVisibility(View.GONE);
                }
            }
        });
        builderSingle.show();
    }

    private void getStatesdata() {
        stateList.clear();
        String data = UIUtility.getAssetJsonData(CreateSeasonActivity.this, "us_states.json");
        Type type = new TypeToken<GetUsStateRes>() {
        }.getType();
        GetUsStateRes modelObject = new Gson().fromJson(data, type);
        stateList = modelObject.states;
    }

    void showStatesDialog(String scope) {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select States");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < stateList.size(); i++) {
            arrayAdapter.add(stateList.get(i).name);
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                binding.tvSelectedState.setText(strName);
                binding.stateEt.setText(strName);
                if (scope.equals("Local")) {
                    getCitiesData(strName);
                }
            }
        });
        builderSingle.show();
    }


    void showMultipleStatesDialog() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < stateList.size(); i++) {
            arrayList.add(stateList.get(i).name);
        }
        //
        boolean[] checkedItems = new boolean[stateList.size()];
        //
        for (int i = 0; i < stateList.size(); i++) {
            if (selectedStates.contains(i)) {
                checkedItems[i] = true;
            } else {
                checkedItems[i] = false;
            }
        }
        //
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select States");
        builderSingle.setCancelable(true);
        final CharSequence[] dialogList = arrayList.toArray(new CharSequence[arrayList.size()]);
        builderSingle.setMultiChoiceItems(dialogList, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked) {
                    // if the user checked the item, add it to the selected items
                    selectedStates.add(which);
                } else if (selectedStates.contains(which)) {
                    // else if the item is already in the mPlaying11List, remove it
                    selectedStates.remove(Integer.valueOf(which));
                }

                // you can also add other codes here,
                // for example a tool tip that gives user an idea of what he is selecting
                // showToast("Just an example description.");
            }

        })
                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        // user clicked OK, so save the mSelectedItems results somewhere
                        // here we are trying to retrieve the selected items indices
                        String selectedIndex = "";

                        for (Integer i : selectedStates) {
                            selectedIndex += i + ", ";
                        }

                        //showToast("Selected index: " + selectedIndex);

                        showSelectedStates(arrayList);
                    }


                })

                .show();

    }

    private void showSelectedStates(ArrayList<String> list) {
        String selectedDivision = "";
        for (int i = 0; i < selectedStates.size(); i++) {
            selectedDivision = selectedDivision + list.get(selectedStates.get(i)) + ", ";
        }
        if (!selectedDivision.isEmpty()) {
            selectedDivision = selectedDivision.substring(0, selectedDivision.length() - 2);
            binding.tvSelectedState.setText(selectedDivision);
        } else {
            binding.tvSelectedState.setText("No State selected");
        }
    }

    private void getCitiesData(String state) {
        cityList = new ArrayList();
        String data = UIUtility.getAssetJsonData(CreateSeasonActivity.this, "us_cities.json");
       /* Type type = new TypeToken<GetUsStateRes>() {
        }.getType();
        GetUsStateRes modelObject = new Gson().fromJson(data, type);*/
        JSONObject object = null;
        try {
            object = new JSONObject(data);
            JSONArray mArray = object.getJSONArray(state);
            if (mArray != null) {
                citiesArray = new String[mArray.length()];
                for (int i = 0; i < mArray.length(); i++) {
                    citiesArray[i] = mArray.getString(i);
                    cityList.add(mArray.getString(i));
                }

                showStatusDialog(cityList, "Select City");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void autoSelectFirstDivision() {
        try {
            selectedDivisions.add(0);
            selectedDivisionsIds.add(mDivisions.get(0).getDivisionId());
            //
            mAllDivisions.clear();
            for (int i = 0; i < mDivisions.size(); i++) {
                mAllDivisions.add(mDivisions.get(i).getDivisionName());
            }
            //
            showSelectedDivisions();
            //
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Boolean checkActiveSeason() {
        for (GetSeasonRes seasonRes : mSeasonsList) {


            if (seasonRes.getSeasonStatus().equals("Active")) {
                isAlreadyActive = true;
                break;
            } else {
                isAlreadyActive = false;
            }

        }
        if (updateSeasonReq != null) {
            if (updateSeasonReq.getSeasonStatus().equals("Active")) {
                isAlreadyActive = false;
            }
        }

    return isAlreadyActive;
    }

}
