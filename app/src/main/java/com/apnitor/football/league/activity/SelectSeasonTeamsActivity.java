package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectTeamListAdapter;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivitySelectLeagueTeamsBinding;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;
import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SelectSeasonTeamsActivity extends BaseActivity {

    private ActivitySelectLeagueTeamsBinding binding;
    private TeamViewModel teamViewModel;
    private SelectSeasonTeamsActivity teamListActivity = this;
    ArrayList<String> listToSend = new ArrayList<>();
    UpdateLeagueTeamReq UpdateLeagueTeamReq;
    LeagueViewModel leagueViewModel;
    private String leagueId;
    ArrayList<GetLeagueTeamsRes> alreadyAddedTeams = new ArrayList<>();
    ArrayList<GetTeamRes> teamsList = new ArrayList<>();
    String divisionName, seasonName,mSeasonId,mDivisionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_select_league_teams);

        teamViewModel = getViewModel(TeamViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            try {
                leagueId = i.getExtras().getString("leagueId").trim();
                seasonName = i.getExtras().getString("seasonName").trim();
                divisionName = i.getExtras().getString("divisionName");
                alreadyAddedTeams = (ArrayList<GetLeagueTeamsRes>) i.getExtras().getSerializable("alreadyAddedTeams");
                mSeasonId = i.getExtras().getString("seasonId").trim();
                mDivisionId = i.getExtras().getString("divisionId").trim();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        UpdateLeagueTeamReq = new UpdateLeagueTeamReq(leagueId, mSeasonId, mDivisionId, listToSend);
        setupToolbar(binding.toolbar, "Select Teams");
        setupRecyclerView();
        observeApiResponse();

    }

    private void observeApiResponse() {

        teamViewModel.getGetAllTeamsLiveData().observe(this,
                teams -> {
            if (teams.size()==0){
                binding.emptyText.setVisibility(View.VISIBLE);
                binding.saveBtn.setVisibility(View.GONE);
            }else{
                binding.emptyText.setVisibility(View.GONE);
                binding.saveBtn.setVisibility(View.VISIBLE);

                    teamsList = teams;
                    for (GetTeamRes getTeamRes : teamsList) {
                        for (GetLeagueTeamsRes team : alreadyAddedTeams) {
                            if (getTeamRes.getTeamId().equals(team.getTeamId())) {
                                getTeamRes.setSelected(true);
                            }
                        }
                    }
                    binding.rvAllTeams.setAdapter(
                            new SelectTeamListAdapter(teamListActivity, teamsList)
                    );
            }
                });

        //
        teamViewModel.getAllTeams();
        //
        leagueViewModel.getUpdateLeagueTeamResLiveData().observe(this,
                res -> {
                    finish();
                });
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void onSaveClick(View view) {
        for (GetTeamRes getTeamRes : teamsList) {
            if (getTeamRes.isSelected()) {
                listToSend.add(getTeamRes.getTeamId());
            }
        }
        leagueViewModel.updateLeagueTeams(UpdateLeagueTeamReq);
    }
}
