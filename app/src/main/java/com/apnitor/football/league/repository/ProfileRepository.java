package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.ProfileApi;
import com.apnitor.football.league.api.request.ForgotPasswordReq;
import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.api.request.NewPasswordReq;
import com.apnitor.football.league.api.request.ResetPasswordReq;
import com.apnitor.football.league.api.request.SignUpReq;
import com.apnitor.football.league.api.request.UpdateProfileReq;
import com.apnitor.football.league.api.request.VrifyOtpReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.ForgotPasswordRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.api.response.ResetPasswordRes;
import com.apnitor.football.league.api.response.SignUpRes;
import com.apnitor.football.league.api.response.UpdatePasswordRes;
import com.apnitor.football.league.api.response.UpdateProfileRes;
import com.apnitor.football.league.api.response.VerifyPasswordRes;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.schedulers.Schedulers;

public class ProfileRepository {

    private ProfileApi profileApi;

    public ProfileRepository(Application application) {
        profileApi = ApiService.getProfileApi(application);
    }

    public Single<BaseRes<LogInRes>> logIn(LogInReq logInReq) {
//        return Single.create((SingleOnSubscribe<BaseRes<LogInRes>>) emitter -> {
//
//            LogInRes logInRes = new LogInRes(logInReq.getUserName(), logInReq.getPassword());
//            emitter.onSuccess(new BaseRes<>(true, logInRes, 0, "", ""));
//        })
//                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io());
//        // TDO: 01/12/18 Please use the following code when api are working.
        return profileApi.logIn(logInReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<LogInRes>> logOut() {
        return profileApi.logOut()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<SignUpRes>> signUp(SignUpReq signUpReq) {
        return profileApi.signUp(signUpReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ResetPasswordRes>> resetPassword(ResetPasswordReq resetPasswordReq) {
        return Single.create((SingleOnSubscribe<BaseRes<ResetPasswordRes>>) emitter -> {

            ResetPasswordRes res = new ResetPasswordRes(resetPasswordReq.getEmail());
            emitter.onSuccess(new BaseRes<>(true, res, 0, "", ""));
        })
                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io());
//        return profileApi.resetPassword(resetPasswordReq)
//                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateProfileRes>> updateProfile(UpdateProfileReq updateProfileReq) {
//        return Single.create((SingleOnSubscribe<BaseRes<UpdateProfileRes>>) emitter ->
//                emitter.onSuccess(new BaseRes<>(true, new UpdateProfileRes(), 0, "", "")))
//                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.io());
        return profileApi.updateProfile(updateProfileReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ForgotPasswordRes>> forgotPassword(ForgotPasswordReq forgotPasswordReq) {
        return profileApi.forgotPassword(forgotPasswordReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<VerifyPasswordRes>> verifyOnPhone(VrifyOtpReq vrifyOtpReq) {
        return profileApi.verifyOnPhone(vrifyOtpReq)
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<VerifyPasswordRes>> verifyOnEmail(VrifyOtpReq vrifyOtpReq) {
        return profileApi.verifyOnEmail(vrifyOtpReq)
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<UpdatePasswordRes>> newPassword(NewPasswordReq newPasswordReq) {
        return profileApi.updatePassword(newPasswordReq)
                .subscribeOn(Schedulers.io());
    }
}
