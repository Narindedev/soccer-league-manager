package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.AddLeagueManagerReq;
import com.apnitor.football.league.api.request.AddLeagueRosterReq;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.DeleteLeagueReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.request.GetLeagueStandingReq;
import com.apnitor.football.league.api.request.UpdateLeagueReq;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.AddLeagueManagerRes;
import com.apnitor.football.league.api.response.AddLeagueRosterRes;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueStandingRes;
import com.apnitor.football.league.api.response.LeagueScheduleResultRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateLeagueRes;
import com.apnitor.football.league.api.response.UpdateLeagueTeamRes;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface LeagueApi {

    @POST("updateLeague")
    Single<BaseRes<UpdateLeagueRes>> updateLeague(@Body UpdateLeagueReq createLeagueReq);

    @POST("addLeagueTeams")
    Single<BaseRes<UpdateLeagueTeamRes>> updateLeagueTeams(@Body UpdateLeagueTeamReq updateLeagueTeamReq);

    @GET("getMyLeagues")
    Single<BaseRes<List<GetLeagueRes>>> getMyLeagues();

    @GET("getAllLeagues")
    Single<BaseRes<List<GetLeagueRes>>> getAllLeagues();

    @POST("getAllLeagues")
    Single<BaseRes<List<GetLeagueRes>>> getAllLeagues(@Body GetAllTeamReq getAllTeamReq);


    @POST("deleteLeague")
    Single<BaseRes<Object>> deleteLeague(@Body DeleteLeagueReq deleteLeagueReq);

    @POST("getLeagueDetail")
    Single<BaseRes<GetLeagueRes>> getLeagueDetails(@Body GetLeagueDetailsReq getLeagueDetailsReq);

    @POST("updateLeagueSchedule")
    Single<BaseRes<AddScheduleRes>> updateLeagueSchedule(@Body AddScheduleLeagueReq getLeagueDetailsReq);

    @POST("updateLeagueSchedule")
    Single<BaseRes<AddScheduleRes>> updateLeagueSchedule(@Body AddPlayerToTeamReq getLeagueDetailsReq);

    @POST("getLeagueSchedules")
    Single<BaseRes<List<AddScheduleLeagueReq>>> getLeagueSchedules(@Body GetLeagueScheduleReq getLeagueDetailsReq);

    @POST("getPastSchedules")
    Single<BaseRes<LeagueScheduleResultRes>> getLeagueResults(@Body GetLeagueScheduleReq getLeagueDetailsReq);

    @POST("getLeagueStanding")
    Single<BaseRes<List<GetLeagueStandingRes>>> getLeagueStanding(@Body GetLeagueStandingReq getLeagueStandingReq);

    @POST("addLeagueRoaster")
    Single<BaseRes<AddLeagueRosterRes>> addLeagueRoaster(@Body AddLeagueRosterReq getLeagueStandingReq);

    @POST("getLeagueRoaster")
    Single<BaseRes<AddLeagueRosterRes>> getLeagueRoaster(@Body AddLeagueRosterReq getLeagueStandingReq);


    @POST("addManagerToLeague")
    Single<BaseRes<AddLeagueManagerRes>> addLeagueManager(@Body AddLeagueManagerReq addLeagueManagerReq);

    @POST("searchLeagueManager")
    Single<BaseRes<AddLeagueManagerRes>> searchLeagueManager(@Body AddLeagueManagerReq addLeagueManagerReq);

    @POST("getSchedule")
    Single<BaseRes<List<AddScheduleLeagueReq>>> getSchedule(@Body GetLeagueScheduleReq getLeagueDetailsReq);

}
