package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.apnitor.football.league.application.FootballLeagueApplication;
import com.apnitor.football.league.application.SharedPreferenceHelper;
import com.apnitor.football.league.viewmodel.BaseViewModel;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProviders;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    protected BaseViewModel baseViewModel;
    private Toast toast;
    private ProgressDialog progressDialog;
    private FootballLeagueApplication application;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (FootballLeagueApplication) getApplication();
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        try {
            AWSMobileClient.getInstance().initialize(this).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
    }

    public void setupToolbar(Toolbar toolbar, String title) {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        assert actionbar != null;
        actionbar.setTitle(title);
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public <T extends BaseViewModel> T getViewModel(Class<T> tClass) {
        T viewModel = ViewModelProviders.of(this).get(tClass);
        this.baseViewModel = viewModel;
        handleProgress();
        handleError();
        return viewModel;
    }

    private void handleError() {
        baseViewModel.getApiErrorLiveData().observe(this, apiError -> {
            showToast(apiError.getErrorMessage());
            Log.d(TAG, apiError.toString());
//            finish();
        });
    }

    private void handleProgress() {
        baseViewModel.getApiProgressLiveData().observe(this, apiProgress -> {
            if (apiProgress.isInProgress()) {
                progressDialog.setMessage(apiProgress.getProgressMessage());
                progressDialog.show();
            } else {
                progressDialog.setMessage("");
                progressDialog.dismiss();
            }
        });
    }

    protected void showToast(String message) {
        toast.setText(message);
        toast.show();
    }

    SharedPreferenceHelper getPrefHelper() {
        return application.getSharedPreferenceHelper();
    }

    <T> void startActivity(Class<T> tClass) {
        Intent intent = new Intent(this, tClass);
        startActivity(intent);
    }

//    <T> void startActivity(Class<T> tClass, Bundle bundle) {
//        Intent intent = new Intent(this, tClass);
//        startActivity(intent, bundle);
//    }

    <T> void startActivity(Class<T> tClass, Bundle bundle) {
        Intent intent = new Intent(this, tClass);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public <T> void startActivityForResultWithExtra(Class<T> tClass, int requestCode, Bundle mBundle) {
        Intent intent = new Intent(this, tClass).putExtras(mBundle);
        startActivityForResult(intent, requestCode);
    }

    <T> void finishStartActivity(Class<T> tClass) {
        Intent intent = new Intent(this, tClass);
        startActivity(intent);
        finish();
    }

    //    <T> void finishStartActivity(Class<T> tClass, Bundle bundle) {
//        Intent intent = new Intent(this, tClass);
//        startActivity(intent, bundle);
//        finish();
//    }
    <T> void finishStartActivity(Class<T> tClass, Bundle bundle) {
        Intent intent = new Intent(this, tClass);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }

    public <T extends ViewDataBinding> T bindContentView(int layoutId) {
        return DataBindingUtil.setContentView(this, layoutId);
    }

    void showErrorOnEditText(EditText editText, String error) {
        editText.setError(error);
        editText.requestFocus();
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
