package com.apnitor.football.league.interfaces;

public interface ListItemMultipleCallback {
    void onListItemClick(Object object);
    void onLeagueListItemClick(Object object);
}
