package com.apnitor.football.league.fragment_binding_callback;

public interface ForgotPasswordFragmentBindingCallback {

    void onBack();

    void onResetPassword();
}
