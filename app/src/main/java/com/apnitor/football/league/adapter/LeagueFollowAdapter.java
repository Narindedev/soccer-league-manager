package com.apnitor.football.league.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class LeagueFollowAdapter extends RecyclerView.Adapter<LeagueFollowAdapter.ViewHolder> {

    private Context context;
    private List<GetLeagueRes> leagueRes;

    ListItemClickCallback listItemClickCallback;
//    private LanguageInterface languageInterface;


    public LeagueFollowAdapter(Context context, List<GetLeagueRes> leagueRes, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.leagueRes = leagueRes;

        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public LeagueFollowAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_league_follow_adapter, parent, false);
        return new LeagueFollowAdapter.ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(LeagueFollowAdapter.ViewHolder holder, int position) {
        try {
            GetLeagueRes model = leagueRes.get(position);
            //   model.isFollowing=false;

            holder.tvLeagueName.setText(model.getLeagueTitle());

            if (model.isFollowing) {
                holder.followImg.setImageResource(R.drawable.ic_favorite_selected);
            } else {
                holder.followImg.setImageResource(R.drawable.ic_favorite_border);
            }
            if (model.getImageUrl() != null) {
                Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_league)
                        .centerCrop()).into(holder.imgLeague);
            }  else{
                holder.imgLeague.setImageResource(R.drawable.ic_league);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueRes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLeague, followImg;
        TextView tvLeagueName;

        ViewHolder(View itemView) {
            super(itemView);
            imgLeague = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            tvLeagueName = (TextView) itemView.findViewById(R.id.tvLeagueName);
            followImg = (ImageView) itemView.findViewById(R.id.ivFollowImage);

            followImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetLeagueRes model = leagueRes.get(getAdapterPosition());
                    model.isFollowing = !model.isFollowing;
                    ((ImageView) v).setImageResource(model.isFollowing ? R.drawable.ic_favorite_selected : R.drawable.ic_favorite_border);
                    listItemClickCallback.onListItemClick(model);
                }
            });

        }
    }
}
