package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SearchLeagueManagerAdapter;
import com.apnitor.football.league.api.request.AddLeagueManagerReq;
import com.apnitor.football.league.api.response.AddLeagueManagerListRes;
import com.apnitor.football.league.databinding.ActivitySearchLeagueManagerBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.LeagueViewModel;

import androidx.recyclerview.widget.LinearLayoutManager;


public class SearchLeagueManager extends BaseActivity implements ListItemClickCallback {
    private final String LEAGUE_ID = "leagueId";
    String mPlayerId;
    AddLeagueManagerListRes mManagerRes;
    private ActivitySearchLeagueManagerBinding binding;
    private LeagueViewModel leagueViewModel;
    private String leagueId,leagueName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_league_manager);
        binding = bindContentView(R.layout.activity_search_league_manager);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        setupToolbar(binding.toolbar, "Add Manager");
        Intent i = getIntent();
        if (i.getExtras() != null) {
            leagueId = i.getExtras().getString(LEAGUE_ID);
            leagueName=i.getExtras().getString("leagueName");
        }

        leagueViewModel.addLeagueManagerResLiveData().observe(this, res -> {
            showToast("Manager added successfully");
            finish();
        });


        leagueViewModel.searchLeagueManagerResLiveData().observe(this, res -> {
            if (res.getManagerList().size() == 0) {
                binding.llSendInvitation.setVisibility(View.VISIBLE);
            } else {
                binding.llSendInvitation.setVisibility(View.GONE);
            }
            binding.rvSearchList.setLayoutManager(new LinearLayoutManager(this));
            binding.rvSearchList.setAdapter(new SearchLeagueManagerAdapter(this, res.getManagerList(), this));
        });

binding.tvInviteManager.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        sendInvitation();
    }
});
        binding.tvSendInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendInvitation();
            }
        });

    }

    private void sendInvitation() {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Football League");
            String shareMessage = "\nI am recommend you to download this app and become manager of league '"+leagueName+"'.\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=com.apnitor.football.league\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public void onSearchClick(View view) {
        String managerName = binding.managerNameEt.getText().toString().trim();
        if (managerName.isEmpty()) {
            showToast("Please enter manager name.");
        } else {
            hideKeyboard(this);
            leagueViewModel.searchLeagueManager(new AddLeagueManagerReq(managerName));
        }

    }

    @Override
    public void onListItemClick(Object object) {
        mManagerRes = (AddLeagueManagerListRes) object;
        leagueViewModel.addLeagueManager(new AddLeagueManagerReq(leagueId, mManagerRes.get_id()));

    }

}
