package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectPlayerAdapter;
import com.apnitor.football.league.adapter.SelectPlayerListAdapter;
import com.apnitor.football.league.adapter.TeamPlayersListAdapter;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import java.util.ArrayList;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SelectPlayerActivity extends BaseActivity implements ListItemClickCallback {


    private PlayerViewModel playerViewModel;
//    private FollowingViewModel followingViewModel;
    private SelectPlayerActivity playerListActivity = this;
    private String followMessage;
    private ArrayList<PlayerRes> mPlayerRes;
    final String MATCH_DETAIL = "match_detail";
    GetTeamScheduleRes mGetMatchDetail;
    androidx.appcompat.widget.Toolbar mToolbar;
    RecyclerView mRvTeams;
    TextView mTvTeam1, mTvTeam2;
    ArrayList<Boolean> mTeam1SelectedPlayers = new ArrayList<>();
    ArrayList<Boolean> mTeam2SelectedPlayers = new ArrayList<>();
    Button mBtnSave;
    String mTeamSelected = "1";
    SelectPlayerAdapter mSelectPlayerAdapter;
    ArrayList<PlayerRes> mTeamsList = new ArrayList<>();
    String LOG_TAG="SelectPlayerActivity";
    ArrayList<GetTeamPlayersRes> teamList = new ArrayList<>();
    String mTeamId="";

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_player);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mTeamId=intent.getStringExtra("TEAM_ID");
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        //
        setUpLayout();
        mPlayerRes = new ArrayList<>();
        playerViewModel = getViewModel(PlayerViewModel.class);
//        followingViewModel = getViewModel(FollowingViewModel.class);
        setupToolbar(mToolbar, mGetMatchDetail.getTeam1Name());
        setupToolbar(mToolbar, "View Players");
        setupRecyclerView();
        observeApiResponse();
    }

    private void setUpLayout() {
        mToolbar = findViewById(R.id.toolbar);
        mRvTeams = findViewById(R.id.rvAllTeams);
        mBtnSave = findViewById(R.id.saveBtn);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlayersList();
            }
        });
        //
        mTvTeam1 = findViewById(R.id.tv_team_1);
        mTvTeam1.setText(mGetMatchDetail.getTeam1Name());
        mTvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvTeam1.setTextColor(getResources().getColor(R.color.dark_black));
                mTvTeam2.setTextColor(getResources().getColor(R.color.colorAccent));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round);
                //
                mTeamSelected = "1";
                //
                mSelectPlayerAdapter = new SelectPlayerAdapter(playerListActivity, mTeamsList, RecyclerView.VERTICAL, null, mTeam1SelectedPlayers, mTeam2SelectedPlayers, mTeamSelected);
                mRvTeams.setAdapter(mSelectPlayerAdapter);
            }
        });

        //
        mTvTeam2 = findViewById(R.id.tv_team_2);
        mTvTeam2.setText(mGetMatchDetail.getTeam2Name());
        mTvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTvTeam1.setTextColor(getResources().getColor(R.color.colorAccent));
                mTvTeam2.setTextColor(getResources().getColor(R.color.dark_black));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
                //
                mTeamSelected = "2";
                //
                mSelectPlayerAdapter = new SelectPlayerAdapter(playerListActivity, mTeamsList, RecyclerView.VERTICAL, null, mTeam1SelectedPlayers, mTeam2SelectedPlayers, mTeamSelected);
                mRvTeams.setAdapter(mSelectPlayerAdapter);
            }
        });
        //
        mTvTeam1.performClick();
        mSelectPlayerAdapter = new SelectPlayerAdapter(playerListActivity, mTeamsList, RecyclerView.VERTICAL, this, mTeam1SelectedPlayers, mTeam2SelectedPlayers, mTeamSelected);
    }

    private void observeApiResponse() {
//        followingViewModel.getfollowPlayerRes().observe(this, follow -> {
//            FollowingFragment.playerFollowing = true;
//            showToast(followMessage);
//        });


        playerViewModel.getAllPlayersLiveData().observe(this,
                teams -> {
                    //
                    mTeam1SelectedPlayers.clear();
                    for (int i = 0; i < teams.size(); i++) {
                        mTeam1SelectedPlayers.add(false);
                        mTeam2SelectedPlayers.add(false);
                    }
                    //
                    mTeamsList.clear();
                    mTeamsList.addAll(teams);
                    //
//                    mRvTeams.setAdapter(
//                            new SelectPlayerAdapter(playerListActivity, teams, RecyclerView.VERTICAL, this, mTeam1SelectedPlayers,mTeam2SelectedPlayers,mTeamSelected)
//                    );
                    mSelectPlayerAdapter.notifyDataSetChanged();
                });
//        playerViewModel.getAllPlayers();

        playerViewModel.getTeamPlayersLiveData().observe(this,
                teams -> {
                    teamList = teams;
                    mRvTeams.setAdapter(
                            new SelectPlayerListAdapter(playerListActivity, teamList,"")
                    );
                });
        playerViewModel.getTeamPlayers(new GetTeamPlayersReq(mTeamId));

    }

    private void setupRecyclerView() {
        mRvTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
        mRvTeams.setAdapter(mSelectPlayerAdapter);
    }

    public void onBackClick(View view) {

        onBackPressed();
        //  finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mPlayerRes.size() > 0) {
            Intent mIntent = new Intent();
            mIntent.putParcelableArrayListExtra("playrList", mPlayerRes);
            setResult(101, mIntent);
        }
        //  finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
        PlayerRes playerRes = (PlayerRes) object;
    }


    void setPlayersList() {
        if (mTeamsList.size() <= 0)
            return;
        mGetMatchDetail.getScheduleId();
        String team1Formation="4-4-2";
        // Team 1 Players
        ArrayList<TeamPlaying11> team1 = new ArrayList<>();
        for (int i = 0; i < mTeam1SelectedPlayers.size(); i++) {
            if (mTeam1SelectedPlayers.get(i).equals(true)) {
//                team1.add(new TeamPlaying11(mTeamsList.get(i).getId(),"10","accept","1","1"));
            }
        }
        String team2Formation="4-4-2";
        // Team 2 Players
        ArrayList<TeamPlaying11> team2 = new ArrayList<>();
        for (int i = 0; i < mTeam2SelectedPlayers.size(); i++) {
            if (mTeam2SelectedPlayers.get(i).equals(true)) {
//                team2.add(new TeamPlaying11(mTeamsList.get(i).getId(),"10","accept","1","1"));
            }
        }
        //

//        AddPlayerToTeamReq addPlayerToTeamReq= new AddPlayerToTeamReq(mGetMatchDetail.getScheduleId(),team1Formation,team2Formation,team1,team2);
//        Log.d(LOG_TAG," Request is "+addPlayerToTeamReq.toString());
    }
}
