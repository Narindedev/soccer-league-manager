package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.TeamPointTableListAdapter;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamStandingRes;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamPointsTableFragment extends BaseFragment {

    private static GetTeamDetailRes mGetLeagueRes;
    TextView mTvSeason, mTvWeek;
    private List<String> mSeasonNameList = new ArrayList<>();
    private List<String> mWeekNameList = new ArrayList<>();
    private SeasonViewModel seasonViewModel;
    private LinearLayout mTableTitle;
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private RecyclerView standingRecycler;

    public static TeamPointsTableFragment newInstance(int i, String s, GetTeamDetailRes getLeagueRes) {
        mGetLeagueRes = getLeagueRes;
        TeamPointsTableFragment fragment = new TeamPointsTableFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeekNameList.add("Week 1");
        mWeekNameList.add("Week 2");
        mWeekNameList.add("Week 3");
        mWeekNameList.add("Week 4");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team_points_table, container, false);
        //
        seasonViewModel = getViewModel(SeasonViewModel.class);
        observeApiResponse();
        setRetainInstance(true);
        //
        mTableTitle = view.findViewById(R.id.llTeamTableTitle);
        mTvSeason = view.findViewById(R.id.tvSelectSeasonBtn);
        mTvWeek = view.findViewById(R.id.tvSelectWeekBtn);
        //
        mTvSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Season", Toast.LENGTH_SHORT).show();
                showSeasonDialog();
            }

        });
        mTvWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeekDialog();
            }
        });
        //
        standingRecycler = view.findViewById(R.id.rvPointsTable);
        standingRecycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        standingRecycler.setAdapter(new TeamPointTableListAdapter(getContext(), new ArrayList<GetTeamStandingRes>()));
        observeTeamStanding();
        return view;
    }


    private void observeTeamStanding() {
        if (mGetLeagueRes.getTeamModel() != null) {
            ArrayList<GetTeamStandingRes> standingRes = mGetLeagueRes.getTeamModel().getLeagueStanding();
            if (standingRes != null && standingRes.size() > 0) {
                mTableTitle.setVisibility(View.VISIBLE);
                standingRecycler.setAdapter(new TeamPointTableListAdapter(getContext(), standingRes));
            } else {
                mTableTitle.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void showSeasonDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select League");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < mSeasonNameList.size(); i++) {
            arrayAdapter.add(mSeasonNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvSeason.setText(strName);
            }
        });
        builderSingle.show();
    }

    void showWeekDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Season");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        for (int i = 0; i < mWeekNameList.size(); i++) {
            arrayAdapter.add(mWeekNameList.get(i));
        }

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvWeek.setText(strName);
            }
        });
        builderSingle.show();
    }

    private void observeApiResponse() {

        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    seasonsList = seasons;
                    mSeasonNameList.clear();
                    for (int i = 0; i < seasons.size(); i++) {
                        mSeasonNameList.add(seasons.get(i).getSeasonYear());
                    }
                    //
                    mTvSeason.setText(mSeasonNameList.get(0));
                    mTvWeek.setText(mWeekNameList.get(0));
                });

    }

    @Override
    public void onResume() {
        super.onResume();
//        if (mGetLeagueRes != null)
//            seasonViewModel.getSeasons(new GetLeagueSeasonsReq(mGetLeagueRes.getTeamId()));
    }

}
