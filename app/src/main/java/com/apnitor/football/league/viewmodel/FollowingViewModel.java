package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetFollowingReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateImageReq;
import com.apnitor.football.league.api.response.GetFollowingRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetNotificationRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.repository.FollowingRepository;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class FollowingViewModel extends BaseViewModel{
    private FollowingRepository followingRepository;
    private MutableLiveData<GetLeagueRes> leaguFollowingResLiveData = new MutableLiveData<>();
    private MutableLiveData<PlayerRes> followPlayerLiveData = new MutableLiveData<>();
    private MutableLiveData<GetTeamRes> getFollowTeamLiveData = new MutableLiveData<>();
    private MutableLiveData<GetFollowingRes> getFollowingPlayerLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamScheduleRes>> getAllFollowingScoresLiveData = new MutableLiveData<>();
    private MutableLiveData<GetNotificationRes> getAllNotificationsLiveData = new MutableLiveData<>();


    public FollowingViewModel(@NonNull Application application) {
        super(application);
        followingRepository=new FollowingRepository(application);
    }

    public LiveData<GetLeagueRes> getLeagueFollowingResLiveData() {
        return leaguFollowingResLiveData;
    }

    public void followLeague(GetLeagueDetailsReq getLeagueDetailsReq) {
        consumeApi(
                followingRepository.followLeague(getLeagueDetailsReq),
                data -> leaguFollowingResLiveData.setValue(data)
        );
    }

    public void unfollowLeague(GetLeagueDetailsReq getLeagueDetailsReq) {
        consumeApi(
                followingRepository.unfollowLeague(getLeagueDetailsReq),
                data -> leaguFollowingResLiveData.setValue(data)
        );
    }

    public LiveData<PlayerRes> getfollowPlayerRes() {
        return followPlayerLiveData;
    }

    public void followTeam(GetTeamPlayersReq playersReq) {
        consumeApi(
                followingRepository.followPlayer(playersReq),
                data -> followPlayerLiveData.setValue(data)
        );
    }

    public void unfollowTeam(GetTeamPlayersReq playersReq) {
        consumeApi(
                followingRepository.unfollowPlayer(playersReq),
                data -> followPlayerLiveData.setValue(data)
        );
    }

    public LiveData<GetTeamRes> followTeamRes() {
        return getFollowTeamLiveData;
    }

    public void followTeam(DeleteTeamReq teamReq) {
        consumeApi(
                followingRepository.followTeam(teamReq),
                data -> getFollowTeamLiveData.setValue(data)
        );
    }

    public void unfollowTeam(DeleteTeamReq teamReq) {
        consumeApi(
                followingRepository.unfollowTeam(teamReq),
                data -> getFollowTeamLiveData.setValue(data)
        );
    }

    public LiveData<GetFollowingRes> getAllfollowingRes() {
        return getFollowingPlayerLiveData;
    }
    public void getAllFollowing() {
        consumeApi(
                followingRepository.getFollowing(),
                data -> getFollowingPlayerLiveData.setValue(data)
        );
    }

    public LiveData<GetFollowingRes> getUpdateImageRes() {
        return getFollowingPlayerLiveData;
    }
    public void updateImageUrl(UpdateImageReq updateImageReq) {
        consumeApi(
                followingRepository.updateImageUrl(updateImageReq),
                data -> getFollowingPlayerLiveData.setValue(data)
        );
    }


    public LiveData<ArrayList<GetTeamScheduleRes>> getAllFollowingScoresResLiveData() {
        return getAllFollowingScoresLiveData;
    }
    public void getFollowingScores() {
        consumeApi(
                followingRepository.getFollowingScores(),
                data -> getAllFollowingScoresLiveData.setValue(data)
        );
    }


    public LiveData<GetNotificationRes> getAllNotificationsResLiveData() {
        return getAllNotificationsLiveData;
    }
    public void getAllNotifications() {
        consumeApi(
                followingRepository.getAllNotifications(),
                data -> getAllNotificationsLiveData.setValue(data)
        );
    }
}
