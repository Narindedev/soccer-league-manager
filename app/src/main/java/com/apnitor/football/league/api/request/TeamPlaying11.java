package com.apnitor.football.league.api.request;

import java.io.Serializable;

public class TeamPlaying11 implements Serializable {

    private String playerId;
    private String firstName;
    private String lastName;
    private String jerseyNumber;
    private String status;
    private String row;
    private String column;
    private Boolean isPlaying;
    private String imageUrl;

    public TeamPlaying11(){

    }

    public TeamPlaying11(String playerId, String jerseyNumber, String status, String row, String column, Boolean isPlaying) {
        this.playerId = playerId;
        this.jerseyNumber = jerseyNumber;
        this.status = status;
        this.row = row;
        this.column = column;
        this.isPlaying = isPlaying;
    }

    public TeamPlaying11(String playerId, String firstName, String lastName, String jerseyNumber, String status, String row, String column, Boolean isPlaying, String imageUrl) {
        this.playerId = playerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jerseyNumber = jerseyNumber;
        this.status = status;
        this.row = row;
        this.column = column;
        this.isPlaying = isPlaying;
        this.imageUrl = imageUrl;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(String jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public Boolean getPlaying() {
        return isPlaying;
    }

    public void setPlaying(Boolean playing) {
        isPlaying = playing;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "TeamPlaying11{" +
                "playerId='" + playerId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", jerseyNumber='" + jerseyNumber + '\'' +
                ", status='" + status + '\'' +
                ", row='" + row + '\'' +
                ", column='" + column + '\'' +
                ", isPlaying=" + isPlaying +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
