package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueShareNewsAdapter;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamShareNewsFragment extends Fragment {

    public static TeamShareNewsFragment newInstance(int i, String s) {
        TeamShareNewsFragment fragment = new TeamShareNewsFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_league_schedules, container, false);

        RecyclerView v = view.findViewById(R.id.rvLeagueSchedules);
        v.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        v.setNestedScrollingEnabled(false);
        v.setAdapter(new LeagueShareNewsAdapter(getContext(), null));
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
