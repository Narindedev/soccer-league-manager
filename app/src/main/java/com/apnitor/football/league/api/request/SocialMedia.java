package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SocialMedia implements Serializable {
    public SocialMedia(){

    }
    public SocialMedia(String website, String facebookLink, String twitterLink, String instaLink) {
        this.website = website;
        this.facebookLink = facebookLink;
        this.twitterLink = twitterLink;
        this.instaLink = instaLink;
    }

    @Expose
    @SerializedName("websiteUrl")
    public String website;

    @Expose
    @SerializedName("facebookUrl")
    public String facebookLink;

    @Expose
    @SerializedName("twitterUrl")
    public String twitterLink;
    @Expose
    @SerializedName("instagramUrl")
    public String instaLink;
}
