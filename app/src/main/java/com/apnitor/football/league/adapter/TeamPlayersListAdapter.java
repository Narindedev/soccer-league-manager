package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamPlayersListAdapter extends RecyclerView.Adapter<TeamPlayersListAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    //    private LanguageInterface languageInterface;
    String type;


    public TeamPlayersListAdapter(Context context, List<PlayerRes> teamList, String type) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

//        View view = LayoutInflater.from(context).inflate(R.layout.row_team_item, parent, false);
//        if (orientation == LinearLayoutManager.VERTICAL)
        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);

        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PlayerRes model = teamList.get(position);
            holder.tvTeamName.setText(model.getFirstName()+" "+model.getLastName());
            if (model.getProfilePhotoUrl() != null)
                Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_player)).into(holder.img);
            else {
                holder.img.setImageResource(R.drawable.ic_player);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;
        CheckBox cbLeagueImage;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            cbLeagueImage = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                }
            });
            //
            if (type.equalsIgnoreCase("ROSTER")){
                cbLeagueImage.setVisibility(View.GONE);
            }
        }
    }
}
