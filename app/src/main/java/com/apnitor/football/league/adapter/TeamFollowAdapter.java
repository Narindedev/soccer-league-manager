package com.apnitor.football.league.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class TeamFollowAdapter extends RecyclerView.Adapter<TeamFollowAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamSearchRes> teamList;
ListItemClickCallback itemClickCallback;
    public TeamFollowAdapter(Context context, ArrayList<GetTeamSearchRes> teamList, ListItemClickCallback callback) {
        this.context = context;
        this.teamList = teamList;
        itemClickCallback=callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_item_follow_team, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamSearchRes model = teamList.get(position);
            if (model.getImageUrl() != null) {
                Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                        .centerCrop()).into(holder.ivTeamImage);
            }  else{
                    holder.ivTeamImage.setImageResource(R.drawable.ic_team);
            }

          //  model.isFollowing=false;
            holder.tvTeamName.setText(model.getTeamName());
            if(model.isFollowing){
                holder.ivFollowImage.setImageResource(R.drawable.ic_favorite_selected);
            }else{
                holder.ivFollowImage.setImageResource(R.drawable.ic_favorite_border);
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivFollowImage,ivTeamImage;
//        ,imgSelect;
        TextView tvTeamName;
//        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            ivTeamImage= (ImageView) itemView.findViewById(R.id.ivTeamImage);
            ivFollowImage = (ImageView) itemView.findViewById(R.id.ivFollowImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
//            linearLayout = itemView.findViewById(R.id.llRow);
//            imgSelect = (ImageView) itemView.findViewById(R.id.img_select);

            ivFollowImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetTeamSearchRes model=teamList.get(getAdapterPosition());
                      model.isFollowing=!model.isFollowing;
                    ((ImageView) v).setImageResource(model.isFollowing ?R.drawable.ic_favorite_selected : R.drawable.ic_favorite_border);
                   /* if(!model.isFollowing){
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_selected);
                    }else{
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_border);
                    }*/

                       /* if (((ImageView) v).getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_favorite_selected).getConstantState()) {
                            ((ImageView) v).setImageResource(R.drawable.ic_favorite_border);
                        }else
                            ((ImageView) v).setImageResource(R.drawable.ic_favorite_selected);*/

                    itemClickCallback.onListItemClick(model);
                }
            });
        }
    }
}