package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectTeamPlayersListAdapter;
import com.apnitor.football.league.adapter.TeamPlayersListAdapter;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivitySelectTeamPlayersBinding;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SelectTeamPlayersActivity extends BaseActivity {

    private ActivitySelectTeamPlayersBinding binding;
    private PlayerViewModel playerViewModel;
    private SelectTeamPlayersActivity selectTeamPlayersActivity = this;
    ArrayList<String> listToSend = new ArrayList<>();
    UpdateTeamPlayersReq updateTeamPlayersReq;
    TeamViewModel teamViewModel;
    private String teamId;
    ArrayList<GetTeamPlayersRes> alreadyAddedPlayers = new ArrayList<>();

    ArrayList<PlayerRes> teamsList = new ArrayList<>();

    private final String ALREADY_ADDED_PLAYERS = "alreadyAddedPlayers";
    private final String TEAM_ID = "teamId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_select_team_players);

        playerViewModel = getViewModel(PlayerViewModel.class);
        teamViewModel = getViewModel(TeamViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            teamId = i.getExtras().getString(TEAM_ID);
            alreadyAddedPlayers = (ArrayList<GetTeamPlayersRes>) i.getExtras().getSerializable(ALREADY_ADDED_PLAYERS);
        }

        updateTeamPlayersReq = new UpdateTeamPlayersReq(teamId, listToSend);
        setupToolbar(binding.toolbar, "Select Players");
        setupRecyclerView();
        observeApiResponse();

    }

    private void observeApiResponse() {

        playerViewModel.getAllPlayersLiveData().observe(this,
                teams -> {
                    teamsList = teams;
                    for (PlayerRes playerRes : teamsList) {
                        for (GetTeamPlayersRes team : alreadyAddedPlayers) {
                            if (playerRes.getId().equals(team.getPlayerId())) {
                                playerRes.setSelected(true);
                            }
                        }
                    }

                    sortLisAplhabatically(teamsList);

                });
        playerViewModel.getAllPlayers();

        teamViewModel.getUpdateTeamPlayersResLiveData().observe(this,
                res -> {
                    finish();
                });
    }

    private void setupRecyclerView() {
        binding.rvAllPlayers.setLayoutManager(
                new LinearLayoutManager(selectTeamPlayersActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void onSaveClick(View view) {
        for (PlayerRes playerRes : teamsList) {
            if (playerRes.isSelected()) {
                listToSend.add(playerRes.getId());
            }
        }
        teamViewModel.updateTeamPlayers(updateTeamPlayersReq);
    }


    void sortLisAplhabatically(ArrayList<PlayerRes> teamList) {
        if (teamList.size() > 0) {
            Collections.sort(teamList, new Comparator<PlayerRes>() {
                @Override
                public int compare(final PlayerRes object1, final PlayerRes object2) {
                    return (object1.getFirstName()+object1.getLastName()).compareTo(object2.getFirstName()+object2.getLastName());
                }
            });
        }
        binding.rvAllPlayers.setAdapter(
                new SelectTeamPlayersListAdapter(selectTeamPlayersActivity, teamsList)
        );
    }
}
