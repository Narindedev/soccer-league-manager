package com.apnitor.football.league.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LoginActivity;
import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.databinding.FragmentLoginBinding;
import com.apnitor.football.league.fragment_binding_callback.LoginFragmentBindingCallback;
import com.apnitor.football.league.util.UIUtility;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

public class LoginFragment extends BaseFragment implements LoginFragmentBindingCallback {

    private FragmentLoginBinding binding;
    private LoginActivity loginActivity;
    private CallbackManager callbackManager;
    private String email = "";
    private String phone = "";
    private String android_id;
    private String fbId, fbFirstName, fbLastName, fbEmail, fbImgUrl = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.loginActivity = (LoginActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        getToken();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        initFBLogin();
    }

    private void initFBLogin() {

        callbackManager = CallbackManager.Factory.create();

        FacebookCallback<LoginResult> loginResultFacebookCallback = new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                getBasicDetail(loginResult);
            }

            private void getBasicDetail(LoginResult loginResult) {
                GraphRequest basicDetailRequest = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (basicDetailJsonObject, basicDetailResponse) -> {

                            try {
                                fbId = basicDetailJsonObject.getString("id");
                                fbFirstName = basicDetailJsonObject.getString("first_name");
                                fbLastName  = basicDetailJsonObject.getString("last_name");
                                fbEmail = basicDetailJsonObject.getString("email");


                                try {
                                    JSONObject fbImgUrlObject = basicDetailJsonObject.getJSONObject("picture");
                                    JSONObject UrlObject = fbImgUrlObject.getJSONObject("data");
                                    fbImgUrl = UrlObject.getString("url");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (fbEmail.isEmpty()) {
                                    showDialog();
                                } else {
                                    loginActivity.login(new LogInReq("", "", fbId, fbFirstName, fbLastName, fbEmail, fbImgUrl, "", getPrefHelper().getFirebaseToken(), "Android", android_id));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email");
                basicDetailRequest.setParameters(parameters);
                basicDetailRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(loginActivity, "You have canceled the facebook login.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(loginActivity, "Error: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
        LoginManager.getInstance().registerCallback(
                callbackManager,
                loginResultFacebookCallback
        );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFacebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("public_profile", "email")
        );
    }

    @Override
    public void onLogin() {
        email = "";
        phone = "";
        String emlPhn = UIUtility.getEditTextValue(binding.usernameEt);
        String password = UIUtility.getEditTextValue(binding.passwordEt);
        if (emlPhn.contains("@")) {
            email = emlPhn;
        } else {
            phone = emlPhn;
        }
       /* if (emlPhn.isEmpty())
            showErrorOnEditText(binding.usernameEt, "Please enter username.");
       else*/
        if (!checkUser(binding.usernameEt, emlPhn, email, phone)) {
        } else if (password.isEmpty())
            showErrorOnEditText(binding.passwordEt, "Please enter password.");
        else {
            if (emlPhn.contains("@")) {
                emlPhn = "";
            }
            loginActivity.login(new LogInReq("", password, phone, email, android_id, "Android", getPrefHelper().getFirebaseToken()));
        }
    }

    @Override
    public void onForgotPassword() {
        navController.navigate(R.id.action_forgot_password);
    }

    @Override
    public void onSignUp() {
        navController.navigate(R.id.action_sign_up);
    }

    private void getToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        getPrefHelper().saveFirebaseToken(token);
                        // Log and toast

                    }
                });
    }

    private void showDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_popup_email);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        AppCompatEditText mEmailEdt = (AppCompatEditText) dialog.findViewById(R.id.etDialogEmail);
        AppCompatButton dialogButton = (AppCompatButton) dialog.findViewById(R.id.btnDialogSubmit);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = "";
                String phone = "";
                String strEmail = UIUtility.getEditTextValue(mEmailEdt);
                if (strEmail.contains("@")) {
                    email = strEmail;
                } else {
                    phone = strEmail;
                }
                if (!checkUser(mEmailEdt, strEmail, email, phone)) {
                } else if (UIUtility.isNotValidEmail(strEmail)) {
                    showErrorOnEditText(mEmailEdt, "Please enter valid email address.");
                } else {
                    loginActivity.login(new LogInReq("", "", fbId, fbFirstName, fbLastName, email, fbImgUrl, "", getPrefHelper().getFirebaseToken(), "Android", android_id));
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
}
