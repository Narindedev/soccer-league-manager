package com.apnitor.football.league.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.FragmentPlayerStatsBinding;


public class PlayerStatsFragment extends BaseFragment {
    public static PlayerProfileRes mPlayerRes;
private FragmentPlayerStatsBinding binding;
    public static PlayerStatsFragment newInstance(PlayerProfileRes playerRes) {
        mPlayerRes =playerRes;
        PlayerStatsFragment fragment = new PlayerStatsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding=FragmentPlayerStatsBinding.inflate(inflater,container,false);
        binding.setPlayerData(mPlayerRes);

        setPlayerData();
        return binding.getRoot();
    }

    private void setPlayerData() {
        try {
            binding.tvGoals.setText("" + mPlayerRes.getGoalScored());
            binding.tvAssists.setText("" + mPlayerRes.getGoalAssisted());
           // binding.tvMinutesGoal.setText("" + mPlayerRes.getMinutesPerGoal());
            binding.tvYellowCards.setText("" + mPlayerRes.getYellowCardCount());
            binding.tvAppearances.setText("" + mPlayerRes.getMatchesPlayed());
            binding.tvRedCards.setText("" + mPlayerRes.getRedCardCount());
            //binding.tvLineups.setText("" + mPlayerRes.getLineUp());
            //binding.tvSubstitutions.setText("" + mPlayerRes.getSubstitutions());
           // binding.tvTimePlayed.setText("" + mPlayerRes.getTimePlayed());
            binding.tvTeamPlayed.setText("" + mPlayerRes.getCurrentTeam());
            binding.tvLeaguePlayed.setText("" + mPlayerRes.getLeaguePlayed().toString());
        }catch(Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
