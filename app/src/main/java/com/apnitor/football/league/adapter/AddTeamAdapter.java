package com.apnitor.football.league.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AddTeamAdapter extends RecyclerView.Adapter<AddTeamAdapter.AddTeamViewHolder> {

    @NonNull
    @Override
    public AddTeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull AddTeamViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class AddTeamViewHolder extends RecyclerView.ViewHolder {

        public AddTeamViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
