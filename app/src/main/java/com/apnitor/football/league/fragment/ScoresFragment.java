package com.apnitor.football.league.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.apnitor.football.league.adapter.ScheduleAdapter;
import com.apnitor.football.league.adapter.ScoreListAdapter;
import com.apnitor.football.league.api.response.GetEventRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.FragmentScoresBinding;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ScoresFragment extends BaseFragment {

    private FollowingViewModel followingViewModel;
    private FragmentScoresBinding binding;
    LogInRes logInRes;
    List<GetTeamScheduleRes> mGetTeamScheduleRes = new ArrayList<>();
    ScoreListAdapter mSCoreListAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentScoresBinding.inflate(inflater, container, false);
        followingViewModel = getViewModel(FollowingViewModel.class);
        setupRecyclerView();
        observeApiResponse();
        return binding.getRoot();
    }


    private void observeApiResponse() {
        followingViewModel.getAllFollowingScoresResLiveData().observe(this,
                teams -> {
                    mGetTeamScheduleRes.clear();
                    //
                    mGetTeamScheduleRes.addAll(teams);
                    if (teams.size()<1){
                        binding.tvScores.setVisibility(View.VISIBLE);
                    }else{
                        binding.tvScores.setVisibility(View.GONE);
                    }
                    //
                    setupRecyclerView();
                    //
                    filterSchedulebyDate(teams);
                });
        followingViewModel.getFollowingScores();

    }


    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false)
        );
        mSCoreListAdapter = new ScoreListAdapter(getActivity(), mGetTeamScheduleRes, null);
        binding.rvAllTeams.setAdapter(mSCoreListAdapter);
    }

    void filterScheduleResponse(List<GetTeamScheduleRes> teams) {
        mGetTeamScheduleRes.clear();
        int size = teams.size();
        for (int i = 0; i < size; i++) {
            if (teams.get(i).getRefereeId() != null && teams.get(i).getRefereeId().equalsIgnoreCase(logInRes.getUserId())) {
                mGetTeamScheduleRes.add(teams.get(i));
            }
        }
        mSCoreListAdapter.notifyDataSetChanged();
    }

    void filterSchedulebyDate(List<GetTeamScheduleRes> teams) {
        mGetTeamScheduleRes.clear();
        int size = teams.size();
        for (int i = 0; i < size; i++) {
            if (UIUtility.checkCurrentDate(teams.get(i).getDate()))
                mGetTeamScheduleRes.add(teams.get(i));
        }
        List<GetTeamScheduleRes> teamScheduleResList=mGetTeamScheduleRes;

        mGetTeamScheduleRes=getTeamsScore(teamScheduleResList);
        mSCoreListAdapter.notifyDataSetChanged();
    }
    private List<GetTeamScheduleRes> getTeamsScore(List<GetTeamScheduleRes> leagueScheduleList){
        if(leagueScheduleList.size()>0) {
            for (int i=0;i<leagueScheduleList.size();i++) {
                int team1Score=0;
                int team2Score=0;
                List<GetEventRes> mEventRes = leagueScheduleList.get(i).eventList;
                if (mEventRes!=null && mEventRes.size()>0){

                    for(int m=0;m<mEventRes.size();m++) {
                        GetEventRes eventRes = mEventRes.get(m);
                        if(eventRes.getEventType().equals("Goal")){
                            if(eventRes.getScoredByTeam().equals(leagueScheduleList.get(i).getTeam1Id())){
                                team1Score=team1Score+1;
                            }else{
                                team2Score=team2Score+1;
                            }
                        }

                    }
                    leagueScheduleList.get(i).setTeam1Score(team1Score);
                    leagueScheduleList.get(i).setTeam2Score(team2Score);
                }else{
                    leagueScheduleList.get(i).setTeam1Score(-1);
                    leagueScheduleList.get(i).setTeam2Score(-1);
                }
            }
        }
        return leagueScheduleList;
    }
}

