package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.fragment.TeamInfoDescFragment;
import com.apnitor.football.league.fragment.TeamLeaguesRosterFragment;
import com.apnitor.football.league.fragment.TeamProfileRosterFragment;
import com.apnitor.football.league.fragment.TeamShareNewsFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TeamRosterPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 2;
    GetTeamDetailRes getLeagueRes;
    private Observable mObservers = new FragmentObserver();

    public TeamRosterPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    public TeamRosterPagerAdapter(FragmentManager fm, GetTeamDetailRes getLeagueRes) {
        super(fm);
        this.getLeagueRes = getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment

                TeamProfileRosterFragment teamProfileRosterFragment = TeamProfileRosterFragment.newInstance(getLeagueRes);

                if (teamProfileRosterFragment instanceof Observer) {
                    mObservers.addObserver((Observer) teamProfileRosterFragment);
                }

                return teamProfileRosterFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title

                TeamLeaguesRosterFragment teamLeaguesRosterFragment = TeamLeaguesRosterFragment.newInstance(getLeagueRes);

                if (teamLeaguesRosterFragment  instanceof Observer) {
                    mObservers.addObserver((Observer) teamLeaguesRosterFragment);
                }


                return teamLeaguesRosterFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public void updateFragments() {
        mObservers.notifyObservers();
    }
}
