package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UpdateSeasonReq implements Serializable {

    @Expose
    @SerializedName("leagueId")
    private String leagueId;
    @Expose
    @SerializedName("seasonId")
    private String seasonId;

    @Expose
    @SerializedName("name")
    private String seasonYear;

    @Expose
    @SerializedName("entryFee")
    private String entryFee;

    @Expose
    @SerializedName("winningPrice")
    private String winningPrize;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("zipCode")
    private String zipCode;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("owner")
    private String owner;

    @Expose
    @SerializedName("status")
    private String seasonStatus;

    @Expose
    @SerializedName("scope")
    public String scope;

    @Expose
    @SerializedName("timeLineSendingRoaster")
    public String timelineSendingRoaster;

    @Expose
    @SerializedName("timelineModifiedRoster")
    public String timelineModifiedRoster;

    @Expose
    @SerializedName("timelinePayfee")
    public String timelinePayFee;

    @Expose
    @SerializedName("tentativeStartDate")
    public String tentativeStartDate;

    @Expose
    @SerializedName("tentativeEndDate")
    public String tentativeEndDate;

    @Expose
    @SerializedName("divisionIds")
    public List<String> divisionIds;


    public UpdateSeasonReq(String leagueId, String seasonId, List<String> divisionIds,String seasonYear, String entryFee, String winningPrize, String state, String city, String zipCode, String description, String owner, String seasonStatus, String scope, String timlineSendingRoaster, String timelineModifiedRoster, String timelinePayFee, String tentativeStartDate, String tentativeEndDate) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionIds=divisionIds;
        this.seasonYear = seasonYear;
        this.entryFee = entryFee;
        this.winningPrize = winningPrize;
        this.state = state;
        this.city = city;
        this.zipCode = zipCode;
        this.description = description;
        this.owner = owner;
        this.seasonStatus=seasonStatus;
        this.scope=scope;
        this.timelineSendingRoaster=timlineSendingRoaster;
        this.timelineModifiedRoster=timelineModifiedRoster;
        this.timelinePayFee=timelinePayFee;
        this.tentativeStartDate=tentativeStartDate;
        this.tentativeEndDate=tentativeEndDate;

    }

    public String getSeasonStatus() {
        return seasonStatus;
    }

    public void setSeasonStatus(String seasonStatus) {
        this.seasonStatus = seasonStatus;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonYear() {
        return seasonYear;
    }

    public void setSeasonYear(String seasonYear) {
        this.seasonYear = seasonYear;
    }

    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getWinningPrize() {
        return winningPrize;
    }

    public void setWinningPrize(String winningPrize) {
        this.winningPrize = winningPrize;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}