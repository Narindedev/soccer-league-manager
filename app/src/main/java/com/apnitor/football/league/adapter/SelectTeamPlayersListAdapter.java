package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class SelectTeamPlayersListAdapter extends RecyclerView.Adapter<SelectTeamPlayersListAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> teamList;

    public SelectTeamPlayersListAdapter(Context context, ArrayList<PlayerRes> teamList) {
        this.context = context;
        this.teamList = teamList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PlayerRes model = teamList.get(position);
            if (model.getProfilePhotoUrl() != null)
                Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions()
                        .centerCrop()).into(holder.img);
            else{
                holder.img.setImageResource(R.drawable.ic_player_default);
            }
            holder.tvTeamName.setText(model.getFirstName()+" "+model.getLastName());
//            holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark));

            if (model.isSelected()) {
                holder.mCb.setChecked(true);
            } else {
                holder.mCb.setChecked(false);
            }

            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlayerRes model = teamList.get(position);
                    model.setSelected(!model.isSelected());
//                    holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark)
//                    );
                    holder.mCb.performClick();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;
        LinearLayout linearLayout;
        CheckBox mCb;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            linearLayout = itemView.findViewById(R.id.llRow);
            mCb = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
        }
    }
}
