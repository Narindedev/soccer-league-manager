package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectTeamListAdapter;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivitySelectLeagueTeamsBinding;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SelectLeagueTeamsActivity extends BaseActivity {

    private ActivitySelectLeagueTeamsBinding binding;
    private TeamViewModel teamViewModel;
    private SelectLeagueTeamsActivity teamListActivity = this;
    ArrayList<String> listToSend = new ArrayList<>();
    UpdateLeagueTeamReq UpdateLeagueTeamReq;
    LeagueViewModel leagueViewModel;

    ArrayList<GetLeagueTeamsRes> alreadyAddedTeams = new ArrayList<>();

    ArrayList<GetTeamRes> teamsList = new ArrayList<>();
    private String leagueId;
    String divisionName, seasonName, mSeasonId, mDivisionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_select_league_teams);

        teamViewModel = getViewModel(TeamViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            mSeasonId = i.getExtras().getString("seasonId");
            mDivisionId = i.getExtras().getString("divisionId");
            leagueId = i.getExtras().getString("leagueId");
            seasonName = i.getExtras().getString("seasonName");
            divisionName = i.getExtras().getString("divisionName");
            alreadyAddedTeams = (ArrayList<GetLeagueTeamsRes>) i.getExtras().getSerializable("alreadyAddedTeams");
        }

        UpdateLeagueTeamReq = new UpdateLeagueTeamReq(leagueId, seasonName, divisionName, listToSend);
        setupToolbar(binding.toolbar, "Select Teams");
        setupRecyclerView();
        observeApiResponse();
        binding.ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("leagueId", leagueId);
                b.putString("seasonId", mSeasonId);
                b.putString("divisionId", mDivisionId);
                startActivity(SearchTeamActivity.class, b);
            }
        });
    }

    private void observeApiResponse() {

        teamViewModel.getGetAllTeamsLiveData().observe(this,
                teams -> {
                    teamsList = teams;
                    for (GetTeamRes getTeamRes : teamsList) {
                        for (GetLeagueTeamsRes team : alreadyAddedTeams) {
                            if (getTeamRes.getTeamId().equals(team.getTeamId())) {
                                getTeamRes.setSelected(true);
                            }
                        }
                    }
                    binding.rvAllTeams.setAdapter(
                            new SelectTeamListAdapter(teamListActivity, teamsList)
                    );
                });
        teamViewModel.getAllTeams();

        leagueViewModel.getUpdateLeagueTeamResLiveData().observe(this,
                res -> {
                    finish();
                });
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void onSaveClick(View view) {
        for (GetTeamRes getTeamRes : teamsList) {
            if (getTeamRes.isSelected()) {
                listToSend.add(getTeamRes.getTeamId());
            }
        }
        leagueViewModel.updateLeagueTeams(UpdateLeagueTeamReq);
    }

    public void onSearchClick(View view) {

    }

    public void onSerchClick(View view) {
        Bundle b = new Bundle();
        b.putString("leagueId", leagueId);
        b.putString("seasonId", mSeasonId);
        b.putString("divisionId", mDivisionId);
        startActivity(SearchTeamActivity.class, b);
    }
}
