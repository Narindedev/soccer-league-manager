package com.apnitor.football.league.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.ViewPropertyTransition;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueListAdapter extends RecyclerView.Adapter<LeagueListAdapter.ViewHolder> {

    private Context context;
    private List<GetLeagueRes> leagueList;
    private int orientation;
    ListItemMultipleCallback listItemClickCallback;

    public LeagueListAdapter(Context context, List<GetLeagueRes> leagueList, int orientation, ListItemMultipleCallback listItemClickCallback) {
        this.context = context;
        this.leagueList = leagueList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_league_item, parent, false);
        if (orientation == LinearLayoutManager.VERTICAL)
            view = LayoutInflater.from(context).inflate(R.layout.row_vertical_league_item, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    ViewPropertyTransition.Animator animationObject = new ViewPropertyTransition.Animator() {
        @Override
        public void animate(View view) {
            view.setAlpha(0f);
            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(700);
            fadeAnim.start();
        }
    };

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetLeagueRes model = leagueList.get(position);
            holder.txtLeagueName.setText(model.getLeagueTitle());
            if (model.getImageUrl() != null)
                Glide.with(context).load(model.getImageUrl()).transition(GenericTransitionOptions.with(animationObject)).apply(new RequestOptions()
                        .centerCrop()).into(holder.img);
            else {
                holder.img.setImageResource(R.drawable.ic_league);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView txtLeagueName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            txtLeagueName = (TextView) itemView.findViewById(R.id.tvLeagueName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onLeagueListItemClick(leagueList.get(getLayoutPosition()));
                }
            });
        }


    }
}
