package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class GameEventAdapter extends RecyclerView.Adapter<GameEventAdapter.ViewHolder> {

    private Context context;
    ArrayList<GetMatchEventRes> teamList;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    String mType;
    GetTeamScheduleRes matchDetail;
    boolean isTeam1;

    public GameEventAdapter(Context context, ArrayList<GetMatchEventRes> updateMatchEventReq, GetTeamScheduleRes matchDetail) {
        this.context = context;
        this.teamList = updateMatchEventReq;
        viewPool = new RecyclerView.RecycledViewPool();
        this.matchDetail = matchDetail;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_game_events, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetMatchEventRes teamPlaying11 = teamList.get(position);
            //

            setEventType(holder, teamPlaying11);
            //
            holder.mTvMinute.setText(teamPlaying11.eventTime + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setTeamView(ViewHolder holder, GetMatchEventRes teamPlaying11) {

        // TEAM 1
        if (matchDetail.getTeam1Id().equalsIgnoreCase(teamPlaying11.scoredByTeam.id)) {
            holder.mIvTeam2Event.setVisibility(View.INVISIBLE);
            holder.mTvTeam2.setVisibility(View.INVISIBLE);
            //
            holder.mIvTeam1Event.setVisibility(View.VISIBLE);
            holder.mTvTeam1.setVisibility(View.VISIBLE);
            //
            isTeam1 = true;
        }
        // TEAM 2
        else {
            holder.mIvTeam1Event.setVisibility(View.INVISIBLE);
            holder.mTvTeam1.setVisibility(View.INVISIBLE);
            //
            holder.mIvTeam2Event.setVisibility(View.VISIBLE);
            holder.mTvTeam2.setVisibility(View.VISIBLE);
            //
            isTeam1 = false;
        }

    }


    private void setTeamViewSub(ViewHolder holder, GetMatchEventRes teamPlaying11) {

        // TEAM 1
        if (matchDetail.getTeam1Id().equalsIgnoreCase(teamPlaying11.teamId.id)) {
            holder.mIvTeam2Event.setVisibility(View.INVISIBLE);
            holder.mTvTeam2.setVisibility(View.INVISIBLE);
            //
            holder.mIvTeam1Event.setVisibility(View.VISIBLE);
            holder.mTvTeam1.setVisibility(View.VISIBLE);
            //
            isTeam1 = true;
        }
        // TEAM 2
        else {
            holder.mIvTeam1Event.setVisibility(View.INVISIBLE);
            holder.mTvTeam1.setVisibility(View.INVISIBLE);
            //
            holder.mIvTeam2Event.setVisibility(View.VISIBLE);
            holder.mTvTeam2.setVisibility(View.VISIBLE);
            //
            isTeam1 = false;
        }

    }


    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTvTeam1, mTvTeam2, mTvMinute;
        ImageView mIvTeam1Event;
        ImageView mIvTeam2Event;
        TextView mTvPlayer1Out, mTvPlayer2Out, mTvOther;
        LinearLayout mLlTop, mLlBelow;

        ViewHolder(View itemView) {
            super(itemView);
            mTvTeam1 = itemView.findViewById(R.id.tv_team_1);
            mTvTeam2 = itemView.findViewById(R.id.tv_team_2);
            mTvPlayer1Out = itemView.findViewById(R.id.tv_team_1_sub);
            mTvPlayer2Out = itemView.findViewById(R.id.tv_team_2_sub);
            mTvMinute = itemView.findViewById(R.id.tv_minute);
            mIvTeam1Event = itemView.findViewById(R.id.iv_team_1);
            mIvTeam2Event = itemView.findViewById(R.id.iv_team_2);
            mTvOther = itemView.findViewById(R.id.tv_other);
            mLlTop = itemView.findViewById(R.id.ll_top);
            mLlBelow = itemView.findViewById(R.id.ll_below);
        }
    }


    void setEventType(ViewHolder holder, GetMatchEventRes teamPlaying11) {
        if (teamPlaying11.eventType.equalsIgnoreCase("RedCard")) {
            holder.mIvTeam1Event.setImageResource(R.drawable.ic_match_event_red_card);
            holder.mIvTeam2Event.setImageResource(R.drawable.ic_match_event_red_card);
            //
            setTeamViewSub(holder, teamPlaying11);
            //
            holder.mTvTeam1.setText(teamPlaying11.givenTo.firstName + " " + teamPlaying11.givenTo.lastName);
            holder.mTvTeam2.setText(teamPlaying11.givenTo.firstName + " " + teamPlaying11.givenTo.lastName);
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("YellowCard")) {
            holder.mIvTeam1Event.setImageResource(R.drawable.ic_match_event_yellow_card);
            holder.mIvTeam2Event.setImageResource(R.drawable.ic_match_event_yellow_card);
            //
            setTeamViewSub(holder, teamPlaying11);
            //
            holder.mTvTeam1.setText(teamPlaying11.givenTo.firstName + " " + teamPlaying11.givenTo.lastName);
            holder.mTvTeam2.setText(teamPlaying11.givenTo.firstName + " " + teamPlaying11.givenTo.lastName);
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("Substitution")) {
            holder.mIvTeam1Event.setImageResource(R.drawable.ic_substitution);
            holder.mIvTeam2Event.setImageResource(R.drawable.ic_substitution);
            //
            if (isTeam1) {
                holder.mTvPlayer1Out.setVisibility(View.VISIBLE);
                holder.mTvPlayer2Out.setVisibility(View.GONE);
            } else {
                holder.mTvPlayer1Out.setVisibility(View.GONE);
                holder.mTvPlayer2Out.setVisibility(View.VISIBLE);
            }
            //
            holder.mTvTeam1.setText(teamPlaying11.playerIn.firstName+" "+teamPlaying11.playerIn.lastName);
            holder.mTvPlayer1Out.setText(teamPlaying11.playerOut.firstName+" "+teamPlaying11.playerOut.lastName);
            holder.mTvPlayer2Out.setText(teamPlaying11.playerOut.firstName+" "+teamPlaying11.playerOut.lastName);
            //
            setTeamViewSub(holder, teamPlaying11);
            //
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("Goal")) {
            holder.mIvTeam1Event.setImageResource(R.drawable.ic_goal);
            holder.mIvTeam2Event.setImageResource(R.drawable.ic_goal);
            //
            setTeamView(holder, teamPlaying11);
            //
            holder.mTvTeam1.setText(teamPlaying11.goalScoredBy.firstName + " " + teamPlaying11.goalScoredBy.lastName);
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("Penalty")) {
            //
            setTeamView(holder, teamPlaying11);
            //
        } else if (teamPlaying11.eventType.equalsIgnoreCase("EndFirstHalf")) {
            holder.mLlTop.setVisibility(View.GONE);
            holder.mLlBelow.setVisibility(View.GONE);
            holder.mTvOther.setVisibility(View.VISIBLE);
            holder.mTvOther.setText("First half has ended");
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("StartMatch")) {
            holder.mLlTop.setVisibility(View.GONE);
            holder.mLlBelow.setVisibility(View.GONE);
            holder.mTvOther.setVisibility(View.VISIBLE);
            holder.mTvOther.setText("Match has started");
            return;
        } else if (teamPlaying11.eventType.equalsIgnoreCase("StartSecondHalf")) {
            holder.mLlTop.setVisibility(View.GONE);
            holder.mLlBelow.setVisibility(View.GONE);
            holder.mTvOther.setVisibility(View.VISIBLE);
            holder.mTvOther.setText("Second half has started");
            return;
        }else if (teamPlaying11.eventType.equalsIgnoreCase("EndMatch")) {
            holder.mLlTop.setVisibility(View.GONE);
            holder.mLlBelow.setVisibility(View.GONE);
            holder.mTvOther.setVisibility(View.VISIBLE);
            holder.mTvOther.setText("Match has Ended");
            return;
        } else {
            holder.mIvTeam1Event.setImageResource(0);
            holder.mIvTeam2Event.setImageResource(0);
        }
    }
}

