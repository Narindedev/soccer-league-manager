package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetLeagueDetailsReq {

@Expose
@SerializedName("leagueId")
    private String leagueId;

    public GetLeagueDetailsReq(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
