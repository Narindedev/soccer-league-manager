package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.UpdateProfileReq;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivitySeasonDetailPageBinding;
import com.apnitor.football.league.viewmodel.ProfileViewModel;

import java.util.ArrayList;

public class SelectUserActivity extends BaseActivity implements View.OnClickListener {

    LinearLayout mLlLeagueManager, mLlTeamManager, mLlPlayers, mLlCoach, mLlFan, mLlReferee, mLlParent;
    Button mLlSave;
    ImageView mIvLeagueManager, mIvTeamManager, mIvPlayers, mIvCoach, mIvFan, mIvReferee, mIvParent;
    boolean isLeagueManager, isTeamManager, isPlayers, isCoach, isFan, isReferee, isParent;
    String mLeagueManager, mTeamManager, mPlayers, mCoach, mFan, mReferee, mParent;
    ArrayList<String> userTypeList = new ArrayList<>();
    String LOG_TAG = "SelectUserActivity";
    LogInRes mLogInRes;
    private ActivitySeasonDetailPageBinding binding;
    private String screenType;
    private UpdateProfileReq updateProfileReq;
    private ProfileViewModel profileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user);
        mLogInRes = getPrefHelper().getLogInRes();
        profileViewModel = getViewModel(ProfileViewModel.class);
        if (getIntent().getExtras() != null) {
            screenType = getIntent().getExtras().getString("ScreenType");

            if (screenType.equals("Profile")) {
                userTypeList = getIntent().getStringArrayListExtra("UserTypes");
            } else {
                LogInRes logInRes = getPrefHelper().getLogInRes();
                userTypeList = logInRes.getUserTypes();
            }
        }

        setUpLayout();
        userTypes();
        setUserTypes();
    }

    private void setUpLayout() {
        mLlLeagueManager = findViewById(R.id.ll_league_manager);
        mLlTeamManager = findViewById(R.id.ll_team_manager);
        mLlPlayers = findViewById(R.id.ll_player);
        mLlCoach = findViewById(R.id.ll_coach);
        mLlFan = findViewById(R.id.ll_fan);
        mLlReferee = findViewById(R.id.ll_referee);
        mLlParent = findViewById(R.id.ll_parent);
        mLlSave = findViewById(R.id.selectUserTypeBtn);
        //
        mIvLeagueManager = findViewById(R.id.iv_league_manager);
        mIvTeamManager = findViewById(R.id.iv_team_manager);
        mIvPlayers = findViewById(R.id.iv_player);
        mIvCoach = findViewById(R.id.iv_coach);
        mIvFan = findViewById(R.id.iv_fan);
        mIvReferee = findViewById(R.id.iv_referee);
        mIvParent = findViewById(R.id.iv_parent);
        //
        mLlLeagueManager.setOnClickListener(this);
        mLlTeamManager.setOnClickListener(this);
        mLlPlayers.setOnClickListener(this);
        mLlCoach.setOnClickListener(this);
        mLlFan.setOnClickListener(this);
        mLlReferee.setOnClickListener(this);
        mLlParent.setOnClickListener(this);
        mLlSave.setOnClickListener(this);

    }

    private void setUserTypes() {

        if (userTypeList != null && !userTypeList.isEmpty()) {
            if (userTypeList.contains("League Manager")) {
                mIvLeagueManager.setImageResource(R.drawable.ic_check);
                isLeagueManager = true;
            }

            if (userTypeList.contains("Team Manager")) {
                mIvTeamManager.setImageResource(R.drawable.ic_check);
                isTeamManager = true;
            }
            if (userTypeList.contains("Player")) {
                mIvPlayers.setImageResource(R.drawable.ic_check);
                isPlayers = true;
            }
            if (userTypeList.contains("Referee")) {
                mIvReferee.setImageResource(R.drawable.ic_check);
                isReferee = true;
            }
            if (userTypeList.contains("Fan")) {
                mIvFan.setImageResource(R.drawable.ic_check);
                isFan = true;
            }
            if (userTypeList.contains("Parent/Guardian")) {
                mIvParent.setImageResource(R.drawable.ic_check);
                isParent = true;
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_league_manager:
                if (isLeagueManager) {
                    mIvLeagueManager.setImageResource(R.drawable.ic_uncheck);
                    isLeagueManager = false;
                    userTypeList.remove(mLeagueManager);
                } else {
                    mIvLeagueManager.setImageResource(R.drawable.ic_check);
                    isLeagueManager = true;
                    userTypeList.add(mLeagueManager);
                }
                break;
            case R.id.ll_team_manager:
                if (isTeamManager) {
                    mIvTeamManager.setImageResource(R.drawable.ic_uncheck);
                    isTeamManager = false;
                    userTypeList.remove(mTeamManager);
                } else {
                    mIvTeamManager.setImageResource(R.drawable.ic_check);
                    isTeamManager = true;
                    userTypeList.add(mTeamManager);
                }
                break;
            case R.id.ll_player:
                if (isPlayers) {
                    mIvPlayers.setImageResource(R.drawable.ic_uncheck);
                    isPlayers = false;
                    userTypeList.remove(mPlayers);
                } else {
                    mIvPlayers.setImageResource(R.drawable.ic_check);
                    isPlayers = true;
                    userTypeList.add(mPlayers);
                }
                break;
            case R.id.ll_coach:
                if (isCoach) {
                    mIvCoach.setImageResource(R.drawable.ic_uncheck);
                    isCoach = false;
                    userTypeList.remove(mCoach);
                } else {
                    mIvCoach.setImageResource(R.drawable.ic_check);
                    isCoach = true;
                    userTypeList.add(mCoach);
                }
                break;
            case R.id.ll_fan:
                if (isFan) {
                    mIvFan.setImageResource(R.drawable.ic_uncheck);
                    isFan = false;
                    userTypeList.remove(mFan);
                } else {
                    mIvFan.setImageResource(R.drawable.ic_check);
                    isFan = true;
                    userTypeList.add(mFan);
                }
                break;
            case R.id.ll_referee:
                if (isReferee) {
                    mIvReferee.setImageResource(R.drawable.ic_uncheck);
                    isReferee = false;
                    userTypeList.remove(mReferee);
                } else {
                    mIvReferee.setImageResource(R.drawable.ic_check);
                    isReferee = true;
                    userTypeList.add(mReferee);
                }
                break;
            case R.id.ll_parent:
                if (isParent) {
                    mIvParent.setImageResource(R.drawable.ic_uncheck);
                    isParent = false;
                    userTypeList.remove(mParent);
                } else {
                    mIvParent.setImageResource(R.drawable.ic_check);
                    isParent = true;
                    userTypeList.add(mParent);
                }
                break;
            case R.id.selectUserTypeBtn:
                Log.d(LOG_TAG, " Size of List is " + userTypeList.size());
                Log.d(LOG_TAG, " List is " + userTypeList.toString());
                if (userTypeList!=null && userTypeList.isEmpty()) {
                    showToast("You must select at least one 'User Type'.");
                } else {


                    updateProfileReq = new UpdateProfileReq(mLogInRes.getProfilePhotoUrl(), mLogInRes.getFirstName(), mLogInRes.getLastName(), mLogInRes.getPhone(), mLogInRes.getCountryName(),
                            mLogInRes.getState(), mLogInRes.getCity(), mLogInRes.getDateOfBirth(),mLogInRes.getHeight(),mLogInRes.getWeight(),mLogInRes.getJerseyNo(),mLogInRes.getMainPosition(),mLogInRes.getOtherPosition(),mLogInRes.getSocialMedia(),userTypeList);


                    profileViewModel.updateProfile(updateProfileReq);
                    profileViewModel.getUpdateProfileResLiveData()
                            .observe(this, res -> {
                                getPrefHelper().profileUpdated(updateProfileReq);
                                if (screenType.equals("Login")) {
                                    if (!userTypeList.contains("Player")) {
                                        showToast("Profile updated.");
                                        finishStartActivity(HomeActivity.class);
                                    } else {
                                        finishStartActivity(ProfileActivity.class);
                                    }
                                } else {
                                    Intent mIntent = new Intent();
                                    mIntent.putStringArrayListExtra("userTypes", userTypeList);
                                    setResult(111, mIntent);
                                    finish();
                                }
                                overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                            });


                }
                break;
        }
    }

    void userTypes() {
        mLeagueManager = getString(R.string.league_manager);
        mTeamManager = getString(R.string.team_manager);
        mPlayers = getString(R.string.player);
        mCoach = getString(R.string.coach);
        mFan = getString(R.string.fan);
        mReferee = getString(R.string.referee);
        mParent = getString(R.string.parent_guardian);
        //
        //
        if (userTypeList.isEmpty()) {
            mLlFan.performClick();
        }
    }

}
