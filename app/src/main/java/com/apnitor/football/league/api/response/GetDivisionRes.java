package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Make it parcelable
 */

public class GetDivisionRes implements Serializable {

    @Expose
    @SerializedName("_id")
    private String divisionId;

    @Expose
    @SerializedName("name")
    private String divisionName;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("entryFee")
    public String entryFee;

    @Expose
    @SerializedName("insuranceFee")
    public String insuranceFee;

    @Expose
    @SerializedName("fieldRentalfee")
    public String fieldRentalfee;

    @Expose
    @SerializedName("winningPrize")
    public String winningPrize;

    public GetDivisionRes(String divisionId, String divisionName) {
        this.divisionId = divisionId;
        this.divisionName = divisionName;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getInsuranceFee() {
        return insuranceFee;
    }

    public void setInsuranceFee(String insuranceFee) {
        this.insuranceFee = insuranceFee;
    }

    public String getFieldRentalfee() {
        return fieldRentalfee;
    }

    public void setFieldRentalfee(String fieldRentalfee) {
        this.fieldRentalfee = fieldRentalfee;
    }

    public String getWinningPrize() {
        return winningPrize;
    }

    public void setWinningPrize(String winningPrize) {
        this.winningPrize = winningPrize;
    }

}