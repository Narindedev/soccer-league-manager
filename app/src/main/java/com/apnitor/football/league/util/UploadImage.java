package com.apnitor.football.league.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

public class UploadImage {
    private Context mContext;
    public String mCurrentPhotoPath;
    private Uri resultUri;
    private String imgUrl;


    public UploadImage(Context context) {
        mContext = context;
    }

    public String imageActivityResult(Uri mUri) {


        CompressImage mCompressImage = new CompressImage(mContext);

        /*Todo Compress selected Image*/
        // imgBitmap.compress(Bitmap.CompressFormat.PNG,90,outputStream);
        String pathToUri = mCompressImage.compressImage(mUri.toString());
        resultUri = Uri.parse(pathToUri);

        // uploadFIle(resultUri);
        /*Todo Upload Image in foreground thread*/
        return uploadFIle(resultUri);
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private String uploadFIle(Uri fileUri) {

        BasicAWSCredentials credentials = new BasicAWSCredentials(mContext.getResources().getString(R.string.s3_access_key), mContext.getResources().getString(R.string.s3_secret));
        final AmazonS3Client s3Client = new AmazonS3Client(credentials);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        // Solution is update the Signer Version.
        clientConfiguration.setSignerOverride("S3SignerType");

        if (!s3Client.doesBucketExist("public/uploadphotos")) {
            s3Client.createBucket("public/uploadphotos");
        }
        //  Log.e("Bucketys",""+s3Client.listBuckets());
        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(s3Client)
                        .build();

        String path = fileUri.getPath(); // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        String[] img = path.split("MyFolder/Images/");

        final String uploadUrl = "public/uploadphotos/" + img[1];
// "jsaS3" will be the folder that contains the file
        final TransferObserver uploadObserver =
                transferUtility.upload(uploadUrl, file);

        uploadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    // Handle a completed download.
                    Log.e("Bucketys", "uploaded successfully");
                    String imgUrl = s3Client.getUrl("", "soccermanager-userfiles-mobilehub-529582508/" + uploadUrl).toString();
                    Log.e("Upload Image url is >>>", "" + imgUrl);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;
                Log.e("upload Handeled", "ok" + percentDone);
            }

            @Override
            public void onError(int id, Exception ex) {
                // Handle errors
                ex.getStackTrace();
                Log.e("error Handeled", "ok" + ex.getMessage());
            }

        });

// If your upload does not trigger the onStateChanged method inside your
// TransferListener, you can directly check the transfer state as shown here.
        if (TransferState.COMPLETED == uploadObserver.getState()) {
            // Handle a completed upload.
            Log.e("upload Handeled", "ok");
        }
        return imgUrl;
    }

}
