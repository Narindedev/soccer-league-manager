package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueListAdapter;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.ActivityLeagueListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.viewmodel.LeagueViewModel;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueListActivity extends BaseActivity implements ListItemMultipleCallback {

    private ActivityLeagueListBinding binding;
    private LeagueViewModel leagueViewModel;
    private LeagueListActivity leagueListActivity = this;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_league_list);

        leagueViewModel = getViewModel(LeagueViewModel.class);

        setupToolbar(binding.toolbar, "My Leagues");
        setUpRecyclerView();
        observeApiResponse();

    }

    private void setUpRecyclerView() {
        binding.rvAllLeagues.setLayoutManager(
                new LinearLayoutManager(leagueListActivity, RecyclerView.VERTICAL, false)
        );
    }

    private void observeApiResponse() {
        leagueViewModel.getLeagueListResLiveData().observe(this,
                leagues -> {
                    binding.rvAllLeagues.setAdapter(
                            new LeagueListAdapter(leagueListActivity, leagues, RecyclerView.VERTICAL, this)
                    );
                });
        leagueViewModel.getMyLeagues();
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {

    }

    @Override
    public void onLeagueListItemClick(Object object) {
        GetLeagueRes getLeagueRes = (GetLeagueRes) object;
        Intent intent = new Intent(leagueListActivity, LeagueProfileActivity.class);
        Bundle b = new Bundle();
        b.putString("league_id", getLeagueRes.getLeagueId());
        b.putString("league_name", getLeagueRes.getLeagueTitle());
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
