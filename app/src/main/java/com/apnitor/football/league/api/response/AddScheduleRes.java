package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddScheduleRes {

    @Expose
    @SerializedName("success")
    private Boolean success;

    private String scheduleId;

    public AddScheduleRes() {

    }
    public Boolean getSuccess() {
        return success;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }


}
