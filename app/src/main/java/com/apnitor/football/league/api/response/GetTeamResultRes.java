package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by deepakkanyan on 06/04/19 at 5:47 PM.
 */

public class GetTeamResultRes {
    @Expose
    @SerializedName("Past20schedules")
    private   List<GetTeamScheduleRes> Past20schedules;
    public List<GetTeamScheduleRes> getPast20schedules() {
        return Past20schedules;
    }

    public void setPast20schedules(List<GetTeamScheduleRes> past20schedules) {
        this.Past20schedules = past20schedules;
    }





}
