package com.apnitor.football.league.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class ProfileUserTypesAdapter extends RecyclerView.Adapter<ProfileUserTypesAdapter.ViewHolder> {

    private Context context;
    private List<String> userTypeList;

    ListItemMultipleCallback listItemClickCallback;

    public ProfileUserTypesAdapter(){

    }

    public ProfileUserTypesAdapter(Context context, ArrayList<String> userTypeList) {
        this.context = context;
        this.userTypeList = userTypeList;

        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ProfileUserTypesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_profile_user_types, parent, false);
        return new ProfileUserTypesAdapter.ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ProfileUserTypesAdapter.ViewHolder holder, int position) {
        try {

            holder.tvUserTypeName.setText(userTypeList.get(position));
           holder.mCb.setChecked(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return userTypeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserTypeName;
        CheckBox mCb;

        ViewHolder(View itemView) {
            super(itemView);
            tvUserTypeName = (TextView) itemView.findViewById(R.id.tvUserTypeName);
            mCb = (CheckBox) itemView.findViewById(R.id.cbUserType);

        }
    }
}
