package com.apnitor.football.league.api.request;

import com.apnitor.football.league.activity.CreateSeasonActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateTeamReq {

    @Expose
    @SerializedName("teamId")
    private String teamId;

    @Expose
    @SerializedName("name")
    private String teamName;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("coachName")
    private String coachName;

    @Expose
    @SerializedName("foundationDate")
    private String foundationDate;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("zipCode")
    private String zipcode;

    @Expose
    @SerializedName("stadium")
    private String stadium;

    @Expose
    @SerializedName("manager")
    private String manager;

    @Expose
    @SerializedName("trophy")
    private String trophy;

    @Expose
    @SerializedName("contactInfo")
    private String contactInfo;

    @Expose
    @SerializedName("owner")
    private String owner;

    @Expose
    @SerializedName("imageUrl")
    private String imageUrl;

    @Expose
    @SerializedName("homeUniform")
    private String homeUniform;
    @Expose
    @SerializedName("awayUniform")
    private String awayUniform;

    @Expose
    @SerializedName("managerName")
    private String managerName;
    @Expose
    @SerializedName("managerEmail")
    private String managerEmail;

    @Expose
    @SerializedName("managerPhone")
    private String managerPhone;


    @Expose
    @SerializedName("socialMedia")
    public SocialMedia socialMedia;

    public UpdateTeamReq(){

    }
    public UpdateTeamReq(String teamId, String teamName, String coachName, String foundationDate, String country, String state,
                         String city, String zipcode, String stadium, String manager, String trophy, String contactInfo,
                         String owner, String imageUrl,String managerName,String managerEmail,String managerPhone,
                         String description,String homeUniform,String awayUniform,SocialMedia socialMedia) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.coachName = coachName;
        this.foundationDate = foundationDate;
        this.country = country;
        this.state = state;
        this.stadium = stadium;
        this.manager = manager;
        this.trophy = trophy;
        this.contactInfo = contactInfo;
        this.owner = owner;
        this.imageUrl = imageUrl;
        this.city=city;
        this.zipcode=zipcode;
        this.managerName=managerName;
        this.managerEmail=managerEmail;
        this.managerPhone=managerPhone;
        this.socialMedia=socialMedia;
        this.description=description;
        this.homeUniform=homeUniform;
        this.awayUniform=awayUniform;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getTrophy() {
        return trophy;
    }

    public void setTrophy(String trophy) {
        this.trophy = trophy;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }



    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomeUniform() {
        return homeUniform;
    }

    public void setHomeUniform(String homeUniform) {
        this.homeUniform = homeUniform;
    }

    public String getAwayUniform() {
        return awayUniform;
    }

    public void setAwayUniform(String awayUniform) {
        this.awayUniform = awayUniform;
    }



    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }


    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }


    @Override
    public String toString() {
        return "UpdateTeamReq{" +
                "teamId='" + teamId + '\'' +
                ", teamName='" + teamName + '\'' +
                ", description='" + description + '\'' +
                ", coachName='" + coachName + '\'' +
                ", foundationDate='" + foundationDate + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", stadium='" + stadium + '\'' +
                ", manager='" + manager + '\'' +
                ", trophy='" + trophy + '\'' +
                ", contactInfo='" + contactInfo + '\'' +
                ", owner='" + owner + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", homeUniform='" + homeUniform + '\'' +
                ", awayUniform='" + awayUniform + '\'' +
                ", managerName='" + managerName + '\'' +
                ", managerEmail='" + managerEmail + '\'' +
                ", managerPhone='" + managerPhone + '\'' +
                ", socialMedia=" + socialMedia +
                '}';
    }
}
