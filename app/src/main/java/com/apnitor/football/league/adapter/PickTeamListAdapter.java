package com.apnitor.football.league.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class PickTeamListAdapter extends RecyclerView.Adapter<PickTeamListAdapter.ViewHolder> {

    ListItemClickCallback listItemClickCallback;
    LinearLayout mOldLinearLayout;

    private Context context;
    private List<GetTeamRes> teamList;
    private String type;
private int pos=-1;

    public PickTeamListAdapter(Context context, List<GetTeamRes> teamList, String type, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.type = type;
        this.listItemClickCallback = listItemClickCallback;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamRes model = teamList.get(position);

            holder.tvTeamName.setText(model.getTeamName());

            if(pos==position){
                holder.mCheckBox.setChecked(true);
            }else{
                holder.mCheckBox.setChecked(false);
            }
            holder.mCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.mCheckBox.isChecked()){
                        pos=position;
                        listItemClickCallback.onListItemClick(teamList.get(position));
                    }else{


                    }
                    notifyDataSetChanged();
                   /* if (mOldCb != null) {
                        mOldCb.setChecked(false);
                    }
                    mOldCb = holder.mCb;*/
                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img, imgSelect;
        TextView tvTeamName;
        LinearLayout linearLayout;
        CheckBox mCheckBox;

        ViewHolder(View itemView) {
            super(itemView);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            img = (ImageView) itemView.findViewById(R.id.ivLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            linearLayout = itemView.findViewById(R.id.llRow);
            imgSelect = (ImageView) itemView.findViewById(R.id.img_select);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCheckBox.performClick();
//                    if (listItemClickCallback != null)
//                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                }
            });
        }
    }
}