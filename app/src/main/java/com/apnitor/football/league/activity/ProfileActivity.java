package com.apnitor.football.league.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.ProfileUserTypesAdapter;
import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.request.UpdateProfileReq;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivityProfileBinding;
import com.apnitor.football.league.util.DateSetter;
import com.apnitor.football.league.util.PermissionUtitlity;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.util.UploadNewImages;
import com.apnitor.football.league.viewmodel.ProfileViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;

public class ProfileActivity extends BaseActivity {

    ArrayList<String> userTypes = new ArrayList<>();
    private ActivityProfileBinding binding;
    private ProfileViewModel profileViewModel;
    private UpdateProfileReq updateProfileReq;
    private LogInRes logInRes;
    private int GALLERY = 1, REQUEST_TAKE_PHOTO = 2;
    private UploadNewImages mUploadImage;
    private Uri mUri;
    private String teamId;
    String LOG_TAG="ProfileActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_profile);
        mUploadImage = new UploadNewImages(this);
        logInRes = getPrefHelper().getLogInRes();
        Log.d(LOG_TAG," Saved Profile is "+logInRes.toString());
        binding.setUserData(logInRes);
        userTypes = logInRes.getUserTypes();
        Glide.with(this).load(logInRes.getProfilePhotoUrl()).apply(new RequestOptions()
                .centerCrop().placeholder(R.drawable.ic_camera)).into(binding.photoBtn);
        profileViewModel = getViewModel(ProfileViewModel.class);
        apiResponse();
        checkIsPlayer();
        new DateSetter(binding.birthDateEt);
        showUserTypes();
    }

    private void apiResponse() {
        profileViewModel.getUpdateProfileResLiveData()
                .observe(this, res -> {
                    getPrefHelper().profileUpdated(updateProfileReq);
                    showToast("Profile updated.");
                    onBackPressed();
                });
    }

    @Override
    public void onBackPressed() {
        finishStartActivity(HomeActivity.class);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onPhotoClick(View view) {
        // Toast.makeText(this, "Functionality is not implemented yet.", Toast.LENGTH_SHORT).show();

        showPictureDialog();
    }

    private void checkIsPlayer() {
        if (userTypes.contains("Player")) {
            binding.playerEtCard.setVisibility(View.VISIBLE);
            binding.cardSocial.setVisibility(View.VISIBLE);
        } else {
            binding.playerEtCard.setVisibility(View.GONE);
            binding.cardSocial.setVisibility(View.GONE);
        }
    }

    public void onSaveClick(View view) {
//        startActivity(SelectUserActivity.class);
        saveProfile();
    }

    public void onEditUserClick(View view) {
        Bundle mBundle = new Bundle();
        mBundle.putString("ScreenType", "Profile");
        mBundle.putStringArrayList("UserTypes", userTypes);
        startActivityForResultWithExtra(SelectUserActivity.class, 111, mBundle);
    }

    public void showJerseyDialog(View view) {
        showJerseyNoDialog();
    }

    private void saveProfile() {

        String firstName = UIUtility.getEditTextValue(binding.firstNameEt);
        String phone = UIUtility.getEditTextValue(binding.phoneEt);
        String lastName = UIUtility.getEditTextValue(binding.lastNameEt);
        String countryName = UIUtility.getEditTextValue(binding.countryEt);
        String state = UIUtility.getEditTextValue(binding.stateEt);
        String city = UIUtility.getEditTextValue(binding.cityEt);
        String dateBirth = UIUtility.getEditTextValue(binding.birthDateEt);
        String height = UIUtility.getEditTextValue(binding.heightEt);
        String weight = UIUtility.getEditTextValue(binding.weightEt);
        String jerseyNo = UIUtility.getEditTextValue(binding.jerseyNoEt);
        String mainPosition = UIUtility.getEditTextValue(binding.mainPositionEt);
        String otherPosition = UIUtility.getEditTextValue(binding.otherPoistionEt);
        String facebook = UIUtility.getEditTextValue(binding.facebookEt);
        String twitter = UIUtility.getEditTextValue(binding.twitterEt);
        String insta = UIUtility.getEditTextValue(binding.instagramEt);
        SocialMedia socialMedia = new SocialMedia("", facebook, twitter, insta);
        if (firstName.isEmpty()) {
            showErrorOnEditText(binding.firstNameEt, "'first name' should not be empty.");

        } else if (checkUserType(dateBirth, mainPosition, height, weight, jerseyNo)) {

        } else {
         /*   List<String> userTypes = new ArrayList<>();
            addInListIfCheckedElseRemove(userTypes,
                    binding.fanCheckBox,
                    binding.leagueManagerCheckBox,
                    binding.teamManagerCheckBox,
                    binding.playerCheckBox,
                    binding.refereeCheckBox,
                    binding.parentGuardianCheckBox,
                    binding.coachCheckBox);
            if (userTypes.isEmpty()) {
                showToast("You must select at least one 'User Type'.");
            }*/

            logInRes = getPrefHelper().getLogInRes();
            updateProfileReq = new UpdateProfileReq(logInRes.getProfilePhotoUrl(), firstName, lastName, phone, countryName,
                    state, city, dateBirth, height, weight, jerseyNo, mainPosition, otherPosition, socialMedia, userTypes);
            profileViewModel.updateProfile(updateProfileReq);
        }
    }

    private void showUserTypes() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.rvUserTypes.setLayoutManager(layoutManager);
        ProfileUserTypesAdapter userTypesAdapter = new ProfileUserTypesAdapter(ProfileActivity.this, userTypes);
        binding.rvUserTypes.setAdapter(userTypesAdapter);

    }

    private boolean checkUserType(String dateBirth, String mainPosition, String height, String weight, String jerseyNo) {
        Boolean checkuser = false;
        if (userTypes.contains("Player")) {
            if (dateBirth.isEmpty()) {
                checkuser = true;
                showErrorOnEditText(binding.birthDateEt, "'Date of Birth' should not be empty.");
            } else if (mainPosition.isEmpty()) {
                checkuser = true;
                showErrorOnEditText(binding.mainPositionEt, "'Main Position' should not be empty.");
            } else if (height.isEmpty()) {
                checkuser = true;
                showErrorOnEditText(binding.heightEt, "'Height' should not be empty.");
            } else if (weight.isEmpty()) {
                checkuser = true;
                showErrorOnEditText(binding.weightEt, "'Weight' should not be empty.");
            } else if (jerseyNo.isEmpty()) {
                checkuser = true;
                showErrorOnEditText(binding.jerseyNoEt, "'Jersey Number' should not be empty.");
            }
        } else {
            checkuser = false;
        }
        return checkuser;
    }

    private void addInListIfCheckedElseRemove(List<String> list, CheckBox... checkBoxList) {
        for (CheckBox checkBox : checkBoxList)
            if (checkBox.isChecked())
                list.add(checkBox.getText().toString());
            else
                list.remove(checkBox.getText().toString());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionUtitlity.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                }
                break;
            case PermissionUtitlity.CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == 111) {
            if (data != null) {
                userTypes = data.getStringArrayListExtra("userTypes");
                checkIsPlayer();
                showUserTypes();

            }
        }
        if (requestCode == GALLERY) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage);//M1
                openCropActivity(selectedImage);
                //   mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage); //M1
                openCropActivity(selectedImage);
                // mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {

                    mUri = result.getUri();
                    binding.photoBtn.setImageURI(mUri);
                    Glide.with(this).load(mUri.toString()).apply(new RequestOptions()
                            .centerCrop().placeholder(R.drawable.ic_camera)).into(binding.photoBtn);
                    getPrefHelper().updateProfilePhoto(mUri.toString());
                    // mProgressBar.setVisibility(View.VISIBLE);

                    mUploadImage.imageActivityResult(mUri.toString(), getPrefHelper().getUserId(), 0);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(ProfileActivity.this);
        pictureDialog.setTitle("Select Picture");
        String[] pictureDialogItems = {
                "Pick from gallery",
                "Capture using camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (PermissionUtitlity.checkPermission(ProfileActivity.this)) {
                                    choosePhotoFromGallary();
                                }
                                break;
                            case 1:
                                if (PermissionUtitlity.checkPermissionCamera(ProfileActivity.this)) {
                                    dispatchTakePictureIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openCropActivity(Uri image_uri) {
        CropImage.activity(image_uri)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .start(ProfileActivity.this);
    }

    void showJerseyNoDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(ProfileActivity.this);
        builderSingle.setTitle("Select Jersey Number");
        builderSingle.setCancelable(true);

        final ArrayAdapter<Integer> arrayAdapter = new ArrayAdapter<Integer>(ProfileActivity.this, android.R.layout.simple_list_item_1);

        for (int i = 1; i < 999; i++) {
            arrayAdapter.add(i);
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                binding.jerseyNoEt.setText("" + which);

            }
        });
        builderSingle.show();
    }


}
