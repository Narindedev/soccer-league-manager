package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.repository.ProfileRepository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class LoginViewModel extends BaseViewModel {

    private ProfileRepository profileRepository;
    private MutableLiveData<LogInRes> logInResLiveData = new MutableLiveData<>();

    public LoginViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository(application);
    }

    public LiveData<LogInRes> getLogInResLiveData() {
        return logInResLiveData;
    }

    public void login(LogInReq logInReq) {
        consumeApi(profileRepository.logIn(logInReq), data -> logInResLiveData.setValue(data));
    }

    public void logout() {
        consumeApi(profileRepository.logOut(), data -> logInResLiveData.setValue(data));
    }
}
