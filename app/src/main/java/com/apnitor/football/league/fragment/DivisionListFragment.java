package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.CreateDivisionActivity;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.RequestCodes;
import com.apnitor.football.league.viewmodel.DivisionViewModel;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DivisionListFragment extends BaseFragment implements ListItemClickCallback, Observer {


    private static GetLeagueRes mGetLeagueRes;
    public List<GetDivisionRes> divisionList;
    RecyclerView mRvDivList;
    DivisionsListAdapter divisionsListAdapter;
    private DivisionViewModel divisionViewModel;
    private LinearLayout mEmptyDivision;

    public static DivisionListFragment newInstance(int i, String s, GetLeagueRes getLeagueRes) {
        mGetLeagueRes = getLeagueRes;
        DivisionListFragment fragment = new DivisionListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_division_list, container, false);
        //
       /* if (getArguments() != null && getArguments().getSerializable("league_info") != null) {
            mGetLeagueRes = (GetLeagueRes) getArguments().getSerializable("league_info");
        }*/

        divisionViewModel = getViewModel(DivisionViewModel.class);
        //
        mEmptyDivision = view.findViewById(R.id.llEmptyDivision);
        mRvDivList = view.findViewById(R.id.rvPointsTable);
        mRvDivList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        observeLoadData();
        observeApiResponse();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void observeLoadData() {
        if (mGetLeagueRes.divisionList != null && mGetLeagueRes.divisionList.size() > 0) {
            mEmptyDivision.setVisibility(View.GONE);
            divisionsListAdapter = new DivisionsListAdapter(getActivity(), mGetLeagueRes.divisionList, this);
            mRvDivList.setAdapter(divisionsListAdapter);
        } else {
            mEmptyDivision.setVisibility(View.VISIBLE);
        }
    }

    private void observeApiResponse() {
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    if (divisions != null && divisions.size() > 0) {
                        mEmptyDivision.setVisibility(View.GONE);
                        divisionsListAdapter = new DivisionsListAdapter(getActivity(), divisions, this);
                        mRvDivList.setAdapter(divisionsListAdapter);
                    } else {
                        mEmptyDivision.setVisibility(View.VISIBLE);
                    }
                });
    }


    @Override
    public void onListItemClick(Object object) {
        GetDivisionRes getDivisionRes = (GetDivisionRes) object;
        Intent intent = new Intent(getActivity(), CreateDivisionActivity.class);
        Bundle b = new Bundle();
        b.putString("leagueId", mGetLeagueRes.getLeagueId());
        b.putSerializable("division_detail", getDivisionRes);
        intent.putExtras(b);
        startActivityForResult(intent,RequestCodes.DIVISION_REQUEST_CODE);
    }





    @Override
    public void update(Observable observable, Object data) {

        Observable o = observable;
        Object obj = data;

        mGetLeagueRes.divisionList = ((LeagueProfileActivity) getActivity()).mGetLeagueRes.divisionList;

        if (mGetLeagueRes.divisionList != null) {
            observeLoadData();
        }


    }
}
