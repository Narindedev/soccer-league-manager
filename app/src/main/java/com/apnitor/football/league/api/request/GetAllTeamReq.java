package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllTeamReq {
    @Expose
    @SerializedName("name")
    public String name;

    @Expose
    @SerializedName("city")
    public String city;


    @Expose
    @SerializedName("zipcode")
    private String zipCode;

    public GetAllTeamReq(String name,String city,String zipCode) {
        this.name=name;
        this.city=city;
        this.zipCode=zipCode;
    }
}
