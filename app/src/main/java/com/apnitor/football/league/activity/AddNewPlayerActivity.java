package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.ViewPlayerAdapter;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.fragment.LineUpFragment;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AddNewPlayerActivity extends BaseActivity implements ListItemClickCallback {

    private PlayerViewModel playerViewModel;
    private AddNewPlayerActivity playerListActivity = this;
    private ArrayList<PlayerRes> mPlayerRes;
    final String MATCH_DETAIL = "match_detail";
    GetTeamScheduleRes mGetMatchDetail;
    androidx.appcompat.widget.Toolbar mToolbar;
    RecyclerView mRvTeams;
    ArrayList<Boolean> mTeam1SelectedPlayers = new ArrayList<>();
    Button mBtnSave;
    String mTeamSelected = "1";
    ViewPlayerAdapter mViewPlayerAdapter;
    ArrayList<GetTeamPlayersRes> mTeamsList = new ArrayList<>();
    String mTeamId = "";
    AddScheduleViewModel addScheduleViewModel;
    String TEAM_TYPE = "";
    TextView mTvTeam1, mTvTeam2;
    String LOG_TAG = "AddNewPlayerActivity";
    String mAddedPlayerID = "";


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_player);
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mTeamId = intent.getStringExtra("TEAM_ID");
                TEAM_TYPE = intent.getStringExtra("TEAM_TYPE");
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        //
        setUpLayout();
        mPlayerRes = new ArrayList<>();
        playerViewModel = getViewModel(PlayerViewModel.class);
        setupToolbar(mToolbar, mGetMatchDetail.getTeam1Name());
        setupToolbar(mToolbar, "Add Player");
        setupRecyclerView();
    }

    private void setUpLayout() {

        mToolbar = findViewById(R.id.toolbar);
        mRvTeams = findViewById(R.id.rvAllTeams);
        mBtnSave = findViewById(R.id.saveBtn);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlayersList();
            }
        });
        mViewPlayerAdapter = new ViewPlayerAdapter(playerListActivity, mTeamsList, this, mTeam1SelectedPlayers, mTeamSelected);


        mTvTeam1 = findViewById(R.id.tvTeam1);
        mTvTeam1.setText(mGetMatchDetail.getTeam1Name());
        mTvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TEAM_TYPE = "ONE";
                    mTvTeam1.setTextColor(getResources().getColor(R.color.dark_black));
                    mTvTeam2.setTextColor(getResources().getColor(R.color.colorAccent));
                    mTvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                    mTvTeam2.setBackgroundResource(R.drawable.shape_right_round);
                    mTeamsList.clear();
                    Log.d(LOG_TAG, " Team 1 " + mGetMatchDetail.getTeam1Playing11().toString());
                    for (int i = 0; i < mGetMatchDetail.getTeam1Playing11().size(); i++) {
                        GetTeamPlayersRes getTeamPlayersRes = new GetTeamPlayersRes();
                        getTeamPlayersRes.setFirstName(mGetMatchDetail.getTeam1Playing11().get(i).getFirstName());
                        getTeamPlayersRes.setLastName(mGetMatchDetail.getTeam1Playing11().get(i).getLastName());
                        getTeamPlayersRes.setPlayerId(mGetMatchDetail.getTeam1Playing11().get(i).getPlayerId());
                        getTeamPlayersRes.setProfilePhotoUrl(mGetMatchDetail.getTeam1Playing11().get(i).getImageUrl());
                        getTeamPlayersRes.setPlayerId(mGetMatchDetail.getTeam1Playing11().get(i).getPlayerId());
                        //                        if (!mGetMatchDetail.getTeam1Playing11().get(i).getPlaying() && mGetMatchDetail.getTeam1Playing11().get(i).getStatus().equalsIgnoreCase("accept"))
                        mTeamsList.add(getTeamPlayersRes);
                    }
                    mViewPlayerAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //
        mTvTeam2 = findViewById(R.id.tvTeam2);
        mTvTeam2.setText(mGetMatchDetail.getTeam2Name());
        mTvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    TEAM_TYPE = "TWO";
                    mTvTeam1.setTextColor(getResources().getColor(R.color.colorAccent));
                    mTvTeam2.setTextColor(getResources().getColor(R.color.dark_black));
                    //
                    mTvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                    mTvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
//                    //
                    mTeamsList.clear();
                    Log.d(LOG_TAG, " Team 2 " + mGetMatchDetail.getTeam2Playing11().toString());
                    for (int i = 0; i < mGetMatchDetail.getTeam2Playing11().size(); i++) {
                        GetTeamPlayersRes getTeamPlayersRes = new GetTeamPlayersRes();
                        getTeamPlayersRes.setFirstName(mGetMatchDetail.getTeam2Playing11().get(i).getFirstName());
                        getTeamPlayersRes.setLastName(mGetMatchDetail.getTeam2Playing11().get(i).getLastName());
                        getTeamPlayersRes.setPlayerId(mGetMatchDetail.getTeam2Playing11().get(i).getPlayerId());
//                        if (!mGetMatchDetail.getTeam1Playing11().get(i).getPlaying() && mGetMatchDetail.getTeam1Playing11().get(i).getStatus().equalsIgnoreCase("accept"))
                        mTeamsList.add(i, getTeamPlayersRes);
                    }
                    mViewPlayerAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mTvTeam1.performClick();
    }


    private void setupRecyclerView() {
        mRvTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
        mRvTeams.setAdapter(mViewPlayerAdapter);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }


    @Override
    public void onListItemClick(Object object) {
        GetTeamPlayersRes playerRes = (GetTeamPlayersRes) object;
        mAddedPlayerID = playerRes.getPlayerId();
    }


    void setPlayersList() {
        if (mTeamsList.size() <= 0)
            return;

        if (mAddedPlayerID.isEmpty()) {
            Toast.makeText(playerListActivity, "Please select a player.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
            for (int i = 0; i < mGetMatchDetail.getTeam1Playing11().size(); i++) {
                if (mGetMatchDetail.getTeam1Playing11().get(i).getPlayerId().equalsIgnoreCase(mAddedPlayerID)) {
                    mGetMatchDetail.getTeam1Playing11().get(i).setStatus("Accept");
                    mGetMatchDetail.getTeam1Playing11().get(i).setPlaying(true);
                    mGetMatchDetail.getTeam1Playing11().get(i).setJerseyNumber("");
                    mGetMatchDetail.getTeam1Playing11().get(i).setRow("-1");
                    mGetMatchDetail.getTeam1Playing11().get(i).setColumn("-1");
                    break;
                }
            }
        } else {
            for (int i = 0; i < mGetMatchDetail.getTeam2Playing11().size(); i++) {
                if (mGetMatchDetail.getTeam2Playing11().get(i).getPlayerId().equalsIgnoreCase(mAddedPlayerID)) {
                    mGetMatchDetail.getTeam2Playing11().get(i).setStatus("Accept");
                    mGetMatchDetail.getTeam2Playing11().get(i).setPlaying(true);
                    mGetMatchDetail.getTeam1Playing11().get(i).setJerseyNumber("");
                    mGetMatchDetail.getTeam1Playing11().get(i).setRow("-1");
                    mGetMatchDetail.getTeam1Playing11().get(i).setColumn("-1");
                    break;
                }
            }
        }

        AddPlayerToTeamReq addPlayerToTeamReq = new AddPlayerToTeamReq();
        addPlayerToTeamReq.setLeagueId(mGetMatchDetail.getLeagueId());
        addPlayerToTeamReq.setDivisionId(mGetMatchDetail.getDivisionId());
        addPlayerToTeamReq.setSeasonId(mGetMatchDetail.getSeasonId());
        addPlayerToTeamReq.setScheduleId(mGetMatchDetail.getScheduleId());
        if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
            addPlayerToTeamReq.setTeam1Formation(mGetMatchDetail.getTeam1Formation());
            addPlayerToTeamReq.setTeam1Playing11(mGetMatchDetail.getTeam1Playing11());
        } else {
            addPlayerToTeamReq.setTeam2Formation(mGetMatchDetail.getTeam2Formation());
            addPlayerToTeamReq.setTeam2Playing11(mGetMatchDetail.getTeam2Playing11());
        }
        addScheduleViewModel.updateScheduleTeam(addPlayerToTeamReq);
        addScheduleViewModel.updateScheduleLiveData().observe(this, addScheduleRes -> {
            if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
                LineUpFragment.mGetMatchDetail.setTeam1Playing11(mGetMatchDetail.getTeam1Playing11());
            } else {
                LineUpFragment.mGetMatchDetail.setTeam2Playing11(mGetMatchDetail.getTeam2Playing11());
            }
            finish();
        });
    }
}
