package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ViewPlayerAdapter extends RecyclerView.Adapter<ViewPlayerAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamPlayersRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    ArrayList<Boolean> mTeam1SelectedPlayers;
    CheckBox mOldCb;


    public ViewPlayerAdapter(Context context, ArrayList<GetTeamPlayersRes> teamList, ListItemClickCallback listItemClickCallback, ArrayList<Boolean> mTeam1SelectedPlayers, String mTeamSelected) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
        this.mTeam1SelectedPlayers = mTeam1SelectedPlayers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_select_team, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamPlayersRes model = teamList.get(position);
            holder.tvTeamName.setText(model.getFirstName() + " " + model.getLastName());
            //
            holder.mCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    mTeam1SelectedPlayers.set(position, isChecked);
                    if (mOldCb != null)
                        mOldCb.setChecked(false);
                    mOldCb = holder.mCb;
                }
            });
            //
//            if (mTeam1SelectedPlayers.get(position) == true) {
//                holder.mCb.setChecked(true);
//            } else {
//                holder.mCb.setChecked(false);
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox mCb;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            mCb = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null) {
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                        mCb.performClick();
                    }
                }
            });
        }
    }
}
