package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apnitor.football.league.R;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class LeaguePlayerStatsListAdapter extends RecyclerView.Adapter<LeaguePlayerStatsListAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;

    public LeaguePlayerStatsListAdapter(Context context, ArrayList<String> teamList) {
        this.context = context;
        this.teamList = teamList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_player_stats, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
//            GetTeamRes model = teamList.get(position);


//            holder.tvTeamName2.setText(model.getTeamName());
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return 3;
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvSeasonDayDate);
        }
    }
}
