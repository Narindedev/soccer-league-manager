package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.PointsTableFragment;
import com.apnitor.football.league.fragment.SeasonsResultFragment;
import com.apnitor.football.league.fragment.StatsFragment;
import com.apnitor.football.league.fragment.TeamPointsTableFragment;
import com.apnitor.football.league.fragment.TeamResultFragment;
import com.apnitor.football.league.fragment.TeamSchedulesFragment;
import com.apnitor.football.league.fragment.TeamStatsFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class TeamSeasonPagerAdapater extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 4;
    GetTeamDetailRes mGetLeagueRes;


    public TeamSeasonPagerAdapater(FragmentManager fm, GetTeamDetailRes getLeagueRes) {
        super(fm);
        this.mGetLeagueRes=getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return TeamResultFragment.newInstance(0, "Page # 1",mGetLeagueRes);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return TeamSchedulesFragment.newInstance(0, mGetLeagueRes);
            case 2: // Fragment # 1 - This will show SecondFragment
                return TeamPointsTableFragment.newInstance(0, "Page # 1",mGetLeagueRes);
            case 3: // Fragment # 1 - This will show SecondFragment
                return TeamStatsFragment.newInstance(mGetLeagueRes);

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
