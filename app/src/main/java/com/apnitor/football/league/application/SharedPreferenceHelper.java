package com.apnitor.football.league.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.request.UpdateProfileReq;
import com.apnitor.football.league.api.response.LogInRes;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class SharedPreferenceHelper {

    private static final String DELIMITER = ";";
    private static final String IS_LOGGED_IN = "isLoggedIn";
    private static final String AUTH_TOKEN = "authToken";
    private static final String USER_ID = "userId";
    private static final String USER_NAME = "username";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String EMAIL = "email";
    private static final String PROFILE_PHOTO_URL = "profilePhotoUrl";
    private static final String IS_FIRST_TIME = "isFirstTime";
    private static final String PHONE = "phone";
    private static final String COUNTRY_NAME = "countryName";
    private static final String STATE = "state";
    private static final String CITY = "city";
    private static final String DATE_BIRTH = "dateBirth";
    private static final String WEIGHT = "weight";
    private static final String HEIGHT = "height";
    private static final String JERSEY_NUMBER = "jerseyNumber";
    private static final String MAIN_POSITION = "mainPosition";
    private static final String OTHER_POSITION = "otherPosition";
    private static final String PLAYER_SOCIAL_MEDIA = "playerSocialMedia";


    private static final String USER_TYPES = "userTypes";
    private static final String FIREBASE_TOKEN = "firebase_token";
    private SharedPreferences sharedPreferences;

    /**
     * Please get the instance of this class using getApplication().getSharedPreferenceHelper().
     */
    public SharedPreferenceHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("FootballLeaguePrefs", Context.MODE_PRIVATE);
    }

    public SharedPreferenceHelper(FootballLeagueApplication application) {
        sharedPreferences = application.getSharedPreferences("FootballLeaguePrefs", Context.MODE_PRIVATE);
    }

    public void logIn(LogInRes logInRes) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(AUTH_TOKEN, logInRes.getAuthToken());
        editor.putString(USER_ID, logInRes.getUserId());
        editor.putString(USER_NAME, logInRes.getUsername());
        editor.putString(FIRST_NAME, logInRes.getFirstName());
        editor.putString(LAST_NAME, logInRes.getLastName());
        editor.putString(EMAIL, logInRes.getEmail());
        editor.putString(PROFILE_PHOTO_URL, logInRes.getProfilePhotoUrl());
        editor.putString(PHONE, logInRes.getPhone());
        editor.putString(COUNTRY_NAME, logInRes.getCountryName());
        editor.putString(STATE, logInRes.getState());
        editor.putString(CITY, logInRes.getCity());
        editor.putString(USER_TYPES, listToString(logInRes.getUserTypes()));
        editor.apply();
    }

    public void profileUpdated(UpdateProfileReq updateProfileReq) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_LOGGED_IN, true);
        editor.putString(FIRST_NAME, updateProfileReq.getFirstName());
        editor.putString(LAST_NAME, updateProfileReq.getLastName());
        editor.putString(PROFILE_PHOTO_URL, updateProfileReq.getProfilePhotoUrl());
        editor.putString(PHONE, updateProfileReq.getPhone());
        editor.putString(COUNTRY_NAME, updateProfileReq.getCountryName());
        editor.putString(STATE, updateProfileReq.getState());
        editor.putString(CITY, updateProfileReq.getCity());
        editor.putString(DATE_BIRTH, updateProfileReq.getDateOfBirth());
        editor.putString(HEIGHT, updateProfileReq.getHeight());
        editor.putString(WEIGHT, updateProfileReq.getWeight());
        editor.putString(JERSEY_NUMBER, updateProfileReq.getJerseyNo());
        editor.putString(MAIN_POSITION, updateProfileReq.getMainPosition());
        editor.putString(OTHER_POSITION, updateProfileReq.getOtherPosition());
        editor.putString(PLAYER_SOCIAL_MEDIA, objectToString(updateProfileReq.getSocialMedia()));
        editor.putString(USER_TYPES, listToString(updateProfileReq.getUserTypes()));
        editor.apply();
    }

    public void updateProfilePhoto(String url) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PROFILE_PHOTO_URL, url);
        editor.apply();
    }

    public void setIsFirstTime(Boolean isFirstTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_FIRST_TIME, isFirstTime);
        editor.apply();
    }

    public void logOut() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_LOGGED_IN, false);
        editor.putString(AUTH_TOKEN, "");
        editor.putString(USER_ID, "");
        editor.putString(USER_NAME, "");
        editor.putString(FIRST_NAME, "");
        editor.putString(LAST_NAME, "");
        editor.putString(EMAIL, "");
        editor.putString(PROFILE_PHOTO_URL, "");
        editor.putString(PHONE, "");
        editor.putString(COUNTRY_NAME, "");
        editor.putString(STATE, "");
        editor.putString(CITY, "");
        editor.putString(DATE_BIRTH, "");
        editor.putString(HEIGHT, "");
        editor.putString(WEIGHT, "");
        editor.putString(JERSEY_NUMBER, "");
        editor.putString(MAIN_POSITION,"");
        editor.putString(OTHER_POSITION, "");
        editor.putString(PLAYER_SOCIAL_MEDIA, "");
        editor.putString(USER_TYPES, "");
        editor.apply();
    }

    public void setUserID(String userID) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_ID, userID);
        editor.apply();
    }


    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public boolean isFirstTime() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME, true);
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, null);
    }

    public void saveFirebaseToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FIREBASE_TOKEN, token);
        editor.apply();
    }

    public String getFirebaseToken() {
        return sharedPreferences.getString(FIREBASE_TOKEN, "");
    }

    public LogInRes getLogInRes() {
        return new LogInRes(
                sharedPreferences.getString(AUTH_TOKEN, ""),
                sharedPreferences.getString(USER_ID, ""),
                sharedPreferences.getString(USER_NAME, ""),
                sharedPreferences.getString(FIRST_NAME, ""),
                sharedPreferences.getString(LAST_NAME, ""),
                sharedPreferences.getString(EMAIL, ""),
                sharedPreferences.getString(PROFILE_PHOTO_URL, ""),
                sharedPreferences.getString(PHONE, ""),
                sharedPreferences.getString(COUNTRY_NAME, ""),
                sharedPreferences.getString(STATE, ""),
                sharedPreferences.getString(CITY, ""),
                sharedPreferences.getString(DATE_BIRTH, ""),
                sharedPreferences.getString(HEIGHT, ""),
                sharedPreferences.getString(WEIGHT, ""),
                sharedPreferences.getString(JERSEY_NUMBER, ""),
                sharedPreferences.getString(MAIN_POSITION, ""),
                sharedPreferences.getString(OTHER_POSITION, ""),
                stringToObject(sharedPreferences.getString(PLAYER_SOCIAL_MEDIA, "")),
                stringToList(Objects.requireNonNull(sharedPreferences.getString(USER_TYPES, "")))
        );
    }

    public String getAuthToken() {
        return sharedPreferences.getString(AUTH_TOKEN, "");
    }

    private String listToString(List<String> list) {
        if (list == null || list.isEmpty()) return "";
        return TextUtils.join(DELIMITER, list);
    }

    private SocialMedia stringToObject(String string){
        Gson gson = new Gson();

        SocialMedia obj = gson.fromJson(string, SocialMedia.class);
        return obj;
    }

    private String objectToString( SocialMedia socialMedia){

        Gson gson = new Gson();
        String json = gson.toJson(socialMedia);
        return json;
    }

    private ArrayList<String> stringToList(String str) {
        ArrayList<String> arrayList=new ArrayList();
        if (str.isEmpty())
            return arrayList;

        List<String> stringList = Arrays.asList(str.split(DELIMITER));
        arrayList.addAll(stringList);
        if (stringList == null)
            return new ArrayList<>();
        else
            return arrayList;
    }
}
