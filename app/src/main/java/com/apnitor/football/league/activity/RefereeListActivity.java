package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.RefereeListAdapter;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivityRefereeListBinding;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RefereeListActivity extends BaseActivity implements ListItemMultipleCallback {

    private RefereeViewModel refereeViewModel;
    ActivityRefereeListBinding binding;
    private RefereeListActivity teamListActivity = this;
    GetRefereeRes getTeamRes;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_referee_list);
        refereeViewModel = getViewModel(RefereeViewModel.class);
        setupToolbar(binding.toolbar, "Select Referee");
        setupRecyclerView();
        observeApiResponse();
        binding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getTeamRes == null) {
                    Toast.makeText(teamListActivity, "Please select a Referee", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(teamListActivity, AddScheduleActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("referee_detail", getTeamRes);
                intent.putExtras(b);
                setResult(201, intent);
                onBackPressed();
            }
        });
    }

    private void observeApiResponse() {
        refereeViewModel.getAllRefereeLiveData().observe(this,
                teams -> {
                    binding.rvAllTeams.setAdapter(
                            new RefereeListAdapter(teamListActivity, teams, RecyclerView.VERTICAL, this)
                    );
                });
        refereeViewModel.getAllReferee();
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
        getTeamRes = (GetRefereeRes) object;

    }

    @Override
    public void onLeagueListItemClick(Object object) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
