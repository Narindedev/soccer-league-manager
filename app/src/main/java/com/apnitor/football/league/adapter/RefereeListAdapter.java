package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetRefereeRes;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class RefereeListAdapter extends RecyclerView.Adapter<RefereeListAdapter.ViewHolder> {

    private Context context;
    private List<GetRefereeRes> refereeList;
    private int orientation;
    ListItemMultipleCallback listItemClickCallback;
    //    private LanguageInterface languageInterface;
    CheckBox mOldCb;
    int pos=-1;


    public RefereeListAdapter(Context context, ArrayList<GetRefereeRes> teamList, int orientation, ListItemMultipleCallback listItemClickCallback) {
        this.context = context;
        this.refereeList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetRefereeRes model = refereeList.get(position);
            holder.tvTeamName.setText(model.getFirstName() + " " + model.getLastName());
            Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_referee)).into(holder.img);
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

            if(pos==position){
                holder.mCb.setChecked(true);
            }else{
                holder.mCb.setChecked(false);
            }

            holder.mCb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.mCb.isChecked()){
                        pos=position;
                        listItemClickCallback.onListItemClick(refereeList.get(position));
                    }else{


                    }
                    notifyDataSetChanged();
                   /* if (mOldCb != null) {
                        mOldCb.setChecked(false);
                    }
                    mOldCb = holder.mCb;*/
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return refereeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;
        CheckBox mCb;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            mCb = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null) {
                        mCb.performClick();
                    }
                }
            });
        }
    }
}
