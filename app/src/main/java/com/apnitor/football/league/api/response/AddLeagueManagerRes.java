package com.apnitor.football.league.api.response;

import java.util.List;

/**
 * Created by deepakkanyan on 06/04/19 at 10:32 PM.
 */

public class AddLeagueManagerRes {
   private List<AddLeagueManagerListRes> managerList;


    public List<AddLeagueManagerListRes> getManagerList() {
        return managerList;
    }

    public void setManagerList(List<AddLeagueManagerListRes> managerList) {
        this.managerList = managerList;
    }
}
