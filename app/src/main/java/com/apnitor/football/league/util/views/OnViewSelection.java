package com.apnitor.football.league.util.views;

/**
 * Created by nct119 on 30/6/17.
 */

public interface OnViewSelection {

    public void viewSelectedPosition(int position,Object tag);
}
