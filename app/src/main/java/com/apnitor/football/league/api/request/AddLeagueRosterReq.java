package com.apnitor.football.league.api.request;

import java.util.ArrayList;

public class AddLeagueRosterReq {
    private String teamId;
    private String leagueId;
    private ArrayList<String> playerIds;


   public AddLeagueRosterReq(String teamId,String leagueId,ArrayList<String> playerIds){
       this.teamId=teamId;
       this.leagueId=leagueId;
       this.playerIds=playerIds;
   }

    public AddLeagueRosterReq(String teamId,String leagueId){
        this.teamId=teamId;
        this.leagueId=leagueId;

    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public ArrayList<String> getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(ArrayList<String> playerIds) {
        this.playerIds = playerIds;
    }
}
