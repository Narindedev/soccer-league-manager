package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.DeleteSeasonReq;
import com.apnitor.football.league.api.request.GetLeagueSeasonsReq;
import com.apnitor.football.league.api.request.UpdateSeasonReq;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.UpdateSeasonRes;
import com.apnitor.football.league.repository.SeasonRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SeasonViewModel extends BaseViewModel {

    private SeasonRepository seasonRepository;
    private MutableLiveData<UpdateSeasonRes> updateSeasonResLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetSeasonRes>> leagueListResLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> deleteSeasonResLiveData = new MutableLiveData<>();

    public SeasonViewModel(@NonNull Application application) {
        super(application);
        seasonRepository = new SeasonRepository(application);
    }

    public LiveData<UpdateSeasonRes> getUpdateSeasonResLiveData() {
        return updateSeasonResLiveData;
    }

    public LiveData<List<GetSeasonRes>> getLeagueListResLiveData() {
        return leagueListResLiveData;
    }


    public void createUpdateSeason(UpdateSeasonReq updateSeasonReq) {
        consumeApi(
                seasonRepository.createUpdateSeason(updateSeasonReq),
                data -> updateSeasonResLiveData.setValue(data)
        );
    }

    public void getSeasons(GetLeagueSeasonsReq getLeagueSeasonsReq) {
        consumeApi(
                seasonRepository.getSeasons(getLeagueSeasonsReq),
                data -> leagueListResLiveData.setValue(data)
        );
    }

    public void deleteSeason(DeleteSeasonReq deleteSeasonReq) {
        consumeApi(
                seasonRepository.deleteSeason(deleteSeasonReq),
                data -> deleteSeasonResLiveData.setValue(data)
        );
    }

    public MutableLiveData<Object> getDeleteSeasonResLiveData() {
        return deleteSeasonResLiveData;
    }
}

