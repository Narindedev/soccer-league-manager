package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.fragment.GameEventFragment;
import com.apnitor.football.league.fragment.LeagueSchedulesFragment;
import com.apnitor.football.league.fragment.LineUpFragment;
import com.apnitor.football.league.fragment.NewsFragment;
import com.apnitor.football.league.fragment.PointsTableFragment;
import com.apnitor.football.league.fragment.SeasonsResultFragment;
import com.apnitor.football.league.fragment.StatsFragment;
import com.apnitor.football.league.fragment.TeamInfoNewsFragment;
import com.apnitor.football.league.fragment.TeamShareNewsFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ScoresPageAdapater extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    GetTeamScheduleRes mGetMatchDetail;


    public ScoresPageAdapater(FragmentManager fm, GetTeamScheduleRes mGetMatchDetail) {
        super(fm);
        this.mGetMatchDetail=mGetMatchDetail;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return GameEventFragment.newInstance(0, "Page # 1",mGetMatchDetail);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return LineUpFragment.newInstance(0, "Page # 1",mGetMatchDetail);
            case 2: // Fragment # 1 - This will show SecondFragment
                return TeamShareNewsFragment.newInstance(0, "Page # 1");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
