package com.apnitor.football.league.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.CreateLeagueActivity;
import com.apnitor.football.league.activity.CreateTeamActivity;
import com.apnitor.football.league.activity.HomeActivity;
import com.apnitor.football.league.activity.LeagueListActivity;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.LeaguesFollowActivity;
import com.apnitor.football.league.activity.PlayerFollowActivity;
import com.apnitor.football.league.activity.PlayerProfileActivity;
import com.apnitor.football.league.activity.TeamFollowActivity;
import com.apnitor.football.league.activity.TeamListActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.FollowingListAdapter;
import com.apnitor.football.league.adapter.LeagueListAdapter;
import com.apnitor.football.league.adapter.MyTeamsListAdapter;
import com.apnitor.football.league.api.response.GetFollowingRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetMyTeamsRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.FragmentFollowingBinding;
import com.apnitor.football.league.fragment_binding_callback.FollowingFragmentBindingCallback;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

public class FollowingFragment extends BaseFragment implements FollowingFragmentBindingCallback, ListItemMultipleCallback {

    public static boolean isFollowing = false;
    public static boolean isTeamCreated = false;
    public static boolean isLeagueCreated = false;
    FollowingListAdapter playerListAdapter;
    private FragmentFollowingBinding binding;
    private HomeActivity homeActivity;
    private LeagueViewModel leagueViewModel;
    private TeamViewModel teamViewModel;
    private PlayerViewModel playerViewModel;
    private FollowingViewModel followingViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.homeActivity = (HomeActivity) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFollowingBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        teamViewModel = getViewModel(TeamViewModel.class);
        playerViewModel = getViewModel(PlayerViewModel.class);
        followingViewModel = getViewModel(FollowingViewModel.class);
        //
        binding.rvLeagues.setLayoutManager(new LinearLayoutManager(this.homeActivity, LinearLayoutManager.HORIZONTAL, false));
        leagueViewModel.getLeagueListResLiveData().observe(this,
                leagues -> {
                    isLeagueCreated = false;
                    binding.rvLeagues.setAdapter(new LeagueListAdapter(this.homeActivity, leagues, LinearLayoutManager.HORIZONTAL, this));
                });
        leagueViewModel.getMyLeagues();
        binding.rvTeams.setLayoutManager(new LinearLayoutManager(this.homeActivity, LinearLayoutManager.HORIZONTAL, false));
        teamViewModel.getGetMyTeamsLiveData().observe(this,
                teams -> {
                    isTeamCreated = false;
                    binding.rvTeams.setAdapter(new MyTeamsListAdapter(this.homeActivity, teams, LinearLayoutManager.HORIZONTAL, this));
                });
        teamViewModel.getMyTeams();

        /*Todo check if user is League Manager or Team Manager then show list accordingly*/
       /* LogInRes logInRes=getPrefHelper().getLogInRes();
        List<String> userTypes=logInRes.getUserTypes();

        if (userTypes!=null && userTypes.size()>0) {


            if (userTypes.contains("League Manager")) {
                leagueViewModel.getMyLeagues();
                binding.myLeaguesTitle.setVisibility(View.VISIBLE);
                binding.myLeaguesCard.setVisibility(View.VISIBLE);
            } else {
                binding.myLeaguesTitle.setVisibility(View.GONE);
                binding.myLeaguesCard.setVisibility(View.GONE);
            }

            if (userTypes.contains("Team Manager")) {
                teamViewModel.getMyTeams();
                binding.rlMyTeamsTitle.setVisibility(View.VISIBLE);
                binding.myTeamsCard.setVisibility(View.VISIBLE);
            } else {
                binding.rlMyTeamsTitle.setVisibility(View.GONE);
                binding.myTeamsCard.setVisibility(View.GONE);
            }
        }else{
            binding.rlMyTeamsTitle.setVisibility(View.GONE);
            binding.myTeamsCard.setVisibility(View.GONE);
            binding.myLeaguesTitle.setVisibility(View.GONE);
            binding.myLeaguesCard.setVisibility(View.GONE);
        }
*/

        /*Todo Get Following Player Res*/
        followingViewModel.getAllfollowingRes().observe(this, res -> {

            try {
                isFollowing = false;
                binding.rvFollowingPlayers.setLayoutManager(new LinearLayoutManager(this.homeActivity, LinearLayoutManager.HORIZONTAL, false));
                binding.tvPlayerCount.setText("Players (" + res.playerList.size() + ")");
                playerListAdapter = new FollowingListAdapter(this.homeActivity, "Player", res.playerList, LinearLayoutManager.HORIZONTAL, R.drawable.ic_player, new ListItemClickCallback() {
                    @Override
                    public void onListItemClick(Object object) {
                        GetFollowingRes.FollowingList getTeamRes = (GetFollowingRes.FollowingList) object;
                        Intent intent = new Intent(getActivity(), PlayerProfileActivity.class);
                        Bundle b = new Bundle();
                        //b.putSerializable("team_detail", getTeamRes);
                        b.putString("player_id", getTeamRes.id2);
                        b.putString("player_name", getTeamRes.firstName + " " + getTeamRes.lastName);
                        intent.putExtras(b);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                    }
                });
                binding.rvFollowingPlayers.setAdapter(playerListAdapter);
                //
                binding.rvFollowingTeams.setLayoutManager(new LinearLayoutManager(this.homeActivity, LinearLayoutManager.HORIZONTAL, false));
                binding.tvTeamsCount.setText("Teams (" + res.teamList.size() + ")");
                binding.rvFollowingTeams.setAdapter(new FollowingListAdapter(this.homeActivity, "Team", res.teamList, LinearLayoutManager.HORIZONTAL, R.drawable.ic_team, new ListItemClickCallback() {
                    @Override
                    public void onListItemClick(Object object) {
                        GetFollowingRes.FollowingList getTeamRes = (GetFollowingRes.FollowingList) object;
                        Intent intent = new Intent(getActivity(), TeamProfileActivity.class);
                        Bundle b = new Bundle();
                        //b.putSerializable("team_detail", getTeamRes);
                        b.putString("team_id", getTeamRes.id);
                        b.putString("team_name", getTeamRes.name);
                        intent.putExtras(b);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                    }
                }));


                binding.rvFollowingLeagues.setLayoutManager(new LinearLayoutManager(this.homeActivity, LinearLayoutManager.HORIZONTAL, false));
                binding.tvCompetitionsCount.setText("Leagues (" + res.leagueList.size() + ")");
                binding.rvFollowingLeagues.setAdapter(new FollowingListAdapter(this.homeActivity, "League", res.leagueList, LinearLayoutManager.HORIZONTAL, R.drawable.ic_league, new ListItemClickCallback() {
                    @Override
                    public void onListItemClick(Object object) {
                        GetFollowingRes.FollowingList getFollowingRes = (GetFollowingRes.FollowingList) object;
                        Intent intent = new Intent(getActivity(), LeagueProfileActivity.class);
                        intent.putExtra("league_id", getFollowingRes.id);
                        intent.putExtra("league_name", getFollowingRes.name);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                    }
                }));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        followingViewModel.getAllFollowing();

        binding.tvAddLeagueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogInRes logInRes=getPrefHelper().getLogInRes();
                List<String> userTypes=logInRes.getUserTypes();
                if(userTypes.contains("League Manager")) {
                    Intent mIntent = new Intent(homeActivity, CreateLeagueActivity.class);
                    mIntent.putExtra("screenType", "create");
                    startActivity(mIntent);
                    getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                }else{
                    showToast("Only League Manager can create a league.");
                }
            }
        });
        binding.tvAddTeamBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogInRes logInRes=getPrefHelper().getLogInRes();
                List<String> userTypes=logInRes.getUserTypes();
                if(userTypes.contains("Team Manager")) {
                    Intent mIntent = new Intent(homeActivity, CreateTeamActivity.class);
                    mIntent.putExtra("screenType", "create");
                    startActivity(mIntent);
                    getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                }else{
                    showToast("Only Team Manager can create a team.");
                }
            }
        });
        binding.ivEditLeagues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, LeagueListActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        binding.ivEditMyTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, TeamListActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        binding.tvBrowsePlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), PlayerFollowActivity.class), 101);
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        binding.tvBrowseTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TeamFollowActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        binding.tvBrowseLeagues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LeaguesFollowActivity.class));
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        return binding.getRoot();
    }


    @Override
    public void onBack() {

    }

    @Override
    public void addLeague() {

    }

    @Override
    public void editLeague() {

    }

    @Override
    public void addTeam() {

    }

    @Override
    public void editTeam() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFollowing) {
            followingViewModel.getAllFollowing();
        }
        if (isTeamCreated) {
            teamViewModel.getMyTeams();
        }
        if (isLeagueCreated) {
            leagueViewModel.getMyLeagues();
        }
    }


    @Override
    public void onListItemClick(Object object) {
        GetMyTeamsRes getTeamRes = (GetMyTeamsRes) object;
        Intent intent = new Intent(getActivity(), TeamProfileActivity.class);
        Bundle b = new Bundle();
        //b.putSerializable("team_detail", getTeamRes);
        b.putString("team_id", getTeamRes.getTeamId());
        b.putString("team_name", getTeamRes.getTeamName());
        intent.putExtras(b);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }

    @Override
    public void onLeagueListItemClick(Object object) {
        GetLeagueRes getLeagueRes = (GetLeagueRes) object;
        Intent intent = new Intent(getActivity(), LeagueProfileActivity.class);
        intent.putExtra("league_id", getLeagueRes.getLeagueId());
        intent.putExtra("league_name", getLeagueRes.getLeagueTitle());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }


}
