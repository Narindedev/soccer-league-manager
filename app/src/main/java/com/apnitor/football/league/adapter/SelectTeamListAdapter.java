package com.apnitor.football.league.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class SelectTeamListAdapter extends RecyclerView.Adapter<SelectTeamListAdapter.ViewHolder> {

    private Context context;
    private List<GetTeamRes> teamList;

    public SelectTeamListAdapter(Context context, ArrayList<GetTeamRes> teamList) {
        this.context = context;
        this.teamList = teamList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamRes model = teamList.get(position);
            holder.tvTeamName.setText(model.getTeamName());
            holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark));
            if (model.getImageUrl() != null)
                Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions()
                        .centerCrop()).into(holder.img);
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    GetTeamRes model = teamList.get(position);
                    model.setSelected(!model.isSelected());
                    holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark)
                    );
                    Log.e("here","here");
                    if (model.isSelected()){
                        holder.imgSelect.setBackgroundResource(R.drawable.ic_check);
                        holder.imgSelect.setColorFilter(ContextCompat.getColor(context, R.color.bottom_nav_item_not_selected_foreground));
                    }
                    else{
                        holder.imgSelect.setBackgroundResource(R.drawable.ic_uncheck);
                        holder.imgSelect.setColorFilter(ContextCompat.getColor(context, R.color.bottom_nav_item_not_selected_foreground));


                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img,imgSelect;
        TextView tvTeamName;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            linearLayout = itemView.findViewById(R.id.llRow);

            imgSelect = (ImageView) itemView.findViewById(R.id.img_select);
        }
    }
}