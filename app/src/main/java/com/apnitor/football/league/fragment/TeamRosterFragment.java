package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.TeamRosterPagerAdapter;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.databinding.FragmentTeamRosterBinding;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;

import java.util.Observable;
import java.util.Observer;

import androidx.viewpager.widget.ViewPager;


public class TeamRosterFragment extends BaseFragment implements ProfileSeasonFragmentBindingCallback, Observer {

    static GetTeamDetailRes mGetTeamDetailRes;
    CustomViewPager viewPager;
    TeamRosterPagerAdapter teamRosterPagerAdapter;
    private FragmentTeamRosterBinding binding;
    private TeamProfileActivity leagueProfileActivity;

    public static TeamRosterFragment newInstance(GetTeamDetailRes getLeagueRes) {
        mGetTeamDetailRes = getLeagueRes;
        TeamRosterFragment fragment = new TeamRosterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.leagueProfileActivity = (TeamProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentTeamRosterBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        //
        viewPager = binding.rosterPageViewPager;
        teamRosterPagerAdapter = new TeamRosterPagerAdapter(getChildFragmentManager(), mGetTeamDetailRes);
        viewPager.setAdapter(teamRosterPagerAdapter);
        //
        binding.ivTeamTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
        binding.ivLeagueTab.setColorFilter(getContext().getResources().getColor(R.color.white));
        viewPager.beginFakeDrag();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        binding.ivTeamTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivLeagueTab.setColorFilter(getContext().getResources().getColor(R.color.white));
                        if(getPrefHelper().getUserId().equals(mGetTeamDetailRes.getTeamModel().getOwner())) {
                            TeamProfileActivity.fab1.setVisibility(View.VISIBLE);
                        }
                       /* binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                    case 1:
                        if(getPrefHelper().getUserId().equals(mGetTeamDetailRes.getTeamModel().getOwner())) {
                            TeamProfileActivity.fab1.setVisibility(View.GONE);
                        }
                        binding.ivTeamTab.setColorFilter(getContext().getResources().getColor(R.color.white));
                        binding.ivLeagueTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                    /*    binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        return binding.getRoot();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showResults() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void showSchedules() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showPointsTable() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void showStats() {
        viewPager.setCurrentItem(3);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetTeamDetailRes = ((TeamProfileActivity) getActivity()).getTeamDetailRes;
        teamRosterPagerAdapter.notifyDataSetChanged();
        teamRosterPagerAdapter.updateFragments();
    }
}
