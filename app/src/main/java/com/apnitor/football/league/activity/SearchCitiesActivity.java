package com.apnitor.football.league.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.apnitor.football.league.R;
import com.apnitor.football.league.util.UIUtility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchCitiesActivity extends BaseActivity {
private SearchView searchView;
    ArrayList<String> cityList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_cities);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                if (query.length() > 2){
                   // mAdapter.getFilter().filter(query);
            }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                if(query.length()>2) {
                  /*  List<String> filteredList = Lists.newArrayList(Collections.filter(
                            cityList, Predicates.containsPattern("How")));*/
                  //  mAdapter.getFilter().filter(query);
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getCitiesData(String state) {
        cityList = new ArrayList();
        String data = UIUtility.getAssetJsonData(SearchCitiesActivity.this,"us_cities_separate.json");
       /* Type type = new TypeToken<GetUsStateRes>() {
        }.getType();
        GetUsStateRes modelObject = new Gson().fromJson(data, type);*/
        JSONObject object= null;
        try {
            object = new JSONObject(data);
            JSONArray mArray =object.getJSONArray(state);
            if (mArray!=null) {

                for (int i=0;i<mArray.length();i++){

                    cityList.add(mArray.getString(i));
                }

               // showStatusDialog(cityList,"Select City");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


