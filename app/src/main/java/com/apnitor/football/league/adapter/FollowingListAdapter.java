package com.apnitor.football.league.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetFollowingRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FollowingListAdapter extends RecyclerView.Adapter<FollowingListAdapter.ViewHolder> {

    private Context context;
    private List<GetFollowingRes.FollowingList> followingResList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    private Integer placeholder;
//    private LanguageInterface languageInterface;
    String mType;


    public FollowingListAdapter(Context context, String mType, List<GetFollowingRes.FollowingList> followingResList, int orientation, Integer placeholder, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.followingResList = followingResList;
        this.orientation = orientation;
        this.placeholder = placeholder;
        this.listItemClickCallback = listItemClickCallback;
        this.mType=mType;
    }

    @Override
    public FollowingListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_following_list_adapter, parent, false);
        if (orientation == LinearLayoutManager.VERTICAL)
            view = LayoutInflater.from(context).inflate(R.layout.item_following_list_adapter, parent, false);

        return new FollowingListAdapter.ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(FollowingListAdapter.ViewHolder holder, int position) {
        try {
            GetFollowingRes.FollowingList model = followingResList.get(position);

            if (mType.equalsIgnoreCase("Player")) {
                holder.tvTeamName.setText(model.firstName + " " + model.lastName);
            }else{
                holder.tvTeamName.setText(model.name);
            }

            if (model.imageUrl != null) {
                Glide.with(context).load(model.imageUrl).apply(new RequestOptions().placeholder(placeholder)
                        .centerCrop()).into(holder.img);
            }  else{
                if (mType.equalsIgnoreCase("Player")) {  
                    holder.img.setImageResource(R.drawable.ic_player);
                }if (mType.equalsIgnoreCase("League")) {
                    holder.img.setImageResource(R.drawable.ic_league);
                }  if (mType.equalsIgnoreCase("Team")) {
                    holder.img.setImageResource(R.drawable.ic_team);
                }
            }
            //   Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return followingResList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivFollowingImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvFollowingName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(followingResList.get(getLayoutPosition()));
                }
            });
        }
    }
}
