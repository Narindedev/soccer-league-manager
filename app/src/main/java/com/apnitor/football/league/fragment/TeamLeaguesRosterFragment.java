package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.AddLeagueRosterAdapter;
import com.apnitor.football.league.adapter.ShowTeamLeaguesAdapter;
import com.apnitor.football.league.adapter.TeamPlayersListAdapter;
import com.apnitor.football.league.api.request.AddLeagueRosterReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.FragmentTeamLeaguesRosterBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.CustomViewPager;
import com.apnitor.football.league.viewmodel.LeagueViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamLeaguesRosterFragment extends BaseFragment implements Observer {

    public static ArrayList<GetTeamPlayersRes> teamListRoster = new ArrayList<>();
    static GetTeamRes mGetTeamRes;
    static GetTeamDetailRes getTeamDetailRes;
    CustomViewPager viewPager;
    ArrayList<Boolean> mAddRosterBoolean = new ArrayList<>();
    private FragmentTeamLeaguesRosterBinding binding;
    private TeamProfileActivity teamProfileActivity;
    private String selectedTeamLeagueId;
    private LeagueViewModel leagueViewModel;
    private View mView;
    private List<GetLeagueRes> leagueList;
    private Boolean isContained = false;
    private ArrayList<String> addedLeagueRoster;

    public static TeamLeaguesRosterFragment newInstance(GetTeamDetailRes getLeagueRes) {
        getTeamDetailRes = getLeagueRes;
        mGetTeamRes = getLeagueRes.getTeamModel();
        TeamLeaguesRosterFragment fragment = new TeamLeaguesRosterFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.teamProfileActivity = (TeamProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment

        binding = FragmentTeamLeaguesRosterBinding.inflate(inflater, container, false);
        addedLeagueRoster = new ArrayList<>();
//        binding.setLeagueInfo(null);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        leagueViewModel.getLeagueRosterResLiveData().observe(this, res -> {
            binding.tvEmptyScreen.setVisibility(View.GONE);
            List<PlayerRes> webPlayerList = res.getPlayers();

            if (webPlayerList != null && webPlayerList.size() > 0) {
                binding.relLeagueRoster.setVisibility(View.VISIBLE);

                binding.rvLeaguesRoster.setLayoutManager(new LinearLayoutManager(getContext()));
                if (addedLeagueRoster.size() > 0) {
                    addedLeagueRoster.clear();
                }
                /*this will perform on get response of added player*/
                for (int i = 0; i < webPlayerList.size(); i++) {
                    mAddRosterBoolean.add(true);
                    webPlayerList.get(i).isLeaguePlayer = true;
                    addedLeagueRoster.add(webPlayerList.get(i).getId());
                }
                AddLeagueRosterAdapter mRosterAdapter = new AddLeagueRosterAdapter(getActivity(), webPlayerList, mAddRosterBoolean, null, false);
                binding.rvLeaguesRoster.setAdapter(mRosterAdapter);
                binding.tvAddLeaguePlayerBtn.setVisibility(View.GONE);
                binding.ivEditLeagueRoster.setVisibility(View.VISIBLE);
                binding.rvLeaguesRoster.setVisibility(View.VISIBLE);
                binding.btnAddRoster.setVisibility(View.GONE);
            } else {
                /*call getLeagueRoster webservice and show them in list*/
                binding.ivEditLeagueRoster.setVisibility(View.GONE);
                binding.tvAddLeaguePlayerBtn.setVisibility(View.VISIBLE);
                binding.rvLeaguesRoster.setVisibility(View.GONE);
                binding.relLeagueRoster.setVisibility(View.VISIBLE);
            }


        });

        binding.ivEditLeagueRoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<PlayerRes> acceptedList = mGetTeamRes.getAcceptedPlayerList();
                    Log.e("GetLeague Player", "" + addedLeagueRoster.toString());
                    if (acceptedList != null && !acceptedList.isEmpty()) {
                        for (int i = 0; i < acceptedList.size(); i++) {
                            acceptedList.get(i).isLeaguePlayer = false;
                            for (int a = 0; a < addedLeagueRoster.size(); a++) {
                                if (addedLeagueRoster.get(a).equals(acceptedList.get(i).getId())) {
                                    acceptedList.get(i).isLeaguePlayer = true;
                                }
                            }
                        }
                        if (addedLeagueRoster.size() > 0) {
                            addedLeagueRoster.clear();
                        }
                        AddLeagueRosterAdapter mRosterAdapter = new AddLeagueRosterAdapter(getActivity(), acceptedList, mAddRosterBoolean, new ListItemClickCallback() {
                            @Override
                            public void onListItemClick(Object object) {
                                PlayerRes playerRes = (PlayerRes) object;
                                if (addedLeagueRoster.size() > 0) {
                                    if(addedLeagueRoster.contains(playerRes.getId())){
                                        addedLeagueRoster.remove(playerRes.getId());
                                    }else{
                                        addedLeagueRoster.add(playerRes.getId());
                                    }
                                   /* for (int i = 0; i < addedLeagueRoster.size(); i++) {
                                        if (addedLeagueRoster.get(i).contains(playerRes.getId())) {
                                            addedLeagueRoster.remove(i);
                                            isContained = true;
                                        }
                                    }
                                    if (!isContained) {
                                        addedLeagueRoster.add(playerRes.getId());
                                    }*/
                                } else {
                                    addedLeagueRoster.add(playerRes.getId());
                                }
                                Log.e("GetLeague Player", "" + addedLeagueRoster.toString());
                            }
                        }, true);
                        binding.rvLeaguesRoster.setAdapter(mRosterAdapter);
                        binding.tvAddLeaguePlayerBtn.setVisibility(View.GONE);
                        binding.ivEditLeagueRoster.setVisibility(View.GONE);
                        binding.btnAddRoster.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        binding.btnAddRoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addedLeagueRoster.size() > 0) {

                    leagueViewModel.addLeagueRoaster(new AddLeagueRosterReq(mGetTeamRes.getTeamId(), selectedTeamLeagueId, addedLeagueRoster));
                } else {
                    Toast.makeText(getActivity(), "Please select player to add in league", Toast.LENGTH_SHORT).show();
                }
            }
        });

        leagueViewModel.addLeagueRoasterResLiveData().observe(this, res -> {
            Toast.makeText(getActivity(), "Players updated successfully", Toast.LENGTH_SHORT).show();
            leagueViewModel.getLeagueRoster(new AddLeagueRosterReq(mGetTeamRes.getTeamId(), selectedTeamLeagueId));
        });

        loadTeamLeagues();
        //setRetainInstance(true);
        return binding.getRoot();
    }


    private void loadTeamLeagues() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        binding.rvTeamLeagues.setLayoutManager(layoutManager);
        leagueList = getTeamDetailRes.getTeamLeagueNames();
        if (leagueList != null && leagueList.size() > 0) {
            binding.tvEmptyScreen.setVisibility(View.GONE);
            binding.rvTeamLeagues.setVisibility(View.VISIBLE);

            ShowTeamLeaguesAdapter mAdapter = new ShowTeamLeaguesAdapter(getActivity(), leagueList, new ListItemClickCallback() {
                @Override
                public void onListItemClick(Object object) {
                    if(getPrefHelper().getUserId().equals(mGetTeamRes.getOwner())) {
                        GetLeagueRes mLeagueRes = (GetLeagueRes) object;
                        selectedTeamLeagueId = mLeagueRes.getTeamLeagueId();
                        leagueViewModel.getLeagueRoster(new AddLeagueRosterReq(mGetTeamRes.getTeamId(), selectedTeamLeagueId));
                    }
                }
            });
            binding.rvTeamLeagues.setAdapter(mAdapter);
        } else {
            binding.tvEmptyScreen.setVisibility(View.VISIBLE);
            binding.rvTeamLeagues.setVisibility(View.GONE);
            binding.relLeagueRoster.setVisibility(View.GONE);
        }
        binding.tvAddLeaguePlayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<PlayerRes> acceptedList = mGetTeamRes.getAcceptedPlayerList();
                if (acceptedList != null && !acceptedList.isEmpty()) {
                    binding.rvTeamLeagues.setVisibility(View.VISIBLE);
                    binding.rvLeaguesRoster.setLayoutManager(new LinearLayoutManager(getContext()));
                    if (addedLeagueRoster.size() > 0) {
                        addedLeagueRoster.clear();
                    }
                    /*this will perform on get response of added player*/
                    for (int i = 0; i < acceptedList.size(); i++) {
                        acceptedList.get(i).isLeaguePlayer = false;
                    }
                    AddLeagueRosterAdapter mRosterAdapter = new AddLeagueRosterAdapter(getActivity(), acceptedList, mAddRosterBoolean, new ListItemClickCallback() {
                        @Override
                        public void onListItemClick(Object object) {
                            PlayerRes playerRes = (PlayerRes) object;
                            if (addedLeagueRoster.size() > 0) {

                                if(addedLeagueRoster.contains(playerRes.getId())){
                                    addedLeagueRoster.remove(playerRes.getId());
                                }else{
                                    addedLeagueRoster.add(playerRes.getId());
                                }
                               /* for (int i = 0; i < addedLeagueRoster.size(); i++) {
                                    if (addedLeagueRoster.get(i).contains(playerRes.getId())) {
                                        addedLeagueRoster.remove(i);
                                        isContained = true;
                                    }
                                }
                                if (!isContained) {
                                    addedLeagueRoster.add(playerRes.getId());

                                }*/
                            } else {
                                addedLeagueRoster.add(playerRes.getId());
                            }
                            Log.e("GetLeague Player", "" + addedLeagueRoster.toString());
                        }
                    }, true);
                    binding.rvLeaguesRoster.setAdapter(mRosterAdapter);
                    binding.tvAddLeaguePlayerBtn.setVisibility(View.GONE);
                    binding.ivEditLeagueRoster.setVisibility(View.GONE);
                    binding.rvLeaguesRoster.setVisibility(View.VISIBLE);
                    binding.btnAddRoster.setVisibility(View.VISIBLE);
                    // sortLisAplhabatically(acceptedList, binding.rvTeamPlayers);
                } else {
                    binding.tvEmptyScreen.setVisibility(View.VISIBLE);
                    binding.tvEmptyScreen.setText("No team roster found.");
                    binding.relLeagueRoster.setVisibility(View.GONE);
                }
            }
        });
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        // playerViewModel.getTeamPlayers(new GetTeamPlayersReq(mGetTeamRes.getTeamId()));
//        playerViewModel.getTeamPlayers(new GetTeamPlayersReq("5c5c77d18efce4001cfb766d"));
    }

    void sortLisAplhabatically(List<PlayerRes> teamList, RecyclerView recyclerView) {
        if (teamList.size() > 0) {
            Collections.sort(teamList, new Comparator<PlayerRes>() {
                @Override
                public int compare(final PlayerRes object1, final PlayerRes object2) {
                    return (object1.getFirstName() + object1.getLastName()).compareTo(object2.getFirstName() + object2.getLastName());
                }
            });
        }

        recyclerView.setAdapter(
                new TeamPlayersListAdapter(getActivity(), teamList, "ROSTER")
        );
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetTeamRes = TeamProfileActivity.getTeamDetailRes.getTeamModel();
        loadTeamLeagues();
    }


}
