package com.apnitor.football.league.adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class AddLeagueRosterAdapter extends RecyclerView.Adapter<AddLeagueRosterAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> playerList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    ArrayList<Boolean> mTeam1SelectedPlayers;
private Boolean isVisibleCheckBox;
    public AddLeagueRosterAdapter(Context context, List<PlayerRes> playerList,  ArrayList<Boolean> mTeam1SelectedPlayers,ListItemClickCallback listItemClickCallback,boolean isVisibleCHeck) {
        this.context = context;
        this.playerList = playerList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
        this.mTeam1SelectedPlayers = mTeam1SelectedPlayers;
        isVisibleCheckBox=isVisibleCHeck;
    }

    @Override
    public AddLeagueRosterAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_add_league_roster_adapter, parent, false);
        return new AddLeagueRosterAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddLeagueRosterAdapter.ViewHolder holder, int position) {
        try {
            PlayerRes model = playerList.get(position);
            holder.tvTeamName.setText(model.getFirstName() + " " + model.getLastName());
            //
            if (model.getProfilePhotoUrl() != null)
                Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_player)).into(holder.ivPlayerImage);
            else {
                holder.ivPlayerImage.setImageResource(R.drawable.ic_player);
            }

            //
            if(isVisibleCheckBox){
                holder.img.setVisibility(View.VISIBLE);
            }else{
                holder.img.setVisibility(View.GONE);
            }

         /*   if (mTeam1SelectedPlayers.get(position) == true) {
                holder.mCb.setChecked(true);
            } else {
                holder.mCb.setChecked(false);
            }*/

            if (model.isLeaguePlayer == true) {
                holder.img.setChecked(true);
            } else {
                holder.img.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox img;
        TextView tvTeamName;
ImageView ivPlayerImage;
        ViewHolder(View itemView) {
            super(itemView);
            img = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            ivPlayerImage=(ImageView)itemView.findViewById(R.id.ivTeamImage);
            img.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(playerList.get(getLayoutPosition()));
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    img.performClick();

                }
            });
        }
    }
}
