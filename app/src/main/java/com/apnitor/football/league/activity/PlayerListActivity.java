package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.PlayerListAdapter;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerListActivity extends BaseActivity implements ListItemClickCallback {

    private ActivityTeamListBinding binding;
    private PlayerViewModel playerViewModel;
    private PlayerListActivity playerListActivity = this;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_list);

        playerViewModel = getViewModel(PlayerViewModel.class);

        setupToolbar(binding.toolbar, "Players");
        setupRecyclerView();
        observeApiResponse();

    }


    private void onClick(){
        binding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
    private void observeApiResponse() {
        playerViewModel.getAllPlayersLiveData().observe(this,
                teams -> {
                    binding.rvAllTeams.setAdapter(
                            new PlayerListAdapter(playerListActivity, teams, RecyclerView.VERTICAL, this)
                    );
                });
        playerViewModel.getAllPlayers();
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(playerListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
//        GetTeamRes getTeamRes = (GetTeamRes) object;
//        Intent intent = new Intent(playerListActivity, TeamDetailPageActivity.class);
//        Bundle b = new Bundle();
//        b.putSerializable("team_detail", getTeamRes);
//        intent.putExtras(b);
//        startActivity(intent);
    }
}
