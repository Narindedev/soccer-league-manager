package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetSeasonTeamsRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.databinding.ActivitySeasonDetailPageBinding;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.util.ArrayList;


public class SeasonDetailPageActivity extends BaseActivity {

    private static final String EXTRA_SEASON_DETAIL = "season_detail";
    private ActivitySeasonDetailPageBinding binding;
    private GetSeasonRes seasonDetails;

    SeasonViewModel seasonViewModel;

    SeasonDetailPageActivity seasonDetailPageActivity = this;

    ArrayList<GetSeasonTeamsRes> teamList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = bindContentView(R.layout.activity_season_detail_page);
        seasonViewModel = getViewModel(SeasonViewModel.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getSerializable(EXTRA_SEASON_DETAIL) != null) {

            seasonDetails = (GetSeasonRes) extras.getSerializable(EXTRA_SEASON_DETAIL);

            String seasonYear = seasonDetails.getSeasonYear();
            binding.seasonYearEt.setText(seasonYear == null ? "" : seasonYear);

            String state = seasonDetails.getState();
            String city = seasonDetails.getCity();
            if (state == null) state = "";
            if (city == null) city = "";
            binding.stateCityEt.setText(state + " to " + city);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onPhotoClick(View view) {
        showToast("Functionality is not implemented yet.");
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void leagueEdit(View view) {
        Intent intent = new Intent(seasonDetailPageActivity, CreateLeagueActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("league_data", seasonDetails);
        intent.putExtras(b);
        startActivity(intent);
    }
}
