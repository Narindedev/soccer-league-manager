package com.apnitor.football.league.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LoginActivity;
import com.apnitor.football.league.databinding.FragmentForgotPasswordBinding;
import com.apnitor.football.league.fragment_binding_callback.ForgotPasswordFragmentBindingCallback;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.ForgotPasswordViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class ForgotPasswordFragment extends BaseFragment implements ForgotPasswordFragmentBindingCallback {

    private FragmentForgotPasswordBinding binding;
    private LoginActivity loginActivity;
    private ForgotPasswordViewModel forgotPasswordViewModel;
    public String email = "";
    private String phone = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginActivity = (LoginActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        forgotPasswordViewModel = getViewModel(ForgotPasswordViewModel.class);
        forgotPasswordViewModel.getForgotPasswordLiveData().observe(
                this,
                signUpRes -> {
                    if(!email.isEmpty()) {
                        Toast.makeText(loginActivity, "An email sent to you to reset password.", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(loginActivity, "A message sent to you to reset password.", Toast.LENGTH_SHORT).show();

                    }

                    Fragment fragment = new UpdatePasswordFragment();
                    Bundle mBundle = new Bundle();
                    mBundle.putString("email", email);
                    mBundle.putString("phone", phone);
                    mBundle.putString("otp", signUpRes.getOtp());
                    startNewFragment(fragment, mBundle);

                }
        );

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentForgotPasswordBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        return binding.getRoot();
    }

    @Override
    public void onBack() {
        navController.navigateUp();
    }

    @Override
    public void onResetPassword() {
//        String phone = "";
//        String email = "";
        String emlPhn = UIUtility.getEditTextValue(binding.emailEt);

        if (emlPhn.contains("@")) {
            email = emlPhn;
        } else {
            phone = emlPhn;
        }
        if (!checkUser(binding.emailEt, emlPhn, email, phone)) {

        } else {
            forgotPasswordViewModel.forgotPassword("",email, phone);
        }
    }

    private void startNewFragment(Fragment fragment, Bundle mBundle) {
        fragment.setArguments(mBundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.my_nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }

}
