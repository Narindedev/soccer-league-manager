package com.apnitor.football.league.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.apnitor.football.league.R;

import androidx.appcompat.widget.TintTypedArray;

public class TabItem extends View {
    //TODO: make package private after the widget migration
    public final CharSequence text;
    //TODO: make package private after the widget migration
    public final Drawable icon;
    //TODO: make package private after the widget migration
    public final int customLayout;

    public TabItem(Context context) {
        this(context, null);
    }

    @SuppressLint("RestrictedApi")
    public TabItem(Context context, AttributeSet attrs) {
        super(context, attrs);

        @SuppressLint("RestrictedApi") final TintTypedArray a =
                TintTypedArray.obtainStyledAttributes(context, attrs, R.styleable.TabItem);
        text = a.getText(R.styleable.TabItem_android_text);
        icon = a.getDrawable(R.styleable.TabItem_android_icon);
        customLayout = a.getResourceId(R.styleable.TabItem_android_layout, 0);
        a.recycle();
    }
}
