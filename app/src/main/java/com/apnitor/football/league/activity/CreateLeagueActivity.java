package com.apnitor.football.league.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.DeleteLeagueReq;
import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.request.UpdateLeagueReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivityCreateLeagueBinding;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.util.DateSetter;
import com.apnitor.football.league.util.PermissionUtitlity;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.util.UploadNewImages;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class CreateLeagueActivity extends BaseActivity {

    public static final String LEAGUE_DATA = "league_data";
    GetLeagueRes mGetLeagueRes;
    private ActivityCreateLeagueBinding binding;
    private LeagueViewModel leagueViewModel;
    private UpdateLeagueReq updateLeagueReq;
    private boolean isCreating = true;
    private String leagueTitle;
    private String leagueID;
    private CreateLeagueActivity createLeagueActivity = this;
    private String screenType;
    private int GALLERY = 1, REQUEST_TAKE_PHOTO = 2;
    private UploadNewImages mUploadImage;
    private Uri mUri;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_create_league);

        mUploadImage = new UploadNewImages(CreateLeagueActivity.this);
        try {
            if (getIntent().getExtras() != null) {
                screenType = getIntent()
                        .getExtras().getString("screenType");
                if (screenType.equals("edit")) {
                    binding.tvMoreField.setVisibility(View.GONE);
                    binding.cardContact.setVisibility(View.VISIBLE);
                    binding.cardSocial.setVisibility(View.VISIBLE);
                    //   binding.cardChampion.setVisibility(View.VISIBLE);
                    //
                    if (getIntent().getExtras() != null && getIntent().getExtras().getSerializable("league_detail") != null) {
                        mGetLeagueRes = (GetLeagueRes) getIntent()
                                .getExtras()
                                .getSerializable("league_detail");

                    }
                    UpdateLeagueReq updateLeagueReq = new UpdateLeagueReq();
                    updateLeagueReq.setLeagueTitle(mGetLeagueRes.getLeagueTitle());
                    updateLeagueReq.setFoundationDate(mGetLeagueRes.getFoundationDate());
                    binding.setLeagueData(updateLeagueReq);
                    binding.tvLeagueTitle.setText("Edit League");
                    binding.nameEt.setText(mGetLeagueRes.getManagerName());
                    binding.emailEt.setText(mGetLeagueRes.getManagerEmail());
                    binding.phoneEt.setText(mGetLeagueRes.getManagerPhone());

                } else {
                    binding.tvMoreField.setVisibility(View.VISIBLE);
                    binding.cardContact.setVisibility(View.GONE);
                    binding.cardSocial.setVisibility(View.GONE);
                    /*Set User Data*/
                    LogInRes logInRes = getPrefHelper().getLogInRes();
                    binding.nameEt.setText(logInRes.getFirstName() + " " + logInRes.getLastName());
                    binding.emailEt.setText(logInRes.getEmail());
                    binding.phoneEt.setText(logInRes.getPhone());
                    // binding.cardChampion.setVisibility(View.GONE);
                }


                if (mGetLeagueRes != null) {
                    leagueID = (mGetLeagueRes.getLeagueId());
                    this.updateLeagueReq = new UpdateLeagueReq(mGetLeagueRes.getLeagueId(), mGetLeagueRes.getLeagueTitle(), mGetLeagueRes.getFoundationDate(), mGetLeagueRes.getCountry()
                            , mGetLeagueRes.getCurrentChampions(), mGetLeagueRes.getMostChampions(), mGetLeagueRes.getContactInfo(), mGetLeagueRes.getDescription(), mGetLeagueRes.getManagerName(), mGetLeagueRes.getManagerEmail(), mGetLeagueRes.getManagerPhone(),
                            mGetLeagueRes.socialMedia, mGetLeagueRes.getOwner());
                    binding.setLeagueData(this.updateLeagueReq);
                    isCreating = false;
                    Glide.with(this).load(mGetLeagueRes.getImageUrl()).apply(new RequestOptions()
                            .centerCrop().placeholder(R.drawable.ic_camera)).into(binding.photoBtn);
                    binding.deleteBtn.setVisibility(View.VISIBLE);
                } else
                    binding.deleteBtn.setVisibility(View.GONE);
            }

            leagueViewModel = getViewModel(LeagueViewModel.class);
            leagueViewModel.getUpdateLeagueResLiveData()
                    .observe(this, res -> {
//                    showToast("League successfully " + (isCreating ? "created" : "updated."));
                        isCreating = false;
                        FollowingFragment.isLeagueCreated = true;
                        leagueID = res.getLeagueId();
                        if (screenType.equals("create") && mUri != null) {
                            mUploadImage.imageActivityResult(mUri.toString(), leagueID, 1);
                        }
                    /*     b.putSerializable("league_detail", new GetLeagueRes(updateLeagueReq.getLeagueId(), updateLeagueReq.getLeagueTitle(), updateLeagueReq.getFoundationDate(),
                            updateLeagueReq.getCountry(), updateLeagueReq.getCurrentChampions(), updateLeagueReq.getMostChampions(),
                            updateLeagueReq.getContactInfo(), updateLeagueReq.getWebsite(),
                            updateLeagueReq.getFacebookLink(), updateLeagueReq.getTwitterLink(), "", updateLeagueReq.getOwner()));
*/
                        if (screenType.equals("edit")) {
                            GetLeagueRes mGetLeagueRes = new GetLeagueRes();
                            mGetLeagueRes.setLeagueId(res.getLeagueId());
                            if (mUri != null) {
                                mGetLeagueRes.setImageUrl(mUri.toString());
                            }
                            mGetLeagueRes.setLeagueTitle(updateLeagueReq.getLeagueTitle());
                            mGetLeagueRes.setFoundationDate(updateLeagueReq.getFoundationDate());
                            mGetLeagueRes.setDescription(updateLeagueReq.getDescription());
                            mGetLeagueRes.setManagerName(updateLeagueReq.getManagerName());
                            mGetLeagueRes.setManagerEmail(updateLeagueReq.getManagerEmail());
                            mGetLeagueRes.setManagerPhone(updateLeagueReq.getManagerPhone());
                            mGetLeagueRes.socialMedia = updateLeagueReq.socialMedia;
                            //    LeagueProfileActivity.isSeasonDivisionUpdated = true;
                            Intent mIntent = new Intent();
                            mIntent.putExtra("league_detail", mGetLeagueRes);
                            setResult(ResultCodes.EDIT_LEAGUE_RESULT_CODE, mIntent);
                        } else {
                            Bundle b = new Bundle();
                            b.putString("league_id", res.getLeagueId());
                            b.putString("league_name", leagueTitle);
                            if (mUri != null) {
                                b.putString("imageUrl", mUri.toString());
                            }
                            startActivity(LeagueProfileActivity.class, b);
                        }
                        finish();
                        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
                    });
            new DateSetter(binding.foundationDateEt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onPhotoClick(View view) {
        showPictureDialog();
    }

    public void onSaveClick(View view) {

        String leagueId = isCreating ? null : updateLeagueReq.getLeagueId();
        leagueTitle = UIUtility.getEditTextValue(binding.leagueNameEt);
        String foundationDate = UIUtility.getEditTextValue(binding.foundationDateEt);

        String description = UIUtility.getEditTextValue(binding.descriptionEt);
        //String mostChampions = UIUtility.getEditTextValue(binding.mostChampionEt);
        // String currentChampion = UIUtility.getEditTextValue(binding.currentChampionEt);
        String website = UIUtility.getEditTextValue(binding.websiteEt);
        String facebook = UIUtility.getEditTextValue(binding.facebookEt);
        String instagram = UIUtility.getEditTextValue(binding.instagramEt);
        String name = UIUtility.getEditTextValue(binding.nameEt);
        String email = UIUtility.getEditTextValue(binding.emailEt);
        String phone = UIUtility.getEditTextValue(binding.phoneEt);
        String twitterLink = UIUtility.getEditTextValue(binding.twitterEt);
        SocialMedia media = new SocialMedia(website, facebook, twitterLink, instagram);
        if (leagueTitle.isEmpty())
            binding.leagueNameEt.setError("'League name' should not be empty.");
        else {
            updateLeagueReq = new UpdateLeagueReq(leagueId, leagueTitle, foundationDate, "United States of America", "", "",
                    phone, description, name, email, phone, media, getPrefHelper().getUserId()
            );
            leagueViewModel.createUpdateLeague(updateLeagueReq);
        }
    }

    public void onDeleteClick(View view) {
        leagueViewModel.deleteLeague(new DeleteLeagueReq(updateLeagueReq.getLeagueId()));
        leagueViewModel.getDeleteLeagueResLiveData().observe(this, res -> {
            finishStartActivity(HomeActivity.class);
        });
    }

    public void showMoreField(View view) {
        binding.cardContact.setVisibility(View.VISIBLE);
        binding.tvMoreField.setVisibility(View.GONE);
        binding.cardSocial.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionUtitlity.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                }
                break;
            case PermissionUtitlity.CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                }
                break;


        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage);//M1
                openCropActivity(selectedImage);
                //   mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage); //M1


                openCropActivity(selectedImage);
                // mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {

                    mUri = result.getUri();
                    binding.photoBtn.setImageURI(mUri);
                    Glide.with(this).load(mUri.toString()).apply(new RequestOptions()
                            .centerCrop().placeholder(R.drawable.ic_camera)).into( binding.photoBtn);
                    // mProgressBar.setVisibility(View.VISIBLE);
                    if (screenType.equals("edit")) {
                        mUploadImage.imageActivityResult(mUri.toString(), leagueID, 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(CreateLeagueActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(CreateLeagueActivity.this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (PermissionUtitlity.checkPermission(CreateLeagueActivity.this)) {
                                    choosePhotoFromGallary();
                                }
                                break;
                            case 1:
                                if (PermissionUtitlity.checkPermissionCamera(CreateLeagueActivity.this)) {
                                    dispatchTakePictureIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(galleryIntent, GALLERY);
    }

    /* private void dispatchTakePictureIntent() {
         Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         // Ensure that there's a camera activity to handle the intent
         if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
             // Create the File where the photo should go
             File photoFile = null;
             try {
                 photoFile = mUploadImage.createImageFile();
             } catch (IOException ex) {
                 // Error occurred while creating the File
             }
             // Continue only if the File was successfully created
             if (photoFile != null) {
                 Uri photoURI = FileProvider.getUriForFile(this,
                         "com.imagecapture.fileprovider",
                         photoFile);
                 takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                 startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
             }
         }
     }*/
    private void dispatchTakePictureIntent() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO);
    }


    private void openCropActivity(Uri image_uri) {
        CropImage.activity(image_uri)
                .setFixAspectRatio(true)
                .setAspectRatio(1, 1)
                .start(CreateLeagueActivity.this);
    }
}
