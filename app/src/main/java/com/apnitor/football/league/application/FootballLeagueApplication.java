package com.apnitor.football.league.application;

import android.app.Application;

public class FootballLeagueApplication extends Application {

    // Singletons
    private SharedPreferenceHelper sharedPreferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        initSingletonsForApp();
    }

    private void initSingletonsForApp() {
        sharedPreferenceHelper = new SharedPreferenceHelper(this);
    }

    public SharedPreferenceHelper getSharedPreferenceHelper() {
        return sharedPreferenceHelper;
    }
}
