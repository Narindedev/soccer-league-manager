package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SeasonListAdapter;
import com.apnitor.football.league.api.request.GetLeagueSeasonsReq;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.databinding.ActivitySeasonsListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SeasonListActivity extends BaseActivity implements ListItemClickCallback {

    private ActivitySeasonsListBinding binding;
    private SeasonViewModel seasonViewModel;
    private SeasonListActivity leagueListActivity = this;
    private String leagueId;
    private final String LEAGUE_ID = "leagueId";

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_seasons_list);
        if (getIntent() != null && getIntent().getExtras() != null) {
            leagueId = getIntent().getStringExtra(LEAGUE_ID);
        }
        seasonViewModel = getViewModel(SeasonViewModel.class);
        setupToolbar(binding.toolbar, "Seasons");
        setUpRecyclerView();
        observeApiResponse();
    }

    private void setUpRecyclerView() {
        binding.rvSeasons.setLayoutManager(
                new LinearLayoutManager(leagueListActivity, RecyclerView.VERTICAL, false)
        );
    }

    private void observeApiResponse() {
        seasonViewModel.getLeagueListResLiveData().observe(this,
                leagues -> {
                    binding.rvSeasons.setAdapter(
                            new SeasonListAdapter(leagueListActivity, leagues, this)
                    );
                });
    }

    public void onBackClick(View view) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        seasonViewModel.getSeasons(new GetLeagueSeasonsReq(leagueId));
    }

    @Override
    public void onListItemClick(Object object) {
        GetSeasonRes getLeagueRes = (GetSeasonRes) object;
        Bundle b = new Bundle();
        b.putSerializable("season_detail", getLeagueRes);
        b.putString("leagueId", leagueId);
//        finishStartActivity(SeasonDetailPageActivity.class,b);
        finishStartActivity(CreateSeasonActivity.class, b);

    }

    public void onAddSeason(View view) {
        Bundle b = new Bundle();
        b.putString("leagueId", leagueId);
        startActivity(CreateSeasonActivity.class, b);
    }
}
