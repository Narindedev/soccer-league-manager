package com.apnitor.football.league.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetUsStateRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.databinding.ActivityCreateTeamBinding;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.util.DateSetter;
import com.apnitor.football.league.util.PermissionUtitlity;
import com.apnitor.football.league.util.ResultCodes;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.util.UploadNewImages;
import com.apnitor.football.league.util.Validation;
import com.apnitor.football.league.viewmodel.TeamViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class CreateTeamActivity extends BaseActivity {

    private static final String TEAM_DATA = "team_data";
    private ActivityCreateTeamBinding binding;
    private TeamViewModel teamViewModel;
    private UpdateTeamReq updateTeamReq;
    private boolean isCreating = true;
    private ArrayList<GetUsStateRes.StateData> stateList;
    private ArrayList cityList;
    private String screenType;
    private int GALLERY = 1, REQUEST_TAKE_PHOTO = 2;
    private UploadNewImages mUploadImage;
    private Uri mUri;
    private String teamId;
    String LOG_TAG = "CreateTeamActivity";

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_create_team);
        mUploadImage = new UploadNewImages(this);
        if (getIntent().getExtras() != null) {
            screenType = getIntent()
                    .getExtras().getString("screenType");
            GetTeamRes getTeamRes = (GetTeamRes) getIntent()
                    .getExtras()
                    .getSerializable(TEAM_DATA);

            if (getTeamRes != null) {
                this.updateTeamReq = new UpdateTeamReq(getTeamRes.getTeamId(), getTeamRes.getTeamName(), getTeamRes.getCoachName(),
                        getTeamRes.getFoundationDate(), getTeamRes.getCountry(), getTeamRes.getState(), getTeamRes.getCity(), getTeamRes.getZipcode(), getTeamRes.getStadium()
                        , getTeamRes.getManager(), getTeamRes.getTrophy(), getTeamRes.getContactInfo(), getTeamRes.getOwner(), getTeamRes.getImageUrl(), getTeamRes.getManagerName(), getTeamRes.getManagerEmail(), getTeamRes.getManagerPhone(), getTeamRes.getDescription(), getTeamRes.getHomeUniform(), getTeamRes.getAwayUniform(), getTeamRes.socialMedia);

                teamId = getTeamRes.getTeamId();
                binding.setTeamData(getTeamRes);
                Glide.with(this).load(getTeamRes.getImageUrl()).apply(new RequestOptions()
                        .centerCrop().placeholder(R.drawable.ic_camera)).into(binding.photoBtn);
                isCreating = false;
                binding.deleteBtn.setVisibility(View.VISIBLE);
            } else
                binding.deleteBtn.setVisibility(View.GONE);


            if (screenType.equals("edit")) {
                binding.tvMoreField.setVisibility(View.GONE);
                binding.consMoreEditField.setVisibility(View.VISIBLE);
                binding.cardContact.setVisibility(View.VISIBLE);
                binding.cardAddress.setVisibility(View.VISIBLE);
                binding.cardSocial.setVisibility(View.VISIBLE);
                binding.cardUniform.setVisibility(View.VISIBLE);
                binding.tvLeagueTitle.setText("Edit Team");
                binding.nameEt.setText(getTeamRes.getManagerName());
                binding.emailEt.setText(getTeamRes.getManagerEmail());
                binding.phoneEt.setText(getTeamRes.getManagerPhone());
            } else {
                binding.tvMoreField.setVisibility(View.VISIBLE);
                binding.consMoreEditField.setVisibility(View.GONE);
                binding.cardContact.setVisibility(View.GONE);
                binding.cardAddress.setVisibility(View.GONE);
                binding.cardUniform.setVisibility(View.GONE);
                binding.cardSocial.setVisibility(View.GONE);

                /*Set User Data*/
                LogInRes logInRes = getPrefHelper().getLogInRes();
                binding.nameEt.setText(logInRes.getFirstName() + " " + logInRes.getLastName());
                binding.emailEt.setText(logInRes.getEmail());
                binding.phoneEt.setText(logInRes.getPhone());
            }


        }
        teamViewModel = getViewModel(TeamViewModel.class);
        teamViewModel.getUpdateTeamResLiveData()
                .observe(this, res -> {
//                    showToast("Team successfully " + (isCreating ? "created" : "updated."));
                    FollowingFragment.isTeamCreated = true;
                    isCreating = false;
                    teamId = res.getTeamId();
                    if (screenType.equals("create") && mUri != null) {
                        mUploadImage.imageActivityResult(mUri.toString(), teamId, 2);
                    }else{
                        GetTeamRes mGetTeamRes = new GetTeamRes();
                        mGetTeamRes.setTeamId(res.getTeamId());
                        mGetTeamRes.setTeamName(updateTeamReq.getTeamName());
                        mGetTeamRes.setFoundationDate(updateTeamReq.getFoundationDate());
                        mGetTeamRes.setDescription(updateTeamReq.getDescription());
                        mGetTeamRes.setCoachName(updateTeamReq.getCoachName());
                        mGetTeamRes.setHomeUniform(updateTeamReq.getHomeUniform());
                        mGetTeamRes.setAwayUniform(updateTeamReq.getAwayUniform());
                        mGetTeamRes.setState(updateTeamReq.getState());
                        mGetTeamRes.setCity(updateTeamReq.getCity());
                        mGetTeamRes.setZipcode(updateTeamReq.getZipcode());
                        mGetTeamRes.setStadium(updateTeamReq.getStadium());
                        mGetTeamRes.setManagerName(updateTeamReq.getManagerName());
                        mGetTeamRes.setManagerEmail(updateTeamReq.getManagerEmail());
                        mGetTeamRes.setManagerPhone(updateTeamReq.getManagerPhone());
                        mGetTeamRes.socialMedia = updateTeamReq.socialMedia;
                        if (mUri != null) {
                            mGetTeamRes.setImageUrl(mUri.toString());
                        }
                        //    LeagueProfileActivity.isSeasonDivisionUpdated = true;
                        Intent mIntent = new Intent();
                        mIntent.putExtra("team_detail", mGetTeamRes);
                        setResult(ResultCodes.EDIT_TEAM_RESULT_CODE, mIntent);
                    }
                    finish();
                    overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);

                });


        /*Get us states from json*/
        getStatesdata();

        /*Set User Data*/
        LogInRes logInRes = getPrefHelper().getLogInRes();
        binding.nameEt.setText(logInRes.getFirstName() + " " + logInRes.getLastName());
        binding.emailEt.setText(logInRes.getEmail());
        binding.phoneEt.setText(logInRes.getPhone());
        binding.stateEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStatesDialog();
            }
        });
        binding.cityEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String state = binding.stateEt.getText().toString();
                if (state.isEmpty()) {
                    showToast("Please select state first.");
                } else {
                    getCitiesData(state);
                }
            }
        });

        new DateSetter(binding.foundationDateEt);
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onPhotoClick(View view) {
        showPictureDialog();
        // Toast.makeText(this, "Functionality is not implemented yet.", Toast.LENGTH_SHORT).show();
    }

    public void onSaveClick(View view) {

        String teamId = isCreating ? null : updateTeamReq.getTeamId();
        String teamName = UIUtility.getEditTextValue(binding.teamNameEt);
        String description = UIUtility.getEditTextValue(binding.descriptionEt);
        String homeUniform = UIUtility.getEditTextValue(binding.homeUniformEt);
        String awayUniform = UIUtility.getEditTextValue(binding.awayUniformEt);
        String coachName = UIUtility.getEditTextValue(binding.coachNameEt);
        String foundationDate = UIUtility.getEditTextValue(binding.foundationDateEt);
        String country = UIUtility.getEditTextValue(binding.countryEt);

        String userName = UIUtility.getEditTextValue(binding.nameEt);
        String email = UIUtility.getEditTextValue(binding.emailEt);
        String phone = UIUtility.getEditTextValue(binding.phoneEt);


        String state = UIUtility.getEditTextValue(binding.stateEt);
        String city = UIUtility.getEditTextValue(binding.cityEt);
        String zipCode = UIUtility.getEditTextValue(binding.zipCodeEt);
        String stadium = UIUtility.getEditTextValue(binding.stadiumEt);
        String manager = UIUtility.getEditTextValue(binding.managerEt);
        String trophy = UIUtility.getEditTextValue(binding.trophyEt);

        String contactInfo = UIUtility.getEditTextValue(binding.trophyEt);
        String website = UIUtility.getEditTextValue(binding.websiteEt);
        String fbLink = UIUtility.getEditTextValue(binding.facebookEt);
        String twitterLink = UIUtility.getEditTextValue(binding.twitterEt);
        String instaLink = UIUtility.getEditTextValue(binding.instagramEt);
        SocialMedia socialMedia = new SocialMedia(website, fbLink, twitterLink, instaLink);
        // todo: Add image url

        String teamImageUrl = "";

        if (teamName.isEmpty())
            binding.teamNameEt.setError("'Team name' should not be empty.");
        else {
             updateTeamReq = new UpdateTeamReq();
            if (Validation.isValid(teamId))
                updateTeamReq.setTeamId(teamId);
            if (Validation.isValid(teamName))
                updateTeamReq.setTeamName(teamName);
            if (Validation.isValid(coachName))
                updateTeamReq.setCoachName(coachName);
            if (Validation.isValid(foundationDate))
                updateTeamReq.setFoundationDate(foundationDate);
            if (Validation.isValid(country))
                updateTeamReq.setCountry(country);
            if (Validation.isValid(state))
                updateTeamReq.setState(state);
            //
            if (Validation.isValid(city))
                updateTeamReq.setCity(city);
            if (Validation.isValid(zipCode))
                updateTeamReq.setZipcode(zipCode);
            if (Validation.isValid(stadium))
                updateTeamReq.setStadium(stadium);
            if (Validation.isValid(manager))
                updateTeamReq.setManager(manager);
            if (Validation.isValid(trophy))
                updateTeamReq.setTrophy(trophy);
            if (Validation.isValid(contactInfo))
                updateTeamReq.setContactInfo(contactInfo);
            //
            if (Validation.isValid(getPrefHelper().getUserId()))
                updateTeamReq.setOwner(getPrefHelper().getUserId());
            if (Validation.isValid(teamImageUrl))
                updateTeamReq.setImageUrl(teamImageUrl);
            if (Validation.isValid(userName))
                updateTeamReq.setManagerName(userName);
            if (Validation.isValid(email))
                updateTeamReq.setManagerEmail(email);
            if (Validation.isValid(phone))
                updateTeamReq.setManagerPhone(phone);
            if (Validation.isValid(description))
                updateTeamReq.setDescription(description);
            //
            if (Validation.isValid(homeUniform))
                updateTeamReq.setHomeUniform(homeUniform);
            if (Validation.isValid(awayUniform))
                updateTeamReq.setAwayUniform(awayUniform);
            if (Validation.isValid(description))
                updateTeamReq.setDescription(description);
            if (Validation.isValid(socialMedia))
                updateTeamReq.setSocialMedia(socialMedia);


            apiCall(updateTeamReq);
        }
    }

    private void apiCall(UpdateTeamReq updateTeamReq) {
        teamViewModel.createUpdateLeague(updateTeamReq);
        Log.d(LOG_TAG, "Team Object is " + updateTeamReq.toString());
    }

    public void onDeleteClick(View view) {
        teamViewModel.deleteTeam(new DeleteTeamReq(updateTeamReq.getTeamId()));
        teamViewModel.getDeleteTeamLiveData().observe(this, res -> {
            finishStartActivity(HomeActivity.class);
        });
    }

    public void showMoreField(View view) {
        binding.tvMoreField.setVisibility(View.GONE);
        binding.consMoreEditField.setVisibility(View.VISIBLE);
        binding.cardContact.setVisibility(View.VISIBLE);
        binding.cardAddress.setVisibility(View.VISIBLE);
        binding.cardUniform.setVisibility(View.VISIBLE);
        binding.cardSocial.setVisibility(View.VISIBLE);
    }

    private void getStatesdata() {
        stateList = new ArrayList<>();
        String data = UIUtility.getAssetJsonData(CreateTeamActivity.this, "us_states.json");
        Type type = new TypeToken<GetUsStateRes>() {
        }.getType();
        GetUsStateRes modelObject = new Gson().fromJson(data, type);
        stateList = modelObject.states;
    }

    void showStatesDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select States");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < stateList.size(); i++) {
            arrayAdapter.add(stateList.get(i).name);
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);

                binding.stateEt.setText(strName);

                getCitiesData(strName);
            }
        });
        builderSingle.show();
    }

    private void getCitiesData(String state) {
        cityList = new ArrayList();
        String data = UIUtility.getAssetJsonData(CreateTeamActivity.this, "us_cities.json");
       /* Type type = new TypeToken<GetUsStateRes>() {
        }.getType();
        GetUsStateRes modelObject = new Gson().fromJson(data, type);*/
        JSONObject object = null;
        try {
            object = new JSONObject(data);
            JSONArray mArray = object.getJSONArray(state);
            if (mArray != null) {
                for (int i = 0; i < mArray.length(); i++) {
                    cityList.add(mArray.getString(i));
                }
                showCityDialog(cityList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void showCityDialog(ArrayList<String> list) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select City");
        builderSingle.setCancelable(true);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < list.size(); i++) {
            arrayAdapter.add(list.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectedItem = arrayAdapter.getItem(which);
                binding.cityEt.setText(selectedItem);
            }
        });
        builderSingle.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionUtitlity.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                }
                break;
            case PermissionUtitlity.CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage);//M1
                openCropActivity(selectedImage);
                //   mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO) {
            try {
                Uri selectedImage = data.getData();
                binding.photoBtn.setImageURI(selectedImage); //M1


                openCropActivity(selectedImage);
                // mIvProfilePic.setImageURI(selectedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {

                    mUri = result.getUri();
                    binding.photoBtn.setImageURI(mUri);
                    Glide.with(this).load(mUri.toString()).apply(new RequestOptions()
                            .centerCrop().placeholder(R.drawable.ic_camera)).into( binding.photoBtn);
                    // mProgressBar.setVisibility(View.VISIBLE);
                    if (screenType.equals("edit")) {
                        mUploadImage.imageActivityResult(mUri.toString(), teamId, 2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(CreateTeamActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(CreateTeamActivity.this);
        pictureDialog.setTitle("Select Picture");
        String[] pictureDialogItems = {
                "Pick from gallery",
                "Capture using camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (PermissionUtitlity.checkPermission(CreateTeamActivity.this)) {
                                    choosePhotoFromGallary();
                                }
                                break;
                            case 1:
                                if (PermissionUtitlity.checkPermissionCamera(CreateTeamActivity.this)) {
                                    dispatchTakePictureIntent();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(galleryIntent, GALLERY);
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_TAKE_PHOTO);
    }

    private void openCropActivity(Uri image_uri) {
        CropImage.activity(image_uri)
                .setFixAspectRatio(true)

                .setAspectRatio(1, 1)
                .start(CreateTeamActivity.this);
    }
}
