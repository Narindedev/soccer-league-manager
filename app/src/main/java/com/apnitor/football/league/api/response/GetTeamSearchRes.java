package com.apnitor.football.league.api.response;

import com.apnitor.football.league.api.request.SocialMedia;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetTeamSearchRes implements Serializable {

    @Expose
    @SerializedName("_id")
    private String teamId;

    @Expose
    @SerializedName("name")
    private String teamName;

    @Expose
    @SerializedName("coachName")
    private String coachName;

    @Expose
    @SerializedName("foundationDate")
    private String foundationDate;

    @Expose
    @SerializedName("country")
    private String country;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("zipcode")
    private String zipcode;

    @Expose
    @SerializedName("stadium")
    private String stadium;

    @Expose
    @SerializedName("manager")
    private String manager;

    @Expose
    @SerializedName("trophy")
    private String trophy;

    @Expose
    @SerializedName("contactInfo")
    private String contactInfo;

    @Expose
    @SerializedName("isFollowing")
    public Boolean isFollowing;

    private boolean isSelected;

    @Expose
    @SerializedName("owner")
    private String owner;

    @Expose
    @SerializedName("imageUrl")
    private String imageUrl;

    @Expose
    @SerializedName("homeUniform")
    private String homeUniform;

    @Expose
    @SerializedName("awayUniform")
    private String awayUniform;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("managerName")
    private String managerName;

    @Expose
    @SerializedName("managerEmail")
    private String managerEmail;

    @Expose
    @SerializedName("managerPhone")
    private String managerPhone;

    @Expose
    @SerializedName("socialMedia")
    public SocialMedia socialMedia;


    @Expose
    @SerializedName("invitedPlayers")
    private ArrayList<String> invitedPlayerList;

    @Expose
    @SerializedName("playerIds")
    private ArrayList<String> acceptedPlayerList;

    @Expose
    @SerializedName("leagueStanding")
    private ArrayList<GetTeamStandingRes> leagueStanding;

    /*public GetTeamRes(String teamId, String teamName, String coachName, String foundationDate, String country, String state, String stadium, String manager, String trophy, String contactInfo, String website, String facebookLink, String twitterLink, String owner) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.coachName = coachName;
        this.foundationDate = foundationDate;
        this.country = country;
        this.state = state;
        this.stadium = stadium;
        this.manager = manager;
        this.trophy = trophy;
        this.contactInfo = contactInfo;
        this.website = website;
        this.facebookLink = facebookLink;
        this.twitterLink = twitterLink;
        this.owner = owner;
    }
*/
    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCoachName() {
        return coachName;
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getFoundationDate() {
        return foundationDate;
    }

    public void setFoundationDate(String foundationDate) {
        this.foundationDate = foundationDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getTrophy() {
        return trophy;
    }

    public void setTrophy(String trophy) {
        this.trophy = trophy;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Boolean getFollowing() {
        return isFollowing;
    }

    public void setFollowing(Boolean following) {
        isFollowing = following;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getHomeUniform() {
        return homeUniform;
    }

    public void setHomeUniform(String homeUniform) {
        this.homeUniform = homeUniform;
    }

    public String getAwayUniform() {
        return awayUniform;
    }

    public void setAwayUniform(String awayUniform) {
        this.awayUniform = awayUniform;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public List<String> getInvitedPlayerList() {
        return invitedPlayerList;
    }

    public void setInvitedPlayerList(ArrayList<String> invitedPlayerList) {
        this.invitedPlayerList = invitedPlayerList;
    }

    public List<String> getAcceptedPlayerList() {
        return acceptedPlayerList;
    }

    public void setAcceptedPlayerList(ArrayList<String> acceptedPlayerList) {
        this.acceptedPlayerList = acceptedPlayerList;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }

    public ArrayList<GetTeamStandingRes> getLeagueStanding() {
        return leagueStanding;
    }

    public void setLeagueStanding(ArrayList<GetTeamStandingRes> leagueStanding) {
        this.leagueStanding = leagueStanding;
    }
}
