package com.apnitor.football.league.api.request;

import java.util.ArrayList;

public class GetMatchEventsReq {

    private String scheduleId;
    private String leagueId;


    public GetMatchEventsReq(String leagueId, String scheduleId) {
        this.leagueId = leagueId;
        this.scheduleId = scheduleId;
    }


    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }


    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }



}
