package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.SwipePlayerActivity;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class LineUpAdapter extends RecyclerView.Adapter<LineUpAdapter.ViewHolder> {

    private Context context;
    private List<TeamPlaying11> teamList;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    String mType;

    public LineUpAdapter(Context context, ArrayList<TeamPlaying11> teamList, String type) {
        this.context = context;
        this.teamList = teamList;
        mType=type;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_line_up, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            TeamPlaying11 teamPlaying11=teamList.get(position);
            if (mType.equalsIgnoreCase("bench")){
                holder.mIvSubTime.setVisibility(View.INVISIBLE);
                holder.mTvTShirtPlayer2.setVisibility(View.GONE);
                holder.mTvPlayer2.setVisibility(View.GONE);
            }

            holder.mTvPlayer1.setText(teamPlaying11.getFirstName()+" "+teamPlaying11.getLastName());
            if (teamPlaying11.getImageUrl() != null)
                Glide.with(context).load(teamPlaying11.getImageUrl()).into(holder.mIvUser);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvSubTime;
        ImageView mIvUser;
        TextView mTvTShirtPlayer1,mTvTShirtPlayer2,mTvPlayer1,mTvPlayer2;

        ViewHolder(View itemView) {
            super(itemView);
            mTvPlayer1=itemView.findViewById(R.id.player_1_name);
            mTvPlayer2=itemView.findViewById(R.id.player_2_name);
            //
            mTvTShirtPlayer1=itemView.findViewById(R.id.tshirt_player_1);
            mTvTShirtPlayer2=itemView.findViewById(R.id.tshirt_player_2);
            //
            mIvSubTime=itemView.findViewById(R.id.iv_sub_time);
            mIvUser=itemView.findViewById(R.id.iv_user);
        }
    }
}
