package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.PointsTableListAdapter;
import com.apnitor.football.league.adapter.SeasonResultsListAdapter;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.request.GetLeagueStandingReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueStandingRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.ScheduleResultRes;
import com.apnitor.football.league.api.response.SelectedSeasonDivisionRes;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PointsTableFragment extends BaseFragment implements Observer {
    View view;
    TextView mTvSeason, mTvWeek;
    private List<String> mSeasonNameList = new ArrayList<>();
    private List<String> mWeekNameList = new ArrayList<>();
    private static GetLeagueRes mGetLeagueRes;
    private SeasonViewModel seasonViewModel;
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private TextView mTvDivision;
    private List<String> mDivisionNameList = new ArrayList<>();
    private List<GetDivisionRes> mDivisionsList = new ArrayList<>();
    private LeagueViewModel leagueViewModel;
    private String mSeasonId, mDivisionId,mLeagueId;
    private RecyclerView rvPoinTable;
    private LinearLayout mTableTitle;
    String seasonStatus;
    private List<GetLeagueStandingRes> standingResList;
    private PointsTableListAdapter mTableListAdapter;
    public static PointsTableFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes=new GetLeagueRes();
        mGetLeagueRes = getLeagueRes;
        PointsTableFragment fragment = new PointsTableFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeekNameList.add("Week 1");
        mWeekNameList.add("Week 2");
        mWeekNameList.add("Week 3");
        mWeekNameList.add("Week 4");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_points_table, container, false);
        //
        seasonViewModel = getViewModel(SeasonViewModel.class);
        leagueViewModel=getViewModel(LeagueViewModel.class);
        mLeagueId=mGetLeagueRes.getLeagueId();


        //
        mTableTitle=view.findViewById(R.id.llTableTitle);
        mTvSeason = view.findViewById(R.id.tvSelectSeasonBtn);
        mTvDivision = view.findViewById(R.id.tvSelectDivisionBtn);
        mTvWeek = view.findViewById(R.id.tvSelectWeekBtn);
        rvPoinTable = view.findViewById(R.id.rvPointsTable);
        rvPoinTable.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
       // rvPoinTable.setAdapter(new PointsTableListAdapter(getContext(), null));

        observeApiResponse();
        observeLeagueScheduleData(mGetLeagueRes);

        //
        mTvDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDivisionDialog();
            }
        });
        //
        mTvSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Season", Toast.LENGTH_SHORT).show();
                showSeasonDialog();
            }

        });
        mTvWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWeekDialog();
            }
        });
        //


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void showSeasonDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Season");
        builderSingle.setCancelable(true);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < mSeasonNameList.size(); i++) {
            arrayAdapter.add(mSeasonNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvSeason.setText(strName);
                mSeasonId=seasonsList.get(which).getSeasonId();

                List<SelectedSeasonDivisionRes> seasonDivisionRes=((LeagueProfileActivity)getActivity()).selectedSeasonDivision(mDivisionId,mSeasonId,which);
                if(seasonDivisionRes!=null && seasonDivisionRes.size()>0){
                    mTvDivision.setText(seasonDivisionRes.get(0).getDivisionName());
                    mDivisionId=seasonDivisionRes.get(0).getDivisionId();
                }

                if(mSeasonId!=null && mDivisionId!=null){
                    leagueViewModel.getLeagueStanding(new GetLeagueStandingReq(mLeagueId,mDivisionId,mSeasonId));
                }
            }
        });
        builderSingle.show();
    }


    void showDivisionDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Division");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);

        for (int i = 0; i < mDivisionNameList.size(); i++) {
            arrayAdapter.add(mDivisionNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvDivision.setText(strName);
                mDivisionId = mDivisionsList.get(which).getDivisionId();

                List<GetSeasonRes> seasonResList=((LeagueProfileActivity)getActivity()).getDivisionSeason(mDivisionId);
                if(seasonResList!=null && seasonResList.size()>0){
                    mSeasonId = seasonResList.get(0).getSeasonId();
                    mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                }


                if(mSeasonId!=null && mDivisionId!=null) {
                    leagueViewModel.getLeagueStanding(new GetLeagueStandingReq(mLeagueId, mDivisionId, mSeasonId));
                }
            }
        });
        builderSingle.show();
    }

    void showWeekDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Week");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < mWeekNameList.size(); i++) {
            arrayAdapter.add(mWeekNameList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvWeek.setText(strName);
            }
        });
        builderSingle.show();
    }


    private void observeLeagueScheduleData(GetLeagueRes leagueRes) {
        /*Show  Season Data*/
        seasonsList = leagueRes.seasonList;
        mSeasonNameList.clear();
        if (seasonsList.size() > 0) {
            for (int i = 0; i < seasonsList.size(); i++) {
                mSeasonNameList.add(seasonsList.get(i).getSeasonYear());
            }
        }

            //

        /*Show  Division Data*/
        mDivisionsList = leagueRes.divisionList;
        Log.e("LOG_TAG", "Point Table are " + mDivisionNameList.toString());
        mDivisionNameList.clear();
        for (int i = 0; i < leagueRes.divisionList.size(); i++) {
            mDivisionNameList.add(leagueRes.divisionList.get(i).getDivisionName());
        }
        //

            List<SelectedSeasonDivisionRes> seasonDivisionResList=((LeagueProfileActivity)getActivity()).getActiveSeasonDivision();

            if(seasonDivisionResList!=null && seasonDivisionResList.size()>0){
                mTvDivision.setVisibility(View.VISIBLE);
                mTvSeason.setVisibility(View.VISIBLE);
                SelectedSeasonDivisionRes res=seasonDivisionResList.get(0);

                mTvSeason.setText(res.getSesonName());
                seasonStatus = res.getSeasonStatus();
                mSeasonId =res.getSesonId();
                mTvDivision.setText(res.getDivisionName());
                mDivisionId=res.getDivisionId();
            }else {
                if (mDivisionNameList.size() > 0) {
                    mTvDivision.setVisibility(View.VISIBLE);
                    mTvDivision.setText(mDivisionNameList.get(0));
                    mDivisionId = mDivisionsList.get(0).getDivisionId();

                } else {
                    mTvDivision.setVisibility(View.GONE);
                }
                if (mDivisionId != null) {
                    List<GetSeasonRes> seasonResList = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
                    if (seasonResList != null && seasonResList.size() > 0) {
                        mTvSeason.setVisibility(View.VISIBLE);
                        mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                        seasonStatus = seasonResList.get(0).getSeasonStatus();
                        mSeasonId = seasonResList.get(0).getSeasonId();
                        //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                    } else {
                        mTvSeason.setVisibility(View.GONE);
                    }
                }
            }



         /*   if (mSeasonNameList.size() > 0) {
                mTvSeason.setVisibility(View.VISIBLE);
                mTvSeason.setText(mSeasonNameList.get(0));
                mSeasonId=seasonsList.get(0).getSeasonId();
            } else {
                mTvSeason.setVisibility(View.GONE);
            }
        }

        if(mDivisionNameList.size()>0) {
            mTvDivision.setVisibility(View.VISIBLE);
            mTvDivision.setText(mDivisionNameList.get(0));
            mDivisionId = mDivisionsList.get(0).getDivisionId();
        }else{
            mTvDivision.setVisibility(View.GONE);
        }*/
        //
        if (mSeasonId != null && mDivisionId != null) {

            standingResList=leagueRes.getGetLeagueStandings();
        //Call Webservice
            if(standingResList.size()>0) {
                mTableTitle.setVisibility(View.VISIBLE);
                mTableListAdapter=new PointsTableListAdapter(getContext(), standingResList);
                rvPoinTable.setAdapter(mTableListAdapter);
            }else{
                if(mTableListAdapter!=null){
                    mTableListAdapter.notifyDataSetChanged();
                }


                mTableTitle.setVisibility(View.GONE);
            }
        }
    }

    private void observeApiResponse() {
        leagueViewModel.getLeagueStandingResLiveData().observe(this,res->{
            standingResList=res;
            if(standingResList.size()>0) {
                mTableTitle.setVisibility(View.VISIBLE);
                mTableListAdapter=new PointsTableListAdapter(getContext(), standingResList);
                rvPoinTable.setAdapter(mTableListAdapter);
            }else{
                if(mTableListAdapter!=null){
                    mTableListAdapter.notifyDataSetChanged();
                }
                mTableTitle.setVisibility(View.GONE);
            }
        });

        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    seasonsList = seasons;
                    mSeasonNameList.clear();
                    if (seasons.size() > 0) {
                        for (int i = 0; i < seasons.size(); i++) {
                            mSeasonNameList.add(seasons.get(i).getSeasonYear());
                        }
                        //
                        mTvSeason.setText(mSeasonNameList.get(0));
                    }
                    mTvWeek.setText(mWeekNameList.get(0));
                });

    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (mGetLeagueRes != null)
            seasonViewModel.getSeasons(new GetLeagueSeasonsReq(mGetLeagueRes.getLeagueId()));*/
    }

    @Override
    public void update(Observable o, Object arg) {
        mGetLeagueRes=((LeagueProfileActivity)getActivity()).mGetLeagueRes;
        observeLeagueScheduleData(mGetLeagueRes);
    }

    public List<GetLeagueStandingRes> getSchedulesTeamStanding(String divisionId, String seasonId){

        List<GetLeagueStandingRes> scheduleList =  mGetLeagueRes.getGetLeagueStandings();

        List<GetLeagueStandingRes> selectedList = new ArrayList<GetLeagueStandingRes>();

        if(scheduleList!=null && scheduleList.size()>0){
            for(GetLeagueStandingRes leagueSchedule:scheduleList){
             //   String scheduleDivisionId=leagueSchedule.getDivisionId().trim();
            //    String scheduleSeasonId=leagueSchedule.getSeasonId().trim();
            //    if(scheduleDivisionId.equals(divisionId) && scheduleSeasonId.equals(seasonId)){
                    selectedList.add(leagueSchedule);
            //    }
            }
        }
        return selectedList;
    }
}
