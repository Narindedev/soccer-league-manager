package com.apnitor.football.league.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetMyTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.ViewPropertyTransition;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MyTeamsListAdapter extends RecyclerView.Adapter<MyTeamsListAdapter.ViewHolder> {

    private Context context;
    private List<GetMyTeamsRes> teamList;
    private int orientation;
    ListItemMultipleCallback listItemClickCallback;
//    private LanguageInterface languageInterface;


    public MyTeamsListAdapter(Context context, ArrayList<GetMyTeamsRes> teamList, int orientation, ListItemMultipleCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public MyTeamsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_team_item, parent, false);
        if (orientation == LinearLayoutManager.VERTICAL)
            view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);

        return new MyTeamsListAdapter.ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    ViewPropertyTransition.Animator animationObject = new ViewPropertyTransition.Animator() {
        @Override
        public void animate(View view) {
            view.setAlpha(0f);
            ObjectAnimator fadeAnim = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f);
            fadeAnim.setDuration(700);
            fadeAnim.start();
        }
    };

    @Override
    public void onBindViewHolder(MyTeamsListAdapter.ViewHolder holder, int position) {
        try {
            GetMyTeamsRes model = teamList.get(position);
            holder.tvTeamName.setText(model.getTeamName());
            if (model.getImageUrl() != null) {
                Glide.with(context).load(model.getImageUrl()).transition(GenericTransitionOptions.with(animationObject)).apply(new RequestOptions()
                        .centerCrop()).into(holder.img);
            } else {
                holder.img.setImageResource(R.drawable.team);
            }
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                }
            });
        }
    }
}
