package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.ScoresPageAdapater;
import com.apnitor.football.league.api.request.GetMatchEventsReq;
import com.apnitor.football.league.api.response.GetMatchEventRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivityMatchDetailsBinding;
import com.apnitor.football.league.viewmodel.RefereeViewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MatchDetailsActivity extends BaseActivity {

    ActivityMatchDetailsBinding binding;
    GetTeamScheduleRes mGetMatchDetail;
    String MATCH_DETAIL = "match_detail";
    RefereeViewModel refereeViewModel;
    ArrayList<GetMatchEventRes> mUpdateMatchEventReq=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_match_details);
        //
        refereeViewModel = getViewModel(RefereeViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        binding.teamName1.setText(mGetMatchDetail.getTeam1Name());
        binding.teamName2.setText(mGetMatchDetail.getTeam2Name());
        binding.matchLocation.setText(mGetMatchDetail.getLocation());
        binding.ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //
        binding.profileViewPager.setAdapter(new ScoresPageAdapater(getSupportFragmentManager(), mGetMatchDetail));
        //
        binding.tabProfile.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.profileViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //
        refereeViewModel.getMatchEvents(new GetMatchEventsReq(mGetMatchDetail.getLeagueId(), mGetMatchDetail.getScheduleId()));
        getMatchDetailsApi();
    }

    private void getMatchDetailsApi() {
        refereeViewModel.getMatchEventsLiveData().observe(this,
                addScheduleRes -> {
                    mUpdateMatchEventReq.clear();
                    mUpdateMatchEventReq.addAll(addScheduleRes);
                }
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.anim_right_in,R.anim.anim_right_out);
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
