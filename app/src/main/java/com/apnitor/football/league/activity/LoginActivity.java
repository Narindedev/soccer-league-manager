package com.apnitor.football.league.activity;

import android.os.Bundle;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.viewmodel.LoginViewModel;

public class LoginActivity extends BaseActivity {

    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginViewModel = getViewModel(LoginViewModel.class);

        loginViewModel.getLogInResLiveData().observe(this, logInRes -> {
            Toast.makeText(this, "You are successfully logged in.", Toast.LENGTH_SHORT).show();
            loginSuccessfully(logInRes);
        });
    }

    public void login(LogInReq logInReq) {
        loginViewModel.login(logInReq);
    }

    public void loginSuccessfully(LogInRes logInRes) {
        getPrefHelper().logIn(logInRes);
        PreferenceHandler.writeString(this, PreferenceHandler.PREF_USER_ID, logInRes.getUserId());
        String firstName = logInRes.getFirstName();
        if (logInRes != null && (firstName == null || firstName.isEmpty())) {
            finishStartActivity(ProfileActivity.class);
        } else if (logInRes.getUserTypes().size() == 1) {
            if (logInRes.getUserTypes().get(0).equalsIgnoreCase("fan")) {
                Bundle mBundle = new Bundle();
                mBundle.putString("ScreenType", "Login");
                finishStartActivity(SelectUserActivity.class,mBundle);
            } else {

                finishStartActivity(HomeActivity.class);
            }
            } else
            finishStartActivity(HomeActivity.class);
        //
        overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
    }
}
