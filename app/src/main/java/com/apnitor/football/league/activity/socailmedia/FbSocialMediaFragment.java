package com.apnitor.football.league.activity.socailmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.fragment.BaseFragment;

public class FbSocialMediaFragment extends BaseFragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static String url = "";
    WebView webView;

    TextView mTextView;

    public static FbSocialMediaFragment newInstance(String socialUrl) {
        url = socialUrl;
        FbSocialMediaFragment fragment = new FbSocialMediaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social_media, container, false);


        webView = view.findViewById(R.id.webView);
        mTextView = view.findViewById(R.id.emptyUrl);


        if (!url.isEmpty()) {
            openSocial(url);
            webView.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.GONE);
        } else {
            mTextView.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
        }
        // Inflate the layout for this fragment
        return view;
    }

    private void openSocial(String url) {
        if (!url.toLowerCase().contains("http://")) {
            if (!url.toLowerCase().contains("www.")) {
                url = "www." + url;
            }
            url = "http://" + url;
        }

        mTextView.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.loadUrl(url);
    }
}
