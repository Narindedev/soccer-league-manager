package com.apnitor.football.league.api.request;

public class GetPlayerDetailReq {
    private String playerId;
    public GetPlayerDetailReq(String playerId){
        this.playerId=playerId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }
}
