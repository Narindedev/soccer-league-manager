package com.apnitor.football.league.adapter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class InviteSearchPlayerAdapter extends RecyclerView.Adapter<InviteSearchPlayerAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> playerList;
    private ListItemClickCallback listItemClickCallback;
    public InviteSearchPlayerAdapter(Context context, ArrayList<PlayerRes> playerList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.playerList = playerList;
        this.listItemClickCallback=listItemClickCallback;
    }

    @Override
    public InviteSearchPlayerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_invite_search_adapter, parent, false);

        return new InviteSearchPlayerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(InviteSearchPlayerAdapter.ViewHolder holder, int position) {
        try {
            PlayerRes model = playerList.get(position);
            holder.tvInvite.setText("Invite Player");
            if (!model.getState().isEmpty()) {
                holder.tvState.setText("State : " + model.getState());
            }else{
                holder.tvState.setVisibility(View.GONE);
            }
            if (!model.getCity().isEmpty()){
                holder.tvCity.setText("City : "+model.getCity());
            }else{
                holder.tvCity.setVisibility(View.GONE);
            }
            holder.tvTeamName.setText(model.getFirstName()+" "+model.getLastName());
            // holder.linearLayout.setBackgroundColor(model.isSelected() ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.button_text_dark));
            Glide.with(context).load(model.getProfilePhotoUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_player)
                    .centerCrop()).into(holder.img);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName,tvInvite,tvState,tvCity;
        RelativeLayout linearLayout;
        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            tvState=(TextView) itemView.findViewById(R.id.tvTeamState);
            tvCity=(TextView) itemView.findViewById(R.id.tvTeamCity);
            tvInvite= (TextView) itemView.findViewById(R.id.tvTeamInvite);
            linearLayout = itemView.findViewById(R.id.llRow);

            tvInvite.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    PlayerRes model = playerList.get(getAdapterPosition());
                    listItemClickCallback.onListItemClick(model);

                }
            });
        }
    }
}