package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.TeamSchedulesListAdapter;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamSchedulesFragment extends BaseFragment implements ListItemClickCallback {

    static GetTeamDetailRes mGetTeamRes;
    List<GetTeamScheduleRes> mGetTeamScheduleRes = new ArrayList<>();
    TeamSchedulesListAdapter mTeamSchedulesListAdapter;
    private TeamViewModel teamViewModel;

    public static TeamSchedulesFragment newInstance(int i, GetTeamDetailRes getTeamRes) {
        mGetTeamRes = getTeamRes;
        TeamSchedulesFragment fragment = new TeamSchedulesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_team_schedules, container, false);
        teamViewModel = getViewModel(TeamViewModel.class);
        RecyclerView v = view.findViewById(R.id.rvLeagueSchedules);
        v.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mTeamSchedulesListAdapter = new TeamSchedulesListAdapter(getContext(), mGetTeamScheduleRes, this, mGetTeamRes.getTeamModel().getTeamId());
        v.setAdapter(mTeamSchedulesListAdapter);
        observeTeamSchedule(mGetTeamRes);
        observeApiResponse();
        setRetainInstance(true);
        return view;
    }

    private void observeTeamSchedule(GetTeamDetailRes getTeamDetailRes) {
        try {
            List<GetTeamScheduleRes> scheduleRes = getTeamDetailRes.getTeamSchedules();
            if (scheduleRes != null && scheduleRes.size() > 0) {
                filterTeamScheduleData(scheduleRes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void observeApiResponse() {
        teamViewModel.getGetTeamScheduleLiveData().observe(this, getTeamScheduleRes -> {
                    filterTeamScheduleData(getTeamScheduleRes);
                }
        );
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            if (mGetTeamRes != null) {
                //     teamViewModel.getTeamSchedule(new GetTeamDetailReq(mGetTeamRes.getTeamModel().getTeamId()));
            }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onListItemClick(Object object) {

    }

    void filterTeamScheduleData(List<GetTeamScheduleRes> getTeamScheduleRes) {
        mGetTeamScheduleRes.clear();
        int size = getTeamScheduleRes.size();
        for (int i = 0; i < size; i++) {
            if (mGetTeamRes.getTeamModel().getTeamId().equalsIgnoreCase(getTeamScheduleRes.get(i).getTeam1Id()) || mGetTeamRes.getTeamModel().getTeamId().equalsIgnoreCase(getTeamScheduleRes.get(i).getTeam2Id())) {
                mGetTeamScheduleRes.add(getTeamScheduleRes.get(i));
            }
        }
        //
        mTeamSchedulesListAdapter.notifyDataSetChanged();
    }
}
