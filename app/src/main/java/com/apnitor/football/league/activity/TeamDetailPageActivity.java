package com.apnitor.football.league.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueTeamsListAdapter;
import com.apnitor.football.league.adapter.SelectPlayerListAdapter;
import com.apnitor.football.league.adapter.TeamPlayersListAdapter;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.ActivityTeamDetailPageBinding;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

import java.util.ArrayList;

public class TeamDetailPageActivity extends BaseActivity {

    private static final String EXTRA_TEAM_DETAIL = "team_detail";

    private ActivityTeamDetailPageBinding binding;

    private GetTeamRes teamDetail;
    private PlayerViewModel playerViewModel;

    private TeamDetailPageActivity teamDetailsPageActivity = this;
    ArrayList<GetTeamPlayersRes> teamList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_detail_page);
        playerViewModel = getViewModel(PlayerViewModel.class);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getSerializable(EXTRA_TEAM_DETAIL) != null) {
            teamDetail = (GetTeamRes) extras.getSerializable(EXTRA_TEAM_DETAIL);

            assert teamDetail != null;
            String teamName = teamDetail.getTeamName();
            binding.teamName.setText(teamName == null ? "" : teamName);

//            String description = teamDetail.getD();
//            if (description == null) description = "";
//            binding.teamDescription.setText(description);
            binding.rvSelectedPlayers.setLayoutManager(new LinearLayoutManager(teamDetailsPageActivity, RecyclerView.VERTICAL, false));
            playerViewModel.getTeamPlayersLiveData().observe(this,
                    teams -> {
                        teamList = teams;
                        binding.rvSelectedPlayers.setAdapter(
                                new SelectPlayerListAdapter(teamDetailsPageActivity, teamList,"")
                        );
                    });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        playerViewModel.getTeamPlayers(new GetTeamPlayersReq(teamDetail.getTeamId()));
    }

    public void onPhotoClick(View view) {
        showToast("Functionality is not implemented yet.");
    }

    public void onAddPlayerlick(View view) {
        Intent i = new Intent(teamDetailsPageActivity, SelectTeamPlayersActivity.class);
        Bundle b = new Bundle();
        b.putString("teamId", teamDetail.getTeamId());
        b.putSerializable("alreadyAddedPlayers", teamList);
        i.putExtras(b);
        startActivity(i);
    }
}
