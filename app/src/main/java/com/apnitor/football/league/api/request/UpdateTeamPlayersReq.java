package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateTeamPlayersReq {

    @Expose
    @SerializedName("teamId")
    private String teamId;

    @Expose
    @SerializedName("playerIds")
    private List<String> playerIds;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    @SerializedName("notificationId")
    public String notificationId;

    public UpdateTeamPlayersReq(String teamId, List<String> playerIds) {
        this.teamId = teamId;
        this.playerIds = playerIds;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public List<String> getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(List<String> playerIds) {
        this.playerIds = playerIds;
    }
}
