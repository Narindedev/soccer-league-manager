package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetLeagueStandingRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class PointsTableListAdapter extends RecyclerView.Adapter<PointsTableListAdapter.ViewHolder> {

    private Context context;
    private List<GetLeagueStandingRes> tableList;

    public PointsTableListAdapter(Context context, List<GetLeagueStandingRes> tableList) {
        this.context = context;
        this.tableList = tableList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_point_table, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetLeagueStandingRes model = tableList.get(position);

            holder.tvPosition.setText((position + 1) + "");

            holder.tvMatchWon.setText(""+model.getMatchesWon());
            holder.tvMatchTie.setText(""+model.getMatchesDrawn());
            holder.tvMatchLoss.setText(""+model.getMatchesLost());
            holder.tvTeamName.setText(""+model.getName());
            holder.tvTeamName.setSelected(true);
            holder.tvPoints.setText(""+model.getPoints());
            holder.tvGoals.setText(""+model.getGoalScored() + "/" + model.getGoalConceded());
            int matchPlayed=model.getMatchesWon()+model.getMatchesDrawn()+model.getMatchesLost();
            holder.tvMatchPlayed.setText(""+matchPlayed);
            Glide.with(context).load(model.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                    .centerCrop()).into(holder.ivTeamImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return tableList.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName, tvMatchPlayed, tvMatchLoss, tvMatchTie, tvMatchWon, tvPoints, tvGoals, tvPosition;
        ImageView ivTeamImage;

        ViewHolder(View itemView) {
            super(itemView);
            ivTeamImage = (ImageView) itemView.findViewById(R.id.ivTeamImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvName);
            tvTeamName.setSelected(true);
            tvPosition = (TextView) itemView.findViewById(R.id.tvSerialNumber);
            tvMatchPlayed = (TextView) itemView.findViewById(R.id.tvMatchPlayed);
            tvMatchLoss = (TextView) itemView.findViewById(R.id.tvMatchLoss);
            tvMatchTie = (TextView) itemView.findViewById(R.id.tvMatchTie);
            tvMatchWon = (TextView) itemView.findViewById(R.id.tvMatchWon);
            tvPoints = (TextView) itemView.findViewById(R.id.tvTeamPoints);
            tvGoals = (TextView) itemView.findViewById(R.id.tvGoalSC);
        }
    }
}
