package com.apnitor.football.league.util;

import android.widget.EditText;

import com.apnitor.football.league.api.request.SocialMedia;

public class Validation {

    public static boolean isValid(String value) {
        if (value == null || value.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean isValid(SocialMedia value) {
        if (value == null) {
            return false;
        }
        return true;
    }
}
