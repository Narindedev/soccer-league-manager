package com.apnitor.football.league.api.response;

import java.io.Serializable;

public class GetSeasonTeamsRes implements Serializable {


    private String teamId;
    private String teamName;

    public GetSeasonTeamsRes(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
