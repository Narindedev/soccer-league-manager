package com.apnitor.football.league.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.activity.PlayerProfileActivity;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.databinding.FragmentPlayerInfoBinding;

import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;

public class PlayerInfoFragment extends Fragment {
    public static PlayerProfileRes mPlayerRes;
private LogInRes logInRes;
    private FragmentPlayerInfoBinding binding;


    public static PlayerInfoFragment newInstance(PlayerProfileRes playerRes) {
        mPlayerRes = playerRes;
        PlayerInfoFragment fragment = new PlayerInfoFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPlayerInfoBinding.inflate(inflater, container, false);

        binding.setPlayerInfo(mPlayerRes);
        setPlayerInfo();
        socialClickListener();


        //binding.rvLeagueInfo.setAdapter();
        return binding.getRoot();
    }

    private void setPlayerInfo() {
        logInRes=((PlayerProfileActivity)getActivity()).logInRes;
        binding.tvDateBirth.setText(logInRes.getDateOfBirth());
        binding.tvMainPoistion.setText(logInRes.getMainPosition());
        binding.tvOtherPosition.setText(logInRes.getOtherPosition());

    }


    private void socialClickListener() {
        binding.ivFbLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String url = mPlayerRes.getSocialMedia().facebookLink;
                    if (url != null && !url.isEmpty()) {
                        openSocial(url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        binding.ivTwitterLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String url = mPlayerRes.getSocialMedia().twitterLink;
                    if (url != null && !url.isEmpty()) {
                        openSocial(url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        binding.ivInstaLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String url = mPlayerRes.getSocialMedia().instaLink;
                    if (url != null && !url.isEmpty()) {
                        openSocial(url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void openSocial(String url) {
        if (!url.toLowerCase().contains("http://")) {
            if (!url.toLowerCase().contains("www.")) {
                url = "www." + url;
            }
            url = "http://" + url;
        }

        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(url));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
