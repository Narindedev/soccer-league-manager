package com.apnitor.football.league.navigation;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigator;
import androidx.navigation.fragment.FragmentNavigator;

/**
 * By default: Navigation Architecture Component with BottomNavigationView setup fragments
 * such that a new fragment is always created when switching fragments by pressing any tab of BottomNavigationView.
 * Hence this class to preserve the fragments so they don't loose state like scrolling position, edit text value etc..
 */
@Navigator.Name("keep_state_fragment") // It will be used in graph xml.
public class KeepStateNavigator extends FragmentNavigator {

    private FragmentManager manager;
    private int containerId;

    public KeepStateNavigator(@NonNull Context context, @NonNull FragmentManager manager, int containerId) {
        super(context, manager, containerId);
        this.manager = manager;
        this.containerId = containerId;
    }

    @Override
    public void navigate(@NonNull Destination destination, @Nullable Bundle args,
                         @Nullable NavOptions navOptions, @Nullable Navigator.Extras navigatorExtras) {

        String tag = String.valueOf(destination.getId());
        FragmentTransaction transaction = this.manager.beginTransaction();
        Fragment currentFragment = this.manager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            transaction.detach(currentFragment);
        }
        Fragment fragment = this.manager.findFragmentByTag(tag);
        if (fragment == null) {
            fragment = destination.createFragment(args);
            transaction.add(this.containerId, fragment, tag);
        } else {
            transaction.attach(fragment);
        }
        transaction.setPrimaryNavigationFragment(fragment);
        transaction.setReorderingAllowed(true);
        transaction.commit();
        this.dispatchOnNavigatorNavigated(destination.getId(), BACK_STACK_DESTINATION_ADDED);
    }
}
