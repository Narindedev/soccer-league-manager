package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateMatchEventReq implements Serializable {
    @Expose
    @SerializedName("matchEventId")
    public String matchEventId;

    @Expose
    @SerializedName("scheduleId")
    public String scheduleId;

    @Expose
    @SerializedName("leagueId")
    public String leagueId;

    @Expose
    @SerializedName("eventType")
    public String eventType;

    @Expose
    @SerializedName("eventTime")
    public String eventTime;

    @Expose
    @SerializedName("goalScoredBy")
    public String goalScoredBy;

    @Expose
    @SerializedName("goalAssistedBy")
    public String goalAssistedBy;

    @Expose
    @SerializedName("givenTo")
    public String givenTo;

    @Expose
    @SerializedName("playerIn")
    public String playerIn;

    @Expose
    @SerializedName("playerOut")
    public String playerOut;

    @Expose
    @SerializedName("startDateTime")
    public String startDateTime;

    @Expose
    @SerializedName("teamId")
    public String teamId;

    @Expose
    @SerializedName("scoredByTeam")
    public String scoredByTeam;

    @Expose
    @SerializedName("concededByTeam")
    public String concededByTeam;

    @Expose
    @SerializedName("endDateTime")
    public String endDateTime;

    @Expose
    @SerializedName("duration")
    public String duration;

    public UpdateMatchEventReq(){

    }

    public UpdateMatchEventReq(String matchEventId, String scheduleId, String leagueId, String eventType, String eventTime, String goalScoredBy, String goalAssistedBy,
                               String givenTo, String playerIn, String playerOut, String startDateTime, String teamId, String scoredByTeam, String concededByTeam,
                               String endDateTime, String duration) {
        this.matchEventId = matchEventId;
        this.scheduleId = scheduleId;
        this.leagueId = leagueId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.goalScoredBy = goalScoredBy;
        this.goalAssistedBy = goalAssistedBy;
        this.givenTo = givenTo;
        this.playerIn = playerIn;
        this.playerOut = playerOut;
        this.startDateTime = startDateTime;
        this.teamId = teamId;
        this.scoredByTeam = scoredByTeam;
        this.concededByTeam = concededByTeam;
        this.endDateTime = endDateTime;
        this.duration = duration;
    }

    public UpdateMatchEventReq(String scheduleId, String leagueId, String eventType, String eventTime, String goalScoredBy, String goalAssistedBy,
                               String givenTo, String playerIn, String playerOut, String startDateTime, String teamId, String scoredByTeam, String concededByTeam,
                               String endDateTime, String duration) {
        this.scheduleId = scheduleId;
        this.leagueId = leagueId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.goalScoredBy = goalScoredBy;
        this.goalAssistedBy = goalAssistedBy;
        this.givenTo = givenTo;
        this.playerIn = playerIn;
        this.playerOut = playerOut;
        this.startDateTime = startDateTime;
        this.teamId = teamId;
        this.scoredByTeam = scoredByTeam;
        this.concededByTeam = concededByTeam;
        this.endDateTime = endDateTime;
        this.duration = duration;
    }

    // SUBSTITUTION
//    public UpdateMatchEventReq(String scheduleId, String leagueId, String eventType, String eventTime, String playerIn, String playerOut, String teamId) {
//        this.scheduleId = scheduleId;
//        this.leagueId = leagueId;
//        this.eventType = eventType;
//        this.eventTime = eventTime;
//        this.playerIn = playerIn;
//        this.playerOut = playerOut;
//        this.teamId = teamId;
//    }

    // ADD EVENT
    public UpdateMatchEventReq(String scheduleId, String leagueId, String eventType, String eventTime) {
        this.scheduleId = scheduleId;
        this.leagueId = leagueId;
        this.eventType = eventType;
        this.eventTime = eventTime;

    }

    //  GOAL WITH ASSISTED BY
    public UpdateMatchEventReq(String scheduleId, String leagueId, String eventType, String eventTime, String goalScoredBy, String goalAssistedBy,
                               String scoredByTeam, String concededByTeam
    ) {
        this.scheduleId = scheduleId;
        this.leagueId = leagueId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.goalScoredBy = goalScoredBy;
        this.goalAssistedBy = goalAssistedBy;
        this.scoredByTeam = scoredByTeam;
        this.concededByTeam = concededByTeam;
    }

    // GOAL
    public UpdateMatchEventReq(String scheduleId, String leagueId, String eventType, String eventTime, String goalScoredBy,
                               String scoredByTeam, String concededByTeam
    ) {
        this.scheduleId = scheduleId;
        this.leagueId = leagueId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.goalScoredBy = goalScoredBy;
        this.scoredByTeam = scoredByTeam;
        this.concededByTeam = concededByTeam;
    }

    public class GoalScoredBy implements Serializable {


        @Expose
        @SerializedName("firstName")
        public String firstName;

        @Expose
        @SerializedName("lastName")
        public String lastName;

        @Expose
        @SerializedName("_id")
        public String id;

    }

}
