package com.apnitor.football.league.interfaces;

public interface ListItemClickCallbackWithPosition {

    void onListItemClick(Object object,int position);

}
