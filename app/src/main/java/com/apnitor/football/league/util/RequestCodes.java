package com.apnitor.football.league.util;

public class RequestCodes {
    public static int EDIT_LEAGUE_CODE = 1000;
    public static int DIVISION_REQUEST_CODE = 1001;
    public static int SEASON_REQUEST_CODE = 1002;
    public static int TEAM_REQUEST_CODE = 1003;
    public static int ADD_TEAM_REQUEST_CODE = 1005;
    public static int SCHEDULE_REQUEST_CODE = 1004;

    /*Team Request Code*/
    public static int ADD_Player_REQUEST_CODE = 1006;
    public static int EDIT_TEAM_CODE = 1007;


    public static String ADD_TEAM_TO_LEAGUE="Add_team_to_league";

    public static String ADD_PLAYER_TO_TEAM="Add_player_to_team";


   /* public static String LEAGUE_PROFILE_TEAM_BROADCAST1="LEAGUE_PROFILE_TEAM_BROADCAST1";
    public static String LEAGUE_PROFILE_TEAM_BROADCAST="LEAGUE_PROFILE_TEAM_BROADCAST";

    public static String LEAGUE_PROFILE_PLAYER_BROADCAST1="LEAGUE_PROFILE_PLAYER_BROADCAST1";
    public static String LEAGUE_PROFILE_PLAYER_BROADCAST="LEAGUE_PROFILE_PLAYER_BROADCAST";*/

}
