package com.apnitor.football.league.api.response;

import com.apnitor.football.league.api.request.SocialMedia;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LogInRes implements Serializable {

    @Expose
    @SerializedName("authToken")
    private String authToken;

    @Expose
    @SerializedName("userId")
    private String userId;

    @Expose
    @SerializedName("username")
    private String username;

    @Expose
    @SerializedName("firstName")
    private String firstName;

    @Expose
    @SerializedName("lastName")
    private String lastName;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("profilePhotoUrl")
    private String profilePhotoUrl;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("countryName")
    private String countryName;

    @Expose
    @SerializedName("state")
    private String state;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("dateOfBirth")
    private String dateOfBirth;


    @Expose
    @SerializedName("height")
    private String height;

    @Expose
    @SerializedName("weight")
    private String weight;

    @Expose
    @SerializedName("jerseyNo")
    private String jerseyNo;

    @Expose
    @SerializedName("mainPosition")
    private String mainPosition;

    @Expose
    @SerializedName("otherPosition")
    private String otherPosition;

    @Expose
    @SerializedName("socialMedia")
    private SocialMedia socialMedia;

    @Expose
    @SerializedName("userTypes")
    private ArrayList<String> userTypes;

    public LogInRes(String authToken, String userId, String username, String firstName,
                    String lastName, String email, String profilePhotoUrl, String phone,
                    String countryName, String state, String city,String dateOfBirth,String height,String weight,String jerseyNo,String mainPosition,String otherPosition,SocialMedia  socialMedia,  ArrayList<String> userTypes) {
        this.authToken = authToken;
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.profilePhotoUrl = profilePhotoUrl;
        this.phone = phone;
        this.countryName = countryName;
        this.state = state;
        this.city = city;
        this.dateOfBirth=dateOfBirth;
        this.height=height;
        this.weight=weight;
        this.jerseyNo=jerseyNo;
        this.mainPosition=mainPosition;
        this.otherPosition=otherPosition;
        this.socialMedia=socialMedia;
        this.userTypes = userTypes;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ArrayList<String> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(ArrayList<String> userTypes) {
        this.userTypes = userTypes;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getJerseyNo() {
        return jerseyNo;
    }

    public void setJerseyNo(String jerseyNo) {
        this.jerseyNo = jerseyNo;
    }

    public String getMainPosition() {
        return mainPosition;
    }

    public void setMainPosition(String mainPosition) {
        this.mainPosition = mainPosition;
    }

    public String getOtherPosition() {
        return otherPosition;
    }

    public void setOtherPosition(String otherPosition) {
        this.otherPosition = otherPosition;
    }

    public SocialMedia getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(SocialMedia socialMedia) {
        this.socialMedia = socialMedia;
    }

    @Override
    public String toString() {
        return "LogInRes{" +
                "authToken='" + authToken + '\'' +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", profilePhotoUrl='" + profilePhotoUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", countryName='" + countryName + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", height='" + height + '\'' +
                ", weight='" + weight + '\'' +
                ", jerseyNo='" + jerseyNo + '\'' +
                ", mainPosition='" + mainPosition + '\'' +
                ", otherPosition='" + otherPosition + '\'' +
                ", socialMedia=" + socialMedia +
                ", userTypes=" + userTypes +
                '}';
    }
}
