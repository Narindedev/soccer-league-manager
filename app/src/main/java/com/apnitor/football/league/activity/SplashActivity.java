package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.apnitor.football.league.R;
import com.apnitor.football.league.application.FootballLeagueApplication;
import com.facebook.AccessToken;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }


        new Handler()
                .postDelayed(this::navigateToNextActivity, 2000);

    }

    private void navigateToNextActivity() {

        boolean isAlreadyLoggedIn = AccessToken.getCurrentAccessToken() != null
                || ((FootballLeagueApplication) getApplication())
                .getSharedPreferenceHelper()
                .isLoggedIn();

        Intent intent;
        if (isAlreadyLoggedIn) {
            ArrayList<String> userType = ((FootballLeagueApplication) getApplication())
                    .getSharedPreferenceHelper().getLogInRes().getUserTypes();

            if (userType != null && userType.isEmpty()) {
                Bundle mBundle = new Bundle();
                mBundle.putString("ScreenType", "Login");
                finishStartActivity(SelectUserActivity.class, mBundle);
            } else {
                intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                finish();
            }


        } else {
            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();

        }
        //
        overridePendingTransition(R.anim.anim_left_in,R.anim.anim_left_out);
    }


}
