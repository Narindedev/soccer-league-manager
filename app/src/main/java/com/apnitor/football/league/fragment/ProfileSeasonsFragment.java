package com.apnitor.football.league.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.SeasonPagePagerAdapater;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.FragmentProfileSeasonsBinding;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;

import java.util.Observable;
import java.util.Observer;

import androidx.viewpager.widget.ViewPager;

public class ProfileSeasonsFragment extends BaseFragment implements ProfileSeasonFragmentBindingCallback, Observer {

    static GetLeagueRes mGetLeagueRes;
    CustomViewPager viewPager;
    SeasonPagePagerAdapater adapter;
    private FragmentProfileSeasonsBinding binding;
    private LeagueProfileActivity leagueProfileActivity;

    public static ProfileSeasonsFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes = new GetLeagueRes();
        mGetLeagueRes = getLeagueRes;
        ProfileSeasonsFragment fragment = new ProfileSeasonsFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.leagueProfileActivity = (LeagueProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProfileSeasonsBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        viewPager = binding.seasonPageViewPager;
        adapter = new SeasonPagePagerAdapater(getChildFragmentManager(), mGetLeagueRes);
        viewPager.setAdapter(adapter);

        //
        binding.ivResultsTab.setImageResource(R.drawable.ic_resultclr);
        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
        binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
        //onFabClick();
        //
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setPage(position);
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return binding.getRoot();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showResults() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void showSchedules() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showPointsTable() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void showStats() {
        viewPager.setCurrentItem(3);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null) {
            viewPager.setCurrentItem(0);
            setPage(viewPager.getCurrentItem());
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        Observable observable = o;
        Object object = arg;
        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;

        if (viewPager != null) {
            adapter = new SeasonPagePagerAdapater(getChildFragmentManager(), mGetLeagueRes);
            viewPager.setAdapter(adapter);
            if(!leagueProfileActivity.scheduleSeasonId.isEmpty()) {
                viewPager.setCurrentItem(1);
            }
            viewPager.setOffscreenPageLimit(4);
          //  adapter.updateFragments();
         /*   if (viewPager.getAdapter() != null) {
                try {
                   viewPager.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/
        }

    }

    void setPage(int position) {
        switch (position) {
            case 0:
                LeagueProfileActivity.FAB_TYPE = "";
                LeagueProfileActivity.fabSchedule.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_resultclr);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                break;
            case 1:
                LeagueProfileActivity.FAB_TYPE = "Schedule";
                if (getPrefHelper().getUserId().equals(mGetLeagueRes.getOwner())) {
                    LeagueProfileActivity.fabSchedule.setVisibility(View.VISIBLE);
                }
                binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechduleclr);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                break;
            case 2:
                LeagueProfileActivity.FAB_TYPE = "";
                LeagueProfileActivity.fabSchedule.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_standingclr);
                binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                break;
            case 3:
                LeagueProfileActivity.FAB_TYPE = "";
                LeagueProfileActivity.fabSchedule.setVisibility(View.GONE);
                binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                binding.ivStatsTab.setImageResource(R.drawable.ic_statsclr);
                break;
        }
    }

}
