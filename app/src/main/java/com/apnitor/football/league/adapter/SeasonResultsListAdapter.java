package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.ScheduleResultRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.UIUtility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SeasonResultsListAdapter extends RecyclerView.Adapter<SeasonResultsListAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    //    private LanguageInterface languageInterface;
    private List<ScheduleResultRes> leagueScheduleList;
    GetLeagueRes mGetLeagueRes;
    String previousDate="";

    public SeasonResultsListAdapter(Context context, ArrayList<String> teamList, List<ScheduleResultRes> leagueScheduleList, GetLeagueRes mGetLeagueRes) {
        this.context = context;
        this.teamList = teamList;
        this.leagueScheduleList = leagueScheduleList;
        this.mGetLeagueRes=mGetLeagueRes;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_season_results, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.dayResultRv.setRecycledViewPool(viewPool);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            holder.dayResultRv.setHasFixedSize(true);
            holder.dayResultRv.setNestedScrollingEnabled(false);

            SeasonDayResultsListAdapter2 adapter = new SeasonDayResultsListAdapter2(context, null, leagueScheduleList.get(position), position,mGetLeagueRes);
            holder.dayResultRv.setAdapter(adapter);
            if (leagueScheduleList.size() > 0) {
                ScheduleResultRes model = leagueScheduleList.get(position);
                String date=UIUtility.getFormattedDate(model.getDate());
                holder.tvTeamName.setText(date);
               /* if(!previousDate.equals(date)) {
                    setMarginCard(holder.cardView);
                    holder.tvTeamName.setVisibility(View.VISIBLE);
                    holder.tvTeamName.setText(date);
                }else{
                    holder.tvTeamName.setVisibility(View.GONE);
                }*/
                previousDate=date;
            }

//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueScheduleList.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;
        private RecyclerView dayResultRv;
        private CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvSeasonDayDate);
            dayResultRv = itemView.findViewById(R.id.rvDayResults);
            cardView=(CardView)itemView.findViewById(R.id.cardView);
            dayResultRv.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        }
    }


    private String convertDate(String date) {
        String sMyDate = date;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.ENGLISH);
            Date myDate = sdf.parse(date);
            sdf.applyPattern("EEE, d MMM yyyy");
            sMyDate = sdf.format(myDate);
            System.out.println("PREV DATE IS " + date);
            System.out.println("FINAL DATE IS " + sMyDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sMyDate;
    }
private void setMarginCard(CardView cardView){
    LinearLayout.LayoutParams cardViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    cardViewParams.setMargins(10,10,10,0);


    cardView.setLayoutParams(cardViewParams);
}
}
