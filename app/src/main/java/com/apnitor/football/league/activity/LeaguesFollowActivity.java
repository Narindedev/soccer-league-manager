package com.apnitor.football.league.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueFollowAdapter;
import com.apnitor.football.league.adapter.PlayerFollowAdapter;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.databinding.ActivityLeaguesFollowBinding;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;

public class LeaguesFollowActivity extends BaseActivity implements ListItemClickCallback {

    private ActivityLeaguesFollowBinding binding;
    private LeagueViewModel leagueViewModel;
    private LeaguesFollowActivity leaguesFollowActivity = this;
    private String followMessage;
    private FollowingViewModel followingViewModel;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_leagues_follow);

        leagueViewModel = getViewModel(LeagueViewModel.class);
        followingViewModel = getViewModel(FollowingViewModel.class);
        setupToolbar(binding.toolbar, "Follow Leagues");
        setupRecyclerView();
        observeApiResponse();
        binding.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teamName = binding.teamNameEt.getText().toString();
                String teamCity = binding.cityEt.getText().toString();
                String teamZipCode = binding.zipCodeEt.getText().toString();

                if (teamName.isEmpty()) {
                    showToast("Please enter league name.");
                } else {
                    hideKeyboard(LeaguesFollowActivity.this);
                    leagueViewModel.getAllLeagues(new GetAllTeamReq(teamName, teamCity, teamZipCode));
                }

            }
        });
    }

    private void observeApiResponse() {
        followingViewModel.getLeagueFollowingResLiveData().observe(this, follow -> {
            FollowingFragment.isFollowing=true;
            showToast(followMessage);
            finish();
                }
        );
        leagueViewModel.getAllLeagueListResLiveData().observe(this,
                leagues -> {
                    binding.rvAllLeagues.setAdapter(
                            new LeagueFollowAdapter(leaguesFollowActivity, leagues, this)
                    );
                });
       // leagueViewModel.getAllLeagues(new GetAllTeamReq(teamName, teamCity, teamZipCode));
    }

    private void setupRecyclerView() {
        binding.rvAllLeagues.setLayoutManager(
                new LinearLayoutManager(leaguesFollowActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
        GetLeagueRes leagueRes = (GetLeagueRes) object;
        if (leagueRes.isFollowing) {
            followMessage = "League followed";
            followingViewModel.followLeague(new GetLeagueDetailsReq(leagueRes.getLeagueId()));
        } else {
            followMessage = "League Unfollowed";
            followingViewModel.unfollowLeague(new GetLeagueDetailsReq(leagueRes.getLeagueId()));
        }

//        Intent intent = new Intent(playerListActivity, TeamDetailPageActivity.class);
//        Bundle b = new Bundle();
//        b.putSerializable("team_detail", getTeamRes);
//        intent.putExtras(b);
//        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


}
