package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.SearchTeamActivity;
import com.apnitor.football.league.adapter.LeagueTeamListAdapter;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.SelectedSeasonDivisionRes;
import com.apnitor.football.league.viewmodel.DivisionViewModel;
import com.apnitor.football.league.viewmodel.SeasonViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueTeamsFragment extends BaseFragment implements Observer {

    public static String mSeasonId = "", mDivisionId = "",mDivisionName="";
    private static GetLeagueRes mGetLeagueRes;
    TextView mTvSeason, mTvWeek, mTvLeagues, mTvInvitedTeams;
    String seasonStatus;
    String LOG_TAG = "LeagueTeamsFragment";
    RecyclerView mRvAddedTeams, mRvInvitedTeams;
    private FloatingActionButton mAddTeamButton;
    private SeasonViewModel seasonViewModel;
    private DivisionViewModel divisionViewModel;
    private List<GetSeasonRes> seasonsList = new ArrayList<>();
    private List<GetDivisionRes> divisionsList = new ArrayList<>();
    private String mLeagueId;
    private TeamViewModel teamViewModel;
    private LeagueTeam leagueTeam = new LeagueTeam();
    private LinearLayout mEmptyTeam;

    public static LeagueTeamsFragment newInstance(GetLeagueRes getLeagueRes) {
        mGetLeagueRes = getLeagueRes;
        LeagueTeamsFragment fragment = new LeagueTeamsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_league_teams, container, false);
        //
        seasonViewModel = getViewModel(SeasonViewModel.class);
        divisionViewModel = getViewModel(DivisionViewModel.class);
        teamViewModel = getViewModel(TeamViewModel.class);

        //
        mEmptyTeam = view.findViewById(R.id.llEmptyTeams);
        mRvAddedTeams = view.findViewById(R.id.rvLeaguesTeams);
        mRvInvitedTeams = view.findViewById(R.id.rvInvitedTeams);
        mTvInvitedTeams = view.findViewById(R.id.tvInvitedTeams);
        mAddTeamButton = view.findViewById(R.id.fabAddLeagueTeam);

        mTvLeagues = view.findViewById(R.id.tvLeagueTeams);
        mRvInvitedTeams.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRvAddedTeams.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        mTvSeason = view.findViewById(R.id.tvSelectSeasonBtn);
        mTvWeek = view.findViewById(R.id.tvSelectWeekBtn);
//        mAddTeamButton.setVisibility(View.VISIBLE);


        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;

        if (mGetLeagueRes.seasonList != null) {
            seasonsList.clear();
            seasonsList.addAll(mGetLeagueRes.seasonList);

        }
        if (mGetLeagueRes.divisionList != null) {
            divisionsList.clear();
            divisionsList.addAll(mGetLeagueRes.divisionList);
        }


        loadObserveData();
        observeApiResponse();
        //
        mTvSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Season", Toast.LENGTH_SHORT).show();
                showSeasonDialog();
            }

        });
        mTvWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDivisionDialog();
            }
        });
        //
        mAddTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSeasonId.isEmpty() || mDivisionId.isEmpty()) {
                    showToast("Please select season and division first");
                } else {
                    Bundle b = new Bundle();
                    b.putString("leagueId", mGetLeagueRes.getLeagueId());
                    b.putString("seasonId", mSeasonId);
                    b.putString("divisionId", mDivisionId);
                    startActivity(SearchTeamActivity.class, b);
                }
            }
        });
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    void showSeasonDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Season");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        for (int i = 0; i < seasonsList.size(); i++) {
            arrayAdapter.add(seasonsList.get(i).getSeasonYear());
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                mTvSeason.setText(strName);
                mSeasonId = seasonsList.get(which).getSeasonId();

                List<SelectedSeasonDivisionRes> seasonDivisionRes=((LeagueProfileActivity)getActivity()).selectedSeasonDivision(mDivisionId,mSeasonId,which);
                if(seasonDivisionRes!=null && seasonDivisionRes.size()>0){
                    mTvWeek.setText(seasonDivisionRes.get(0).getDivisionName());
                    mDivisionId=seasonDivisionRes.get(0).getDivisionId();
                    mDivisionName=seasonDivisionRes.get(0).getDivisionName();
                }

                if(mSeasonId!=null && mDivisionId!=null){
                    apiCall();
                }
            }
        });
        builderSingle.show();
    }

    void showDivisionDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle("Select Division");
        builderSingle.setCancelable(true);

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
        if (!divisionsList.isEmpty()) {
            for (int i = 0; i < divisionsList.size(); i++) {
                arrayAdapter.add(divisionsList.get(i).getDivisionName());
            }

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    mTvWeek.setText(strName);
                    mDivisionName=strName;
                    mDivisionId = divisionsList.get(which).getDivisionId();

                    List<GetSeasonRes> seasonResList=((LeagueProfileActivity)getActivity()).getDivisionSeason(mDivisionId);
                    if(seasonResList!=null && seasonResList.size()>0){
                        mSeasonId = seasonResList.get(0).getSeasonId();
                        mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                    }

                    if(mSeasonId!=null && mDivisionId!=null) {
                        apiCall();
                    }
                }
            });
        }
        builderSingle.show();
    }

    private void loadObserveData() {
        mSeasonId = "";
        mDivisionId = "";
        mDivisionName="";
        /*Set Season Data from League Response*/
        seasonsList.clear();
        seasonsList.addAll(mGetLeagueRes.seasonList);

        divisionsList.clear();
        divisionsList.addAll(mGetLeagueRes.divisionList);


        if (seasonsList.size() > 0) {
            mTvSeason.setVisibility(View.VISIBLE);
            mTvSeason.setText(seasonsList.get(0).getSeasonYear());
            mSeasonId = seasonsList.get(0).getSeasonId();
            seasonStatus=seasonsList.get(0).getSeasonStatus();
        } else {
            mTvSeason.setVisibility(View.GONE);
        }
        /*Set Division Data from League Response*/

        if (divisionsList.size() > 0) {
            mTvWeek.setVisibility(View.VISIBLE);
            mTvWeek.setText(divisionsList.get(0).getDivisionName());
            mDivisionId = divisionsList.get(0).getDivisionId();
            mDivisionName = divisionsList.get(0).getDivisionName();
            // apiCall();

        } else {
            mTvWeek.setVisibility(View.GONE);
        }


       /* List<SelectedSeasonDivisionRes> seasonDivisionResList=((LeagueProfileActivity)getActivity()).getActiveSeasonDivision();

        if(seasonDivisionResList!=null && seasonDivisionResList.size()>0){
            SelectedSeasonDivisionRes res=seasonDivisionResList.get(0);
            mTvSeason.setText(res.getSesonName());
            seasonStatus = res.getSeasonStatus();
            mSeasonId =res.getSesonId();
            mTvWeek.setText(res.getDivisionName());
            mDivisionId=res.getDivisionId();
        }else {
            mTvSeason.setVisibility(View.GONE);
            mTvWeek.setVisibility(View.GONE);
            if (divisionsList.size() > 0) {
                mTvWeek.setVisibility(View.VISIBLE);
                mTvWeek.setText(divisionsList.get(0).getDivisionName());
                mDivisionId = divisionsList.get(0).getDivisionId();

            } else {
                mTvWeek.setVisibility(View.GONE);
            }
            if (!mDivisionId.isEmpty()) {
                List<GetSeasonRes> seasonResList = ((LeagueProfileActivity) getActivity()).getDivisionSeason(mDivisionId);
                if (seasonResList != null && seasonResList.size() > 0) {
                    mTvSeason.setVisibility(View.VISIBLE);
                    mTvSeason.setText(seasonResList.get(0).getSeasonYear());
                    seasonStatus = seasonResList.get(0).getSeasonStatus();
                    mSeasonId = seasonResList.get(0).getSeasonId();
                    //  divisionViewModel.getDivisions(new GetLeagueDivisionsReq(mGetLeagueRes.getLeagueId()));
                } else {
                    mTvSeason.setVisibility(View.GONE);
                }
            }*//*else{
                mTvSeason.setVisibility(View.GONE);
                mTvWeek.setVisibility(View.GONE);
            }*//*
        }*/
        if((!((LeagueProfileActivity) getActivity()).teamDivisionId.isEmpty())){
            mDivisionId = ((LeagueProfileActivity) getActivity()).teamDivisionId;
            mDivisionName=((LeagueProfileActivity) getActivity()).teamDivisionName;
            mTvWeek.setText(mDivisionName);
        }


        List<GetSeasonRes> selectedivisonSeason=((LeagueProfileActivity)getActivity()).getDivisionSeason(mDivisionId);
        if(selectedivisonSeason!=null) {
            for (GetSeasonRes seasonRes : selectedivisonSeason) {
                try {
                    if (mGetLeagueRes.getLeagueTeamList() != null && !mGetLeagueRes.getLeagueTeamList().isEmpty()) {
                        String newSeasonId = mGetLeagueRes.getLeagueTeamList().get(0).getSeasonId();

                        if (seasonRes.getSeasonId().equals(newSeasonId)) {
                            mSeasonId = newSeasonId;
                            seasonStatus = seasonRes.getSeasonStatus();
                            mTvSeason.setVisibility(View.VISIBLE);
                            mTvSeason.setText(seasonRes.getSeasonYear());
                        }

                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

        if(mSeasonId.isEmpty() || mDivisionId.isEmpty()){
            mEmptyTeam.setVisibility(View.VISIBLE);

        }else{
            loadLeagueTeams();
        }
    }

    private void loadLeagueTeams() {
        try {
            if (mGetLeagueRes.getLeagueTeamList() != null && !mGetLeagueRes.getLeagueTeamList().isEmpty()) {
                mEmptyTeam.setVisibility(View.GONE);
                List<GetTeamRes> invitedTeamRes = getInvitedTeams(mDivisionId,mSeasonId,mGetLeagueRes.getLeagueTeamList());
                List<GetTeamRes> addedTeamRes = getTeamsIDS(mDivisionId,mSeasonId,mGetLeagueRes.getLeagueTeamList());

                if (addedTeamRes != null) {
                    if (addedTeamRes.size() > 0) {
                        mTvLeagues.setVisibility(View.VISIBLE);
                        mRvAddedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), addedTeamRes));
                    } else {
                        mTvLeagues.setVisibility(View.GONE);
                        mRvAddedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), addedTeamRes));
                    }
                }

                if (invitedTeamRes != null) {
                    if (invitedTeamRes.size() > 0) {
                        mTvInvitedTeams.setVisibility(View.VISIBLE);
                        mRvInvitedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), invitedTeamRes));
                    } else {
                        mRvInvitedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), invitedTeamRes));
                        mTvInvitedTeams.setVisibility(View.GONE);
                    }
                } else {
                    mTvInvitedTeams.setVisibility(View.GONE);
                    mTvLeagues.setVisibility(View.GONE);
                }
            } else {
                mEmptyTeam.setVisibility(View.VISIBLE);
                if(seasonStatus.equals("Not Started")) {
                    if(LeagueProfileActivity.isLeagueTeamLoadFIrtsTime) {
                        LeagueProfileActivity.isLeagueTeamLoadFIrtsTime=false;
                        autoSelectFirstDivision();
                    }
                }
                }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void observeApiResponse() {

        seasonViewModel.getLeagueListResLiveData().observe(this,
                seasons -> {
                    seasonsList.clear();
                    seasonsList.addAll(seasons);
                    //
                    if (seasons.size() > 0) {
                        mTvSeason.setText(seasonsList.get(0).getSeasonYear());
                        mSeasonId = seasonsList.get(0).getSeasonId();
                    }
                });
        //
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    divisionsList.clear();
                    divisionsList.addAll(divisions);
                });
        //
        teamViewModel.getLeagueTeamsLiveData().observe(this,
                teams -> {
                    leagueTeam = teams;
                    //
                    mGetLeagueRes.setLeagueTeamList(new ArrayList<>());
                    mGetLeagueRes.getLeagueTeamList().add(leagueTeam);
                    //
                    if (leagueTeam != null) {
                        mEmptyTeam.setVisibility(View.GONE);
                        List<GetTeamRes> invitedTeamRes = leagueTeam.getInvitedTeams();
                        List<GetTeamRes> addedTeamRes = leagueTeam.getTeamIds();
                        /*Show added league teams*/
                        if (addedTeamRes.size() > 0) {
                            mEmptyTeam.setVisibility(View.GONE);
                            mTvLeagues.setVisibility(View.VISIBLE);
                            mRvAddedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), addedTeamRes));
                        } else {
                            mTvLeagues.setVisibility(View.GONE);
                            mRvAddedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), addedTeamRes));
                        }

                        /*Show invited teams*/
                        if (invitedTeamRes.size() > 0) {
                            mEmptyTeam.setVisibility(View.GONE);
                            mTvInvitedTeams.setVisibility(View.VISIBLE);
                            mRvInvitedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), invitedTeamRes));
                        } else {
                            mTvInvitedTeams.setVisibility(View.GONE);
                        }
                    }else{
                        List<GetTeamRes> invitedTeamRes = new ArrayList<>();
                        List<GetTeamRes> addedTeamRes = new ArrayList<>();
                        mRvAddedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), addedTeamRes));
                        mRvInvitedTeams.setAdapter(new LeagueTeamListAdapter(getContext(), invitedTeamRes));
                        mEmptyTeam.setVisibility(View.VISIBLE);
                        mTvInvitedTeams.setVisibility(View.GONE);
                        mTvLeagues.setVisibility(View.GONE);

                    }
                });


    }

    void apiCall() {
         ((LeagueProfileActivity) getActivity()).teamDivisionId="";
        ((LeagueProfileActivity) getActivity()).teamDivisionName="";

        mLeagueId = mGetLeagueRes.getLeagueId();
        Log.d(LOG_TAG, " League Id is " + mLeagueId);
        Log.d(LOG_TAG, " Season Id Id is " + mSeasonId);
        Log.d(LOG_TAG, " Division Id is " + mDivisionId);
        if(!mSeasonId.isEmpty() && !mDivisionId.isEmpty()) {
            teamViewModel.getLeagueTeams(new GetLeagueTeamsReq(mLeagueId, mSeasonId,
                    mDivisionId));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void update(Observable o, Object arg) {



        /**
         * TODO
         * Update Adapters only
         *
         */
        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;
            /*if(mGetLeagueRes.seasonList!=null && mGetLeagueRes.seasonList.size()>0){
                seasonsList.clear();
                seasonsList =    mGetLeagueRes.seasonList;
            }
            if(mGetLeagueRes.divisionList!=null && mGetLeagueRes.divisionList.size()>0){
                divisionsList.clear();
                divisionsList=mGetLeagueRes.divisionList;
            }
*/
        loadObserveData();
      //  loadLeagueTeams();
    }


    public List<GetTeamRes> getTeamsIDS(String divisionId, String seasonId,List<LeagueTeam> getTeamRes){
        List<GetTeamRes> getTeamRes1 = getTeamRes.get(0).getTeamIds();
        List<GetTeamRes> selectedList = new ArrayList<GetTeamRes>();
        if(getTeamRes1!=null && getTeamRes1.size()>0){
            for(GetTeamRes leagueSchedule:getTeamRes1){
                String scheduleDivisionId=getTeamRes.get(0).getDivisionId().trim();
                String scheduleSeasonId=getTeamRes.get(0).getSeasonId().trim();
                if(scheduleDivisionId.equals(divisionId) && scheduleSeasonId.equals(seasonId)){
                    selectedList.add(leagueSchedule);
                }
            }
        }
        return selectedList;
    }

    public List<GetTeamRes> getInvitedTeams(String divisionId, String seasonId,List<LeagueTeam> getTeamRes){

        List<GetTeamRes> getTeamRes1 = getTeamRes.get(0).getInvitedTeams();
        List<GetTeamRes> selectedList = new ArrayList<GetTeamRes>();

        if(getTeamRes1!=null && getTeamRes1.size()>0){
            for(GetTeamRes leagueSchedule:getTeamRes1){
                String scheduleDivisionId=getTeamRes.get(0).getDivisionId().trim();
                String scheduleSeasonId=getTeamRes.get(0).getSeasonId().trim();
                if(scheduleDivisionId.equals(divisionId) && scheduleSeasonId.equals(seasonId)){
                    selectedList.add(leagueSchedule);
                }
            }
        }
        return selectedList;
    }


    void autoSelectFirstDivision() {
        try {
            apiCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
