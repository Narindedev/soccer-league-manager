package com.apnitor.football.league.api.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateLeagueTeamReq {

    private String leagueId;
    String seasonId;
    String divisionId;
    private List<String> teamIds;
    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    @SerializedName("notificationId")
    public String notificationId;

    public UpdateLeagueTeamReq(String leagueId, String seasonId, String divisionId, List<String> teamIds) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
        this.teamIds = teamIds;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public List<String> getTeamIds() {
        return teamIds;
    }

    public void setTeamIds(List<String> teamIds) {
        this.teamIds = teamIds;
    }
}
