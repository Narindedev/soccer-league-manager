package com.apnitor.football.league.activity.referee;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.request.UpdateMatchEventReq;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivityGoalDetailBinding;
import com.apnitor.football.league.databinding.ActivitySubstitutionBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.apnitor.football.league.viewmodel.RefereeViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;

public class SubstitutionActivity extends BaseActivity implements ListItemClickCallback {
    private ActivitySubstitutionBinding binding;
    private List<TeamPlaying11> mTeamPlayersRes;
    private PlayerViewModel playerViewModel;
    private LeagueViewModel leagueViewModel;
    private RefereeViewModel refereeViewModel;
    private String playerId = "", playerOutId = "";
    private GoalDetailAdapter mTeam1Adapter;
    private GoalDetailAdapter mTeam2Adapter;
    private GoalDetailAdapter mPlayerOutAdapter;
    private List<TeamPlaying11> team1List, team2List, mPlayerInList;
    private String mSeasonId, mDivisionId, mLeagueId, scheduleId;
    private GetTeamScheduleRes teamScheduleRes;
    private String team1Id = "", team2Id = "", mConcededById = "", mScoredByTeamId = "";
    private String eventTime, eventId;
    long mStartTime = 0, resumeTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playerViewModel = getViewModel(PlayerViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        refereeViewModel = getViewModel(RefereeViewModel.class);
        binding = bindContentView(R.layout.activity_substitution);
        //
        Typeface font = Typeface.createFromAsset(getAssets(), "DS_DIGI.TTF");
        binding.tvTime1Countdown.setTypeface(font, Typeface.NORMAL);
        binding.tvTime1Countdown.setTextSize(25);
        //

        Bundle b = getIntent().getExtras();
        if (b != null) {
            eventTime = b.getString("eventTime");
            eventId = b.getString("matchEventId");
            teamScheduleRes = (GetTeamScheduleRes) b.getSerializable("match_detail");
            if (teamScheduleRes != null) {
                mSeasonId = teamScheduleRes.getSeasonId();
                mDivisionId = teamScheduleRes.getDivisionId();
                mLeagueId = teamScheduleRes.getLeagueId();
                scheduleId = teamScheduleRes.getScheduleId();
                binding.tvTeam1.setText(teamScheduleRes.getTeam1Name());
                binding.tvTeam2.setText(teamScheduleRes.getTeam2Name());
            }
        }

        setupToolbar(binding.toolbar, "Substitution");

        mTeamPlayersRes = new ArrayList<>();
        team1List = new ArrayList<>();
        team2List = new ArrayList<>();
        mPlayerInList = new ArrayList<>();
        observeApiResponse();
    }

    private void observeApiResponse() {
        team1List.clear();
        team2List.clear();
        mPlayerInList.clear();
        //
        int team1size = teamScheduleRes.getTeam1Playing11().size();
        for (int i = 0; i < team1size; i++) {
            if (teamScheduleRes.getTeam1Playing11().get(i).getPlaying())
                team1List.add(teamScheduleRes.getTeam1Playing11().get(i));
            else
                mPlayerInList.add(teamScheduleRes.getTeam1Playing11().get(i));
        }
        //
        int team2size = teamScheduleRes.getTeam2Playing11().size();
        for (int i = 0; i < team2size; i++) {
            if (teamScheduleRes.getTeam2Playing11().get(i).getPlaying())
                team2List.add(teamScheduleRes.getTeam2Playing11().get(i));
        }
        //
        mTeamPlayersRes.addAll(team1List);
        //
        binding.rvSelectPlayer.setLayoutManager(new LinearLayoutManager(this));
        binding.rvSelectPlayer2.setLayoutManager(new LinearLayoutManager(this));
        //
        mTeam1Adapter = new GoalDetailAdapter(this, mPlayerInList, this);
        mTeam2Adapter = new GoalDetailAdapter(this, mTeamPlayersRes, this);
        mPlayerOutAdapter = new GoalDetailAdapter(this, mTeamPlayersRes, playerOutListener);
        //
        binding.rvSelectPlayer.setAdapter(mTeam1Adapter);
        binding.rvSelectPlayer2.setAdapter(mPlayerOutAdapter);
        //
        refereeViewModel.updateMatchEventsLiveData().observe(this, res -> {
            Intent mIntent = new Intent();
            eventId = res.matchEventId;
            mIntent.putExtra("eventId", eventId);
            setResult(101, mIntent);
            finish();
        });
        onClick();
    }

    private void onClick() {
        binding.tvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.tvTeam1.setTextColor(getResources().getColor(R.color.dark_black));
                    binding.tvTeam2.setTextColor(getResources().getColor(R.color.colorAccent));
                    binding.tvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                    binding.tvTeam2.setBackgroundResource(R.drawable.shape_right_round);
                    mTeamPlayersRes.clear();
                    mTeamPlayersRes.addAll(team1List);
                    team1Id = teamScheduleRes.getTeam1Id();
                    //
                    mConcededById = team2Id;
                    mScoredByTeamId = team1Id;
                    //
                    mTeam1Adapter.notifyDataSetChanged();
                    binding.rvSelectPlayer.setAdapter(mTeam1Adapter);
                    binding.rvSelectPlayer2.setAdapter(mPlayerOutAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        //
        binding.tvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.tvTeam1.setTextColor(getResources().getColor(R.color.colorAccent));
                    binding.tvTeam2.setTextColor(getResources().getColor(R.color.dark_black));
                    //
                    binding.tvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                    binding.tvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
                    //
                    mTeamPlayersRes.clear();
                    mTeamPlayersRes.addAll(team2List);
                    mTeam2Adapter.notifyDataSetChanged();
                    binding.rvSelectPlayer.setAdapter(mTeam2Adapter);
                    binding.rvSelectPlayer2.setAdapter(mPlayerOutAdapter);
                    //
                    team2Id = teamScheduleRes.getTeam2Id();
                    //
                    mConcededById = team1Id;
                    mScoredByTeamId = team2Id;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        binding.tvTeam1.performClick();
    }

    @Override
    public void onListItemClick(Object object) {
        TeamPlaying11 playingList = (TeamPlaying11) object;
        playerId = playingList.getPlayerId();
    }

    public void onDoneClick(View view) {
        String time = binding.tvTime1Countdown.getText().toString().trim();
//        time = time.length() < 2 ? time : time.substring(0, 2);
//        if (time.charAt(0)=='0'){
//            time=time.substring(1, 2);
//        }
        Log.d("SubstitutionActivity", time);

        UpdateMatchEventReq updateMatchEventReq = new UpdateMatchEventReq();
        updateMatchEventReq.scheduleId = scheduleId;
        updateMatchEventReq.leagueId = mLeagueId;
        updateMatchEventReq.eventType = "Substitution";
        updateMatchEventReq.eventTime = time;
        updateMatchEventReq.playerIn = playerId;
        updateMatchEventReq.playerOut = playerOutId;
        updateMatchEventReq.teamId = team1Id;

        refereeViewModel.updateMatchEvents(updateMatchEventReq);
    }

    ListItemClickCallback playerOutListener = new ListItemClickCallback() {
        @Override
        public void onListItemClick(Object object) {
            TeamPlaying11 playingList = (TeamPlaying11) object;
            playerOutId = playingList.getPlayerId();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceHandler.readLong(this, PreferenceHandler.PREF_KEY_TIME, 0) == 0) {
            resumeTime = 0;
        } else
            resumeTime = System.currentTimeMillis() - PreferenceHandler.readLong(this, PreferenceHandler.PREF_KEY_TIME, 0);
        startTimer();
    }


    void startTimer() {
        mStartTime = SystemClock.elapsedRealtime() - resumeTime;
        binding.tvTime1Countdown.setBase(mStartTime);
        binding.tvTime1Countdown.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }
}
