package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context context;
    private List<NotificationResponse> responses;
    private int orientation;
    ListItemMultipleCallback multipleCallback;
//    private LanguageInterface languageInterface;


    public NotificationAdapter(Context context, ArrayList<NotificationResponse> responses,  ListItemMultipleCallback multipleCallback) {
        this.context = context;
        this.responses = responses;
        this.orientation = orientation;
        this.multipleCallback = multipleCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_notification_adapter, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            NotificationResponse model = responses.get(position);

            /*Cases for add team to league*/


            if (model.type.equals("managerInvite")) {
                holder.tvName.setText(model.leagueName);
                Glide.with(context).load("").apply(new RequestOptions().placeholder(R.drawable.ic_manager)
                        .centerCrop()).into(holder.img);
                // holder.tvNotificationText.setText("Invitation to join "+ model.leagueName+" league.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }

            if (model.type.equals("inviteTeam")) {
                holder.tvName.setText(model.leagueName);
                Glide.with(context).load(model.teamImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_league)
                        .centerCrop()).into(holder.img);
               // holder.tvNotificationText.setText("Invitation to join "+ model.leagueName+" league.");
                holder.btnReject.setVisibility(View.VISIBLE);
                holder.btnAccept.setVisibility(View.VISIBLE);
            } if (model.type.equals("joinLeague")) {
                holder.tvName.setText(model.teamName);
                Glide.with(context).load(model.teamImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                        .centerCrop()).into(holder.img);
              //  holder.tvNotificationText.setText(model.teamName +" joined your league.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            } if (model.type.equals("rejectLeagueInvite")) {
                holder.tvName.setText(model.teamName);
                Glide.with(context).load(model.teamImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                        .centerCrop()).into(holder.img);
              //  holder.tvNotificationText.setText(model.teamName +" refused to join your league.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }

            if (model.type.equals("acceptLeagueInvite")) {
                holder.tvName.setText(model.teamName);
                Glide.with(context).load(model.teamImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                        .centerCrop()).into(holder.img);
                //  holder.tvNotificationText.setText(model.teamName +" refused to join your league.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }




            /*Cases for add player to team*/
            if (model.type.equals("invitePlayer")) {
                holder.tvName.setText(model.teamName);
                Glide.with(context).load(model.teamImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_team)
                        .centerCrop()).into(holder.img);
             //   holder.tvNotificationText.setText("Invitation to join "+ model.teamName+" team.");
                holder.btnReject.setVisibility(View.VISIBLE);
                holder.btnAccept.setVisibility(View.VISIBLE);
            }if (model.type.equals("joinTeam")) {
                holder.tvName.setText(model.firstName+" "+model.lastName);
                Glide.with(context).load(model.playerImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_player)
                        .centerCrop()).into(holder.img);
               // holder.tvNotificationText.setText(model.firstName+" "+model.lastName+" joined your team.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }if (model.type.equals("rejectTeamInvite")) {
                holder.tvName.setText(model.firstName+" "+model.lastName);
                Glide.with(context).load(model.playerImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_player)
                        .centerCrop()).into(holder.img);
               // holder.tvNotificationText.setText(model.firstName+" "+model.lastName+" refused to join your team.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }
            if (model.type.equals("acceptTeamInvite")) {
                holder.tvName.setText(model.firstName+" "+model.lastName);
                Glide.with(context).load(model.playerImageUrl).apply(new RequestOptions().placeholder(R.drawable.ic_player)
                        .centerCrop()).into(holder.img);
                // holder.tvNotificationText.setText(model.firstName+" "+model.lastName+" refused to join your team.");
                holder.btnReject.setVisibility(View.GONE);
                holder.btnAccept.setVisibility(View.GONE);
            }

            holder.tvNotificationText.setText(model.message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvName, tvNotificationText;
        Button btnAccept, btnReject;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imgNotification);
            tvName = (TextView) itemView.findViewById(R.id.tvNotificationName);
            tvNotificationText = (TextView) itemView.findViewById(R.id.tvNotificationText);
            btnAccept = (Button) itemView.findViewById(R.id.btnNotificationAccept);
            btnReject = (Button) itemView.findViewById(R.id.btnNotificationReject);

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NotificationResponse response=responses.get(getAdapterPosition());
                    multipleCallback.onLeagueListItemClick(response);
                }
            });
            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NotificationResponse response=responses.get(getAdapterPosition());
                    multipleCallback.onListItemClick(response);
                }
            });
        }
    }
}
