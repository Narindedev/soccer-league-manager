package com.apnitor.football.league.api.response;

import java.util.ArrayList;

public class GetNotificationRes {
    private ArrayList<NotificationResponse> notifications;

    public ArrayList<NotificationResponse> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<NotificationResponse> notifications) {
        this.notifications = notifications;
    }
}
