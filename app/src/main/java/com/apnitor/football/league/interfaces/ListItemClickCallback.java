package com.apnitor.football.league.interfaces;

public interface ListItemClickCallback {

    void onListItemClick(Object object);

}
