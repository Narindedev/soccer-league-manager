package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectTeamListAdapter;
import com.apnitor.football.league.adapter.TeamFollowAdapter;
import com.apnitor.football.league.adapter.TeamListAdapter;
import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.databinding.ActivityTeamListBinding;
import com.apnitor.football.league.fragment.FollowingFragment;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.interfaces.ListItemMultipleCallback;
import com.apnitor.football.league.viewmodel.FollowingViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamFollowActivity extends BaseActivity implements ListItemClickCallback {

    private ActivityTeamListBinding binding;
    private TeamViewModel teamViewModel;
    private TeamFollowActivity teamListActivity = this;
    ArrayList<GetTeamSearchRes> teamsList = new ArrayList<>();
    private String followMessage;
    private  FollowingViewModel followingViewModel;
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_team_list);
        teamViewModel = getViewModel(TeamViewModel.class);
        followingViewModel = getViewModel(FollowingViewModel.class);
        setupToolbar(binding.toolbar, "Follow Teams");
        setupRecyclerView();
        observeApiResponse();

        binding.searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String teamName = binding.teamNameEt.getText().toString();
                String teamCity = binding.cityEt.getText().toString();
                String teamZipCode = binding.zipCodeEt.getText().toString();

                if (teamName.isEmpty()) {
                    showToast("Please enter player name.");
                } else {
                    hideKeyboard(TeamFollowActivity.this);
                    teamViewModel.getAllTeams(new GetAllTeamReq(teamName, teamCity, teamZipCode));
                }

            }
        });
    }

    private void observeApiResponse() {
        followingViewModel.followTeamRes().observe(this,team->{
            FollowingFragment.isFollowing=true;
            showToast(followMessage);
            finish();
        });

        teamViewModel.getAllTeamsResLiveData().observe(this,
                teams -> {
                    teamsList = teams;
//                    for (GetTeamRes getTeamRes : teamsList) {
//                        for (GetLeagueTeamsRes team : alreadyAddedTeams) {
//                            if (getTeamRes.getTeamId().equals(team.getTeamId())) {
//                                getTeamRes.setSelected(true);
//                            }
//                        }
//                    }
                    binding.rvAllTeams.setAdapter(
                            new TeamFollowAdapter(teamListActivity, teamsList,this)
                    );
                });
      //  teamViewModel.getAllTeams();

    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    @Override
    public void onListItemClick(Object object) {
        GetTeamSearchRes getTeamRes = (GetTeamSearchRes) object;
     /*   Intent intent = new Intent(teamListActivity, TeamProfileActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("team_detail", getTeamRes);
        intent.putExtras(b);
        startActivity(intent);*/
        if (getTeamRes.isFollowing) {
            followMessage="Team Followed";
            followingViewModel.followTeam(new DeleteTeamReq(getTeamRes.getTeamId()));
        } else {
            followMessage="Team Unfollowed";
            followingViewModel.unfollowTeam(new DeleteTeamReq(getTeamRes.getTeamId()));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }

}
