package com.apnitor.football.league.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.fragment.TeamInfoDescFragment;
import com.apnitor.football.league.fragment.TeamInfoFragment;
import com.apnitor.football.league.fragment.TeamProfileSeasonsFragment;
import com.apnitor.football.league.fragment.TeamProfileRosterFragment;
import com.apnitor.football.league.fragment.TeamRosterFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;


public class TeamProfileViewPagerAdapter extends FragmentPagerAdapter {

    private final int NUM_ITEMS = 3;
    private GetTeamDetailRes getTeamDetailRes;
    private Observable mObservers = new FragmentObserver();
    public TeamProfileViewPagerAdapter(FragmentManager fm, GetTeamDetailRes getTeamDetailRes) {
        super(fm);
        this.getTeamDetailRes = getTeamDetailRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TeamProfileSeasonsFragment.newInstance(getTeamDetailRes);
            case 1:
                TeamInfoDescFragment teamInfoFragment=  TeamInfoDescFragment.newInstance(getTeamDetailRes.getTeamModel());

                if (teamInfoFragment instanceof Observer) {
                    mObservers.addObserver((Observer) teamInfoFragment);
                }

                return teamInfoFragment;
            case 2:

                TeamRosterFragment rosterFragment=TeamRosterFragment.newInstance(getTeamDetailRes);
                if(rosterFragment instanceof Observer){
                    mObservers.addObserver((Observer)rosterFragment);
                }

                return rosterFragment;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SEASON";
            case 1:
                return "INFO";
            case 2:
                return "ROSTER";
            default:
                return "SEASON";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    public void updateFragments() {
        mObservers.notifyObservers();
    }
}

