package com.apnitor.football.league.api.response;

import com.apnitor.football.league.api.request.AddScheduleLeagueReq;

import java.util.List;

public class LeagueScheduleResultRes {
    private boolean isMoreAvailable;
    private List<ScheduleResultRes> scheduleList;

    public boolean isMoreAvailable() {
        return isMoreAvailable;
    }

    public void setMoreAvailable(boolean moreAvailable) {
        isMoreAvailable = moreAvailable;
    }

    public List<ScheduleResultRes> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<ScheduleResultRes> scheduleList) {
        this.scheduleList = scheduleList;
    }
}
