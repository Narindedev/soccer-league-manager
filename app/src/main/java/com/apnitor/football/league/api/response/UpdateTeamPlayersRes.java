package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateTeamPlayersRes {

    @Expose
    @SerializedName("teamId")
    private String teamId;

    public UpdateTeamPlayersRes(String teamId) {
        this.teamId = teamId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
