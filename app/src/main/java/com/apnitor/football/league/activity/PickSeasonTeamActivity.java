package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.PickTeamListAdapter;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.databinding.ActivitySelectLeagueTeamsBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.apnitor.football.league.viewmodel.TeamViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PickSeasonTeamActivity extends BaseActivity implements ListItemClickCallback {

    ArrayList<String> listToSend = new ArrayList<>();
    UpdateLeagueTeamReq UpdateLeagueTeamReq;
    LeagueViewModel leagueViewModel;
    ArrayList<GetTeamRes> alreadyAddedTeams = new ArrayList<>();
    LeagueTeam leagueTeam = new LeagueTeam();
    String divisionName, seasonName, mSeasonId, mDivisionId, mTeamId;
    GetTeamRes mGetRsult;
    int team;
    private ActivitySelectLeagueTeamsBinding binding;
    private TeamViewModel teamViewModel;
    private PickSeasonTeamActivity teamListActivity = this;
    private String leagueId;
    private TeamViewModel GetTeamRes;
    private List<GetTeamRes> seasonsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_select_league_teams);

        teamViewModel = getViewModel(TeamViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            try {
                leagueId = i.getExtras().getString("leagueId").trim();
                team = i.getExtras().getInt("TEAM");
                mSeasonId = i.getExtras().getString("seasonId").trim();
                mDivisionId = i.getExtras().getString("divisionId").trim();
                mTeamId = i.getExtras().getString("teamId").trim();
                alreadyAddedTeams = (ArrayList<GetTeamRes>) i.getExtras().getSerializable("alreadyAddedTeams");
                //seasonName = i.getExtras().getString("seasonName").trim();
                //   divisionName = i.getExtras().getString("divisionName");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        UpdateLeagueTeamReq = new UpdateLeagueTeamReq(leagueId, mSeasonId, mDivisionId, listToSend);
        if (team == 1)
            setupToolbar(binding.toolbar, "Select Team 1");
        else
            setupToolbar(binding.toolbar, "Select Team 2");
        setupRecyclerView();
        observeApiResponse();
        binding.saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick(v);
            }
        });
    }

    private void observeApiResponse() {
        teamViewModel.getLeagueTeamsLiveData().observe(this,
                teams -> {
                    try {
                        leagueTeam = teams;
                        if (leagueTeam != null) {

                            List<GetTeamRes> teamsList = leagueTeam.getTeamIds();

                            if (teamsList != null && teamsList.size() > 0) {
                                binding.saveBtn.setVisibility(View.VISIBLE);
                                for (GetTeamRes getTeamRes : teamsList) {

                                    if (getTeamRes.getTeamId().equals(mTeamId)) {
                                        teamsList.remove(getTeamRes);
                                        break;
                                    }
                                }
                                binding.rvAllTeams.setAdapter(
                                        new PickTeamListAdapter(teamListActivity, teamsList, "Schedule", this)
                                );
                            } else {
                                binding.saveBtn.setVisibility(View.GONE);
                            }
                        }else{
                            binding.saveBtn.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
        //
        teamViewModel.getLeagueTeams(new GetLeagueTeamsReq(leagueId, mSeasonId,
                mDivisionId));
        //
        leagueViewModel.getUpdateLeagueTeamResLiveData().observe(this,
                res -> {
                    finish();
                });
    }

    private void setupRecyclerView() {
        binding.rvAllTeams.setLayoutManager(
                new LinearLayoutManager(teamListActivity, RecyclerView.VERTICAL, false)
        );
    }

    public void onBackClick(View view) {
        finishStartActivity(HomeActivity.class);
    }

    public void onSaveClick(View view) {
        int resultCode = 111;
        if (team == 1) {
            resultCode = 111;
        } else if (team == 2) {
            resultCode = 112;
        }
        Intent i = new Intent(this, AddScheduleActivity.class);
        i.putExtra("name", mGetRsult);
        setResult(resultCode, i);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


    @Override
    public void onListItemClick(Object object) {
        Log.e("onListItemClick", "==" + object.toString());
        mGetRsult = ((GetTeamRes) object);
        mGetRsult.getTeamName();

        int resultCode = 111;
        if (team == 1) {
            resultCode = 111;
        } else if (team == 2) {
            resultCode = 112;
        }
//        Intent i = new Intent(this, AddScheduleActivity.class);
//        i.putExtra("name", mGetRsult);
//        setResult(resultCode, i);
//        finish();

    }
    //h

}
