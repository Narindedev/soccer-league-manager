package com.apnitor.football.league.api.request;

public class GetLeagueScheduleReq {


    private String leagueId;
    String seasonId;
    String divisionId;
    String date;

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    String scheduleId;

    public GetLeagueScheduleReq(String leagueId, String seasonId, String divisionId, String date) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
        this.date = date;
    }

    public GetLeagueScheduleReq(String leagueId, String seasonId, String divisionId) {
        this.leagueId = leagueId;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
    }

    public GetLeagueScheduleReq(String scheduleId){
        this.scheduleId=scheduleId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
