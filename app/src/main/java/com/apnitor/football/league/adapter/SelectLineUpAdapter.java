package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class SelectLineUpAdapter extends RecyclerView.Adapter<SelectLineUpAdapter.ViewHolder> {

    private Context context;
    private List<TeamPlaying11> teamList;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    String mType;

    public SelectLineUpAdapter(Context context, ArrayList<TeamPlaying11> teamList, String type) {
        this.context = context;
        this.teamList = teamList;
        mType = type;
        viewPool = new RecyclerView.RecycledViewPool();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_select_line_up, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            TeamPlaying11 teamPlaying11 = teamList.get(position);
            holder.mTvPlayer1.setText(teamPlaying11.getFirstName()+" "+teamPlaying11.getLastName());
            holder.mTvTShirtPlayer1.setText(teamPlaying11.getJerseyNumber());
            if (teamPlaying11.getImageUrl() != null)
                Glide.with(context).load(teamPlaying11.getImageUrl()).into(holder.mIvUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mIvUser;
        TextView mTvTShirtPlayer1, mTvPlayer1;
        Button mBtnAccept, mBtnReject;

        ViewHolder(View itemView) {
            super(itemView);
            mTvPlayer1 = itemView.findViewById(R.id.player_1_name);
            mBtnAccept = itemView.findViewById(R.id.acceptBtn);
            mBtnReject = itemView.findViewById(R.id.rejectBtn);
            //
            mTvTShirtPlayer1 = itemView.findViewById(R.id.tshirt_player_1);
            //
            mIvUser = itemView.findViewById(R.id.iv_user);
        }
    }
}
