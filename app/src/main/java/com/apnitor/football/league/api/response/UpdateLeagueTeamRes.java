package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateLeagueTeamRes {

    @Expose
    @SerializedName("leagueId")
    private String leagueId;

    public UpdateLeagueTeamRes(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
