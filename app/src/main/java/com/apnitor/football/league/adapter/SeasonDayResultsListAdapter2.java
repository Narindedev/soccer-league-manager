package com.apnitor.football.league.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.MatchDetailsActivity;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.ScheduleResultRes;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class SeasonDayResultsListAdapter2 extends RecyclerView.Adapter<SeasonDayResultsListAdapter2.ViewHolder> {

    private Context context;
    private List<String> teamList;
    ScheduleResultRes leagueScheduleList;
    String LOG_TAG = "SeasonDayResultsListAdapter";
    int headerPosition;
    GetLeagueRes mGetLeagueRes;

    public SeasonDayResultsListAdapter2(Context context, ArrayList<String> teamList, ScheduleResultRes leagueScheduleList, int headerPosition, GetLeagueRes mGetLeagueRes) {
        this.context = context;
        this.teamList = teamList;
        this.leagueScheduleList = leagueScheduleList;
        this.headerPosition = headerPosition;
        this.mGetLeagueRes=mGetLeagueRes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_season_day_results, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            if (leagueScheduleList != null) {
                ScheduleResultRes model = leagueScheduleList;
                holder.tvTeamName1.setText(model.getTeam1Detail().teamName);
                holder.tvTeamName2.setText(model.getTeam2Detail().teamName);
                if (model.getTeam1Score() != null)
                    holder.tvLeftTeamScore.setText(model.getTeam1Score() + "");
                if (model.getTeam2Score() != null)
                    holder.tvRightTeamScore.setText(model.getTeam2Score() + "");




            Glide.with(context).load(model.getTeam1Detail().imageUrl).apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeam1Image);

            Glide.with(context).load(model.getTeam2Detail().imageUrl).apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeam2Image);
            }
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return 1;
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName1;
        TextView tvTeamName2;
        TextView tvVenue, tvLeftTeamScore, tvRightTeamScore;
        ImageView ivTeam1Image,ivTeam2Image;
        ViewHolder(View itemView) {
            super(itemView);
            ivTeam1Image = (ImageView) itemView.findViewById(R.id.ivLeftTeam);
            ivTeam2Image = (ImageView) itemView.findViewById(R.id.ivRightTeam);
            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_team_1);
            tvLeftTeamScore = (TextView) itemView.findViewById(R.id.tvLeftTeamScore);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_team_2);
            tvRightTeamScore = (TextView) itemView.findViewById(R.id.tvRightTeamScore);
            tvVenue = (TextView) itemView.findViewById(R.id.tvVenue);
            //
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                    //
                    GetTeamScheduleRes model = new GetTeamScheduleRes();
                    model.setLeagueId(mGetLeagueRes.getLeagueId());
                    model.setScheduleId(leagueScheduleList.get_id());
                    model.setRefereeId(leagueScheduleList.getRefereeId());
                    model.setTeam1Name(leagueScheduleList.getTeam1Detail().teamName);
                    model.setTeam2Name(leagueScheduleList.getTeam2Detail().teamName);
                    model.setLocation(leagueScheduleList.getLocation());
                    //
                    Intent intent = new Intent(context, MatchDetailsActivity.class);
                    Bundle b = new Bundle();
                    b.putSerializable("match_detail", model);
                    intent.putExtras(b);
//                    context.startActivity(intent);
//                    ((Activity) context).overridePendingTransition(R.anim.anim_left_in,
//                            R.anim.anim_left_out);
                }
            });
        }
    }
}
