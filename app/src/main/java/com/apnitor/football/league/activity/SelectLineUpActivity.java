package com.apnitor.football.league.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.SelectFormationAdapter;
import com.apnitor.football.league.api.request.AddLeagueRosterReq;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.databinding.ActivitySelectLineUpBinding;
import com.apnitor.football.league.util.views.DraggableViewMain;
import com.apnitor.football.league.util.views.OnViewSelection;
import com.apnitor.football.league.viewmodel.AddScheduleViewModel;
import com.apnitor.football.league.viewmodel.LeagueViewModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.apnitor.football.league.fragment.LeagueSchedulesFragment.mGetLeagueRes;

public class SelectLineUpActivity extends BaseActivity implements SelectFormationAdapter.ItemClickListener, OnViewSelection {

    String LOG_TAG = "SelectLineUpActivity";
    private ActivitySelectLineUpBinding binding;
    SelectFormationAdapter mSelectFormationAdapter;
    private List<String> mFormationsList = new ArrayList<>();
    LinearLayout mLl1, mLl2, mLl3, mLl4;
    int[] ids = new int[]{R.id.rl_1, R.id.rl_2, R.id.rl_3, R.id.rl_4, R.id.rl_5};
    int[] textIds = new int[]{R.id.tvPlayer1, R.id.tvPlayer2, R.id.tvPlayer3, R.id.tvPlayer4, R.id.tvPlayer5};
    LinearLayout[] mLl;
    private DraggableViewMain draggableViewMain;
    ArrayList<DraggableViewMain> mDraggableViewList = new ArrayList<>();
    AddScheduleViewModel addScheduleViewModel;
    final String MATCH_DETAIL = "match_detail";
    String mTeamId = "";
    String TEAM_TYPE = "";
    GetTeamScheduleRes mGetMatchDetail;
    ArrayList<TeamPlaying11> mTeamsList = new ArrayList<>();
    ArrayList<TeamPlaying11> mNotPlayingPlayers = new ArrayList<>();
    ArrayList<PlayersData> mTeam1SelectedPlayers = new ArrayList<>();
    String mFormation = "";
    Button mSaveBtn, mRejectedPlayers;
    private LeagueViewModel leagueViewModel;
    HorizontalScrollView scrollView;
    ArrayList<ImageView> mAllImages = new ArrayList<>();
    ArrayList<TextView> mAllTexts = new ArrayList<>();
    LinearLayout topLinearLayout;
    int rejectedPlayerCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_select_line_up);
        setupToolbar(binding.toolbar, "Select Players");
        //
        addScheduleViewModel = getViewModel(AddScheduleViewModel.class);
        leagueViewModel = getViewModel(LeagueViewModel.class);
        //
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            if (intent.getExtras() != null && intent.getExtras().getSerializable(MATCH_DETAIL) != null) {
                mTeamId = intent.getStringExtra("TEAM_ID");
                TEAM_TYPE = intent.getStringExtra("TEAM_TYPE");
                mGetMatchDetail = (GetTeamScheduleRes) intent
                        .getExtras()
                        .getSerializable(MATCH_DETAIL);
            }
        }
        //
        addFormationsList();
        //
        binding.tvSelectDivisionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFormationDialog();
            }
        });
        binding.tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(SelectLineUpActivity.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Confirm Clear");
                alertBuilder.setMessage("Are you sure you want to clear this Formation?");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        addPlayersView();
             //
                        removeAllViews();

                    }
                });

                alertBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();
                //
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.greenColor));
                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.redCardColor));

            }
        });
        //
        setUpLayout();
        //
        setUpDragView();
        //
        hideAll();
        //
        observeApiResponse();
    }

    private void setUpLayout() {
        mLl1 = findViewById(R.id.ll_1);
        mLl2 = findViewById(R.id.ll_2);
        mLl3 = findViewById(R.id.ll_3);
        mLl4 = findViewById(R.id.ll_4);
        //
        mLl = new LinearLayout[]{mLl1, mLl2, mLl3, mLl4};
        //
        mSaveBtn = findViewById(R.id.saveBtn);





        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiCall();
            }
        });
        //
        mRejectedPlayers = findViewById(R.id.rejectBtn);
        mRejectedPlayers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetTeamScheduleRes model = mGetMatchDetail;
                Intent intent = new Intent(SelectLineUpActivity.this, RejectedPlayersActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("match_detail", model);
                intent.putExtras(b);
                intent.putExtra("TEAM_ID", mTeamId);
                if (mTeamId.equalsIgnoreCase(model.getTeam1Id())) {
                    intent.putExtra("TEAM_TYPE", "ONE");
                } else {
                    intent.putExtra("TEAM_TYPE", "TWO");
                }
                startActivity(intent);
                //
                overridePendingTransition(R.anim.anim_left_in,
                        R.anim.anim_left_out);
            }
        });
    }

    private void addFormationsList() {
        mFormationsList.add("4 - 4 - 2");
        mFormationsList.add("4 - 3 - 3");
        mFormationsList.add("3 - 4 - 3");
        mFormationsList.add("4 - 3 - 3");
        mFormationsList.add("3 - 5 - 2");
    }

    private void addPlayersView() {
        try {


            //
            scrollView = (HorizontalScrollView) findViewById(R.id.scrollView1);
            topLinearLayout = new LinearLayout(this);
            topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            topLinearLayout.setGravity(Gravity.CENTER_VERTICAL);

            for (int i = 0; i < mTeamsList.size(); i++) {
                final CircleImageView imageView = new CircleImageView(this);
                LinearLayout.LayoutParams vp =
                        new LinearLayout.LayoutParams(100,
                                100);
                vp.leftMargin = 40;
                imageView.setLayoutParams(vp);
                imageView.setId(i);
                if (mTeamsList.get(i).getImageUrl() != null)
                    Glide.with(this).load(mTeamsList.get(i).getImageUrl()).into(imageView);
                else
                    imageView.setImageResource(R.drawable.ic_person);

                topLinearLayout.addView(imageView);
                //
                draggableViewMain.addView(imageView);
                //
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e("Tag", "" + imageView.getTag());
                    }
                });
            }
            scrollView.addView(topLinearLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAll() {
        mLl1.setVisibility(View.GONE);
        mLl2.setVisibility(View.GONE);
        mLl3.setVisibility(View.GONE);
        mLl4.setVisibility(View.GONE);
    }

    private void showAll() {
        for (int i = 0; i < mLl.length; i++) {
            mLl[i].setVisibility(View.VISIBLE);
            for (int j = 0; j < ids.length; j++) {
                mLl[i].findViewById(ids[j]).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
//        Log.i("TAG", "You clicked number " + mSelectFormationAdapter.getItem(position) + ", which is at cell position " + position);
    }

    private void showFormationDialog() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select Formation");
        builderSingle.setCancelable(false);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for (int i = 0; i < mFormationsList.size(); i++) {
            arrayAdapter.add(mFormationsList.get(i));
        }
        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFormation = arrayAdapter.getItem(which);
                binding.tvSelectDivisionBtn.setText(mFormation);
                showAll();
                String[] formation = mFormation.split(Pattern.quote("-"));
                setFormationRow1(formation[0].trim());
                setFormationRow2(formation[1].trim());
                setFormationRow3(formation[2].trim());
            }
        });
        builderSingle.show();
    }


    void setFormationRow1(String rowOne) {
        hideViews(mLl3, Integer.parseInt(rowOne));
    }

    void setFormationRow2(String rowTwo) {
        hideViews(mLl2, Integer.parseInt(rowTwo));
    }

    void setFormationRow3(String rowThree) {
        hideViews(mLl1, Integer.parseInt(rowThree));
    }

    void setGoalKeeperUI() {
        mLl4.findViewById(R.id.rl_1).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_2).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_4).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_5).setVisibility(View.GONE);
    }

    void hideViews(LinearLayout ll, int count) {
        switch (count) {
            case 2:
                ll.findViewById(R.id.rl_3).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_4).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 3:
                ll.findViewById(R.id.rl_4).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 4:
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 5:
                break;

        }
        setGoalKeeperUI();
    }

    void setUpDragView() {
        for (int i = 0; i < mLl.length; i++) {
            for (int j = 0; j < ids.length; j++) {
                ViewGroup destiViewGroup = mLl[i].findViewById(ids[j]);
                destiViewGroup.setTag(i + "-" + j);
                draggableViewMain = new DraggableViewMain(this, destiViewGroup);
                mDraggableViewList.add(draggableViewMain);
            }
        }
    }

    @Override
    public void viewSelectedPosition(int position, Object tag) {
        String rowColumn[] = tag.toString().split(Pattern.quote("-"));
        mTeam1SelectedPlayers.get(position).setSelected(true);
        mTeam1SelectedPlayers.get(position).setRow(rowColumn[0].trim());
        mTeam1SelectedPlayers.get(position).setColumn(rowColumn[1].trim());
        setRowColumnViews(rowColumn[0].trim(), rowColumn[1].trim(), mTeamsList.get(position).getImageUrl(), mTeamsList.get(position).getFirstName() + " " + mTeamsList.get(position).getLastName());
    }

//    void onDoneClick(View view) {
//        setPlayersList();
//    }

    void apiCall() {
        if (mTeamsList.size() <= 0)
            return;
        // Team 1 Players
        ArrayList<TeamPlaying11> team = new ArrayList<>();
        for (int i = 0; i < mTeam1SelectedPlayers.size(); i++) {
            if (mTeam1SelectedPlayers.get(i).isSelected()) {
                team.add(new TeamPlaying11(mTeamsList.get(i).getPlayerId(), mTeamsList.get(i).getFirstName(), mTeamsList.get(i).getLastName(), "10", "accept", mTeam1SelectedPlayers.get(i).getRow(), mTeam1SelectedPlayers.get(i).getColumn(), true, mTeamsList.get(i).getImageUrl()));
            } else {
                team.add(new TeamPlaying11(mTeamsList.get(i).getPlayerId(), mTeamsList.get(i).getFirstName(), mTeamsList.get(i).getLastName(), "10", "accept", mTeam1SelectedPlayers.get(i).getRow(), mTeam1SelectedPlayers.get(i).getColumn(), false, mTeamsList.get(i).getImageUrl()));
            }
        }
        AddPlayerToTeamReq addPlayerToTeamReq = new AddPlayerToTeamReq();
        addPlayerToTeamReq.setLeagueId(mGetMatchDetail.getLeagueId());
        addPlayerToTeamReq.setDivisionId(mGetMatchDetail.getDivisionId());
        addPlayerToTeamReq.setSeasonId(mGetMatchDetail.getSeasonId());
        addPlayerToTeamReq.setScheduleId(mGetMatchDetail.getScheduleId());
        if (TEAM_TYPE.equalsIgnoreCase("ONE")) {
            addPlayerToTeamReq.setTeam1Formation(mFormation);
            addPlayerToTeamReq.setTeam1Playing11(team);
        } else {
            addPlayerToTeamReq.setTeam2Formation(mFormation);
            addPlayerToTeamReq.setTeam2Playing11(team);
        }
        Log.d(LOG_TAG, "API Payload is " + addPlayerToTeamReq.toString());

        addScheduleViewModel.updateScheduleTeam(addPlayerToTeamReq);
        addScheduleViewModel.updateScheduleLiveData().
                observe(this, addScheduleRes -> {
                    finish();
                });
    }

    private void observeApiResponse() {
        String prevFormation = "";
        if (TEAM_TYPE.equalsIgnoreCase("one")) {
            prevFormation = mGetMatchDetail.getTeam1Formation();
            mTeamsList.clear();
            mTeamsList.addAll(mGetMatchDetail.getTeam1Playing11());
        } else {
            prevFormation = mGetMatchDetail.getTeam2Formation();
            mTeamsList.clear();
            mTeamsList.addAll(mGetMatchDetail.getTeam2Playing11());
        }
        //

        filterPlaying11();
        //

        Log.d(LOG_TAG, "Players info is " + mTeamsList.toString());
        //
        for (int i = 0; i < mTeamsList.size(); i++) {
            mTeam1SelectedPlayers.add(i, new PlayersData(mTeamsList.get(i).getRow(), mTeamsList.get(i).getColumn(), false));
        }

        if (prevFormation != null && !prevFormation.isEmpty())
            showPrevSelectedFormation(prevFormation);
        else {
            if (mTeamsList.size() > 0) {
                addPlayersView();
                showFormationDialog();
            } else {
                leagueViewModel.getLeagueRoster(new AddLeagueRosterReq(mTeamId, mGetMatchDetail.getLeagueId()));
                leagueViewModel.getLeagueRosterResLiveData().observe(this, res -> {
                    for (int i = 0; i < res.getPlayers().size(); i++) {
                        TeamPlaying11 teamPlaying11 = new TeamPlaying11();
                        //
                        teamPlaying11.setFirstName(res.getPlayers().get(i).getFirstName());
                        teamPlaying11.setLastName(res.getPlayers().get(i).getLastName());
                        teamPlaying11.setImageUrl(res.getPlayers().get(i).getProfilePhotoUrl());
                        teamPlaying11.setPlayerId(res.getPlayers().get(i).getId());
                        //
                        mTeamsList.add(teamPlaying11);
                        mTeam1SelectedPlayers.add(i, new PlayersData("-1", "-1", false));
                    }
                    if (mTeamsList.size() > 0) {
                        addPlayersView();
                        showFormationDialog();
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }


    public class PlayersData {
        String row;
        String column;
        boolean isSelected;

        public PlayersData(String row, String column, boolean isSelected) {
            this.row = row;
            this.column = column;
            this.isSelected = isSelected;
        }

        public String getRow() {
            return row;
        }

        public void setRow(String row) {
            this.row = row;
        }

        public String getColumn() {
            return column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }

    void showPrevSelectedFormation(String mFormation) {
        binding.tvSelectDivisionBtn.setText(mFormation);
        showAll();
        String[] formation = mFormation.split(Pattern.quote("-"));
        setFormationRow1(formation[0].trim());
        setFormationRow2(formation[1].trim());
        setFormationRow3(formation[2].trim());
        //
        mAllTexts.clear();
        mAllImages.clear();
        for (int i = 0; i < mTeamsList.size(); i++) {
            if (mTeamsList.get(i).getPlaying())
                setRowColumnViews(mTeamsList.get(i).getRow(), mTeamsList.get(i).getColumn(), mTeamsList.get(i).getImageUrl(), mTeamsList.get(i).getFirstName() + " " + mTeamsList.get(i).getLastName());
            else
                mNotPlayingPlayers.add(mTeamsList.get(i));
        }

        if (mNotPlayingPlayers.size()>0){
            showNotPlaying();
        }
    }

    public void setRowColumnViews(String row, String column, String imageUrl, String name) {

        if (row.equalsIgnoreCase("-1") || column.equalsIgnoreCase("-1")) {
            return;
        }

        LinearLayout Ll = null;
        int mTv = -1;
        int Iv = -1;

        switch (row) {
            case "0":
                Ll = mLl1;
                break;
            case "1":
                Ll = mLl2;
                break;
            case "2":
                Ll = mLl3;
                break;
            case "3":
                Ll = mLl4;
                break;
        }

        switch (column) {
            case "0":
                mTv = R.id.tvPlayer1;
                break;
            case "1":
                mTv = R.id.tvPlayer2;
                break;
            case "2":
                mTv = R.id.tvPlayer3;
                break;
            case "3":
                mTv = R.id.tvPlayer4;
                break;
            case "4":
                mTv = R.id.tvPlayer5;
                break;
        }

        switch (column) {
            case "0":
                Iv = R.id.ivPlayer1;
                break;
            case "1":
                Iv = R.id.ivPlayer2;
                break;
            case "2":
                Iv = R.id.ivPlayer3;
                break;
            case "3":
                Iv = R.id.ivPlayer4;
                break;
            case "4":
                Iv = R.id.ivPlayer5;
                break;
        }

        ((TextView) Ll.findViewById(mTv)).setVisibility(View.VISIBLE);
        ((TextView) Ll.findViewById(mTv)).setText(" " + name + " ");

        //
        ImageView mIv = Ll.findViewById(Iv);
        if (imageUrl != null) {
            Glide.with(this).load(imageUrl).into(mIv);
        } else {
            mIv.setImageResource(R.drawable.ic_person);
        }

        //
        mAllTexts.add(((TextView) Ll.findViewById(mTv)));
        mAllImages.add(mIv);
    }


    void removeAllViews() {
        try {
            for (int i = 0; i < mAllImages.size(); i++) {
                Glide.with(this).load(R.drawable.shape_circle).into(mAllImages.get(i));
                mAllTexts.get(i).setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void filterPlaying11() {
        for (int i = 0; i < mTeamsList.size(); i++) {
            if (mTeamsList.get(i).getStatus().equalsIgnoreCase("reject")) {
                mTeamsList.remove(i);
                rejectedPlayerCount++;
            }
        }
        //
        if (rejectedPlayerCount > 0) {
            mRejectedPlayers.setText("View Rejected Players (" + rejectedPlayerCount + ")");
        }
    }


    private void showNotPlaying() {
        try {
            //
            scrollView = (HorizontalScrollView) findViewById(R.id.scrollView1);
            topLinearLayout = new LinearLayout(this);
            topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
            topLinearLayout.setGravity(Gravity.CENTER_VERTICAL);
            for (int i = 0; i < mNotPlayingPlayers.size(); i++) {
                final CircleImageView imageView = new CircleImageView(this);
                LinearLayout.LayoutParams vp =
                        new LinearLayout.LayoutParams(100,
                                100);
                vp.leftMargin = 40;
                imageView.setLayoutParams(vp);
                imageView.setId(i);
                if (mTeamsList.get(i).getImageUrl() != null)
                    Glide.with(this).load(mNotPlayingPlayers.get(i).getImageUrl()).into(imageView);
                else
                    imageView.setImageResource(R.drawable.ic_person);
                //
                topLinearLayout.addView(imageView);
                //
                draggableViewMain.addView(imageView);
            }
            scrollView.addView(topLinearLayout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
