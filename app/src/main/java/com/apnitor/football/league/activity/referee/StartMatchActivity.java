package com.apnitor.football.league.activity.referee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;

public class StartMatchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_match);
    }

    public void onStartMatch(View view) {
        startActivity(new Intent(this, MatchDetailActivity.class));
    }
}
