package com.apnitor.football.league.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DivisionsListAdapter extends RecyclerView.Adapter<DivisionsListAdapter.ViewHolder> {

    private Context context;
    private List<GetDivisionRes> leagueList;
    ListItemClickCallback listItemClickCallback;

    public DivisionsListAdapter(Activity context, List<GetDivisionRes> leagueList, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.leagueList = leagueList;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_vertical_league_item, parent, false);
        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.txtLeagueName.setText(leagueList.get(position).getDivisionName());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return leagueList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtLeagueName;

        ViewHolder(View itemView) {
            super(itemView);
            txtLeagueName = (TextView) itemView.findViewById(R.id.tvLeagueName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(leagueList.get(getLayoutPosition()));
                }
            });
        }
    }
}
