package com.apnitor.football.league.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetUsStateRes {

    @SerializedName("states")
    public ArrayList<StateData> states;

    public class StateData{
        @SerializedName("name")
        public String name;
        @SerializedName("abbreviation")
        public String abbreviation;
    }
}
