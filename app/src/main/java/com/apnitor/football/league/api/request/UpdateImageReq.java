package com.apnitor.football.league.api.request;

public class UpdateImageReq {
    private String id;
    private String imageUrl;
    private Integer type;

    public UpdateImageReq(String id,String imageUrl,Integer type){
        this.id=id;
        this.imageUrl=imageUrl;
        this.type=type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
