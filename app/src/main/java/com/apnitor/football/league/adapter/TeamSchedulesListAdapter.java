package com.apnitor.football.league.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.SelectLineUpActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.activity.ViewPlayerActivity;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.UIUtility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class TeamSchedulesListAdapter extends RecyclerView.Adapter<TeamSchedulesListAdapter.ViewHolder> {

    private Context context;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    String mTeamId;
    List<GetTeamScheduleRes> mGetTeamScheduleRes;


    public TeamSchedulesListAdapter(Context context, List<GetTeamScheduleRes> mGetTeamScheduleRes, ListItemClickCallback listItemClickCallback, String teamId) {
        this.context = context;
        this.listItemClickCallback = listItemClickCallback;
        this.mGetTeamScheduleRes = mGetTeamScheduleRes;
        viewPool = new RecyclerView.RecycledViewPool();
        mTeamId = teamId;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_team_schedule, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    } 9719127111

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {

            GetTeamScheduleRes model = mGetTeamScheduleRes.get(position);
            holder.tvTeamName1.setText(model.getTeam1Name());
            holder.tvTeamName2.setText(model.getTeam2Name());
            holder.tvDate.setText(UIUtility.getFormattedDate(model.getDate()));
            holder.tvVenue.setText(model.getLocation());
            if (mTeamId.equalsIgnoreCase(model.getTeam1Id())) {
                int size = model.getTeam1Playing11().size();
                holder.tvViewPlayers.setText(size > 0 ? "View Starting 11" : "Add Starting XI");
            } else {
                int size = model.getTeam2Playing11().size();
                holder.tvViewPlayers.setText(size > 0 ? "View Starting 11" : "Add Starting XI");
            }
            if (TeamProfileActivity.isTeamOwner) {
                holder.tvViewPlayers.setVisibility(View.VISIBLE);
            } else
                holder.tvViewPlayers.setVisibility(View.GONE);


            Glide.with(context).load("").apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeamImage1);
            Glide.with(context).load("").apply(new RequestOptions()
                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeamImage2);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mGetTeamScheduleRes.size();
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName1;
        TextView tvTeamName2;
        TextView tvDate;
        //  TextView tvTime;
        TextView tvViewPlayers;
        TextView tvVenue;
        ImageView ivTeamImage1;
        ImageView ivTeamImage2;

        ViewHolder(View itemView) {
            super(itemView);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            //  tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvVenue = (TextView) itemView.findViewById(R.id.tv_venue);
            tvViewPlayers = (TextView) itemView.findViewById(R.id.tvViewPlayers);

            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_teamname1);
            ivTeamImage1 = (ImageView) itemView.findViewById(R.id.iv_teamimage1);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_teamname2);
            ivTeamImage2 = (ImageView) itemView.findViewById(R.id.iv_teamimage2);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetTeamScheduleRes model = mGetTeamScheduleRes.get(getAdapterPosition());
                    listItemClickCallback.onListItemClick(model);
                }
            });

            tvViewPlayers.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GetTeamScheduleRes model = mGetTeamScheduleRes.get(getAdapterPosition());
                    Intent intent = new Intent(context, SelectLineUpActivity.class);
                    Bundle b = new Bundle();
                    b.putSerializable("match_detail", model);
                    intent.putExtras(b);
                    intent.putExtra("TEAM_ID", mTeamId);
                    if (mTeamId.equalsIgnoreCase(model.getTeam1Id())) {
                        intent.putExtra("TEAM_TYPE", "ONE");
                    } else {
                        intent.putExtra("TEAM_TYPE", "TWO");
                    }
                    context.startActivity(intent);
                }
            });
        }
    }

    private String getFilteredDate(String date) {
        String filteredDate = "";
        String[] filteredDateArray = date.split(Pattern.quote("-"));
        if (filteredDateArray.length == 0) {
            filteredDateArray = date.split(Pattern.quote("/"));
        }
        switch (filteredDateArray[1]) {
            case "1":
                filteredDate = "Jan, ";
                break;
            case "2":
                filteredDate = "Feb, ";
                break;
            case "3":
                filteredDate = "Mar, ";
                break;
            case "4":
                filteredDate = "Apr, ";
                break;
            case "5":
                filteredDate = "May, ";
                break;
            case "6":
                filteredDate = "Jun, ";
                break;
            case "7":
                filteredDate = "Jul, ";
                break;
            case "8":
                filteredDate = "Aug, ";
                break;
            case "9":
                filteredDate = "Sep, ";
                break;
            case "10":
                filteredDate = "Oct, ";
                break;
            case "11":
                filteredDate = "Nov, ";
                break;
            case "12":
                filteredDate = "Dec, ";
                break;
        }

        filteredDate += filteredDateArray[0];
        return filteredDate;

    }
}
