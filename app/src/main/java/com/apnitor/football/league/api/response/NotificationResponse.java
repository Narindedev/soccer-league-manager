package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationResponse implements Serializable {

    /*Player Res*/
    @Expose
    @SerializedName("firstName")
    public String firstName;

    @Expose
    @SerializedName("lastName")
    public String lastName;

    @Expose
    @SerializedName("playerImageUrl")
    public String playerImageUrl;

    @Expose
    @SerializedName("playerId")
    public String playerId;

    /*Team Res*/
    @Expose
    @SerializedName("teamId")
    public String teamId;

    @Expose
    @SerializedName("teamImageUrl")
    public String teamImageUrl;

    @Expose
    @SerializedName("teamName")
    public String teamName;

    /*League res*/
    @Expose
    @SerializedName("leagueName")
    public String leagueName;

    @Expose
    @SerializedName("seasonId")
    public String seasonId;

    @Expose
    @SerializedName("leagueId")
    public String leagueId;

    @Expose
    @SerializedName("seasonName")
    public String seasonName;

    @Expose
    @SerializedName("divisionName")
    public String divisionName;

    @Expose
    @SerializedName("divisionId")
    public String divisionId;

    @Expose
    @SerializedName("type")
    public String type;

    @Expose
    @SerializedName("message")
    public String message;

    @Expose
    @SerializedName("notificationId")
    public String notificationId;

}
