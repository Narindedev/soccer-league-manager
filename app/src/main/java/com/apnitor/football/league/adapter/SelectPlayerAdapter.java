package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class SelectPlayerAdapter extends RecyclerView.Adapter<SelectPlayerAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
    ArrayList<Boolean> mTeam1SelectedPlayers;
    ArrayList<Boolean> mTeam2SelectedPlayers;
    String mTeamSelected;

    public SelectPlayerAdapter(Context context, ArrayList<PlayerRes> teamList, int orientation, ListItemClickCallback listItemClickCallback, ArrayList<Boolean> mTeam1SelectedPlayers, ArrayList<Boolean> mTeam2SelectedPlayers, String mTeamSelected) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
        this.mTeam1SelectedPlayers = mTeam1SelectedPlayers;
        this.mTeam2SelectedPlayers = mTeam2SelectedPlayers;
        this.mTeamSelected = mTeamSelected;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_select_team, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PlayerRes model = teamList.get(position);
            model.isFollowing = false;
            holder.tvTeamName.setText(model.getFirstName() + " " + model.getLastName());
            //
            holder.img.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mTeamSelected.equalsIgnoreCase("1"))
                        mTeam1SelectedPlayers.set(position, isChecked);
                    else mTeam2SelectedPlayers.set(position, isChecked);
                }
            });
            //
            if (mTeamSelected.equalsIgnoreCase("1")) {
                if (mTeam1SelectedPlayers.get(position) == true) {
                    holder.img.setChecked(true);
                } else {
                    holder.img.setChecked(false);
                }
            } else {
                if (mTeam2SelectedPlayers.get(position) == true) {
                    holder.img.setChecked(true);
                } else {
                    holder.img.setChecked(false);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox img;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (CheckBox) itemView.findViewById(R.id.cbLeagueImage);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                    img.performClick();
                }
            });
        }
    }
}
