package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetTeamPlayersRes implements Serializable {

    @Expose
    @SerializedName("_id")
    private String playerId;
    private String email;
    private String city;
    private String countryName;
    private String state;
    private String firstName;
    private String lastName;
    private String phone;



    @Expose
    @SerializedName("imageUrl")
    private String profilePhotoUrl;

    public GetTeamPlayersRes(){

    }

    public GetTeamPlayersRes(String playerId, String email, String city, String countryName, String state, String firstName, String lastName, String phone, String profilePhotoUrl) {
        this.playerId = playerId;
        this.email = email;
        this.city = city;
        this.countryName = countryName;
        this.state = state;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public String getPlayerName() {
        return this.firstName + " " + this.lastName;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }

}

