package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.fragment.LeagueTeamsFragment;
import com.apnitor.football.league.fragment.PlayerInfoFragment;
import com.apnitor.football.league.fragment.PlayerMediaFragment;
import com.apnitor.football.league.fragment.PlayerStatsFragment;
import com.apnitor.football.league.fragment.ProfileLeagueFragment;
import com.apnitor.football.league.fragment.ProfileSeasonsFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PlayerViewPagerAdapter extends FragmentPagerAdapter {

    private final int NUM_ITEMS = 3;
    private PlayerProfileRes playerRes;

    public PlayerViewPagerAdapter(FragmentManager fm, PlayerProfileRes playerRes) {
        super(fm);
        this.playerRes = playerRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return PlayerInfoFragment.newInstance(playerRes);
            case 1:
                return PlayerStatsFragment.newInstance(playerRes);
            case 2:
                return PlayerMediaFragment.newInstance(playerRes);
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "PERSONAL INFO";
            case 1:
                return "STATS";
            case 2:
                return "MEDIA";
            default:
                return "PERSONAL INFO";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
