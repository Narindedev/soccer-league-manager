package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.TeamApi;
import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.GetTeamResultReq;
import com.apnitor.football.league.api.request.InviteTeamReq;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetMyTeamsRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamResultRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.TeamProfileRes;
import com.apnitor.football.league.api.response.UpdateTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class TeamRepository {

    private TeamApi teamApi;

    public TeamRepository(Application application) {
        teamApi = ApiService.getTeamApi(application);
    }

    public Single<BaseRes<ArrayList<GetTeamRes>>> getAllTeams() {
        return teamApi.getAllTeams()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamSearchRes>>> getAllTeams(GetAllTeamReq getAllTeamReq) {
        return teamApi.getAllTeams(getAllTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamSearchRes>>> getAllTeamsSearch(GetAllTeamReq getAllTeamReq) {
        return teamApi.getAllTeamsSearch(getAllTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetMyTeamsRes>>> getMyTeams() {
        return teamApi.getMyTeams()
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<GetTeamDetailRes>> getTeamDetail(GetTeamDetailReq getTeamDetailReq) {
        return teamApi.getTeamDetail(getTeamDetailReq)
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<UpdateTeamRes>> createUpdateTeam(UpdateTeamReq updateTeamReq) {
        return teamApi.updateTeam(updateTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<Object>> deleteTeam(DeleteTeamReq deleteTeamReq) {
        return teamApi.deleteTeam(deleteTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<LeagueTeam>> getLeagueTeams(GetLeagueTeamsReq getLeagueTeamsReq) {
        return teamApi.getLeagueTeams(getLeagueTeamsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<UpdateTeamPlayersRes>> updateTeamPlayers(UpdateTeamPlayersReq updateTeamPlayersReq) {
        return teamApi.updateTeamPlayers(updateTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<List<GetTeamScheduleRes>>> getTeamSchedule(GetTeamDetailReq updateTeamPlayersReq) {
        return teamApi.getTeamSchedule(updateTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetTeamRes>> getInviteTeam(InviteTeamReq inviteTeamReq) {
        return teamApi.inviteTeam(inviteTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetTeamRes>> joinLeague(InviteTeamReq inviteTeamReq) {
        return teamApi.joinLeague(inviteTeamReq)
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<GetTeamRes>> rejectLeagueInvitation(InviteTeamReq inviteTeamReq) {
        return teamApi.rejectLeagueInvitation(inviteTeamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetTeamResultRes>> getTeamResult(GetTeamDetailReq getTeamResultReq) {
        return teamApi.getTeamResult(getTeamResultReq)
                .subscribeOn(Schedulers.io());
    }

}
