package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.ForgotPasswordReq;
import com.apnitor.football.league.api.request.LogInReq;
import com.apnitor.football.league.api.request.NewPasswordReq;
import com.apnitor.football.league.api.request.ResetPasswordReq;
import com.apnitor.football.league.api.request.SignUpReq;
import com.apnitor.football.league.api.request.UpdateProfileReq;
import com.apnitor.football.league.api.request.VrifyOtpReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.ForgotPasswordRes;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.api.response.ResetPasswordRes;
import com.apnitor.football.league.api.response.SignUpRes;
import com.apnitor.football.league.api.response.UpdatePasswordRes;
import com.apnitor.football.league.api.response.UpdateProfileRes;
import com.apnitor.football.league.api.response.VerifyPasswordRes;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ProfileApi {

    @POST("login")
    Single<BaseRes<LogInRes>> logIn(@Body LogInReq logInReq);

    @POST("logout")
    Single<BaseRes<LogInRes>> logOut();

    @POST("signup")
    Single<BaseRes<SignUpRes>> signUp(@Body SignUpReq signUpReq);

    @POST("resetPassword")
    Single<BaseRes<ResetPasswordRes>> resetPassword(@Body ResetPasswordReq resetPasswordReq);

    @POST("updateProfile")
    Single<BaseRes<UpdateProfileRes>> updateProfile(@Body UpdateProfileReq updateProfileReq);

    @POST("resetPassword")
    Single<BaseRes<ForgotPasswordRes>> forgotPassword(@Body ForgotPasswordReq forgotPasswordReq);

    @POST("updatePasswordWithOtp")
    Single<BaseRes<UpdatePasswordRes>> updatePassword(@Body NewPasswordReq newPasswordReq);


    @POST("verifyPhoneOnSignup")
    Single<BaseRes<VerifyPasswordRes>> verifyOnPhone(@Body VrifyOtpReq vrifyOtpReq);

    @POST("verifyEmailOnSignup")
    Single<BaseRes<VerifyPasswordRes>> verifyOnEmail(@Body VrifyOtpReq vrifyOtpReq);
}
