/*
package com.apnitor.football.league.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.myapplication.Interface.SignApi;
import com.myapplication.Model.SignModel;
import com.myapplication.Model.SignRequest;
import com.myapplication.R;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    EditText fristname, lastname, email, password,phone,usetype;
    Button button;
    SignApi signApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fristname = (EditText) findViewById(R.id.fristname);
        lastname = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        phone = (EditText) findViewById(R.id.phone);
        usetype = (EditText) findViewById(R.id.usetype);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               SignRequest signRequest=new SignRequest(email.getText().toString(),
                        fristname.getText().toString(),
                        lastname.getText().toString(),
                        password.getText().toString(),
                        phone.getText().toString(),
                usetype.getText().toString());

                getResponse(signRequest);
            }
        });
    }

    public void getResponse(SignRequest signRequest) {
        String name = fristname.getText().toString().trim();
        String name1=lastname.getText().toString().trim();
        String mail=email.getText().toString().trim();
        String pass=password.getText().toString().trim();

        if (name.isEmpty())
        {
            fristname.setError("Enter your name");
            fristname.requestFocus();
            return;
        }
        if (mail.isEmpty())
        {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches())
        {
            email.setError("Email is  not valid");
            email.requestFocus();
            return;
        }
        if (pass.isEmpty())
        {
            password.setError("Enter password");
            password.requestFocus();
            return;
        }
        if (pass.length()<6)
        {
            password.setError("Password should be Six characters Long");
            password.requestFocus();
            return;
        }

        final OkHttpClient.Builder clientBuilder=new OkHttpClient.Builder();
        clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Log.e("Result","Inside the Intersepter callback");
                Request request=chain.request();
                okhttp3.Response response=chain.proceed(request);
                if (request.method().compareToIgnoreCase("post")==0)
                {
                    Log.e("Result","Response from http "+response.body());

                }
                String bodyString=response.body().string();
                Log.e("Result","Response"+"\n"+bodyString);

                return response.newBuilder().body(ResponseBody.create(response.body().contentType(),bodyString)).build();
            }
        });


        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("https://safe-depths-94053.herokuapp.com/maidPicker/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder.build())
                .build();
        signApi=retrofit.create(SignApi.class);
       */
/* signRequest.setEmail(email.getText().toString());
            signRequest.setFristname(fristname.getText().toString());
            signRequest.setLastname(lastname.getText().toString());
            signRequest.setPassword(password.getText().toString());*//*

        Call<SignModel>call=signApi.createwithfield(signRequest);
        call.enqueue(new Callback<SignModel>() {
            @Override
            public void onResponse(Call<SignModel> call, Response<SignModel> response) {
                */
/*int id=response.code();
                String signModel=response.body().toString();*//*

                //Log.e("Result","Response"+id);
                Log.e("Result","Response "+response);
                Toast.makeText(MainActivity.this, "Response ", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<SignModel> call, Throwable t) {
                  Log.e("Result","Response is failed"+t.getMessage());
                Toast.makeText(MainActivity.this, "Response is failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
*/
