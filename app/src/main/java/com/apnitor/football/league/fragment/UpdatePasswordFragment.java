package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LoginActivity;
import com.apnitor.football.league.databinding.FragmentResetPasswordBinding;
import com.apnitor.football.league.fragment_binding_callback.ResetPasswordFragmentBindingCallback;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.ForgotPasswordViewModel;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public class UpdatePasswordFragment extends BaseFragment implements ResetPasswordFragmentBindingCallback {

    private FragmentResetPasswordBinding binding;
    private LoginActivity loginActivity;
    private ForgotPasswordViewModel forgotPasswordViewModel;
    private  String email="",otp="";
    private String phone="";
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loginActivity = (LoginActivity) context;
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         email = getArguments().getString("email");
       // otp= getArguments().getString("otp");
         phone = getArguments().getString("phone");
        forgotPasswordViewModel = getViewModel(ForgotPasswordViewModel.class);
        forgotPasswordViewModel.getNewPasswordResLiveData().observe(
                this,
                signUpRes ->{
                    Fragment fragment = new LoginFragment();
                    Bundle mBundle = new Bundle();
                    startNewFragment(fragment, mBundle);
                    navController.navigateUp();
                 Toast.makeText(loginActivity, "Your password is reset successfully.", Toast.LENGTH_SHORT).show();
                }
                    );
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentResetPasswordBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        passwordValidations();
        return binding.getRoot();
    }

    @Override
    public void onBack() {
        navController.navigateUp();
    }

    @Override
    public void onResetPassword() {
        String strOtp = UIUtility.getEditTextValue(binding.etOtp);
        String newPassword = UIUtility.getEditTextValue(binding.etNewPassword).trim();
        String confirmPassword = UIUtility.getEditTextValue(binding.etConfirmPassword).trim();
        if (strOtp.isEmpty()||strOtp.length()!=4)
            showErrorOnEditText(binding.etOtp, "Please enter correct OTP");
        else if (newPassword.isEmpty())
            showErrorOnEditText(binding.etNewPassword, "Please enter New Password");
        else  if (confirmPassword.isEmpty())
            showErrorOnEditText(binding.etConfirmPassword, "Please enter Confirm Password");
        else if (!newPassword.equals(confirmPassword)) {
            showErrorOnEditText(binding.etConfirmPassword, "New Password and Confirm password do not match");
        } else {
            forgotPasswordViewModel.newPassword(newPassword, phone, email, strOtp);
        }
        }
    private void startNewFragment(Fragment fragment, Bundle mBundle) {
        fragment.setArguments(mBundle);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.my_nav_host_fragment, fragment);
        fragmentTransaction.commit();
    }
    private void passwordValidations() {
        binding.etNewPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.cardPasswordValid.setVisibility(View.VISIBLE);
                } else {
                    binding.cardPasswordValid.setVisibility(View.GONE);
                }

            }
        });


        binding.etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkCharacters(s.toString());
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkCharacters(String s) {
        if (UIUtility.checkSpcChar(s)) {
            binding.tvPasswordSpcChr.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordSpcChr.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }

        if (UIUtility.checkSmall(s)) {
            binding.tvPasswordSmall.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordSmall.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }

        if (UIUtility.checkCaps(s)) {
            binding.tvPasswordCapital.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        } else {
            binding.tvPasswordCapital.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        }
        if (s.length() < 8) {
            binding.tvPasswordLength.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel, 0, 0, 0);
        } else {
            binding.tvPasswordLength.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
        }


    }
}
