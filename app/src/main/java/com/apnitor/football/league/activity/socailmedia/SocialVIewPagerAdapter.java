package com.apnitor.football.league.activity.socailmedia;

import com.apnitor.football.league.activity.socailmedia.FbSocialMediaFragment;
import com.apnitor.football.league.activity.socailmedia.InstaSocailMediaFragment;
import com.apnitor.football.league.activity.socailmedia.TwitterSocialMediaFragment;
import com.apnitor.football.league.api.request.SocialMedia;

import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class SocialVIewPagerAdapter extends FragmentPagerAdapter {

    private static int NUM_ITEMS = 3;
    SocialMedia socialMedia;
    String url;
    private Observable mObservers = new FragmentObserver();



    public SocialVIewPagerAdapter(FragmentManager fm, SocialMedia socialMedia) {
        super(fm);
        this.socialMedia = socialMedia;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                if(socialMedia!=null){
                    String fburl=socialMedia.facebookLink;
                    if(fburl!=null){
                        url=fburl;
                    }else{
                        url="";
                    }
                }else{
                    url="";
                }


                return FbSocialMediaFragment.newInstance(url);
            case 1: // Fragment # 0 - This will show FirstFragment different title


                if(socialMedia!=null){
                    String twitterUrl=socialMedia.twitterLink;
                    if(twitterUrl!=null){
                        url=twitterUrl;
                    }else{
                        url="";
                    }
                }else{
                    url="";
                }


                return TwitterSocialMediaFragment.newInstance(url);


            // return DivisionListFragment.newInstance(0, "Page # 1", getLeagueRes);



            case 2: // Fragment # 1 - This will show SecondFragment

                if(socialMedia!=null){
                    String instaUrl=socialMedia.instaLink;
                    if(instaUrl!=null){
                        url=instaUrl;
                    }else{
                        url="";
                    }
                }else{
                    url="";
                }


                return InstaSocailMediaFragment.newInstance(url);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void updateFragments(){
        mObservers.notifyObservers();
    }
}
