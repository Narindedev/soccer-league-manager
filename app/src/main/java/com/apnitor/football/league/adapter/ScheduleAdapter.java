package com.apnitor.football.league.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.MatchDetailsActivity;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.PreferenceHandler;
import com.apnitor.football.league.util.UIUtility;

import java.util.List;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.RecyclerView;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ViewHolder> {

    private Context context;
    ListItemClickCallback listItemClickCallback;
    RecyclerView.RecycledViewPool viewPool;
    List<GetTeamScheduleRes> mGetTeamScheduleRes;
    String LOG_TAG="ScheduleAdapter";


    public ScheduleAdapter(Context context, List<GetTeamScheduleRes> mGetTeamScheduleRes, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.mGetTeamScheduleRes = mGetTeamScheduleRes;
        viewPool = new RecyclerView.RecycledViewPool();
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_referee_schedule, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            GetTeamScheduleRes model = mGetTeamScheduleRes.get(position);
            holder.tvTeamName1.setText(model.getTeam1Name());
            holder.tvTeamName2.setText(model.getTeam2Name());
            holder.tvDate.setText(UIUtility.getFormattedDate(model.getDate()));
            holder.tvVenue.setText(model.getLocation());
            // Check for referee's match
//            Log.d(LOG_TAG," Login res "+PreferenceHandler.readString(context, PreferenceHandler.PREF_USER_ID, ""));
//            Log.d(LOG_TAG," Login api "+model.getRefereeId());
            if (model.getRefereeId()!=null && model.getRefereeId().equalsIgnoreCase(PreferenceHandler.readString(context, PreferenceHandler.PREF_USER_ID, ""))) {
                holder.ivReferee.setVisibility(View.VISIBLE);
            } else {
                holder.ivReferee.setVisibility(View.INVISIBLE);
            }


//            Glide.with(getActivity()).load(imageUrl).into(mIv);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mGetTeamScheduleRes.size();
//        return teamList.size();getMatchDetails
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName2;
        TextView tvTeamName1;
        TextView tvDate;
       // TextView tvTime;
        TextView tvVenue;
        ImageView ivTeamImage2,ivTeamImage1, ivReferee;

        ViewHolder(View itemView) {
            super(itemView);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
         //   tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_teamname1);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_teamname2);
            tvVenue = (TextView) itemView.findViewById(R.id.tv_venue);
            ivTeamImage2 = (ImageView) itemView.findViewById(R.id.iv_teamimage);
            ivTeamImage1 = (ImageView) itemView.findViewById(R.id.iv_teamimage1);
            ivReferee = (ImageView) itemView.findViewById(R.id.iv_referee);
            //
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(mGetTeamScheduleRes.get(getLayoutPosition()));
                    //
                    GetTeamScheduleRes model =mGetTeamScheduleRes.get(getLayoutPosition());
                    Intent intent = new Intent(context, MatchDetailsActivity.class);
                    Bundle b = new Bundle();
                    b.putSerializable("match_detail", model);
                    intent.putExtras(b);
                    context.startActivity(intent);
                    ((Activity)context).overridePendingTransition(R.anim.anim_left_in,
                            R.anim.anim_left_out);
                }
            });
        }
    }

    private String getFilteredDate(String date) {
        String filteredDate = "";
        String[] filteredDateArray = date.split(Pattern.quote("-"));
        switch (filteredDateArray[1]) {
            case "1":
                filteredDate = "Jan, ";
                break;
            case "2":
                filteredDate = "Feb, ";
                break;
            case "3":
                filteredDate = "Mar, ";
                break;
            case "4":
                filteredDate = "Apr, ";
                break;
            case "5":
                filteredDate = "May, ";
                break;
            case "6":
                filteredDate = "Jun, ";
                break;
            case "7":
                filteredDate = "Jul, ";
                break;
            case "8":
                filteredDate = "Aug, ";
                break;
            case "9":
                filteredDate = "Sep, ";
                break;
            case "10":
                filteredDate = "Oct, ";
                break;
            case "11":
                filteredDate = "Nov, ";
                break;
            case "12":
                filteredDate = "Dec, ";
                break;
        }

        filteredDate += filteredDateArray[0];
        return filteredDate;

    }
}
