package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetPlayerDetailReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.repository.PlayerRepository;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class PlayerViewModel extends BaseViewModel {

    private PlayerRepository playerRepository;

    private MutableLiveData<ArrayList<GetTeamPlayersRes>> teamPlayersLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<PlayerRes>> allPlayersLiveData = new MutableLiveData<>();
    private MutableLiveData<PlayerRes> playerResMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<PlayerProfileRes> playerProfileMutableLiveData = new MutableLiveData<>();



    public PlayerViewModel(@NonNull Application application) {
        super(application);
        playerRepository = new PlayerRepository(application);
    }

    public void getAllPlayers() {
        consumeApi(
                playerRepository.getAllPlayers(),
                data -> allPlayersLiveData.setValue(data)
        );
    }
    public void getAllPlayers(GetAllTeamReq getAllTeamReq) {
        consumeApi(
                playerRepository.getAllPlayers(getAllTeamReq),
                data -> allPlayersLiveData.setValue(data)
        );
    }

    public void getTeamPlayers(GetTeamPlayersReq getTeamPlayersReq) {
        consumeApi(
                playerRepository.getTeamPlayers(getTeamPlayersReq),
                data -> teamPlayersLiveData.setValue(data)
        );
    }

    public LiveData<ArrayList<PlayerRes>> getAllPlayersLiveData() {
        return allPlayersLiveData;
    }

    public LiveData<ArrayList<GetTeamPlayersRes>> getTeamPlayersLiveData() {
        return teamPlayersLiveData;
    }

    public LiveData<PlayerRes> getPlayersLiveData() {
        return playerResMutableLiveData;
    }

    public void getInviteTeam(GetTeamPlayersReq getTeamPlayersReq) {
        consumeApi(
                playerRepository.getInvitePlayer(getTeamPlayersReq),
                data -> playerResMutableLiveData.setValue(data)
        );
    }

    public void getJoinTeam(GetTeamPlayersReq getTeamPlayersReq) {
        consumeApi(
                playerRepository.getJoinTeam(getTeamPlayersReq),
                data -> playerResMutableLiveData.setValue(data)
        );
    }

    public void getRejectTeamInvite(GetTeamPlayersReq getTeamPlayersReq) {
        consumeApi(
                playerRepository.getRejectTeamInvite(getTeamPlayersReq),
                data -> playerResMutableLiveData.setValue(data)
        );
    }

    public LiveData<PlayerProfileRes> getPlayerProfileDetailLiveData() {
        return playerProfileMutableLiveData;
    }

    public void getPlayerDetail(GetPlayerDetailReq getPlayerDetailReq) {
        consumeApi(
                playerRepository.getPlayerDetail(getPlayerDetailReq),
                data -> playerProfileMutableLiveData.setValue(data)
        );
    }
}
