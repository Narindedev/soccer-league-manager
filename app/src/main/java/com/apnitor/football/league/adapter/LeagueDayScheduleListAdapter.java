package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class LeagueDayScheduleListAdapter extends RecyclerView.Adapter<LeagueDayScheduleListAdapter.ViewHolder> {

    private Context context;
    private List<String> teamList;
    AddScheduleLeagueReq leagueScheduleList;
    String LOG_TAG = "SeasonDayResultsListAdapter";
    int headerPosition;

    public LeagueDayScheduleListAdapter(Context context, ArrayList<String> teamList, AddScheduleLeagueReq leagueScheduleList, int headerPosition) {
        this.context = context;
        this.teamList = teamList;
        this.leagueScheduleList = leagueScheduleList;
        this.headerPosition = headerPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_league_day_schedule, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            try {
                if (leagueScheduleList != null) {
                    AddScheduleLeagueReq model = leagueScheduleList;
                    holder.tvVenue.setText(model.getLocation() + ", " + model.getState());

                    if(model.getTeam1Name()==null){
                        holder.tvTeamName1.setText(model.getTeam1Detail().teamName);
                        holder.tvTeamName2.setText(model.getTeam2Detail().teamName);
                    }else{
                        holder.tvTeamName1.setText(model.getTeam1Name());
                        holder.tvTeamName2.setText(model.getTeam2Name());
                    }
                    if(model.getTeam1Detail()!=null) {
                        if (model.getTeam1Detail().imageUrl!=null) {
                            Glide.with(context).load(model.getTeam1Detail().imageUrl).apply(new RequestOptions()
                                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeam1Image);
                        }
                    }
                    if(model.getTeam2Detail()!=null) {
                        if (model.getTeam2Detail().imageUrl!=null) {
                            Glide.with(context).load(model.getTeam2Detail().imageUrl).apply(new RequestOptions()
                                    .centerCrop().placeholder(R.drawable.ic_team)).into(holder.ivTeam2Image);
                        }
                    }
                } else {

                }
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }

            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @Override
    public int getItemCount() {
        return 1;
//        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName1;
        TextView tvTeamName2;
        TextView tvVenue;
        ImageView ivTeam1Image,ivTeam2Image;

        ViewHolder(View itemView) {
            super(itemView);
            ivTeam1Image = (ImageView) itemView.findViewById(R.id.ivLeftTeam);
            ivTeam2Image = (ImageView) itemView.findViewById(R.id.ivRightTeam);
            tvTeamName1 = (TextView) itemView.findViewById(R.id.tv_team_1);
            tvTeamName2 = (TextView) itemView.findViewById(R.id.tv_team_2);
            tvVenue = (TextView) itemView.findViewById(R.id.tvVenue);
        }
    }
}
