package com.apnitor.football.league.fragment_binding_callback;

public interface LoginFragmentBindingCallback {

    void onLogin();

    void onForgotPassword();

    void onSignUp();

    void onFacebookLogin();
}
