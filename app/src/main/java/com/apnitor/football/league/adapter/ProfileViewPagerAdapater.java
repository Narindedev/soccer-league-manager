package com.apnitor.football.league.adapter;

import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.fragment.LeagueTeamsFragment;
import com.apnitor.football.league.fragment.ProfileLeagueFragment;
import com.apnitor.football.league.fragment.ProfileSeasonsFragment;
import com.apnitor.football.league.fragment.util.FragmentObserver;

import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ProfileViewPagerAdapater extends FragmentPagerAdapter {

    private final int NUM_ITEMS = 3;
    private GetLeagueRes getLeagueRes;
    private Observable mObservers = new FragmentObserver();

    public ProfileViewPagerAdapater(FragmentManager fm, GetLeagueRes getLeagueRes) {
        super(fm);
        this.getLeagueRes = new GetLeagueRes();
        this.getLeagueRes = getLeagueRes;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ProfileSeasonsFragment profileSeasonsFragment = ProfileSeasonsFragment.newInstance(getLeagueRes);
                //mObservers.deleteObservers(); // Clear existing observers.
                if (profileSeasonsFragment instanceof Observer)
                    mObservers.addObserver((Observer) profileSeasonsFragment);
                return profileSeasonsFragment;
            case 1:
                ProfileLeagueFragment profileLeagueFragment = ProfileLeagueFragment.newInstance(getLeagueRes);
                // mObservers.deleteObservers(); // Clear existing observers.
                if (profileLeagueFragment instanceof Observer)
                    mObservers.addObserver((Observer) profileLeagueFragment);
                return profileLeagueFragment;
            case 2:
                LeagueTeamsFragment leagueTeamsFragment = LeagueTeamsFragment.newInstance(getLeagueRes);
                // mObservers.deleteObservers(); // Clear existing observers.
                if (leagueTeamsFragment instanceof Observer)
                    mObservers.addObserver((Observer) leagueTeamsFragment);
                return leagueTeamsFragment;
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SEASON";
            case 1:
                return "INFO";
            case 2:
                return "TEAMS";
            default:
                return "SEASON";
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void updateFragments() {
        mObservers.notifyObservers();
    }

}
