package com.apnitor.football.league.api.request;

import com.apnitor.football.league.api.response.TeamLeagueRes;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AddScheduleLeagueReq implements Serializable {

    private String leagueId;
    private String team1Name;
    private String team2Name;
    private Integer team1Score;
    private Integer team2Score;
    private String scheduleId;
    private String _id;

    @Expose
    @SerializedName("team1Playing11")
    public List<PlayingList> team1Playing11;
    @Expose
    @SerializedName("team2Playing11")
    public List<PlayingList> team2Playing11;
    @Expose
    @SerializedName("team1Substitution")
    public List<PlayingList> team1Substitution;

    public List<PlayingList> team2Substitution;
    private String seasonId;
    private String divisionId;
    private String date;
    private String time;
    private String team1Id;
    private String team2Id;
    private String location;
    private String state;
    private String city;
    private String zipcode;
    private String description;
    private String refereeId;

    private TeamLeagueRes team1Detail;

    private TeamLeagueRes team2Detail;

    public AddScheduleLeagueReq(String leagueId, String scheduleId, String _id, String seasonId, String divisionId, String date, String time, String team1Id, String team2Id, String location, String state, String city, String zipcode, String description, String team1Name, String team2Name, String refereeID) {
        this.leagueId = leagueId;
        this.scheduleId = scheduleId;
        this._id = _id;
        this.seasonId = seasonId;
        this.divisionId = divisionId;
        this.date = date;
        this.team1Id = team1Id;
        this.team2Id = team2Id;
        this.location = location;
        this.state = state;
        this.city = city;
        this.zipcode = zipcode;
        this.description = description;
        this.team1Name = team1Name;
        this.team2Name = team2Name;
        this.refereeId = refereeID;
        this.time=time;
    }


    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(String seasonId) {
        this.seasonId = seasonId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTeam1Id() {
        return team1Id;
    }

    public void setTeam1Id(String team1Id) {
        this.team1Id = team1Id;
    }

    public String getTeam2Id() {
        return team2Id;
    }

    public void setTeam2Id(String team2Id) {
        this.team2Id = team2Id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public String getRefereeId() {
        return refereeId;
    }

    public void setRefereeId(String refereeId) {
        this.refereeId = refereeId;
    }

    public Integer getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(Integer team1Score) {
        this.team1Score = team1Score;
    }

    public Integer getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(Integer team2Score) {
        this.team2Score = team2Score;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public TeamLeagueRes getTeam1Detail() {
        return team1Detail;
    }

    public void setTeam1Detail(TeamLeagueRes team1Detail) {
        this.team1Detail = team1Detail;
    }

    public TeamLeagueRes getTeam2Detail() {
        return team2Detail;
    }

    public void setTeam2Detail(TeamLeagueRes team2Detail) {
        this.team2Detail = team2Detail;
    }


    public List<PlayingList> getTeam1Playing11() {
        return team1Playing11;
    }

    public void setTeam1Playing11(List<PlayingList> team1Playing11) {
        this.team1Playing11 = team1Playing11;
    }

    public List<PlayingList> getTeam2Playing11() {
        return team2Playing11;
    }

    public void setTeam2Playing11(List<PlayingList> team2Playing11) {
        this.team2Playing11 = team2Playing11;
    }


    public class PlayingList implements Serializable{
        public String _id;
        public String name;
        public String column;
        public String jerseyNumber;
        public String playerId;
        public String row;
        public String status;
        public String updatedAt;
        public String createdAt;
    }
}
