package com.apnitor.football.league.activity.socailmedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.BaseActivity;
import com.apnitor.football.league.adapter.LeaguePagerAdapater;

import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.databinding.ActivitySocialMediaBinding;
import com.apnitor.football.league.databinding.FragmentShareNewsBinding;
import com.apnitor.football.league.util.CustomViewPager;

public class SocialMediaActivity extends BaseActivity {

    CustomViewPager viewPager;
    SocialVIewPagerAdapter adapter;
    SocialMedia socialMedia;

    ActivitySocialMediaBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);
        binding=bindContentView(R.layout.activity_social_media);
        viewPager = binding.mediaViewPager;
        setupToolbar(binding.toolbar, "Social Media");
        Intent mIntent=getIntent();
        if(getIntent()
                .getExtras()!=null){
            socialMedia=(SocialMedia)getIntent()
                    .getExtras()
                    .getSerializable("socialMedia");
        }else{
            socialMedia=new SocialMedia();
        }

        adapter = new SocialVIewPagerAdapter(getSupportFragmentManager(), socialMedia);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        //
        binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.colorAccent));
        binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
        binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));

        //
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setPage(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(0);


         onClickListener();

    }

    private void onClickListener(){
        binding.ivfacebookTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(0);
            }
        });

        binding.ivTwitterTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1);
            }
        });

        binding.ivInstagramTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2);
            }
        });

    }

    @SuppressLint("RestrictedApi")
    void setPage(int position) {

        switch (position) {
            case 0:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));
                break;
            case 1:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.white));
                break;
            case 2:
                binding.ivfacebookTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivTwitterTab.setColorFilter(getResources().getColor(R.color.white));
                binding.ivInstagramTab.setColorFilter(getResources().getColor(R.color.colorAccent));
                break;
            default:
                break;
        }
    }
}
