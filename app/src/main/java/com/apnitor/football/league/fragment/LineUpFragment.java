package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.SwipePlayerActivity;
import com.apnitor.football.league.adapter.LineUpAdapter;
import com.apnitor.football.league.adapter.SelectLineUpAdapter;
import com.apnitor.football.league.api.request.TeamPlaying11;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.util.PreferenceHandler;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.regex.Pattern;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LineUpFragment extends BaseFragment {

    RecyclerView mRvSubstitutions;
    RecyclerView mRvBench;
    RecyclerView mRvPlaying11;
    ImageView mIvSubTime;
    TextView mTvTShirtPlayer1, mTvTShirtPlayer2, mTvPlayer1, mTvPlayer2, mTvTeam1, mTvTeam2, mTvApproveReject;
    public static GetTeamScheduleRes mGetMatchDetail;
    String selectedTeam = "";
    LinearLayout mLl1, mLl2, mLl3, mLl4;
    int[] ids = new int[]{R.id.rl_1, R.id.rl_2, R.id.rl_3, R.id.rl_4, R.id.rl_5};
    int[] idsImageViews = new int[]{R.id.ivPlayer1, R.id.ivPlayer2, R.id.ivPlayer3, R.id.ivPlayer4, R.id.ivPlayer5};
    LinearLayout[] mLl;
    public ArrayList<TeamPlaying11> mPlayingList = new ArrayList<>();
    String LOG_TAG = "LineUpFragment";


    public static LineUpFragment newInstance(int i, String s, GetTeamScheduleRes getMatchDetail) {
        mGetMatchDetail = getMatchDetail;
        LineUpFragment fragment = new LineUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_line_up, container, false);
        setUpLayout(view);
        return view;
    }

    private void setUpLayout(View view) {
        //
        mLl1 = view.findViewById(R.id.ll_1);
        mLl2 = view.findViewById(R.id.ll_2);
        mLl3 = view.findViewById(R.id.ll_3);
        mLl4 = view.findViewById(R.id.ll_4);
        //
        mLl = new LinearLayout[]{mLl1, mLl2, mLl3, mLl4};
        mRvPlaying11 = view.findViewById(R.id.playing11);
        mRvPlaying11.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvPlaying11.setNestedScrollingEnabled(false);
        //
        mRvBench = view.findViewById(R.id.bench);
        mRvBench.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvBench.setNestedScrollingEnabled(false);
        //
        mTvTeam1 = view.findViewById(R.id.tv_team_1);
        mTvTeam1.setText(mGetMatchDetail.getTeam1Name());
        mTvTeam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTeam = "TEAM1";
                setFormation();
                mTvTeam1.setTextColor(getActivity().getResources().getColor(R.color.dark_black));
                mTvTeam2.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round_selected);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round);
                //
                mRvPlaying11.setAdapter(new SelectLineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam1Playing11(), "playing"), "PLAYING11"));
                mRvBench.setAdapter(new LineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam1Playing11(), "bench"), "BENCH"));
            }
        });
        //
        mTvTeam2 = view.findViewById(R.id.tv_team_2);
        mTvTeam2.setText(mGetMatchDetail.getTeam2Name());
        mTvTeam2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedTeam = "TEAM2";
                setFormation();
                mTvTeam1.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                mTvTeam2.setTextColor(getActivity().getResources().getColor(R.color.dark_black));
                //
                mTvTeam1.setBackgroundResource(R.drawable.shape_left_round);
                mTvTeam2.setBackgroundResource(R.drawable.shape_right_round_selected);
                //
                mRvPlaying11.setAdapter(new SelectLineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam2Playing11(), "playing"), "PLAYING11"));
                mRvBench.setAdapter(new LineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam2Playing11(), "bench"), "BENCH"));
            }
        });
        //
        mTvTeam1.performClick();
        mTvPlayer1 = view.findViewById(R.id.player_1_name);
        mTvPlayer2 = view.findViewById(R.id.player_2_name);
        //
        mTvTShirtPlayer1 = view.findViewById(R.id.tshirt_player_1);
        mTvTShirtPlayer2 = view.findViewById(R.id.tshirt_player_2);
        //
        mIvSubTime = view.findViewById(R.id.iv_sub_time);
        //
        mRvSubstitutions = view.findViewById(R.id.substitutions);
        mRvSubstitutions.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvSubstitutions.setNestedScrollingEnabled(false);
        //
        mTvApproveReject = view.findViewById(R.id.approveReject);
        mTvApproveReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SwipePlayerActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("match_detail", mGetMatchDetail);
                intent.putExtras(b);
                intent.putExtra("TEAM", selectedTeam);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.anim_left_in, R.anim.anim_left_out);
            }
        });
        setCoach();
        //
        if (mGetMatchDetail.getRefereeId() != null && !mGetMatchDetail.getRefereeId().equalsIgnoreCase(PreferenceHandler.readString(getActivity(), PreferenceHandler.PREF_USER_ID, ""))) {
            view.findViewById(R.id.approveReject).setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mRvSubstitutions.setAdapter(new LineUpAdapter(getContext(), new ArrayList<TeamPlaying11>(), "SUB"));
        mRvBench.setAdapter(new LineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam1Playing11(), "bench"), "BENCH"));
        mRvPlaying11.setAdapter(new SelectLineUpAdapter(getContext(), filterPlaying11(mGetMatchDetail.getTeam1Playing11(), "playing"), "PLAYING11"));
    }

    private void setCoach() {
        mIvSubTime.setVisibility(View.INVISIBLE);
        mTvTShirtPlayer1.setVisibility(View.GONE);
        mTvTShirtPlayer2.setVisibility(View.GONE);
        //
        mTvPlayer1.setText("John Doe");
        mTvPlayer2.setText("Coach");
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public ArrayList<TeamPlaying11> filterPlaying11(ArrayList<TeamPlaying11> allList, String playing) {
        if (allList == null) {
            return new ArrayList<>();
        }
        ArrayList<TeamPlaying11> playing11 = new ArrayList<>();
        ArrayList<TeamPlaying11> bench = new ArrayList<>();
        playing11.clear();
        for (int i = 0; i < allList.size(); i++) {
            if (allList.get(i).getPlaying() == null) {
                bench.add(allList.get(i));
            } else if (allList.get(i).getPlaying() == true && allList.get(i).getStatus().equalsIgnoreCase("accept")) {
                playing11.add(allList.get(i));
            } else {
                bench.add(allList.get(i));
            }
        }
        if (playing.equalsIgnoreCase("playing"))
            return playing11;
        else {
            return bench;
        }
    }

    void setFormation() {
        showAll();
        //
        String[] formation = null;

        if (selectedTeam.equalsIgnoreCase("TEAM1")) {
            if (mGetMatchDetail.getTeam1Formation() != null) {
                formation = mGetMatchDetail.getTeam1Formation().split(Pattern.quote("-"));
            }
        } else {
            if (mGetMatchDetail.getTeam2Formation() != null) {
                formation = mGetMatchDetail.getTeam2Formation().split(Pattern.quote("-"));
            }
        }
        if (formation == null || formation.length < 2) {
            hideAll();
            return;
        }

        setFormationRow1(formation[0].trim());
        setFormationRow2(formation[1].trim());
        setFormationRow3(formation[2].trim());
        resetAllViews();
        setPlayersFormation();
    }

    private void resetAllViews() {
        try {
            for (int i = 0; i < mLl.length; i++) {
                for (int j = 0; j < idsImageViews.length; j++) {
                    ((ImageView) mLl[i].findViewById(idsImageViews[j])).setImageResource(R.drawable.shape_circle);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPlayersFormation() {
        mPlayingList.clear();
        if (selectedTeam.equalsIgnoreCase("TEAM1")) {
            mPlayingList.addAll(mGetMatchDetail.getTeam1Playing11());
        } else {
            mPlayingList.addAll(mGetMatchDetail.getTeam2Playing11());
        }
        for (int i = 0; i < mPlayingList.size(); i++) {
            setRowColumnViews(mPlayingList.get(i).getRow(), mPlayingList.get(i).getColumn(), mPlayingList.get(i).getImageUrl());
        }
    }

    private void hideAll() {
        mLl1.setVisibility(View.GONE);
        mLl2.setVisibility(View.GONE);
        mLl3.setVisibility(View.GONE);
        mLl4.setVisibility(View.GONE);
    }

    private void showAll() {
        for (int i = 0; i < mLl.length; i++) {
            mLl[i].setVisibility(View.VISIBLE);
            for (int j = 0; j < ids.length; j++) {
                mLl[i].findViewById(ids[j]).setVisibility(View.VISIBLE);
            }
        }
    }

    void setFormationRow1(String rowOne) {
        if (rowOne != null && !rowOne.isEmpty())
            hideViews(mLl3, Integer.parseInt(rowOne));
    }

    void setFormationRow2(String rowTwo) {
        if (rowTwo != null && !rowTwo.isEmpty())
            hideViews(mLl2, Integer.parseInt(rowTwo));
    }

    void setFormationRow3(String rowThree) {
        if (rowThree != null && !rowThree.isEmpty())
            hideViews(mLl1, Integer.parseInt(rowThree));
    }

    void setGoalKeeperUI() {
        mLl4.findViewById(R.id.rl_1).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_2).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_4).setVisibility(View.GONE);
        mLl4.findViewById(R.id.rl_5).setVisibility(View.GONE);
    }

    void hideViews(LinearLayout ll, int count) {
        switch (count) {
            case 2:
                ll.findViewById(R.id.rl_3).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_4).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 3:
                ll.findViewById(R.id.rl_4).setVisibility(View.GONE);
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 4:
                ll.findViewById(R.id.rl_5).setVisibility(View.GONE);
                break;
            case 5:
                break;
        }
        setGoalKeeperUI();
    }


    public void setRowColumnViews(String row, String column, String imageUrl) {

        if (row.equalsIgnoreCase("-1") || column.equalsIgnoreCase("-1")) {
            return;
        }
//        Log.d(LOG_TAG," ROW "+row + " COLUMN "+column +" Image "+imageUrl);

        LinearLayout Ll = null;
        int Iv = 0;

        switch (row) {
            case "0":
                Ll = mLl1;
                break;
            case "1":
                Ll = mLl2;
                break;
            case "2":
                Ll = mLl3;
                break;
            case "3":
                Ll = mLl4;
                break;
        }

        switch (column) {
            case "0":
                Iv = R.id.ivPlayer1;
                break;
            case "1":
                Iv = R.id.ivPlayer2;
                break;
            case "2":
                Iv = R.id.ivPlayer3;
                break;
            case "3":
                Iv = R.id.ivPlayer4;
                break;
            case "4":
                Iv = R.id.ivPlayer5;
                break;
        }

        ImageView mIv = Ll.findViewById(Iv);
        if (imageUrl != null)
            Glide.with(getActivity()).load(imageUrl).into(mIv);
        else {
            mIv.setImageResource(R.drawable.ic_person);
        }

    }

}
