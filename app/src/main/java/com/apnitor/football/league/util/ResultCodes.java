package com.apnitor.football.league.util;

public class ResultCodes {
    public static int EDIT_LEAGUE_RESULT_CODE = 2000;
    public static int DIVISION_RESULT_CODE = 2001;
    public static int SEASON_RESULT_CODE = 2002;
    public static int TEAM_RESULT_CODE = 2003;
    public static int ADD_Schedule_RESULT_CODE = 2004;
    public static int ADD_TEAM_RESULT_CODE = 2005;

    /*Team Player Result*/
    public static int EDIT_TEAM_RESULT_CODE = 3000;
    public static int ADD_Player_RESULT_CODE = 3001;
}
