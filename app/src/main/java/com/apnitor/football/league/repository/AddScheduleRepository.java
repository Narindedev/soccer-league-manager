package com.apnitor.football.league.repository;

import io.reactivex.Single;
import android.app.Application;
import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.LeagueApi;
import com.apnitor.football.league.api.request.AddPlayerToTeamReq;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.BaseRes;
import io.reactivex.schedulers.Schedulers;

public class AddScheduleRepository {

    private LeagueApi leagueApi;

    public AddScheduleRepository(Application application) {
        leagueApi = ApiService.getLeagueApi(application);
    }

    public Single<BaseRes<AddScheduleRes>> getDivisions(AddScheduleLeagueReq addScheduleLeagueReq) {
        return leagueApi.updateLeagueSchedule(addScheduleLeagueReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<AddScheduleRes>> updateTeamPlayers(AddPlayerToTeamReq addScheduleLeagueReq) {
        return leagueApi.updateLeagueSchedule(addScheduleLeagueReq)
                .subscribeOn(Schedulers.io());
    }

}
