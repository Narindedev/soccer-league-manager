package com.apnitor.football.league.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.socailmedia.SocialMediaActivity;
import com.apnitor.football.league.adapter.PlayerViewPagerAdapter;
import com.apnitor.football.league.api.request.GetPlayerDetailReq;
import com.apnitor.football.league.api.request.SocialMedia;
import com.apnitor.football.league.api.response.LogInRes;
import com.apnitor.football.league.api.response.PlayerProfileRes;
import com.apnitor.football.league.databinding.ActivityPlayerProfileBinding;
import com.apnitor.football.league.viewmodel.PlayerViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.viewpager.widget.ViewPager;

public class PlayerProfileActivity extends BaseActivity {
    private final String PLAYER_ID = "player_id";
    private final String PLAYER_NAME = "player_name";
    private NavController navController;
    private NavHostFragment navHostFragment;
    private PlayerViewModel playerViewModel;
    private String playerId;
    private ActivityPlayerProfileBinding binding;
    private PlayerProfileRes playerRes;
    public LogInRes logInRes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile);
        binding = bindContentView(R.layout.activity_player_profile);
        playerViewModel = getViewModel(PlayerViewModel.class);
        Intent i = getIntent();
        if (i.getExtras() != null) {
            playerId = i.getExtras().getString(PLAYER_ID);
            binding.tvName.setText(i.getExtras().getString(PLAYER_NAME));

        }

        logInRes=getPrefHelper().getLogInRes();
        setLeagueDetails();
        setPlayerInfo();
        binding.ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.ivSocialBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(PlayerProfileActivity.this, SocialMediaActivity.class);
                Bundle bundle = new Bundle();
                if(logInRes.getUserId().equals(playerId)){
                    bundle.putSerializable("socialMedia", logInRes.getSocialMedia());
                }else{
                    bundle.putSerializable("socialMedia", new SocialMedia());
                }

                mIntent.putExtras(bundle);
                startActivity(mIntent);
            }
        });
    }

    private void setPlayerInfo(){
        if(logInRes.getUserId().equals(playerId)) {
            binding.tvAge.setText("Age\n" + calculateAge(logInRes.getDateOfBirth()));
            binding.tvHeight.setText("Height\n" + logInRes.getHeight()+" cms");
            binding.tvWeight.setText("Weight\n" + logInRes.getWeight()+" lbs");
            binding.tvShirtNo.setText("Shirt No\n" + logInRes.getJerseyNo());

        }
        }

    private void setLeagueDetails() {
        ViewPager viewPager = binding.profileViewPager;

        binding.tabProfile.setupWithViewPager(viewPager);


        playerViewModel.getPlayerProfileDetailLiveData().observe(this,
                data -> {

                    // getLeagueRes = league;
                    //  binding.setLeague(getLeagueRes);
                    // setUpViewPager();

                  /*  getLeagueRes=new GetLeagueRes(league.getLeagueId(),
                            league.getLeagueTitle(),league.getFoundationDate(),
                            league.getCountry(),league.getCurrentChampions(),league.getMostChampions(),
                            league.getContactInfo(),league.getWebsite(),league.getFacebookLink(),
                            league.getTwitterLink(),league.getImageUrl(),league.getOwner());*/
                    // getLeagueRes = league;

                    playerRes = new PlayerProfileRes();
                    playerRes = data;
                    binding.setPlayerData(data);
                   /* binding.tvAge.setText("Age\n"+playerRes.getAge());
                    binding.tvHeight.setText("Height\n"+playerRes.getHeight());
                    binding.tvWeight.setText("Weight\n"+playerRes.getWeight());
                    binding.tvShirtNo.setText("Shirt No\n"+playerRes.getShirtNumber());
                    binding.tvName.setText(data.getFirstName()+" "+data.getLastName());
                   */
                    Glide.with(this).load(data.getProfilePhotoUrl()).apply(new RequestOptions()
                            .centerCrop().placeholder(R.drawable.ic_player)).into(binding.ivProfileImage);
                    viewPager.setAdapter(new PlayerViewPagerAdapter(getSupportFragmentManager(), data));
                    //viewPager.setOffscreenPageLimit(3);
                    viewPager.getAdapter().notifyDataSetChanged();
                    viewPager.setCurrentItem(0);
                    // binding.tabProfile.getTabAt(0).select();

                }
        );
        playerViewModel.getPlayerDetail(new GetPlayerDetailReq(playerId));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_in,
                R.anim.anim_right_out);
    }



    public static int calculateAge(String dateOfBirth) {
        int yearDifference=0;
        try {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date birthdate = sdf.parse(dateOfBirth);

        Calendar birth = Calendar.getInstance();
        birth.setTime(birthdate);
        Calendar today = Calendar.getInstance();

         yearDifference = today.get(Calendar.YEAR)
                - birth.get(Calendar.YEAR);

        if (today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
            yearDifference--;
        } else {
            if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)
                    && today.get(Calendar.DAY_OF_MONTH) < birth
                    .get(Calendar.DAY_OF_MONTH)) {
                yearDifference--;
            }

        }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return yearDifference;
    }


}
