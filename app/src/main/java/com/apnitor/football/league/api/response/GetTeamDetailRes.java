package com.apnitor.football.league.api.response;

import java.io.Serializable;
import java.util.List;

public class GetTeamDetailRes implements Serializable {
    private GetTeamRes teamModel;
    private List<GetTeamScheduleRes> teamSchedules;
    private List<GetLeagueRes> teamLeagueNames;

    public GetTeamRes getTeamModel() {
        return teamModel;
    }

    public void setTeamModel(GetTeamRes teamModel) {
        this.teamModel = teamModel;
    }

    public List<GetTeamScheduleRes> getTeamSchedules() {
        return teamSchedules;
    }

    public void setTeamSchedules(List<GetTeamScheduleRes> teamSchedules) {
        this.teamSchedules = teamSchedules;
    }

    public List<GetLeagueRes> getTeamLeagueNames() {
        return teamLeagueNames;
    }

    public void setTeamLeagueNames(List<GetLeagueRes> teamLeagueNames) {
        this.teamLeagueNames = teamLeagueNames;
    }
}
