package com.apnitor.football.league.api.request;

public class GetLeagueSeasonsReq {


    private String leagueId;

    public GetLeagueSeasonsReq(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }
}
