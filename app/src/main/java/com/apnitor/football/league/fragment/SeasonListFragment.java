package com.apnitor.football.league.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.CreateSeasonActivity;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.adapter.SeasonListAdapter;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.RequestCodes;
import com.apnitor.football.league.viewmodel.SeasonViewModel;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SeasonListFragment extends BaseFragment implements ListItemClickCallback, Observer {


    private static GetLeagueRes mGetLeagueRes;
    RecyclerView mRvDivList;
    SeasonListAdapter adapter;
    private SeasonViewModel seasonViewModel;
    private LinearLayout mEmptySeason;

    public static SeasonListFragment newInstance(int i, String s, GetLeagueRes getLeagueRes) {
        mGetLeagueRes = getLeagueRes;
        SeasonListFragment fragment = new SeasonListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_season_list, container, false);
        //
       /* if (getArguments() != null && getArguments().getSerializable("league_info") != null) {
            mGetLeagueRes = (GetLeagueRes) getArguments().getSerializable("league_info");
        }*/
        seasonViewModel = getViewModel(SeasonViewModel.class);

        //
        mEmptySeason = view.findViewById(R.id.llEmptySeason);
        mRvDivList = view.findViewById(R.id.rvSeasonList);
        mRvDivList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        mRvDivList.setAdapter(new SeasonListAdapter(getContext(), null, null));
        // observeApiResponse();
        loadSeasonData();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void loadSeasonData() {
        if (mGetLeagueRes.seasonList != null && mGetLeagueRes.seasonList.size() > 0) {
            mEmptySeason.setVisibility(View.GONE);
            adapter = new SeasonListAdapter(getContext(), mGetLeagueRes.seasonList, this);
            mRvDivList.setAdapter(adapter);
        } else {
            mEmptySeason.setVisibility(View.VISIBLE);
        }
    }
  /*  private void observeApiResponse() {
        seasonViewModel.getLeagueListResLiveData().observe(this,
                leagues -> {
                    mRvDivList.setAdapter(
                            new SeasonListAdapter(getContext(), leagues, this)
                    );
                });
    }
*/

    @Override
    public void onListItemClick(Object object) {
        GetSeasonRes getSeasonRes = (GetSeasonRes) object;

        Intent intent = new Intent(getActivity(), CreateSeasonActivity.class);
        Bundle b = new Bundle();
        b.putString("leagueId", ((LeagueProfileActivity) getActivity()).getLeagueId());
        b.putSerializable("divisions", (Serializable) mGetLeagueRes.divisionList);
        b.putSerializable("seasons", (Serializable) mGetLeagueRes.seasonList);
        b.putSerializable("season_detail", getSeasonRes);
        intent.putExtras(b);
        startActivityForResult(intent,RequestCodes.SEASON_REQUEST_CODE);

    }

    @Override
    public void update(Observable o, Object arg) {
        mGetLeagueRes = ((LeagueProfileActivity) getActivity()).mGetLeagueRes;
        if ( mGetLeagueRes.seasonList != null) {
            loadSeasonData();
        }
    }
}
