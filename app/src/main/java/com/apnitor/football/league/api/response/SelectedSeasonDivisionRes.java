package com.apnitor.football.league.api.response;

public class SelectedSeasonDivisionRes {
    private String sesonId;
    private String sesonName;
    private String seasonStatus;
    private String divisionId;
    private String divisionName;

    public String getSesonId() {
        return sesonId;
    }

    public void setSesonId(String sesonId) {
        this.sesonId = sesonId;
    }

    public String getSesonName() {
        return sesonName;
    }

    public void setSesonName(String sesonName) {
        this.sesonName = sesonName;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getSeasonStatus() {
        return seasonStatus;
    }

    public void setSeasonStatus(String seasonStatus) {
        this.seasonStatus = seasonStatus;
    }
}
