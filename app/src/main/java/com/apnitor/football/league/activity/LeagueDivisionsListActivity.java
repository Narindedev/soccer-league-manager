package com.apnitor.football.league.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.DivisionsListAdapter;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.UpdateDivisionReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.databinding.ActivityLeagueDivisionsListBinding;
import com.apnitor.football.league.databinding.ActivitySeasonsListBinding;
import com.apnitor.football.league.interfaces.ListItemClickCallback;
import com.apnitor.football.league.util.UIUtility;
import com.apnitor.football.league.viewmodel.DivisionViewModel;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeagueDivisionsListActivity extends BaseActivity implements ListItemClickCallback {

    private ActivityLeagueDivisionsListBinding binding;
    private DivisionViewModel divisionViewModel;
    private LeagueDivisionsListActivity leagueDivisionsListActivity = this;
    private String leagueId;
    private final String LEAGUE_ID = "leagueId";
    private UpdateDivisionReq updateDivisionReq;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = bindContentView(R.layout.activity_league_divisions_list);

        if (getIntent() != null && getIntent().getExtras() != null) {
            leagueId = getIntent().getExtras().getString(LEAGUE_ID);
        }
        divisionViewModel = getViewModel(DivisionViewModel.class);
        setupToolbar(binding.toolbar, "League Divisions");
        setUpRecyclerView();
        observeApiResponse();

    }

    private void setUpRecyclerView() {
        binding.rvAllDivisions.setLayoutManager(
                new LinearLayoutManager(leagueDivisionsListActivity, RecyclerView.VERTICAL, false)
        );
    }

    private void observeApiResponse() {
        divisionViewModel.getDivisonsListResLiveData().observe(this,
                divisions -> {
                    binding.rvAllDivisions.setAdapter(
                            new DivisionsListAdapter(leagueDivisionsListActivity, divisions, this)
                    );
                });

        divisionViewModel.getUpdateDivisionResLiveData().observe(this,
                res -> {
                    divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
                });
    }

    public void onBackClick(View view) {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        divisionViewModel.getDivisions(new GetLeagueDivisionsReq(leagueId));
    }

    @Override
    public void onListItemClick(Object object) {
//        String selectedDivision = ((GetDivisionRes) object).getDivisionName();
//        showInputDialog(selectedDivision);
//        showAddDivisionDialog(selectedDivision);


    }

//    public void onAddDivision(View view) {
////        showInputDialog(null);
////        showAddDivisionDialog(null);
//
//    }

    public void onAddDivision(View view) {
        Bundle b = new Bundle();
        b.putString("leagueId", leagueId);
        startActivity(CreateDivisionActivity.class, b);
    }

    private void showInputDialog(String selectedDivisionToUpdate) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DialogInterface dialog = null;
// Set up the input
        LinearLayout parent = new LinearLayout(this);

        parent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        parent.setOrientation(LinearLayout.VERTICAL);

        final EditText input = new EditText(this);
        input.setTextColor(Color.BLACK);
        input.setHint("Division Name");
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        final EditText desc = new EditText(this);
        desc.setTextColor(Color.BLACK);
        desc.setHint("Division Description");
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        parent.addView(input);
        parent.addView(desc);
        if (selectedDivisionToUpdate != null) {
            input.setText(selectedDivisionToUpdate);
        }

        builder.setView(parent);
        builder.setTitle("Division");


// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String divisionName = UIUtility.getEditTextValue(input);
                String divisionDesc = UIUtility.getEditTextValue(desc);
                if (divisionName.equals(""))
                    Toast.makeText(leagueDivisionsListActivity, "'Division name' should not be empty.", Toast.LENGTH_LONG).show();
                else {
                    if (selectedDivisionToUpdate != null)
                        updateDivisionReq = new UpdateDivisionReq(null, leagueId, divisionName, selectedDivisionToUpdate, divisionDesc,"","","","");
                    else
                        updateDivisionReq = new UpdateDivisionReq(null, leagueId, divisionName, null, divisionDesc,"","","","'");

                    dialog.cancel();
                    divisionViewModel.createUpdateDivision(updateDivisionReq);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


    public void showAddDivisionDialog(String selectedDivisionToUpdate) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_add_division);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        final EditText et_div_name = (EditText) dialog.findViewById(R.id.et_div_name);
        EditText et_desc = (EditText) dialog.findViewById(R.id.et_desc);


        if (selectedDivisionToUpdate != null) {
            et_div_name.setText(selectedDivisionToUpdate);
            et_div_name.setSelection(selectedDivisionToUpdate.length());
        }

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.saveBtn);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String divisionName = UIUtility.getEditTextValue(et_div_name);
                String divisionDesc = UIUtility.getEditTextValue(et_desc);
                if (divisionName.equals(""))
                    Toast.makeText(leagueDivisionsListActivity, "'Division name' should not be empty.", Toast.LENGTH_LONG).show();
                else {
                    if (selectedDivisionToUpdate != null)
                        updateDivisionReq = new UpdateDivisionReq(null, leagueId, divisionName, selectedDivisionToUpdate, divisionDesc,"","","","");
                    else
                        updateDivisionReq = new UpdateDivisionReq(null, leagueId, divisionName, null, divisionDesc,"","","","");
                    dialog.cancel();
                    divisionViewModel.createUpdateDivision(updateDivisionReq);
                }
                dialog.dismiss();
            }
        });
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onResume();
                    }
                },500);
            }
        });
    }

}
