package com.apnitor.football.league.api;

import android.app.Application;
import android.util.Log;

import com.apnitor.football.league.application.FootballLeagueApplication;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {

    private static volatile Retrofit retrofit = null;
    private static String LOG_TAG = "ApiService";
    static String TEST_URL = "https://damp-reef-18383.herokuapp.com";
    static String LIVE_URL = "http://ec2-3-88-248-72.compute-1.amazonaws.com:3000";

    private ApiService() {
    }

    private static Retrofit getInstance(Application application) {
        if (retrofit == null) {
            synchronized (ApiService.class) {
                if (retrofit == null) {

//                    String ip = "192.168.1.3:3000";   // Local
//                    String ip = "18.225.33.228:3000";     // AWS
                    String ip = "calm-basin-34367.herokuapp.com";     // Heroku

                    /*old Url*/
                    // Heroku 2
//                    String baseUrl = "http://" + ip + "/football/league/";

                    /*new Url*/
                    String baseUrl = TEST_URL + "/football/league/";


                    Interceptor authorizationInterceptor = chain -> {
                        String authToken = ((FootballLeagueApplication) application)
                                .getSharedPreferenceHelper()
                                .getAuthToken();
                        Log.d(LOG_TAG, "access-token " + authToken);
                        return chain.proceed(
                                authToken.isEmpty()
                                        ? chain.request()
                                        : chain.request().newBuilder()
                                        .addHeader("access-token", authToken)
                                        .build()
                        );
                    };
                    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(msg -> Log.d("logger", msg));
                    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                    final OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                    clientBuilder.addInterceptor(loggingInterceptor).addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Log.e("Result", "Inside the Intersepter callback");
                            Request request = chain.request();
                            Response response = chain.proceed(request);
                            if (request.method().compareToIgnoreCase("post") == 0) {
                                Log.e("Result", "Response from http " + response.body());

                            }
                            String bodyString = response.body().string();
                            Log.e("Result", "Response" + "\n" + bodyString);


                            String newBodyString = null;
                            newBodyString = bodyString;

                            int stringLength = newBodyString.length();
                            JSONObject jsonObject = null;
                            try {
                                jsonObject = new JSONObject(newBodyString);
                                JSONObject mObject = jsonObject.getJSONObject("data");

                                mObject.accumulate("description", "5c7d21f7344446001cbbefb6");
                                mObject.accumulate("zipcode", "123456");
                                mObject.accumulate("stadium", "Mathana");
                                mObject.accumulate("homeUniform", "https://graph.facebook.com/2015978405136508/picture?type=square");
                                mObject.accumulate("awayUniform", "Mathana");
                                mObject.accumulate("managerName", "Vishal");
                                //  mObject.accumulate("name","Juventus");
                                mObject.accumulate("managerEmail", "vishal@gmail.com");
                                mObject.accumulate("managerPhone", "9253330003");


                            /*   JSONObject mJSonArray=mObject.getJSONObject("socialMedia");
                               mJSonArray.accumulate("websiteUrl","google.com");
                               mJSonArray.accumulate("instagramUrl","instagram.com/vishal_graak");
                               mJSonArray.accumulate("facebookUrl","facebook.com/vishal.graakror");*/
                            } catch (Exception ex) {

                            }


                            MediaType contentType = response.body().contentType();
                            ResponseBody body = ResponseBody.create(contentType, bodyString/*jsonObject.toString()*/);
                            return response.newBuilder().body(body).build();
                         /*
                           ResponseBody body = ResponseBody.create(response.body().contentType(), newBodyString);
                            Response build = response.newBuilder().body(body).build();*/
                            // return build;


                        }
                    }).addInterceptor(authorizationInterceptor);


                    OkHttpClient client = new OkHttpClient.Builder()
                            .addInterceptor(loggingInterceptor)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .connectTimeout(60, TimeUnit.SECONDS)
                            .addInterceptor(authorizationInterceptor)
                            .build();

                    retrofit = new Retrofit
                            .Builder()
                            .baseUrl(baseUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(clientBuilder.build())
                            .build();
                }
            }
        }
        return retrofit;
    }

    public static ProfileApi getProfileApi(Application application) {
        return getInstance(application).create(ProfileApi.class);
    }

    public static LeagueApi getLeagueApi(Application application) {
        return getInstance(application).create(LeagueApi.class);
    }

    public static TeamApi getTeamApi(Application application) {
        return getInstance(application).create(TeamApi.class);
    }

    public static FollowingApi getFollowingApi(Application application) {
        return getInstance(application).create(FollowingApi.class);
    }

    public static PlayerApi getPlayerApi(Application application) {
        return getInstance(application).create(PlayerApi.class);
    }

    public static SeasonApi getSeasonApi(Application application) {
        return getInstance(application).create(SeasonApi.class);
    }

    public static DivisionApi getDivisionApi(Application application) {
        return getInstance(application).create(DivisionApi.class);
    }

    public static RefereeApi getRefereeApi(Application application) {
        return getInstance(application).create(RefereeApi.class);
    }
}

