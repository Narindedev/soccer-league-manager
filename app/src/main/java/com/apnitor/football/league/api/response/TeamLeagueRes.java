package com.apnitor.football.league.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TeamLeagueRes implements Serializable {

    @Expose
    @SerializedName("_id")
    public String teamId;

    @Expose
    @SerializedName("name")
    public String teamName;

    @Expose
    @SerializedName("imageUrl")
    public String imageUrl;
}
