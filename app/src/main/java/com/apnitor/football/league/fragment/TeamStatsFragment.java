package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.adapter.LeagueStatsListAdapter;
import com.apnitor.football.league.api.response.GetTeamDetailRes;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.reflect.KProperty2;

public class TeamStatsFragment extends Fragment {
static GetTeamDetailRes mGetTeamDetailRes;
    public static TeamStatsFragment newInstance(GetTeamDetailRes getTeamDetailRes) {
        mGetTeamDetailRes=getTeamDetailRes;
        TeamStatsFragment fragment = new TeamStatsFragment();
        Bundle args = new Bundle();
//        args.putInt("someInt", page);
//        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_team_stats, container, false);

        RecyclerView v = view.findViewById(R.id.rvStats);
        v.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        v.setAdapter(new LeagueStatsListAdapter(getContext(), null));
        setRetainInstance(true);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
