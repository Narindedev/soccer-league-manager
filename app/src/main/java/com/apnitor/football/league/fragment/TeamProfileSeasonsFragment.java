package com.apnitor.football.league.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apnitor.football.league.R;
import com.apnitor.football.league.activity.LeagueProfileActivity;
import com.apnitor.football.league.activity.TeamProfileActivity;
import com.apnitor.football.league.adapter.SeasonPagePagerAdapater;
import com.apnitor.football.league.adapter.TeamSeasonPagerAdapater;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.databinding.FragmentTeamProfileSeasonsBinding;
import com.apnitor.football.league.fragment_binding_callback.ProfileSeasonFragmentBindingCallback;
import com.apnitor.football.league.util.CustomViewPager;

import androidx.viewpager.widget.ViewPager;

public class TeamProfileSeasonsFragment  extends BaseFragment implements ProfileSeasonFragmentBindingCallback {

    private FragmentTeamProfileSeasonsBinding binding;
    private TeamProfileActivity teamProfileActivity;
    CustomViewPager viewPager;
    static GetTeamDetailRes mTeamRes;


    public static TeamProfileSeasonsFragment newInstance(GetTeamDetailRes getTeamRes) {
        mTeamRes=getTeamRes;
        TeamProfileSeasonsFragment fragment = new TeamProfileSeasonsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.teamProfileActivity = (TeamProfileActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentTeamProfileSeasonsBinding.inflate(inflater, container, false);
        binding.setCallback(this);
        viewPager = binding.seasonPageViewPager;
        viewPager.setAdapter(new TeamSeasonPagerAdapater(getChildFragmentManager(),mTeamRes));
        viewPager.setOffscreenPageLimit(4);
        //
        binding.ivResultsTab.setImageResource(R.drawable.ic_resultclr);
        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
        binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
        //
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_resultclr);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                        binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                        /*binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivStatsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                    case 1:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechduleclr);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                        binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                      /*  binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivStatsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                    case 2:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standingclr);
                        binding.ivStatsTab.setImageResource(R.drawable.ic_stats);
                      /*  binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));
                        binding.ivStatsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));*/
                        break;
                    case 3:
                        binding.ivResultsTab.setImageResource(R.drawable.ic_result);
                        binding.ivSchedulesTab.setImageResource(R.drawable.ic_sechdule);
                        binding.ivPointTableTab.setImageResource(R.drawable.ic_standing);
                        binding.ivStatsTab.setImageResource(R.drawable.ic_statsclr);
                  /*      binding.ivResultsTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivSchedulesTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivPointTableTab.setColorFilter(getContext().getResources().getColor(R.color.ripple_dark));
                        binding.ivStatsTab.setColorFilter(getContext().getResources().getColor(R.color.colorAccent));*/
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return binding.getRoot();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showResults() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void showSchedules() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showPointsTable() {
        viewPager.setCurrentItem(2);
    }

    @Override
    public void showStats() {
        viewPager.setCurrentItem(3);
    }
}
