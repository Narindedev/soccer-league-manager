package com.apnitor.football.league.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apnitor.football.league.R;
import com.apnitor.football.league.api.response.GetTeamPlayersRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.interfaces.ListItemClickCallback;

import java.util.ArrayList;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {

    private Context context;
    private List<PlayerRes> teamList;
    private int orientation;
    ListItemClickCallback listItemClickCallback;
//    private LanguageInterface languageInterface;


    public PlayerListAdapter(Context context, ArrayList<PlayerRes> teamList, int orientation, ListItemClickCallback listItemClickCallback) {
        this.context = context;
        this.teamList = teamList;
        this.orientation = orientation;
        this.listItemClickCallback = listItemClickCallback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_team_item, parent, false);
        if (orientation == LinearLayoutManager.VERTICAL)
            view = LayoutInflater.from(context).inflate(R.layout.row_vertical_team_item, parent, false);

        return new ViewHolder(view);
    }

//    public void setOnFlagClicked(LanguageInterface languageInterface) {
//        this.languageInterface = languageInterface;
//    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            PlayerRes model = teamList.get(position);
            model.setSelected(!model.isSelected());
            holder.tvTeamName.setText(model.getFirstName()+" "+model.getLastName());
//            Picasso.with(context).load(AppSharedPreference.getString(AppSharedPreference.SUB_BASE_URL, "", context) + model.getLanguageFlag()).into(holder.imgFlag);
//            if (languageCode.equalsIgnoreCase(model.getLanguageCode())) {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.light_grey));
//            } else {
//                holder.languageRowParent.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent));
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTeamName;

        ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img_select);
            tvTeamName = (TextView) itemView.findViewById(R.id.tvTeamName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PlayerRes model = teamList.get(getAdapterPosition());
                        model.setSelected(!model.isSelected());
                    if (model.isSelected()){
                        img.setBackgroundResource(R.drawable.ic_check);

                    }
                    else{
                        img.setBackgroundResource(R.drawable.ic_uncheck);
                    }
                    if (listItemClickCallback != null)
                        listItemClickCallback.onListItemClick(teamList.get(getLayoutPosition()));
                }
            });
        }
    }
}
