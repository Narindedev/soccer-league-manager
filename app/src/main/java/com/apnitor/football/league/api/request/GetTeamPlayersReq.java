package com.apnitor.football.league.api.request;

import com.google.gson.annotations.SerializedName;

public class GetTeamPlayersReq {

    @SerializedName("teamId")
    private String teamId;

    @SerializedName("playerId")
    private String playerId;

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    @SerializedName("notificationId")
    public String notificationId;

    public GetTeamPlayersReq(String teamId) {
        this.teamId = teamId;
    }

    public GetTeamPlayersReq(String teamId,String playerId) {
        this.teamId = teamId;
        this.playerId = playerId;
//        this.notificationId = notificationId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }
}
