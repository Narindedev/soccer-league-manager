package com.apnitor.football.league.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateDivisionReq implements Serializable {

    @Expose
    @SerializedName("divisionId")
    private String divisionId;

    @Expose
    @SerializedName("leagueId")
    private String leagueId;

    @Expose
    @SerializedName("name")
    private String divisionName;

    @Expose
    @SerializedName("divisionPreName")
    private String preName;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("entryFee")
    public String entryFee;

    @Expose
    @SerializedName("insuranceFee")
    public String insuranceFee;

    @Expose
    @SerializedName("fieldRentalfee")
    public String fieldRentalfee;

    @Expose
    @SerializedName("winningPrize")
    public String winningPrize;

    public UpdateDivisionReq(String divisionId, String leagueId, String divisionName, String preName, String desc,String entryFee,String insuranceFee,String fieldRentalfee,String winningPrize) {
        this.divisionId = divisionId;
        this.leagueId = leagueId;
        this.divisionName = divisionName;
        this.preName = preName;
        description=desc;
        this.entryFee=entryFee;
        this.insuranceFee=insuranceFee;
        this.fieldRentalfee=fieldRentalfee;
        this.winningPrize=winningPrize;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public String getLeagueId() {
        return leagueId;
    }

    public void setLeagueId(String leagueId) {
        this.leagueId = leagueId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getPreName() {
        return preName;
    }

    public void setPreName(String preName) {
        this.preName = preName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(String entryFee) {
        this.entryFee = entryFee;
    }

    public String getInsuranceFee() {
        return insuranceFee;
    }

    public void setInsuranceFee(String insuranceFee) {
        this.insuranceFee = insuranceFee;
    }

    public String getFieldRentalfee() {
        return fieldRentalfee;
    }

    public void setFieldRentalfee(String fieldRentalfee) {
        this.fieldRentalfee = fieldRentalfee;
    }

    public String getWinningPrize() {
        return winningPrize;
    }

    public void setWinningPrize(String winningPrize) {
        this.winningPrize = winningPrize;
    }

}