package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.GetTeamResultReq;
import com.apnitor.football.league.api.request.InviteTeamReq;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetMyTeamsRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamResultRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.TeamProfileRes;
import com.apnitor.football.league.api.response.UpdateTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TeamApi {

    @POST("updateTeam")
    Single<BaseRes<UpdateTeamRes>> updateTeam(@Body UpdateTeamReq updateTeamReq);

    @GET("getAllTeams")
    Single<BaseRes<ArrayList<GetTeamRes>>> getAllTeams();

    @POST("getAllTeams")
    Single<BaseRes<ArrayList<GetTeamSearchRes>>> getAllTeams(@Body GetAllTeamReq getAllTeamReq);

    @POST("getAllTeams")
    Single<BaseRes<ArrayList<GetTeamSearchRes>>> getAllTeamsSearch(@Body GetAllTeamReq getAllTeamReq);

    @GET("getMyTeams")
    Single<BaseRes<ArrayList<GetMyTeamsRes>>> getMyTeams();

    @POST("deleteTeam")
    Single<BaseRes<Object>> deleteTeam(@Body DeleteTeamReq deleteTeamReq);

    @POST("getTeam")
    Single<BaseRes<GetTeamDetailRes>> getTeamDetail(@Body GetTeamDetailReq getLeagueTeamsReq);

    @POST("getLeagueTeams")
    Single<BaseRes<LeagueTeam>> getLeagueTeams(@Body GetLeagueTeamsReq getLeagueTeamsReq);

    @POST("addTeamPlayers")
    Single<BaseRes<UpdateTeamPlayersRes>> updateTeamPlayers(@Body UpdateTeamPlayersReq updateTeamPlayersReq);

    @POST("getTeamSchedule")
    Single<BaseRes<List<GetTeamScheduleRes>>> getTeamSchedule(@Body GetTeamDetailReq getLeagueTeamsReq);

    @POST("inviteTeam")
    Single<BaseRes<GetTeamRes>> inviteTeam(@Body InviteTeamReq inviteTeamReq);

    @POST("joinLeague")
    Single<BaseRes<GetTeamRes>> joinLeague(@Body InviteTeamReq inviteTeamReq);

    @POST("rejectLeagueInvite")
    Single<BaseRes<GetTeamRes>> rejectLeagueInvitation(@Body InviteTeamReq inviteTeamReq);

    @POST("getTeamResults")
    Single<BaseRes<GetTeamResultRes>> getTeamResult(@Body GetTeamDetailReq getTeamResultReq);
}
