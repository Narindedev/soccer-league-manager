package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueDivisionsReq;
import com.apnitor.football.league.api.request.GetLeagueTeamsReq;
import com.apnitor.football.league.api.request.GetTeamDetailReq;
import com.apnitor.football.league.api.request.GetTeamResultReq;
import com.apnitor.football.league.api.request.InviteTeamReq;
import com.apnitor.football.league.api.request.UpdateTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateTeamReq;
import com.apnitor.football.league.api.response.GetDivisionRes;
import com.apnitor.football.league.api.response.GetLeagueTeamsRes;
import com.apnitor.football.league.api.response.GetMyTeamsRes;
import com.apnitor.football.league.api.response.GetTeamDetailRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamResultRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.GetTeamSearchRes;
import com.apnitor.football.league.api.response.LeagueTeam;
import com.apnitor.football.league.api.response.TeamProfileRes;
import com.apnitor.football.league.api.response.UpdateTeamPlayersRes;
import com.apnitor.football.league.api.response.UpdateTeamRes;
import com.apnitor.football.league.repository.TeamRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class TeamViewModel extends BaseViewModel {

    private TeamRepository teamRepository;
    private MutableLiveData<UpdateTeamRes> updateTeamResLiveData = new MutableLiveData<>();
    private MutableLiveData<UpdateTeamPlayersRes> UpdateTeamPlayersResLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamRes>> getAllTeamsLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamSearchRes>> getAllTeamsResLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamSearchRes>> getAllTeamsSearchLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetTeamRes>> getMyTeamsListLiveData = new MutableLiveData<>();
    private MutableLiveData<ArrayList<GetMyTeamsRes>> getMyTeamsLiveData = new MutableLiveData<>();
    private MutableLiveData<GetTeamRes> getTeamLiveData = new MutableLiveData<>();
    private MutableLiveData<GetTeamDetailRes> getMyTeamDetailLiveData = new MutableLiveData<>();
//    private MutableLiveData<TeamProfileRes> getTeamProfileLiveData = new MutableLiveData<>();

    private MutableLiveData<GetTeamRes> getTeamDetailLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> deleteTeamLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetTeamScheduleRes>> getTeamScheduleLiveData = new MutableLiveData<>();
    private MutableLiveData<GetTeamResultRes> getTeamResultLiveData = new MutableLiveData<>();

    private MutableLiveData<LeagueTeam> getLeagueTeams = new MutableLiveData<>();


    public TeamViewModel(@NonNull Application application) {
        super(application);
        teamRepository = new TeamRepository(application);
    }

    public MutableLiveData<UpdateTeamRes> getUpdateTeamResLiveData() {
        return updateTeamResLiveData;
    }

    public LiveData<ArrayList<GetTeamRes>> getGetAllTeamsLiveData() {
        return getAllTeamsLiveData;
    }

    public LiveData<ArrayList<GetTeamSearchRes>> getGetAllTeamsSearchLiveData() {
        return getAllTeamsSearchLiveData;
    }

    public LiveData<ArrayList<GetMyTeamsRes>> getGetMyTeamsLiveData() {
        return getMyTeamsLiveData;
    }

    public LiveData<ArrayList<GetTeamRes>> getMyTeamListLiveData() {
        return getMyTeamsListLiveData;
    }

    public LiveData<GetTeamDetailRes> getTeamsDetailLiveData() {
        return getMyTeamDetailLiveData;
    }

//    public LiveData<TeamProfileRes> getTeamsProfileLiveData() {
//        return getTeamProfileLiveData;
//    }

    public LiveData<Object> getDeleteTeamLiveData() {
        return deleteTeamLiveData;
    }

    public void createUpdateLeague(UpdateTeamReq updateTeamReq) {
        consumeApi(
                teamRepository.createUpdateTeam(updateTeamReq),
                data -> updateTeamResLiveData.setValue(data)
        );
    }

    public void getAllTeams() {
        consumeApi(
                teamRepository.getAllTeams(),
                data -> getAllTeamsLiveData.setValue(data)
        );
    }

    public LiveData<ArrayList<GetTeamSearchRes>> getAllTeamsResLiveData() {
        return getAllTeamsResLiveData;
    }

    public void getAllTeams(GetAllTeamReq getAllTeamReq) {
        consumeApi(
                teamRepository.getAllTeams(getAllTeamReq),
                data -> getAllTeamsResLiveData.setValue(data)
        );
    }

    public void getAllTeamsSearch(GetAllTeamReq getAllTeamReq) {
        consumeApi(
                teamRepository.getAllTeamsSearch(getAllTeamReq),
                data -> getAllTeamsSearchLiveData.setValue(data)
        );
    }


    public void getMyTeams() {
        consumeApi(
                teamRepository.getMyTeams(),
                data -> getMyTeamsLiveData.setValue(data)
        );
    }


    public void getTeamDetail(GetTeamDetailReq teamDetailReq) {
        consumeApi(
                teamRepository.getTeamDetail(teamDetailReq),
                data -> getMyTeamDetailLiveData.setValue(data)
        );
    }

    public void deleteTeam(DeleteTeamReq deleteTeamReq) {
        consumeApi(
                teamRepository.deleteTeam(deleteTeamReq),
                data -> deleteTeamLiveData.setValue(data)
        );
    }

    public void getLeagueTeams(GetLeagueTeamsReq getLeagueTeamsReq) {
        consumeApi(
                teamRepository.getLeagueTeams(getLeagueTeamsReq),
                data -> getLeagueTeams.setValue(data)
        );
    }

    public LiveData<LeagueTeam> getLeagueTeamsLiveData() {
        return getLeagueTeams;
    }

    public void updateTeamPlayers(UpdateTeamPlayersReq updateTeamPlayersReq) {
        consumeApi(
                teamRepository.updateTeamPlayers(updateTeamPlayersReq),
                data -> UpdateTeamPlayersResLiveData.setValue(data)
        );
    }

    public MutableLiveData<UpdateTeamPlayersRes> getUpdateTeamPlayersResLiveData() {
        return UpdateTeamPlayersResLiveData;
    }

    public LiveData<List<GetTeamScheduleRes>> getGetTeamScheduleLiveData() {
        return getTeamScheduleLiveData;
    }

    public void getTeamSchedule(GetTeamDetailReq getLeagueDivisionsReq) {
        consumeApi(
                teamRepository.getTeamSchedule(getLeagueDivisionsReq),
                data -> getTeamScheduleLiveData.setValue(data)
        );
    }

    public LiveData<GetTeamRes> getTeamsLiveData() {
        return getTeamLiveData;
    }


    public void getInviteTeam(InviteTeamReq inviteTeamReq) {
        consumeApi(
                teamRepository.getInviteTeam(inviteTeamReq),
                data -> getTeamLiveData.setValue(data)
        );
    }


    public void joinLeague(InviteTeamReq inviteTeamReq) {
        consumeApi(
                teamRepository.joinLeague(inviteTeamReq),
                data -> getTeamLiveData.setValue(data)
        );
    }

    public void rejectLeagueInvitation(InviteTeamReq inviteTeamReq) {
        consumeApi(
                teamRepository.rejectLeagueInvitation(inviteTeamReq),
                data -> getTeamLiveData.setValue(data)
        );
    }


    public LiveData<GetTeamResultRes> getGetTeamResultLiveData() {
        return getTeamResultLiveData;
    }

    public void getTeamResult(GetTeamDetailReq getTeamResultReq) {
        consumeApi(
                teamRepository.getTeamResult(getTeamResultReq),
                data -> getTeamResultLiveData.setValue(data)
        );
    }
}
