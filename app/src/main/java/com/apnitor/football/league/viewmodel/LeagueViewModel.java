package com.apnitor.football.league.viewmodel;

import android.app.Application;

import com.apnitor.football.league.api.request.AddLeagueManagerReq;
import com.apnitor.football.league.api.request.AddLeagueRosterReq;
import com.apnitor.football.league.api.request.AddScheduleLeagueReq;
import com.apnitor.football.league.api.request.DeleteLeagueReq;
import com.apnitor.football.league.api.request.GetAllTeamReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetLeagueScheduleReq;
import com.apnitor.football.league.api.request.GetLeagueStandingReq;
import com.apnitor.football.league.api.request.UpdateLeagueReq;
import com.apnitor.football.league.api.request.UpdateLeagueTeamReq;
import com.apnitor.football.league.api.response.AddLeagueManagerRes;
import com.apnitor.football.league.api.response.AddLeagueRosterRes;
import com.apnitor.football.league.api.response.AddScheduleRes;
import com.apnitor.football.league.api.response.GetEventRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetLeagueStandingRes;
import com.apnitor.football.league.api.response.LeagueScheduleResultRes;
import com.apnitor.football.league.api.response.PlayerRes;
import com.apnitor.football.league.api.response.UpdateLeagueRes;
import com.apnitor.football.league.api.response.UpdateLeagueTeamRes;
import com.apnitor.football.league.repository.LeagueRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class LeagueViewModel extends BaseViewModel {

    private LeagueRepository leagueRepository;
    private MutableLiveData<UpdateLeagueRes> updateLeagueResLiveData = new MutableLiveData<>();
    private MutableLiveData<UpdateLeagueTeamRes> updateLeagueTeamResLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetLeagueRes>> leagueListResLiveData = new MutableLiveData<>();
    private MutableLiveData<Object> deleteLeagueResLiveData = new MutableLiveData<>();
    private MutableLiveData<GetLeagueRes> leagueDetailsResLiveData = new MutableLiveData<>();
    private MutableLiveData<List<AddScheduleLeagueReq>> leagueScheduleResLiveData = new MutableLiveData<>();
    private MutableLiveData<LeagueScheduleResultRes> leagueScheduleResultLiveData = new MutableLiveData<>();
    private MutableLiveData<List<GetLeagueStandingRes>> leagueStandingResLiveData = new MutableLiveData<>();
    private MutableLiveData<AddLeagueRosterRes> leagueRosterResLiveData = new MutableLiveData<>();
    private MutableLiveData<AddLeagueRosterRes> addLeagueRosterResLiveData = new MutableLiveData<>();
    private MutableLiveData<AddLeagueManagerRes> searchLeagueManagerResLiveData = new MutableLiveData<>();
    private MutableLiveData<AddLeagueManagerRes> addLeagueManagerResLiveData = new MutableLiveData<>();



    public LeagueViewModel(@NonNull Application application) {
        super(application);
        leagueRepository = new LeagueRepository(application);
    }

    public LiveData<Object> getDeleteLeagueResLiveData() {
        return deleteLeagueResLiveData;
    }

    public LiveData<UpdateLeagueRes> getUpdateLeagueResLiveData() {
        return updateLeagueResLiveData;
    }

    public LiveData<UpdateLeagueTeamRes> getUpdateLeagueTeamResLiveData() {
        return updateLeagueTeamResLiveData;
    }

    public LiveData<List<GetLeagueRes>> getLeagueListResLiveData() {
        return leagueListResLiveData;
    }

    public LiveData<List<GetLeagueRes>> getAllLeagueListResLiveData() {
        return leagueListResLiveData;
    }

    public LiveData<GetLeagueRes> getLeagueDetailsResLiveData() {
        return leagueDetailsResLiveData;
    }

    public LiveData<List<AddScheduleLeagueReq>> getLeaguScheduleResLiveData() {
        return leagueScheduleResLiveData;
    }

    public LiveData<List<AddScheduleLeagueReq>> setLeaguScheduleResLiveData(MutableLiveData<List<AddScheduleLeagueReq>> liveData) {
        return leagueScheduleResLiveData=liveData;
    }

    public void createUpdateLeague(UpdateLeagueReq updateLeagueReq) {
        consumeApi(
                leagueRepository.createUpdateLeague(updateLeagueReq),
                data -> updateLeagueResLiveData.setValue(data)
        );
    }

    public void updateLeagueTeams(UpdateLeagueTeamReq updateLeagueTeamReq) {
        consumeApi(
                leagueRepository.updateLeagueTeams(updateLeagueTeamReq),
                data -> updateLeagueTeamResLiveData.setValue(data)
        );
    }

    public void getMyLeagues() {
        consumeApi(
                leagueRepository.getMyLeagues(),
                data -> leagueListResLiveData.setValue(data)
        );
    }

    public void getAllLeagues() {
        consumeApi(
                leagueRepository.getMyAllLeagues(),
                data -> leagueListResLiveData.setValue(data)
        );
    }
    public void getAllLeagues(GetAllTeamReq getAllTeamReq) {
        consumeApi(
                leagueRepository.getMyAllLeagues(getAllTeamReq),
                data -> leagueListResLiveData.setValue(data)
        );
    }
    public void deleteLeague(DeleteLeagueReq deleteLeagueReq) {
        consumeApi(
                leagueRepository.deleteLeague(deleteLeagueReq),
                data -> deleteLeagueResLiveData.setValue(data)
        );
    }

    public void getLeagueDetails(GetLeagueDetailsReq getLeagueDetailsReq) {
        consumeApi(
                leagueRepository.getLeagueDetails(getLeagueDetailsReq),
                data -> leagueDetailsResLiveData.setValue(data)
        );
    }

    public void getLeagueSchedule(GetLeagueScheduleReq getLeagueDetailsReq) {
        consumeApi(
                leagueRepository.getLeagueSchedule(getLeagueDetailsReq),
                data -> leagueScheduleResLiveData.setValue(data)
        );
    }

    public void getSchedule(GetLeagueScheduleReq getLeagueDetailsReq) {
        consumeApi(
                leagueRepository.getLeagueSchedule(getLeagueDetailsReq),
                data -> leagueScheduleResLiveData.setValue(data)
        );
    }

    public LiveData<LeagueScheduleResultRes> getLeaguScheduleResultLiveData() {


        return leagueScheduleResultLiveData;
    }


    public void getLeagueResults(GetLeagueScheduleReq getLeagueDetailsReq) {
        consumeApi(
                leagueRepository.getLeagueResults(getLeagueDetailsReq),
                data -> leagueScheduleResultLiveData.setValue(data)
        );
    }


    public LiveData<List<GetLeagueStandingRes>> getLeagueStandingResLiveData() {
        return leagueStandingResLiveData;
    }

    public void getLeagueStanding(GetLeagueStandingReq getLeagueStandingReq) {
        consumeApi(
                leagueRepository.getLeagueStanding(getLeagueStandingReq),
                data -> leagueStandingResLiveData.setValue(data)
        );
    }

    public LiveData<AddLeagueRosterRes> getLeagueRosterResLiveData() {
        return leagueRosterResLiveData;
    }

    public void getLeagueRoster(AddLeagueRosterReq addLeagueRosterReq) {
        consumeApi(
                leagueRepository.getLeagueRoaster(addLeagueRosterReq),
                data -> leagueRosterResLiveData.setValue(data)
        );
    }

    public LiveData<AddLeagueRosterRes> addLeagueRoasterResLiveData() {
        return addLeagueRosterResLiveData;
    }

    public void addLeagueRoaster(AddLeagueRosterReq addLeagueRosterReq) {
        consumeApi(
                leagueRepository.addLeagueRoaster(addLeagueRosterReq),
                data -> addLeagueRosterResLiveData.setValue(data)
        );
    }

    public LiveData<AddLeagueManagerRes> searchLeagueManagerResLiveData() {
        return searchLeagueManagerResLiveData;
    }


    public void searchLeagueManager(AddLeagueManagerReq addLeagueManagerReq) {
        consumeApi(
                leagueRepository.searchLeagueManager(addLeagueManagerReq),
                data -> searchLeagueManagerResLiveData.setValue(data)
        );
    }

    public LiveData<AddLeagueManagerRes> addLeagueManagerResLiveData() {
        return addLeagueManagerResLiveData;
    }

    public void addLeagueManager(AddLeagueManagerReq addLeagueManagerReq) {
        consumeApi(
                leagueRepository.addLeagueManager(addLeagueManagerReq),
                data -> addLeagueManagerResLiveData.setValue(data)
        );
    }


}

