package com.apnitor.football.league.api;

import com.apnitor.football.league.api.request.DeleteSeasonReq;
import com.apnitor.football.league.api.request.GetLeagueSeasonsReq;
import com.apnitor.football.league.api.request.UpdateSeasonReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetSeasonRes;
import com.apnitor.football.league.api.response.UpdateSeasonRes;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SeasonApi {

    @POST("updateLeagueSeason")
    Single<BaseRes<UpdateSeasonRes>> updateSeason(@Body UpdateSeasonReq updateSeasonReq);

    @POST("deleteSeason")
    Single<BaseRes<Object>> deleteSeason(@Body DeleteSeasonReq deleteSeasonReq);

    @POST("getLeagueSeasons")
    Single<BaseRes<List<GetSeasonRes>>> getLeagueSeasons(@Body GetLeagueSeasonsReq getLeagueSeasonsReq);
}
