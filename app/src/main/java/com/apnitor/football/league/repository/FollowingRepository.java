package com.apnitor.football.league.repository;

import android.app.Application;

import com.apnitor.football.league.api.ApiService;
import com.apnitor.football.league.api.FollowingApi;
import com.apnitor.football.league.api.request.DeleteTeamReq;
import com.apnitor.football.league.api.request.GetFollowingReq;
import com.apnitor.football.league.api.request.GetLeagueDetailsReq;
import com.apnitor.football.league.api.request.GetTeamPlayersReq;
import com.apnitor.football.league.api.request.UpdateImageReq;
import com.apnitor.football.league.api.response.BaseRes;
import com.apnitor.football.league.api.response.GetFollowingRes;
import com.apnitor.football.league.api.response.GetLeagueRes;
import com.apnitor.football.league.api.response.GetNotificationRes;
import com.apnitor.football.league.api.response.GetTeamRes;
import com.apnitor.football.league.api.response.GetTeamScheduleRes;
import com.apnitor.football.league.api.response.NotificationResponse;
import com.apnitor.football.league.api.response.PlayerRes;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class FollowingRepository {
    FollowingApi followingApi;

    public FollowingRepository(Application application) {
        followingApi = ApiService.getFollowingApi(application);
    }

    public Single<BaseRes<GetLeagueRes>> followLeague(GetLeagueDetailsReq getLeagueDetailsReq) {
        return followingApi.followLeague(getLeagueDetailsReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetLeagueRes>> unfollowLeague(GetLeagueDetailsReq getLeagueDetailsReq) {
        return followingApi.unfollowLeague(getLeagueDetailsReq)
                .subscribeOn(Schedulers.io());
    }


    public Single<BaseRes<PlayerRes>> followPlayer(GetTeamPlayersReq getTeamPlayersReq) {
        return followingApi.followPlayers(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<PlayerRes>> unfollowPlayer(GetTeamPlayersReq getTeamPlayersReq) {
        return followingApi.unfollowPlayers(getTeamPlayersReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetTeamRes>> followTeam(DeleteTeamReq teamReq) {
        return followingApi.followTeam(teamReq)
                .subscribeOn(Schedulers.io());
    }
    public Single<BaseRes<GetTeamRes>> unfollowTeam(DeleteTeamReq teamReq) {
        return followingApi.unFollowTeam(teamReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetFollowingRes>> getFollowing() {
        return followingApi.getFollowing()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetFollowingRes>> updateImageUrl(UpdateImageReq updateImageReq) {
        return followingApi.updateImageUrl(updateImageReq)
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<ArrayList<GetTeamScheduleRes>>> getFollowingScores() {
        return followingApi.getFollowingScores()
                .subscribeOn(Schedulers.io());
    }

    public Single<BaseRes<GetNotificationRes>> getAllNotifications() {
        return followingApi.getAllNotifications()
                .subscribeOn(Schedulers.io());
    }

}
